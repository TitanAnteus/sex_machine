{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 2,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sfx_computer.wav",
  "duration": 0.67373,
  "parent": {
    "name": "OverworldSounds",
    "path": "folders/Sounds/OverworldSounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_computer",
  "tags": [],
  "resourceType": "GMSound",
}