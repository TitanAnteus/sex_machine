{
  "compression": 0,
  "volume": 0.7,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sfx_falling.mp3",
  "duration": 2.403265,
  "parent": {
    "name": "OverworldSounds",
    "path": "folders/Sounds/OverworldSounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_falling",
  "tags": [],
  "resourceType": "GMSound",
}