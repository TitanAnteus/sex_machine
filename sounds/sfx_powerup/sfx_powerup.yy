{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sfx_powerup",
  "duration": 0.710306,
  "parent": {
    "name": "BattleSounds",
    "path": "folders/Sounds/BattleSounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_powerup",
  "tags": [],
  "resourceType": "GMSound",
}