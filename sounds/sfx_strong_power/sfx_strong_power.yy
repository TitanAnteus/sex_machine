{
  "compression": 0,
  "volume": 0.5,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sfx_strong_power",
  "duration": 0.553571,
  "parent": {
    "name": "BattleSounds",
    "path": "folders/Sounds/BattleSounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_strong_power",
  "tags": [],
  "resourceType": "GMSound",
}