{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sfx_slap",
  "duration": 0.154388,
  "parent": {
    "name": "Hsounds",
    "path": "folders/Sounds/Hsounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_slap",
  "tags": [],
  "resourceType": "GMSound",
}