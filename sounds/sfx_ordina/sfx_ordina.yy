{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sfx_ordina.wav",
  "duration": 0.361508,
  "parent": {
    "name": "OverworldSounds",
    "path": "folders/Sounds/OverworldSounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_ordina",
  "tags": [],
  "resourceType": "GMSound",
}