{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sfx_save",
  "duration": 1.39700007,
  "parent": {
    "name": "MenuSounds",
    "path": "folders/Sounds/MenuSounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_save",
  "tags": [],
  "resourceType": "GMSound",
}