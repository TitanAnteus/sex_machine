{
  "compression": 2,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_music",
    "path": "audiogroups/audiogroup_music",
  },
  "soundFile": "bgm_battle2",
  "duration": 121.897667,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "bgm_battle2",
  "tags": [],
  "resourceType": "GMSound",
}