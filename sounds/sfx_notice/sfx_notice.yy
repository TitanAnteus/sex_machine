{
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.7,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 2,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sfx_notice",
  "duration": 0.626939,
  "parent": {
    "name": "OverworldSounds",
    "path": "folders/Sounds/OverworldSounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_notice",
  "tags": [],
  "resourceType": "GMSound",
}