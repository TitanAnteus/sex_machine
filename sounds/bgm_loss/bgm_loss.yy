{
  "compression": 2,
  "volume": 0.5,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_music",
    "path": "audiogroups/audiogroup_music",
  },
  "soundFile": "bgm_loss",
  "duration": 89.50051,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "bgm_loss",
  "tags": [],
  "resourceType": "GMSound",
}