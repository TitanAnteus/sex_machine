event_talk(igrec_name2,"Let's see here... | ここで見てみましょう...",0)

var ar;
ar[0] = "Keen Eye | するどいめ"
ar[1] = "Shaky Hips | 不安定な腰"
ar[2] = "Shaky Hands | 手ぶれ"
ar[3] = "Conviction | 信念"

event_learn_passives(ar,0)
event_talk(igrec_name,"Wish I could've had some of these sooner. | これらのいくつかをもっと早く手に入れることができたらいいのにと思います。",0)
event_talk(igrec_name,"I got everything I could use. | 使えるものはすべて手に入れた。",1)