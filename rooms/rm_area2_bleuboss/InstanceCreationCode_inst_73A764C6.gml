event_talk(igrec_name2,"I'm loving these terminals... | 私はこれらの端末が大好きです...",0)

var ar;
ar[0] = "Rhythm | リズム"
ar[1] = "Strong Will | 強い意志"

event_learn_passives(ar,0)
event_talk(igrec_name2,"Seems useful. | 便利そうです。",0)
event_talk(igrec_name,"I wish there was more useful stuff on here. | ここにもっと便利なものがあったらいいのにと思います。",1)