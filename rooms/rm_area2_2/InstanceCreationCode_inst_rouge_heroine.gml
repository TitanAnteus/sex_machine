ds_list_clear(start)
ds_list_add(start,"Can I put it in? I can put it in right master? | 入れてもいいですか？ 私はそれを正しいマスターに入れることができますか？ ")
ds_list_add(start,"<shake strong=3>YEESSSS!! \nI'm so glad I met youuuu!!!!!</shake> | <shake strong=3>スゴイ!! \nお会いできてうれしいです!!!! !</shake>")
ds_list_add(start,"<shake strong=2>There's no way I can live without this now!!</shake> | <shake strong=2>今これなしでは生きていけない！ !</shake>")

ds_list_clear(win)
ds_list_add(win,"Uooohh... That's a lot! |うーん...それはたくさんです！ ")
ds_list_add(win,"<wave>Oh master, your penis can still keep going?</wave> | <wave>マスター、あなたのペニスはまだ続けることができますか？ </wave>")
ds_list_add(win,"Don't worry master, I want to keep going too. | マスターを心配しないでください、私も続けたいです。 ")
ds_list_add(win,"<wave>Ahhh... I hope this never ends.</wave> | <wave>ああ...これが終わらないことを願っています。</wave> ")

ds_list_clear(loss)
ds_list_add(loss,"<shake strong=1>Ah it feels so good!</shake> | <shake strong=1>ああ、とても気持ちいい！ </shake>")
ds_list_add(loss,"<shake strong=2>I hope it feels good for you too!</shake> | <shake strong=1>あなたにも気持ちいいと思います！ </shake>")
ds_list_add(loss,"<shake strong=3>I'm cumming!</shake> | <shake strong=1>あぁああぁぁッ!!!!</shake>")

random_set_seed(global.seed)

stopid = instance_create_depth(x,y,depth,obj_stop)

//Battle Variables
habit[0][0] = choose("L","R")
habit[0][1] = habit[0][0]
habit[0][2] = choose("R","L")
habit[0][3] = habit[0][2]

habit[1][0] = choose("U","D")
habit[1][1] = habit[1][0]
habit[1][2] = choose("D","U")
habit[1][3] = habit[1][2]

habit[2][0] = opposite_dir(habit[0][0])
habit[2][1] = habit[2][0]
habit[2][2] = opposite_dir(habit[0][2])
habit[2][3] = habit[2][2]

habit[3][0] = opposite_dir(habit[1][0])
habit[3][1] = habit[3][0]
habit[3][2] = opposite_dir(habit[1][2])
habit[3][3] = habit[3][2]

mybits = 30+floor(random(5))
randomize()

//Special Boss
min_speed = array_length_2d(habit,0)
if(global.difficulty = EASY)
	{
	mxwillpower = 170
	willpower = mxwillpower
	mxtimeline = 0
	cream = 4
	free_answer = 1

	battle_speed = array_length_2d(habit,0)+1+floor(random(2))-floor(random(2))
	max_speed = 10
	strength = 6
	skill = 10
	turn_time = 40
	}
else
	{
	mxwillpower = 250
	willpower = mxwillpower
	mxtimeline = 0
	cream = 4
	free_answer = 0

	battle_speed = array_length_2d(habit,0)+1+floor(random(2))-floor(random(2))
	max_speed = 12
	strength = 6
	skill = 15
	turn_time = 36
	}
/*
ds_map_delete(enemy_action,"Overheat | オーバーヒート")
ds_map_delete(enemy_text,"Overheat | オーバーヒート")
ds_map_delete(enemy_mod,"Overheat | オーバーヒート")

ds_map_delete(enemy_action,"Energetic | 元気")
ds_map_delete(enemy_text,"Energetic | 元気")
ds_map_delete(enemy_mod,"Energetic | 元気")

ds_map_add(enemy_action,"Superheat | 過熱",30+irandom(10))
ds_map_add(enemy_text,"Superheat | 過熱","For <col @c_yellow>2<col @c_white> turns, damage given increased, and damage \nreceived increased. Increases battle speed to <col @c_yellow>10<col @c_white>. | <col @c_yellow>2<col @c_white>ターンの間、与えられるダメージが増加し、\n受けるダメージが増加しました。 ターン数を<col @c_yellow>10<col @c_white>に増やします。")
ds_map_add(enemy_mod,"Superheat | 過熱",superheat_mod)*/