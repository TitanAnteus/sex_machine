event_talk(igrec_name,"Hmmm... This should help. | うーん...これは役立つはずです。",0)
event_talk(igrec_name,"There's quite a lot of useful information on these terminals. | これらの端末には非常に多くの有用な情報があります。",0)

var ar;
ar[0] = "Ready | 角郷"
ar[1] = "Meditate | 瞑想"
ar[2] = "Pressure | 圧力"

event_learn_skills(ar,0)
event_talk(igrec_name2,"Some defensive skills and an offensive one. This should let me tackle most challenges. | いくつかの防御スキルと攻撃スキル。 これにより、ほとんどの課題に取り組むことができます。",0)
event_talk(igrec_name,"I can't find anything else that's useful. | 他に役立つものは見つかりません。",1)