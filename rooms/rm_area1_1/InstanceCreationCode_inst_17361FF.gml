event_talk(igrec_name,"A vitals check? | バイタルチェック？ ",0)
event_talk(igrec_name,"Was this machine monitoring me? | このマシンは私を監視していましたか？ ",0)
event_talk(igrec_name,"I wonder how long I was in there... | どれくらいそこにいたのかしら... ",0)
event_play_sound(sfx_computer,0,0)
event_talk(igrec_name,"...",0)
event_talk(igrec_name,"Hunh? They didn't even keep track of that. | フン？ 彼らはそれを追跡することさえしませんでした。 ",0)
event_talk(igrec_name,"<shake>Mrghh</shake>\nThat's so annoying... | <shake>むかつく </shake>\nそれはとても迷惑です... ",0)
event_talk(igrec_name,"This stupid thing is useless. | この愚かなことは役に立たない。 ",1)