event_talk(igrec_name,"Ok... and... | わかりました...そして...",0)
event_talk(igrec_name2,"Oh? | おお？",0)

var ar;
ar[0] = "Calm | 落ち着く"
ar[1] = "Psych Up | サイケアップ"

event_learn_skills(ar,0)

event_talk(igrec_name2,"I can see this being very useful. | これはとても便利だと思います。",0)
event_talk(igrec_name,"Might as well check again to see if I missed anything. | 私が何かを逃したかどうかをもう一度確認することもできます。",1)
event_talk(igrec_name,"...",1)
event_talk(igrec_name,"Nothing.\nGuess I'm pretty thorough. | 何もない。\n私はかなり徹底していると思います。",1)