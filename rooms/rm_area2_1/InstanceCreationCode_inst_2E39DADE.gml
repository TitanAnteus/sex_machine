event_talk(igrec_name,"Let's see here. | ここで見てみましょう。",0)
event_talk(igrec_name2,"Seems like there's quite a lot of useful data here. | ここには非常に多くの有用なデータがあるようです。",0)

var ar;
ar[0] = "Focus | 集中"
ar[1] = "Victory Cry | 勝利の叫ぶ"
ar[2] = "Surrender | 降伏"

event_learn_skills(ar,0)
event_talk(igrec_name2,"This should help against any Androids that attack me. | これは、私を攻撃するAndroidに対して役立つはずです。",0)
event_talk(igrec_name,"I can't find anything else that's useful. | 他に役立つものは見つかりません。",1)