event_inherited()

//Setup Sprites
icon = spr_cielhead
stand_south = spr_ciel_south
stand_east = spr_ciel_east
stand_west = spr_ciel_west
stand_north = spr_ciel_north

walk_south = spr_ciel_south_walk
walk_east = spr_ciel_east_walk
walk_west = spr_ciel_west_walk
walk_north = spr_ciel_north_walk

battle[0] = spr_ciel_battle
battle[1] = spr_ciel_creampie
sound[0] = tm_ciel_battle
sound[1] = tm_ciel_creampie

assist[0] = spr_cielassist
assist[1] = spr_cielassistcum

//Main Variables
name = ciel_name
state = "Wander"

//Editable Variables
random_set_seed(global.seed)
walk_speed = 1.05
run_speed = 1.6
decision_timer = 60
wander_dist = 240
follow_time = 0
mxfollow_time = 80
follow_dist = 90

//Game Variables
move_speed = 0
tmove_speed = 0
tdirection = direction



//Battle Variables
habit[0][0] = choose("U","L","R","D")
habit[0][1] = counterclock_dir(habit[0][0])

habit[1][0] = counterclock_dir(habit[0][1])
habit[1][1] = counterclock_dir(habit[1][0])

habit[2][0] = counterclock_dir(habit[1][1])
habit[2][1] = opposite_dir(habit[2][0])

mybits = 12+floor(random(3))
timer[0] = decision_timer+irandom(20)-irandom(20)
randomize()
//chab = floor(random(array_height_2d(habit)))
chab = 0
lhab = chab
//STATS
min_speed = array_length_2d(habit,0)
if(global.difficulty = EASY)
	{
	mxwillpower = 100+irandom(10)
	willpower = mxwillpower
	mxtimeline = 0
	cream = 5
	free_answer = 1

	battle_speed = array_length_2d(habit,0)+choose(0,2)
	max_speed = 8
	min_speed = array_length_2d(habit,0)
	strength = 3
	skill = 11
	turn_time = 20
	}
else
	{
	mxwillpower = 120+irandom(10)
	//mxwillpower = 1
	willpower = mxwillpower
	mxtimeline = 0
	cream = 5
	free_answer = 0

	battle_speed = array_length_2d(habit,0)+choose(0,1,2,3)
	max_speed = 10
	strength = 3
	skill = 14
	turn_time = 15
	}

//Skills
enemy_action = ds_map_create()
enemy_text = ds_map_create()
enemy_mod = ds_map_create()

ds_map_add(enemy_action,"Wait | 待つ",5+irandom(5))

ds_map_add(enemy_text,"Wait | 待つ","She stares with an impassioned gaze. | 彼女はイグレックの足をしっかり握りながら腰を動かしている。")

ds_map_add(enemy_mod,"Wait | 待つ",wait_mod)

start = ds_list_create()
ds_list_add(start,"All I want to do is have some fun. | 私と楽しいことをしましょう。")
ds_list_add(start,"Let's feel good together. | 一緒に気持ちよくなるの。")

win = ds_list_create()
ds_list_add(win,"<wave>Your face is so cute when you cum.</wave> | <wave>イってるあなたの顔はとってもかわいいわ。</wave>")
ds_list_add(win,"I'm not letting go...\nI haven't had enough yet. | 手放さないわよ...。\n私はまだイってないもの。")
ds_list_add(win,"Did you see this coming? \nIs this why you wanted to run? | あぁ・・・アナタの精液が溢れてくるわ \n見えるでしょう？もっと出していいのよ？")

loss = ds_list_create()
ds_list_add(loss,"<shake strong=.5>This is it!</shake> | <shake strong=.5>これよ！</shake>")
ds_list_add(loss,"<shake strong=1>This pleasure is making me numb!!!</shake> | <shake strong=1>この快感が私を痺れさせるわ!!!</shake>")
ds_list_add(loss,"<shake strong=1.5>I wish I could feel this forever!</shake> | <shake strong=1.5>これをずっと感じることができたらいいのに！</shake>")
ds_list_add(loss,"<shake strong=2.5>It's here!!!</shake> | <shake strong=2.5>来たぁ!!!!</shake>")

//Starting Actions
snap_x_center()
snap_y_center()