var x1,y1;
draw_set_font(global.big_font)
height = max((string_height("H")*(array_length(skills)+1))+10,200)
x1 = (view_wport/2)-(width/2)
y1 = (view_hport/2)-(height/2)

if(draw_get_color() != c_black)draw_set_color(c_black)
if(draw_get_alpha() != 1)draw_set_alpha(1)
draw_rectangle(x1,y1,x1+width,y1+height,0)
draw_set_color(c_white)
draw_rectangle(x1,y1,x1+width,y1+height,1)

draw_set_halign(fa_center)
draw_set_valign(fa_top)
draw_text_width(x1+(width/2),y1+1,"New Skills | 新しい能力",width,-1)
draw_line(x1+4,y1+string_height("H")+3,x1+width-4,y1+string_height("H")+3)
draw_line(x1+(width/2),y1+string_height("H")+3,x1+(width/2),y1+height-4)

var lx,rx,ly,cy,h,txt;
lx = x1+(width/4)
rx = x1+width-(width/4)
h = string_height("H")+3
ly = (y1+(height/2)+h)-((array_length(skills)*h)/2)


for(i=0;i<array_length(skills);i+=1)
	{	
	draw_set_valign(fa_middle)
	draw_set_font(global.big_font)
	//if(csel == i)draw_rectangle(x1,ly+(h*i),x1+width/2,ly+(h*(i+1)),0)
	var tcol;
	tcol = c_white
	if(csel == i)tcol = c_yellow
	draw_text_outline(lx,ly+(h*i),string_split(skills[i]),tcol,c_black)
	
	if(csel == i)
		{
		//Draw Skill Description
		txt = string(global.skill_map[? skills[i]])
		//if(string_count(txt,"Slightly increase defense") > 0)show_message("What?")
		if(string_count("undefined",txt) == 0)
			{
			draw_set_font(global.main_font)
			cy = y1+(height/2)+string_height("H")+6
			draw_text_width(rx,cy,txt,width/2,-1)
			}
		}
	
	}