if(frozen = true)exit
if(active = false)exit
if(can_wander = false)exit
if(state = "Cutscene")exit
if(path_index == -1)
	{
	if(follow_time == 0)state = "Wander"
	
	if(check_snapx_precise())
	and(check_snapy_precise())
		{
		direction = tdirection
		if(check_forward(direction))
			{
			move_speed = tmove_speed
			}
		else
			{
			move_speed = 0
			}
		var xto,yto,dist1,dist2;
		xto = (x+(global.grid/2))+lengthdir_x(global.grid,direction)
		yto = (y+(global.grid/2))+lengthdir_y(global.grid,direction)
		dist1 = point_distance(x,y,xstart,ystart)
		dist2 = point_distance(xto,yto,xstart,ystart)
		if(dist1 > wander_dist)
		and(dist2 > dist1)
			{
			move_speed = 0
			}
		}
	
	var xto,yto;
	xto = x+lengthdir_x(move_speed,direction)
	yto = y+lengthdir_y(move_speed,direction)
	
	x = xto
	y = yto
	
	if(move_speed = 0)
		{
		snap_x_center()
		snap_y_center()
		}
	}
var search;
search = search_hero()
if(escape = true)
	{
	search = false
	follow_time = 0
	state = "Wander"
	}
if(search == true)
or(follow_time > 0)
	{
	if(follow_time > 0)
		{
		follow_time -= 1
		move_speed = walk_speed
		}
	if(check_snapx_precise())
	and(check_snapy_precise())
		{
		if(search = true)
			{
			follow_time = mxfollow_time
			}
		snap_x_center()
		snap_y_center()
		path_to(togrid(obj_player.x),togrid(obj_player.y),run_speed)
		if(state != "Follow")
			{
			state = "Follow"
			if(! instance_exists(obj_battle))audio_play_sound_on(myemitter,sfx_notice,0,1)
			}
		}
	}
	
x = clamp(x,0,((room_width div global.grid)*global.grid))
y = clamp(y,0,((room_height div global.grid)*global.grid)-global.grid)