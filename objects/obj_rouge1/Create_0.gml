event_inherited()

//Setup Sprites
icon = spr_rougehead
stand_south = spr_red_south
stand_east = spr_red_east
stand_west = spr_red_west
stand_north = spr_red_north

walk_south = spr_red_south_walk
walk_east = spr_red_east_walk
walk_west = spr_red_west_walk
walk_north = spr_red_north_walk

battle[0] = spr_rouge_battle
battle[1] = spr_rouge_creampie
sound[0] = tm_rouge_battle
sound[1] = tm_rouge_creampie

assist[0] = spr_rougeassist
assist[1] = spr_rougeassistcum

//Main Variables
name = rouge_name
state = "Wander"


//Editable Variables
random_set_seed(global.seed)
walk_speed = 1.2
run_speed = 1.75
decision_timer = 80
wander_dist = 120
follow_time = 0
mxfollow_time = 120
follow_dist = 120


//Game Variables
move_speed = 0
tmove_speed = 0
tdirection = direction

//Battle Variables
habit[0][0] = choose("L","R")
habit[0][1] = habit[0][0]
habit[0][2] = choose("R","L")
habit[0][3] = habit[0][2]

habit[1][0] = choose("U","D")
habit[1][1] = habit[1][0]
habit[1][2] = choose("D","U")
habit[1][3] = habit[1][2]
mybits = 10+floor(random(5))
timer[0] = decision_timer+irandom(20)-irandom(20)
randomize()
//chab = floor(random(array_height_2d(habit)))
chab = 0
lhab = chab

//STATS
min_speed = array_length_2d(habit,0)
if(global.difficulty = EASY)
	{
	mxwillpower = 100+irandom(80)
	willpower = mxwillpower
	mxtimeline = 0
	cream = 4
	free_answer = 1

	battle_speed = array_length_2d(habit,0)+1+floor(random(2))-floor(random(2))
	max_speed = 10
	
	strength = 4
	skill = 8
	turn_time = 35
	}
else
	{
	mxwillpower = 160+irandom(100)
	//mxwillpower = 1
	willpower = mxwillpower
	mxtimeline = 0
	cream = 4
	free_answer = 0

	battle_speed = array_length_2d(habit,0)+1+floor(random(2))-floor(random(2))
	max_speed = 12
	strength = 4
	skill = 10
	turn_time = 30
	}
	

//Skills
enemy_action = ds_map_create()
enemy_text = ds_map_create()
enemy_mod = ds_map_create()
//ds_map_add(enemy_action,"Overheat | オーバーヒート",10+irandom(10))
//ds_map_add(enemy_action,"Unpredictable | 予測不可能な",5+irandom(15))
ds_map_add(enemy_action,"Rising Intensity | 強度の上昇",5+irandom(15))
//ds_map_add(enemy_action,"Paralyzing Pressure | 麻痺圧",5+irandom(15))
ds_map_add(enemy_action,"Energetic | 元気",20+irandom(15))
ds_map_add(enemy_action,"Wait | 待つ",10+irandom(5))

ds_map_add(enemy_text,"Wait | 待つ","She watches you as she moves her hips. | 彼女は腰を動かしながらあなたを見ている。")
ds_map_add(enemy_text,"Energetic | 元気","She's getting excited.\nThe speed of battle increases by <col @c_yellow>2<col @c_white>. | 彼女は興奮している。\n戦闘速度が<col @c_yellow>2増加します。")
//ds_map_add(enemy_text,"Unpredictable | 予測不可能な","She gives in to the pleasure. \n<col @global.pink>There's no telling what she'll do!<col @c_white> | 彼女は喜びに屈します。 \n<col @global.pink>彼女が何をするか予測できない！<col @c_white>")
//ds_map_add(enemy_text,"Overheat | オーバーヒート","Damage given increased, and damage \nreceived increased. Increases battle speed to <col @c_yellow>8<col @c_white>.\nEnds after guarding succesfully <col @c_yellow>5<col @c_white> times. | 与えられるダメージが増加し、\n受けるダメージが増加しました。 ターン数を<col @c_yellow>8<col @c_white>に増やします。\n<col @c_yellow>5<col @c_white>回ガードした後終了します。")
ds_map_add(enemy_text,"Rising Intensity | 強度の上昇","For <col @c_yellow>5<col @c_white> turns, the battle speed will increase by <col @c_yellow>1<col @c_white>. \nThe action timer will also be halved. | <col @c_yellow>5<col @c_yellow>ターンの間、戦闘速度は<col @c_yellow>1<col @c_white>ずつ増加します。\nアクションタイマーも半分になります。")
//ds_map_add(enemy_text,"Paralyzing Pressure | 麻痺圧","While the battle speed is lower than <col @c_yellow>6<col @c_white>, \nthe action timer is set to <col @c_yellow>12<col @c_white>.\nIgrec's damage is also slightly reduced. | 戦闘速度が<col @c_yellow>6<col @c_white>未満の場合、アクションタイマーは<col @c_yellow>12<col @c_white>に設定されます。\nイグレックのダメージも少し減少しました。")

//ds_map_add(enemy_mod,"Overheat | オーバーヒート",overheat_mod)
//ds_map_add(enemy_mod,"Unpredictable | 予測不可能な",unpredictable_mod)
ds_map_add(enemy_mod,"Rising Intensity | 強度の上昇",rising_intensity_mod)
//ds_map_add(enemy_mod,"Paralyzing Pressure | 麻痺圧",paralyzing_pressure_mod)
ds_map_add(enemy_mod,"Energetic | 元気",energetic_mod)
ds_map_add(enemy_mod,"Wait | 待つ",wait_mod)

start = ds_list_create()
ds_list_add(start,"I'm putting it in. | あぁ、熱いのが中に・・・。")
ds_list_add(start,"Don't cum too soon. | すぐに射精したら許さないわよ。")

win = ds_list_create()
ds_list_add(win,"Hahahaha. Lame. | ハハハッ！ もう出ちゃったの？")
ds_list_add(win,"<wave>Let's play some more.</wave> | <wave>もっと遊びましょうよ。</wave>")
ds_list_add(win,"<shake strong=2>Let it out! Let it out!</shake> \nI know you've got more in there! | <shake strong=2>ほら！ほら！ほら！</shake> \nもっと精子が出せるはずよ！")

loss = ds_list_create()
ds_list_add(loss,"<shake strong=1>No. I can't.</shake> | <shake strong=1>ウソッ！？</shake>")
ds_list_add(loss,"<shake strong=2>I won't lose to you!</shake> | <shake strong=1>私がイかされる！？</shake>")
ds_list_add(loss,"<shake strong=3>I'm cumming!</shake> | <shake strong=1>あぁああぁぁッ!!!!</shake>")