battle_list = ds_list_create()
ds_list_add(battle_list,spr_rougehead)
ds_list_add(battle_list,spr_bleuhead)
ds_list_add(battle_list,spr_verthead)
ds_list_add(battle_list,spr_acierhead)
ds_list_add(battle_list,spr_acierhead2)
ds_list_add(battle_list,spr_violethead)
ds_list_add(battle_list,spr_jaunehead)
ds_list_add(battle_list,choose(spr_argenthead,spr_doreehead))
ds_list_add(battle_list,spr_electrumhead)
ds_list_add(battle_list,spr_tilleulhead)
ds_list_add(battle_list,spr_cielhead)
ds_list_add(battle_list,spr_mercurehead)
ds_list_add(battle_list,spr_mercurehead2)

assist_list = ds_list_create()
ds_list_add(assist_list,spr_ordinahead)
ds_list_add(assist_list,spr_rougehead)
ds_list_add(assist_list,spr_bleuhead)
ds_list_add(assist_list,spr_verthead)
ds_list_add(assist_list,spr_acierhead)
ds_list_add(assist_list,spr_violethead)
ds_list_add(assist_list,spr_jaunehead)
ds_list_add(assist_list,spr_electrumhead)
ds_list_add(assist_list,spr_tilleulhead)
ds_list_add(assist_list,spr_cielhead)
ds_list_add(assist_list,spr_mercurehead)

mwidth = 6
csel = 0

show_sex = false

scene[0] = -1
scene[1] = -1

scene_width = 480
scene_height = 480

image_speed = 1.3

scene_x = (view_wport/2)-(scene_width/2)
scene_y = 60

cscene = 0
min_speed = .8
max_speed = 2
speed_change = .1

battle_complete = ds_list_create()
assist_complete = ds_list_create()
ini_open("settings.ini")
if(ini_read_real("Enemy","Rouge",0))ds_list_add(battle_complete,spr_rougehead)
if(ini_read_real("Enemy","Bleu",0))ds_list_add(battle_complete,spr_bleuhead)
if(ini_read_real("Enemy","Vert",0))ds_list_add(battle_complete,spr_verthead)
if(ini_read_real("Enemy","Acier",0))ds_list_add(battle_complete,spr_acierhead)
if(ini_read_real("Enemy","Acier",0))ds_list_add(battle_complete,spr_acierhead2)
if(ini_read_real("Enemy","Violet",0))ds_list_add(battle_complete,spr_violethead)
if(ini_read_real("Enemy","Jaune",0))ds_list_add(battle_complete,spr_jaunehead)
if(ini_read_real("Enemy","Argent",0))ds_list_add(battle_complete,spr_argenthead)
if(ini_read_real("Enemy","Doree",0))ds_list_add(battle_complete,spr_doreehead)
if(ini_read_real("Enemy","Electrum",0))ds_list_add(battle_complete,spr_electrumhead)
if(ini_read_real("Enemy","Tilleul",0))ds_list_add(battle_complete,spr_tilleulhead)
if(ini_read_real("Enemy","Ciel",0))ds_list_add(battle_complete,spr_cielhead)
if(ini_read_real("Enemy","Mercure",0))ds_list_add(battle_complete,spr_mercurehead)
if(ini_read_real("Enemy","Mercure",0))ds_list_add(battle_complete,spr_mercurehead2)

if(ini_read_real("Enemy","Ordina",0))ds_list_add(assist_complete,spr_ordinahead)
if(ini_read_real("Assist","Rouge",0))ds_list_add(assist_complete,spr_rougehead)
if(ini_read_real("Assist","Bleu",0))ds_list_add(assist_complete,spr_bleuhead)
if(ini_read_real("Assist","Vert",0))ds_list_add(assist_complete,spr_verthead)
if(ini_read_real("Assist","Acier",0))ds_list_add(assist_complete,spr_acierhead)
if(ini_read_real("Assist","Violet",0))ds_list_add(assist_complete,spr_violethead)
if(ini_read_real("Assist","Jaune",0))ds_list_add(assist_complete,spr_jaunehead)
if(ini_read_real("Assist","Electrum",0))ds_list_add(assist_complete,spr_electrumhead)
if(ini_read_real("Assist","Tilleul",0))ds_list_add(assist_complete,spr_tilleulhead)
if(ini_read_real("Assist","Ciel",0))ds_list_add(assist_complete,spr_cielhead)
if(ini_read_real("Assist","Mercure",0))ds_list_add(assist_complete,spr_mercurehead)

ini_close()