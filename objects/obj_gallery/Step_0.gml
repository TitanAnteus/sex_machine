if(show_sex = false)
	{
	if(right_pressed())
		{
		csel += 1
		audio_play_sound(sfx_menumove,1,0)
		}
	if(left_pressed())
		{
		csel -= 1
		audio_play_sound(sfx_menumove,1,0)
		}
	if(up_pressed())
		{
		csel -= mwidth
		audio_play_sound(sfx_menumove,1,0)
		}
	if(down_pressed())
		{
		csel += mwidth
		audio_play_sound(sfx_menumove,1,0)
		}
	
	var maxlist;
	maxlist = ds_list_size(battle_list)+ds_list_size(assist_list)
	if(csel < 0)csel = 0
	if(csel > maxlist-1)csel = maxlist-1
	}
else
	{
	if(left_pressed())
		{
		image_speed -= speed_change
		audio_play_sound(sfx_menumove,1,0)
		}
	if(right_pressed())
		{
		image_speed += speed_change
		audio_play_sound(sfx_menumove,1,0)
		}
	if(up_pressed())
		{
		image_speed += speed_change*2
		audio_play_sound(sfx_menumove,1,0)
		}
	if(down_pressed())
		{
		image_speed -= speed_change*2
		audio_play_sound(sfx_menumove,1,0)
		}
	image_speed = clamp(image_speed,min_speed,max_speed)
	}
if(select_pressed())
	{
	if(show_sex = false)
		{
		var citem;
		if(csel < ds_list_size(battle_list))
			{
			citem = battle_list[| csel]
			set_scene(citem,0)
			}
		else
			{
			citem = assist_list[| csel-ds_list_size(battle_list)]
			set_scene(citem,1)
			}
		}
	else
		{
		cscene = !cscene
		if(cscene = 1)
			{
			last_speed = image_speed
			image_speed = 1.3
			}
		else
			{
			image_speed = last_speed
			}
		sprite_index = scene[cscene]
		image_index = 0
		}
	}
if(back_pressed())
	{
	if(show_sex = true)
		{
		show_sex = false
		sprite_index = -1
		audio_play_sound(sfx_back,1,0)
		}
	else
		{
		instance_destroy()
		}
	}