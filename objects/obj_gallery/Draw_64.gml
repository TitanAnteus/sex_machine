draw_set_halign(fa_center)
draw_set_valign(fa_top)

draw_set_font(global.main_font)

draw_text_outline(room_width/2,20,string_split("Battle Gallery | バトルギャラリー"),c_white,c_black)

var iwidth,iheight;
iwidth = 66
iheight = 66

var bwidth,left_x,top_y;
bwidth = min(mwidth,ds_list_size(battle_list))*iwidth
left_x = (room_width/2)-(bwidth/2)
top_y = 64

draw_line(100,top_y-8,room_width-100,top_y-8)

for(i=0;i<ds_list_size(battle_list);i+=1)
	{
	var citem,col;
	col = c_white
	citem = battle_list[| i]
	
	if(csel == i)col = merge_color(c_white,c_red,.5)
	if(ds_list_find_index(battle_complete,citem) == -1)col = merge_color(col,c_black,.7)
	
	var li,ly;
	li = i mod mwidth
	ly = i div mwidth
	draw_sprite_ext(citem,0,left_x+(iwidth*li),top_y+(iheight*ly),1,1,0,col,1)
	}

draw_text_outline(room_width/2,(room_height/2)+20,string_split("Assist Gallery | アシストギャラリー"),c_white,c_black)
bwidth = min(mwidth,ds_list_size(assist_list))*iwidth
left_x = (room_width/2)-(bwidth/2)
top_y = (room_height/2)+64

draw_line(100,top_y-8,room_width-100,top_y-8)

for(i=0;i<ds_list_size(assist_list);i+=1)
	{
	var citem,col;
	col = c_white
	citem = assist_list[| i]
	
	if(csel == i+ds_list_size(battle_list))col = merge_color(c_white,c_red,.5)
	if(ds_list_find_index(assist_complete,citem) == -1)col = merge_color(col,c_black,.7)
	
	var li,ly;
	li = i mod mwidth
	ly = i div mwidth
	draw_sprite_ext(citem,0,left_x+(iwidth*li),top_y+(iheight*ly),1,1,0,col,1)
	}
	
if(show_sex)
	{
	draw_set_alpha(.5)
	draw_set_color(c_black)
	draw_rectangle(0,0,room_width,room_height,0)
	
	draw_set_alpha(1)
	draw_set_color(c_white)
	
	if(sprite_exists(sprite_index))
		{
		var scl;
		scl = scene_width/sprite_get_width(sprite_index)
		draw_set_color(c_white)
		draw_rectangle(scene_x-1,scene_y-1,scene_x+scene_width,scene_y+scene_height,1)
		draw_sprite_ext(sprite_index,image_index,scene_x,scene_y,scl,scl,0,c_white,1)
		
		var topy,boxh,th;
		topy = scene_y+scene_height+2
		th = string_height("H")+2
		boxh = th*3+2
		draw_set_color(c_black)
		draw_set_alpha(.5)
		draw_rectangle(scene_x,topy,scene_x+scene_width,topy+boxh,0)
		draw_set_color(c_white)
		draw_set_alpha(1)
		draw_rectangle(scene_x,topy,scene_x+scene_width,topy+boxh,1)
		
		draw_set_halign(fa_left)
		draw_text(scene_x+4,topy+4,string_split("Arrow Keys:  | 矢印キー： "))
		draw_text(scene_x+4,topy+4+th,keyname(global.k_select)+ " "+ string_split("Key:  | ボタン： "))
		draw_text(scene_x+4,topy+4+th+th,keyname(global.k_back)+ " "+ string_split("Key:  | ボタン： "))
		
		draw_set_halign(fa_right)
		draw_text(scene_x+scene_width-4,topy+4,string_split("Change speed | 速度を変える"))
		draw_text(scene_x+scene_width-4,topy+4+th,string_split("Toggle Creampie | 射精を切り替え"))
		draw_text(scene_x+scene_width-4,topy+4+th+th,string_split("Return | 戻る"))
		}
	}