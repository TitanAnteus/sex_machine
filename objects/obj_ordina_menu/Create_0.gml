battle[0] = spr_ordina_battle
battle[1] = spr_ordina_creampie
sound[0] = tm_ordina_battle
sound[1] = tm_ordina_creampie

blursurf = -1
menusurf = -1
rightsurf = -1
itemsurf = -1
passivesurf = -1
shopsurf = -1
peskill = ""

menuwidth = 700
menuheight = 600
rightwidth = menuwidth*.5
rightheight = menuheight

menu_centerx = view_wport*.3
menu_centery = view_hport*.5

text_x = (menuwidth-rightwidth)/2
text_y = menuheight/2
text_pad = font_get_size(global.big_font)+32

menu[0,0] = "P. Equip | 受動的変化"
menu[0,1] = "Item Shop | アイテムショップ"
menu[0,2] = "Equip Shop | 装備店"
menu[0,3] = "Restore | 休息"
menu[0,4] = "Save | セーブ"
menu[0,5] = "Load | ロード"
menu[0,6] = "Back | 戻る"

cmenu = 0
cpos = 0

item_menu = global.item_list

selecting = false
equip_select = false

dpos = 0
dmax = 0

itemhor = 2

with(obj_canfreeze)
	{
	freeze_char(-1)
	}

anim = 0
toend = false
toload = false
sprite_index = battle[anim]

image_xscale = 8
image_yscale = 8

img_start = image_speed
cumgrowth = 25
cumcount = 0

save_sprite = sprite_add(working_directory+"save.png",1,0,0,0,0)