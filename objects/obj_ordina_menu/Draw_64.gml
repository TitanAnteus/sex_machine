if(surface_exists(blursurf))
	{
	surface_set_target(blursurf)
	surface_copy(blursurf,0,0,application_surface)
	surface_reset_target()
	var offset;
	offset = 2
	draw_set_alpha(.4)
	
	draw_surface(blursurf,-offset,-offset)
	draw_surface(blursurf,offset,offset)
	draw_surface(blursurf,offset,-offset)
	draw_surface(blursurf,-offset,offset)
	
	draw_set_alpha(1)
	}
else
	{
	blursurf = surface_create(view_wport[0],view_hport[0])
	}
	
if(surface_exists(menusurf))
	{
	surface_set_target(menusurf)
	
	draw_set_color(c_black)
	draw_set_alpha(.8)
	draw_roundrect(1,1,menuwidth-1,menuheight-1,0)
	
	draw_set_color(c_white)
	draw_set_alpha(1)
	draw_roundrect(1,1,menuwidth-2,menuheight-2,1)	
	
	draw_set_halign(fa_center)
	draw_set_valign(fa_center)
	draw_set_font(global.big_font)
	var citem,tx,ty,theight,tcol;
	tx = text_x
	theight = array_length_2d(menu,cmenu)*text_pad
	ty = (menuheight/2)-(theight/2)+(text_pad/2)
	tcol = c_white
	for(i=0;i<array_length_2d(menu,cmenu);i+=1)
		{
		tcol = c_white
		if(i = cpos)
			{
			if(selecting = false)tcol = c_red
			if(selecting = true)tcol = c_yellow
			}
		if(toend = true)tcol = c_dkgray
		if(draw_get_color() != tcol)
			{
			draw_set_color(tcol)
			}
		citem = menu[cmenu,i]
		draw_text(tx,ty+(i*text_pad),string_split(citem))
		}
	if(draw_get_color() != c_white)draw_set_color(c_white)
	draw_line(menuwidth-rightwidth,25,menuwidth-rightwidth,menuheight-25)
	
	draw_set_halign(fa_left)
	draw_set_valign(fa_bottom)
	draw_set_font(global.big_font)
	if(draw_get_color() != c_white)draw_set_color(c_white)
	draw_set_alpha(1)
	draw_text(10,menuheight-3,string_split("Bits:"+string(global.bits)))
	
	if(surface_exists(rightsurf))
		{
		citem = menu[cmenu,cpos]
		surface_set_target(rightsurf)
		draw_clear_alpha(c_white,0)
		
		if(string_count("Back",citem))draw_back_menu()
		if(string_count("P. Equip",citem))draw_pequip_menu()
		if(string_count("Item Shop",citem))draw_itemshop_menu()
		if(string_count("Equip Shop",citem))draw_equipshop_menu()
		if(string_count("Restore",citem))draw_restore_menu()
		if(string_count("Save",citem))draw_save_menu()
		if(string_count("Load",citem))draw_load_menu()
		surface_reset_target()
		
		draw_surface(rightsurf,menuwidth-rightwidth,0)
		}
	else
		{
		rightsurf = surface_create(rightwidth,rightheight)
		}
	
	surface_reset_target()
	
	var lx,ly,shake;
	shake = 0
	draw_set_alpha(1)
	if(anim = 1)
		{
		draw_set_alpha(.2+random(.6))
		shake = random(5)-random(5)
		}
	lx = menu_centerx-(menuwidth/2)+shake
	ly = menu_centery-(menuheight/2)+shake
	draw_surface(menusurf,lx,ly);
	}
else
	{
	menusurf = surface_create(menuwidth,menuheight)
	}
var sx,sy,rightcent;
rightcent = (view_wport-(menu_centerx+(menuwidth/2)))/2
sx = round((view_wport-rightcent)-(sprite_width/2))
sy = round(view_hport/2-(sprite_height/2))
if(draw_get_color() != c_white)draw_set_color(c_white)
draw_sprite_ext(sprite_index,image_index,sx,sy,image_xscale,image_yscale,image_angle,image_blend,image_alpha)
draw_rectangle(sx,sy,sx+sprite_width-1,sy+sprite_height,1)
sy = sy+sprite_height+2

draw_rectangle(sx,sy,sx+(sprite_width*(global.cum/(global.mxcum+(cumgrowth*cumcount))))-1,sy+24,0)
draw_rectangle(sx,sy,sx+(sprite_width)-1,sy+24,1)