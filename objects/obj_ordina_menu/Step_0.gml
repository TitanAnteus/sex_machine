var citem;
citem = menu[cmenu,cpos]
if(anim = 1)exit
if(toend = 1)exit
if(selecting = false)
	{
	if(down_pressed())
	or(right_pressed())
		{
		cpos += 1
		dpos = 0
		audio_play_sound(sfx_menumove,1,0)
		}
	if(up_pressed())
	or(left_pressed())
		{
		cpos -= 1
		dpos = 0
		audio_play_sound(sfx_menumove,1,0)
		}
	if(cpos = -1)cpos = array_length_2d(menu,cmenu)-1
	cpos = cpos mod array_length_2d(menu,cmenu)
	}
else
	{
	if(down_pressed())
	or(right_pressed())
		{
		dpos += 1
		audio_play_sound(sfx_menumove,1,0)
		}
	if(up_pressed())
	or(left_pressed())
		{
		dpos -= 1
		audio_play_sound(sfx_menumove,1,0)
		}
	dpos = clamp(dpos,-1,dmax+1)
	if(dpos < 0)dpos = dmax-1
	dpos = dpos mod (dmax)
	}


if(select_pressed())
	{
	if(string_count("Restore",citem) > 0)
		{
		if(global.bits >= global.restcost)
			{
			if(global.willpower != global.mxwillpower)
				{
				global.willpower = global.mxwillpower
				global.stamina = global.mxstamina
				global.bits -= global.restcost
				audio_play_sound(sfx_heal,1,0)
				}
			else
				{
				audio_play_sound(sfx_fail,1,0)
				}
			}
		else
			{
			audio_play_sound(sfx_fail,1,0)
			}
		exit
		}
	if(string_count("P. Equip",citem) > 0)
		{
		if(selecting = false)
			{
			selecting = true
			dpos = 0
			dmax = ds_map_size(global.all_passive)
			}
		else
			{
			var ccost;
			ccost = get_passive_cost(peskill)
			if(! ds_map_exists(global.equip_passive,peskill))
				{
				if(global.cPP+ccost <= global.maxPP)
					{
					ds_map_add(global.equip_passive,peskill,ds_map_find_value(global.all_passive,peskill))
					audio_play_sound(sfx_select,1,0)
					}
				else
					{
					audio_play_sound(sfx_fail,1,0)
					}
				}
			else
				{
				if(global.cPP-ccost <= global.maxPP)
					{
					ds_map_delete(global.equip_passive,peskill)
					audio_play_sound(sfx_back,1,0)
					}
				else
					{
					audio_play_sound(sfx_fail,1,0)
					}
				}
			}
		}
		
	if(string_count("Item Shop",citem) > 0)
		{
		if(selecting = false)
			{
			selecting = true
			dpos = 0
			dmax = ds_list_size(itemshop_list)
			}
		else
			{
			var citem,ccost;
			citem = itemshop_list[| dpos]
			ccost = get_item_cost(citem)
			if(global.bits >= ccost)
			and(ds_list_size(global.item_list) < global.mxitems)
				{
				ds_list_add(global.item_list,citem)
				global.bits -= ccost
				audio_play_sound(sfx_select,1,0)
				}
			else
				{
				audio_play_sound(sfx_fail,1,0)
				}
			}
		}
	if(string_count("Equip Shop",citem) > 0)
		{
		if(selecting = false)
			{
			selecting = true
			dpos = 0
			dmax = ds_list_size(equipshop_list)
			}
		else
			{
			var citem,ccost;
			citem = equipshop_list[| dpos]
			ccost = get_item_cost(citem)
			if(global.bits >= ccost)
			and(ds_list_find_index(global.equip_list,citem) == -1)
			and(citem != global.equipment)
				{
				ds_list_add(global.equip_list,citem)
				global.bits -= ccost
				audio_play_sound(sfx_select,1,0)
				}
			else
				{
				audio_play_sound(sfx_fail,1,0)
				}
			}
		}
	if(string_count("Save",citem) > 0)
		{
		save_game()
		}
	if(string_count("Load",citem) > 0)
		{
		load_game()
		}
	if(string_count("Back",citem) > 0)
		{
		if(cmenu = 0)
			{
			if(alarm[0] = -1)alarm[0] = 2
			exit
			}
		else if(cmenu = 2)
			{
			cmenu = 1
			audio_play_sound(sfx_back,1,0)
			}
		else
			{
			cmenu = 0
			audio_play_sound(sfx_back,1,0)
			}
		cpos = 0
		}
	}
if(back_pressed())
	{
	if(selecting = false)
		{
		if(cmenu = 0)
			{
			if(alarm[0] = -1)alarm[0] = 2
			exit
			}
		else if(cmenu = 2)
			{
			cmenu = 1
			audio_play_sound(sfx_back,1,0)
			}
		else
			{
			cmenu = 0
			audio_play_sound(sfx_back,1,0)
			}
		cpos = 0
		}
	if(selecting = true)
		{
		if(equip_select = true)
			{
			dpos = 0
			equip_select = false
			item_menu = global.item_list
			dmax = ds_list_size(item_menu)
			}
		else
			{
			selecting = false
			}
		audio_play_sound(sfx_back,1,0)
		}
	}