//draw_sprite(mask_index,0,x,y)
draw_sprite_ext(spr_shadow,0,x+(global.grid/2),y+global.grid,image_xscale,image_yscale,image_angle,image_blend,image_alpha)
draw_self()

//draw_text(x,y-50,depth)

if(instance_exists(obj_cutscene))
	{
	var nearest_event,ndist;
	nearest_event = instance_nearest(x,y,obj_cutscene)
	ndist = distance_to_object(nearest_event)

	if(instance_place(x+lengthdir_x(global.grid,direction),y+lengthdir_y(global.grid,direction),nearest_event))
	and(nearest_event.start_condition = cut_examine)
	and(state != "Cutscene")draw_sprite_ext(spr_thinking,0,x+8,y-16,.5,.5,0,c_white,1)
	}

//draw_text(x,y-100,depth)
//draw_set_color(c_white)
//if(path_index != -1)draw_path(path_index,0,0,true)