event_inherited()
depth = depth_start+(-(y+16))

if(frozen = true)exit
//Change Image
if(direction_locked = false)
	{
	if(move_speed > 0)
	or(path_index != -1)
		{
		if(direction = 0)sprite_index = walk_east
		if(direction = 180)sprite_index = walk_west
		if(direction = 90)sprite_index = walk_north
		if(direction = 270)sprite_index = walk_south		
		}
	else
		{
		if(direction = 0)sprite_index = stand_east
		if(direction = 180)sprite_index = stand_west
		if(direction = 90)sprite_index = stand_north
		if(direction = 270)sprite_index = stand_south
		}
	}
else
	{
	if(move_speed > 0)
	or(path_index != -1)
		{
		if(lockdir = 0)sprite_index = walk_east
		if(lockdir = 180)sprite_index = walk_west
		if(lockdir = 90)sprite_index = walk_north
		if(lockdir = 270)sprite_index = walk_south		
		}
	else
		{
		if(lockdir = 0)sprite_index = stand_east
		if(lockdir = 180)sprite_index = stand_west
		if(lockdir = 90)sprite_index = stand_north
		if(lockdir = 270)sprite_index = stand_south
		}
	}
	