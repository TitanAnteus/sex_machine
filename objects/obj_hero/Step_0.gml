if(frozen = true)exit
player_move()

if(path_index == -1)
	{
	x += lengthdir_x(move_speed,direction)
	y += lengthdir_y(move_speed,direction)

	x = clamp(x,0,((room_width div global.grid)*global.grid))
	y = clamp(y,0,((room_height div global.grid)*global.grid)-global.grid)
	}

if(select_pressed())
and(state = "Normal")
	{
	var _list,_num;
	_list = ds_list_create()
	_num = instance_place_list(x+lengthdir_x(global.grid,direction),y+lengthdir_y(global.grid,direction),obj_canfreeze,_list,0)
	if(_num > 0)
		{
		for(i=0;i<ds_list_size(_list);i+=1)
			{
			var inst;
			inst = _list[|i]
			with(inst)
				{
				if(action != -1)action()
				}
			}
		}
	}


/*
if(mouse_check_button(mb_left))
and(check_snapx_center())
and(check_snapy_center())
	{
	snap_x_center()
	snap_y_center()
	path_to(togrid(mouse_x),togrid(mouse_y),walk_speed)
	}
*/
