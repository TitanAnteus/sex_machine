event_inherited()
direction_locked = false
lockdir = direction
//Setup Sprites
stand_south = spr_hero_south
stand_east = spr_hero_east
stand_west = spr_hero_west
stand_north = spr_hero_north

walk_south = spr_hero_south_walk
walk_east = spr_hero_east_walk
walk_west = spr_hero_west_walk
walk_north = spr_hero_north_walk

//Main Variables
state = "Normal"

//Editable Variables
walk_speed = 2

//Game Variables
move_speed = 0
tdirection = direction

key_list = ds_list_create()

//Starting Actions
snap_x_center()
snap_y_center()

followlight = instance_create_depth(x,y,room_height+100,obj_character_light)
followlight.par = id

if(direction = 0)sprite_index = stand_east
if(direction = 180)sprite_index = stand_west
if(direction = 90)sprite_index = stand_north
if(direction = 270)sprite_index = stand_south