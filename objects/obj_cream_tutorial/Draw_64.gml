if(surface_exists(darksurf))
	{
	surface_set_target(darksurf)
	draw_set_alpha(dark_alph)
	draw_set_color(c_black)
	draw_rectangle(0,0,view_wport,view_hport,0)
	draw_set_alpha(1)
	
	focus += clamp((0-focus)/20,-4,0)
	
	if(ctext = 1)
		{
		var w,h;
		w = ((sprite_get_width(spr_heart)+2)*obj_battle.max_cream_count)*2
		h = sprite_get_height(spr_heart)*2
		gpu_set_blendmode(bm_subtract)
		draw_set_color(c_black)
		draw_roundrect(obj_battle.heart_x-1-focus,obj_battle.command_y-4-focus-h,obj_battle.heart_x+w+1+focus,
		obj_battle.command_y+1+focus,0)
		gpu_set_blendmode(bm_normal)
		}
	surface_reset_target()
	}
else
	{
	darksurf = surface_create(view_wport[0],view_hport[0])
	draw_set_alpha(dark_alph)
	draw_set_color(c_black)
	draw_rectangle(0,0,view_wport,view_hport,0)
	}
draw_surface(darksurf,0,0)
draw_set_color(c_black)
draw_set_alpha(.8)

bx1 += (box_x1-bx1)/spd
bx2 += (box_x2-bx2)/spd
by1 += (box_y1-by1)/spd
by2 += (box_y2-by2)/spd
if(abs(bx1-box_x1) < .2)
and(box_open = false)
	{
	bx1 = box_x1
	bx2 = box_x2
	by1 = box_y1
	by2 = box_y2
	box_open = true
	}
draw_roundrect(bx1,by1,bx2,by2,0)
draw_set_color(c_white)
draw_set_alpha(1)
draw_roundrect(bx1,by1,bx2,by2,1)

if(box_open = true)
	{
	draw_set_font(global.main_font)
	if(tpos <= string_length(string_split(dtext)))
		{
		tpos += tspd
		tpos = skip_color(string_split(dtext),tpos)
		draw_continue = false
		}
	if(tpos >= string_length(string_split(dtext)))
		{
		draw_continue = true
		}
	var ww;
	draw_set_halign(fa_center)
	draw_set_valign(fa_center)
	ww = (box_width)-(padding*2)
	tpos = clamp(tpos,0,string_length(string_split(dtext)))
	draw_text_width(center_x,center_y,string_copy(string_split(dtext),0,tpos),ww,text_height)
		
	if(draw_continue = true)
		{
		var symb;
		symb = keyname(global.k_select)
		draw_set_halign(fa_right)
		draw_set_valign(fa_bottom)
		if(draw_get_color() != c_white)draw_set_color(c_white)
		draw_text(box_x2-padding,box_y2-padding-text_bob,symb)
		}
	if(text_bdir == true)
		{
		text_bob += text_bspd
		if(text_bob > mx_text_bob)text_bdir = !text_bdir
		}
	else
		{
		text_bob -= text_bspd
		if(text_bob < 0)text_bdir = !text_bdir
		}
	}