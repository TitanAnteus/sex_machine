if(place_meeting(x,y,obj_player))
	{
	with(obj_player)
		{
		state = "Cutscene"
		if(other.trans_dir == 0)path_to(min(x+100,room_width-global.grid),y,2)
		if(other.trans_dir == 90)path_to(x,max(y-100,global.grid),2)
		if(other.trans_dir == 180)path_to(max(x-100,global.grid),y,2)
		if(other.trans_dir == 270)path_to(x,min(y+100,room_height-global.grid),2)
		}
	alpha_start = 1
	}
	
if(alpha >= 1)
	{
	global.trans_pos = trans_pos
	global.trans_dir = trans_dir
	room_goto(nextroom)
	}