event_inherited()

//Setup Sprites
icon = spr_bleuhead
stand_south = spr_blue_south
stand_east = spr_blue_east
stand_west = spr_blue_west
stand_north = spr_blue_north

walk_south = spr_blue_south_walk
walk_east = spr_blue_east_walk
walk_west = spr_blue_west_walk
walk_north = spr_blue_north_walk

battle[0] = spr_bleu_battle
battle[1] = spr_bleu_creampie
sound[0] = tm_bleu_battle
sound[1] = tm_bleu_creampie

assist[0] = spr_bleuassist
assist[1] = spr_bleuassistcum

//Main Variables
name = bleu_name
state = "Wander"

//Editable Variables
random_set_seed(global.seed)
walk_speed = 1.2
run_speed = 1.6
decision_timer = 60
wander_dist = 240
follow_time = 0
mxfollow_time = 80
follow_dist = 90

//Game Variables
move_speed = 0
tmove_speed = 0
tdirection = direction



//Battle Variables
habit[0][0] = choose("D","U")
habit[0][1] = opposite_dir(habit[0,0])

habit[1][0] = choose("R","L")
habit[1][1] = opposite_dir(habit[1][0])

if(global.difficulty = NORMAL)
	{
	habit[2][0] = choose(opposite_dir(habit[0][0]),opposite_dir(habit[1][0]))
	habit[2][1] = opposite_dir(habit[2][0])
	}

mybits = 8+floor(random(15))
timer[0] = decision_timer+irandom(20)-irandom(20)
randomize()
//chab = floor(random(array_height_2d(habit)))
chab = 0
lhab = chab
//STATS
min_speed = array_length_2d(habit,0)
if(global.difficulty = EASY)
	{
	mxwillpower = 90+irandom(20)
	willpower = mxwillpower
	mxtimeline = 0
	cream = 4
	free_answer = 1

	battle_speed = array_length_2d(habit,0)+1+floor(random(3))-floor(random(3))
	max_speed = 7
	strength = 2
	skill = 12
	turn_time = 20
	}
else
	{
	mxwillpower = 110+irandom(20)
	willpower = mxwillpower
	mxtimeline = 0
	cream = 6
	free_answer = 0

	battle_speed = array_length_2d(habit,0)+1+floor(random(3))-floor(random(3))
	max_speed = 8
	strength = 2
	skill = 13
	turn_time = 15
	}

//Skills
enemy_action = ds_map_create()
enemy_text = ds_map_create()
enemy_mod = ds_map_create()
ds_map_add(enemy_action,"Loving Caress | 愛する愛撫",5+irandom(10))
ds_map_add(enemy_action,"Sadist | サディスト",20+irandom(40))
ds_map_add(enemy_action,"Hyper | ハイパー",15+irandom(20))
ds_map_add(enemy_action,"Heart Piercing Smile | ハートピアススマイル",15+irandom(25))
ds_map_add(enemy_action,"Wait | 待つ",5+irandom(5))

ds_map_add(enemy_text,"Wait | 待つ","She moves while paying attention to your expressions. | 彼女はあなたの表現に注意を払いながら動きます。")
ds_map_add(enemy_text,"Sadist | サディスト","The battle speed will increase \nfor every perfect guard for <col @c_yellow><return @sadist_count><col @c_white> turns. | <col @c_yellow><return @sadist_count><col @c_white>ターンの間に完璧なガードをするたびに、\n戦闘速度が上がります。")
ds_map_add(enemy_text,"Hyper | ハイパー","She's getting excited.\nThe speed of battle increases by <col @c_yellow><return @hyper_speed><col @c_white>. | 彼女は興奮し始めており、\n戦闘速度は<col @c_yellow><return @hyper_speed><col @c_white>増加します。")
ds_map_add(enemy_text,"Heart Piercing Smile | ハートピアススマイル","Her smile strikes you right in the heart.\nStamina has been reduced. | 彼女の笑顔はあなたを心の底から打ちます。 \nスタミナが減少しました。")
ds_map_add(enemy_text,"Loving Caress | 愛する愛撫","She selfishly pumps her hips. \nFor this turn, all failures will heal her significantly. | 彼女は利己的に腰を動かします。 \nこのターン、すべての失敗は彼女を癒します。")

ds_map_add(enemy_mod,"Loving Caress | 愛する愛撫",loving_caress_mod)
ds_map_add(enemy_mod,"Sadist | サディスト",sadist_mod)
ds_map_add(enemy_mod,"Hyper | ハイパー",hyper_mod)
ds_map_add(enemy_mod,"Heart Piercing Smile | ハートピアススマイル",heart_pierce_mod)
ds_map_add(enemy_mod,"Wait | 待つ",wait_mod)

//Starting Actions
snap_x_center()
snap_y_center()

start = ds_list_create()
ds_list_add(start,"Oh I can't wait. | もう待てないよぉ。")

win = ds_list_create()
ds_list_add(win,"<wave>It felt too good hunh?</wave> | <wave>気持ち良かった？</wave>")
ds_list_add(win,"Well I haven't had enough. | でもわたしはまだイってないよ?")
ds_list_add(win,"You can cum as much as you want. \nI'm not stopping. | もっと出せるよね？ \nこのまま搾り取っちゃうんだから。")
ds_list_add(win,"Don't look at me like that. \n<wave>You're making me want to bully you.</wave> | そんな風に私を見ちゃって・・・。 \n<wave>もっとアナタをいじめたいなぁ。</wave>")

loss = ds_list_create()
ds_list_add(loss,"<shake strong=.5>This... this is nothing...</shake> | <shake strong=.5>コ...コレはダメ...。</shake>")
ds_list_add(loss,"<shake strong=1>Not yet...</shake> | <shake strong=1>ダメ...！</shake>")
ds_list_add(loss,"<shake strong=2>Please...</shake> | <shake strong=2>待って...！</shake>")
ds_list_add(loss,"<shake strong=3>I'm cumming!</shake> | <shake strong=3>イクゥゥゥゥゥ!!!!</shake>")