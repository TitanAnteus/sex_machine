if(start_condition = cut_collide)
and(place_meeting(x,y,obj_player))
and(obj_player.state != "Cutscene")cutscene_start()

if(start == 0)exit

if(tofreeze == true)
	{
	with(obj_canfreeze)
		{
		if(id != other.id)freeze_char(-1)
		}
		
	with(obj_player){
	unfreeze_char()
	prevstate = state
	state = "Cutscene"
	}
	
	with(obj_enemy){
	prevstate = state
	state = "Cutscene"
	}
	
	if(mysfx != noone)
	and(cevent == 0)audio_play_sound(mysfx,1,0)
	tofreeze = 2
	}
	
if(cevent >= ds_list_size(event[event_count])){
	if(start_condition == cut_auto){
		if(alarm[1] = -1)alarm[1] = 1
	}
	else{
		if(! instance_exists(obj_battle))
			{
			with(obj_canfreeze){unfreeze_char()}
			}
		with(obj_player)
			{
			state = prevstate
			direction_locked = false
			}
		with(obj_enemy){
			state = prevstate
			timer[0] = decision_timer+irandom(20)-irandom(20)
			direction_locked = false
			}
		if(event_count == array_length(event)-1)isdone = true
		event_count = clamp(event_count+1,0,array_length(event)-1)
		alarm[2] = 1
	}
	exit
}

if(select_pressed())
or(wait = 0)
	{
	wait = mxwait
	if(event[event_count][| cevent] == "Talk")
		{
		if(tpos < string_length(string_split(ctext)))
			{
			tpos = string_length(string_split(ctext))
			}
		else
			{
			cevent += 3
			}
		}
	if(event[event_count][| cevent] != "Wait")
		{
		if(darken == 1)darken_alpha = 1
		if(darken == 2)darken_alpha = 0
		}
	}

#region Sound
if(event[event_count][| cevent] == "Play Sound")
	{
	audio_play_sound(event[event_count][| cevent+1],1,event[event_count][| cevent+2])
	cevent += 3
	}
if(event[event_count][| cevent] == "Stop Sound")
	{
	audio_stop_sound(event[event_count][| cevent+1])
	cevent += 2
	}
#endregion
#region BGM
if(event[event_count][| cevent] == "BGM")
	{
	bgm_set(event[event_count][| cevent+1])
	play_music()
	cevent += 2
	}
#endregion
#region Darken Command
if(event[event_count][| cevent] == "Darken")
	{
	darken = 1
	d_speed = event[event_count][| cevent+1]
	cevent += 2
	}
if(darken = 1)
	{
	darken_alpha += 1/d_speed
	if(skip_direct())repeat(4){darken_alpha += 1/d_speed}
	darken_alpha = clamp(darken_alpha,0,1)
	if(darken_alpha == 1)darken = 0
	}
#endregion
#region Brighten Command
if(event[event_count][| cevent] == "Brighten")
	{
	darken = 2
	d_speed = event[event_count][| cevent+1]
	cevent += 2
	}
if(darken == 2)
	{
	darken_alpha -= 1/d_speed
	if(skip_direct())repeat(4){darken_alpha -= 1/d_speed}
	darken_alpha = clamp(darken_alpha,0,1)
	if(darken_alpha == 0)darken = 0
	}
#endregion
#region Text Speed
if(event[event_count][| cevent] == "Text Speed")
	{
	var spd;
	spd = event[event_count][| cevent+1]
	if(is_undefined(spd))
		{
		tspd = tspd_start
		}
	else
		{
		tspd = spd
		if(spd == -1)tspd = tspd_start
		}
	cevent += 2
	}
#endregion
#region Talk Command
if(event[event_count][| cevent] == "Talk")
	{
	cname = event[event_count][| cevent+1]
	oname = cname
	if(ctext != event[event_count][| cevent+2])
		{
		tpos = 0
		ctext = event[event_count][| cevent+2]
		}
		
	for(i=0;i<10;i+=1)
		{
		cname = string_replace(cname,"("+string(i)+")","")
		}
	if(tpos < string_length(string_split(ctext)))tpos += tspd
	}
#endregion
#region Wait
if(event[event_count][| cevent] == "Wait")
	{
	wait_count += 1
	if(skip_direct())wait_count += 4
	
	if(wait_count >= event[event_count][| cevent+1])
	or(instance_exists(obj_battle))
		{
		cevent += 2
		wait_count = 0
		}
	}
#endregion
#region Save
if(event[event_count][| cevent] == "Save")
	{
	tosave = true
	cevent += 1
	}
#endregion
#region Instance Face
if(event[event_count][| cevent] == "Instance Face")
	{
	var inst,dir;
	inst = event[event_count][| cevent+1]
	dir = event[event_count][| cevent+2]
	
	if(inst == LAST)inst = created
	with(inst){freeze_char(-1)}
	
	with(inst)
		{
		direction = dir
		if(direction = 0)sprite_index = stand_east
		if(direction = 180)sprite_index = stand_west
		if(direction = 90)sprite_index = stand_north
		if(direction = 270)sprite_index = stand_south
		}
	cevent += 3
	}
#endregion
#region Instance Create
if(event[event_count][| cevent] == "Instance Create")
	{
	var inst,xx,yy,dir,in;
	inst = event[event_count][| cevent+1]
	xx = event[event_count][| cevent+2]
	yy = event[event_count][| cevent+3]
	dir = event[event_count][| cevent+4]
	
	created = instance_create_depth(xx,yy,1000,inst)
	created.image_alpha = 0
	
	with(created)
		{
		prevstate = state
		state = "Cutscene"
		direction = dir
		if(direction = 0)sprite_index = stand_east
		if(direction = 180)sprite_index = stand_west
		if(direction = 90)sprite_index = stand_north
		if(direction = 270)sprite_index = stand_south
		}
	cevent += 5
	}
#endregion
#region Instance Move
if(event[event_count][| cevent] == "Instance Move")
	{
	var inst,xx,yy,dir,in;
	inst = event[event_count][| cevent+1]
	xx = event[event_count][| cevent+2]
	yy = event[event_count][| cevent+3]
	
	if(inst == LAST)inst = created
	with(inst){unfreeze_char()}
	
	if(xx = CURRENT)xx = inst.x
	if(yy = CURRENT)yy = inst.y
	
	if(xx = PLAYER)xx = obj_player.x
	if(yy = PLAYER)yy = obj_player.y
	
	if(instance_exists(obj_battle))cevent += 4
	
	with(inst)
		{
		if(path_index == -1)
			{
			if(point_distance(x,y,xx,yy) > 2)
				{
				path_to(xx,yy,2)
				}
			else
				{
				x = xx
				y = yy
				snap_x_center()
				snap_y_center()
				other.cevent += 4
				}
			}
		}
	}
#endregion
#region Instance Move Relative
if(event[event_count][| cevent] == "Instance Move Relative")
	{
	var inst,xx,yy,dir;
	inst = event[event_count][| cevent+1]
	xx = event[event_count][| cevent+2]
	yy = event[event_count][| cevent+3]
	
	if(inst == LAST)inst = created
	with(inst){unfreeze_char()}
	
	if(instance_exists(obj_battle))cevent += 4
	
	with(inst)
		{
		if(path_index == -1)
			{
			startx = x
			starty = y
			if(point_distance(startx,starty,startx+xx,starty+yy) > 2)
				{
				path_to(startx+xx,starty+yy,2)
				path_endaction = path_action_continue
				}
			}
		if(point_distance(x,y,startx+xx,starty+yy) <= 2)
			{
			path_end()
			x = startx+xx
			y = starty+yy
			snap_x_center()
			snap_y_center()
			other.cevent += 4
			}
		}
	}
#endregion
#region Instance Destroy
if(event[event_count][| cevent] == "Instance Destroy")
	{
	var inst;
	inst = event[event_count][| cevent+1]
	
	if(inst == LAST)inst = created
	if(instance_exists(inst))
		{
		with(inst)
			{
			instance_destroy()
			}
		}
	cevent += 2
	}
#endregion
#region State Change
if(event[event_count][| cevent] == "State Change")
	{
	var inst;
	inst = event[event_count][| cevent+1]
	
	if(inst == LAST)inst = created
	inst.state = event[event_count][| cevent+2]
	cevent += 3
	}
#endregion
#region Room Change
if(event[event_count][| cevent] == "Room Change")
	{
	var nrm;
	nrm = event[event_count][| cevent+1]
	
	global.trans_pos = event[event_count][| cevent+2]
	room_goto(nrm)
	cevent += 3
	}
#endregion
#region Learn Skills
if(event[event_count][| cevent] == "Learn Skills")
and(instance_number(obj_skill_window) == 0)
	{
	var ar,skl;
	ar = event[event_count][| cevent+1]
	offset = start_offset
	skl = instance_create_depth(0,0,0,obj_skill_window)
	skl.skills = ar
	audio_play_sound(sfx_computer,1,0)
	
	add_skills(ar)
	}
#endregion
#region Learn Passive
if(event[event_count][| cevent] == "Learn Passive")
and(instance_number(obj_passive_window) == 0)
	{
	var ar,pass;
	ar = event[event_count][| cevent+1]
	offset = start_offset
	pass = instance_create_depth(0,0,0,obj_passive_window)
	pass.list = ar
	audio_play_sound(sfx_computer,1,0)
	
	add_passives(ar)
	}
#endregion
#region Gain Assist
if(event[event_count][| cevent] == "Gain Assist")
and(instance_number(obj_assist_window) == 0)
	{
	var ar,ass;
	ar = event[event_count][| cevent+1]
	offset = start_offset
	ass = instance_create_depth(0,0,0,obj_assist_window)
	ass.assist = ar
	audio_play_sound(sfx_computer,1,0)
	
	add_assist(ar)
	}
#endregion
#region Increase PP
if(event[event_count][| cevent] == "Increase PP")
and(instance_number(obj_passive_increase_window) == 0)
	{
	var amount,pp;
	amount = event[event_count][| cevent+1]
	offset = start_offset
	pp = instance_create_depth(0,0,0,obj_passive_increase_window)
	pp.increase = amount
	audio_play_sound(sfx_computer,1,0)
	
	increase_pp(amount)
	}
#endregion
#region Increase Carry
if(event[event_count][| cevent] == "Increase Carry")
and(instance_number(obj_carry_increase_window) == 0)
	{
	var amount,pp;
	amount = event[event_count][| cevent+1]
	offset = start_offset
	pp = instance_create_depth(0,0,0,obj_carry_increase_window)
	pp.increase = amount
	audio_play_sound(sfx_computer,1,0)
	
	increase_carry(amount)
	}
#endregion
#region Activate Object
if(event[event_count][| cevent] == "Activate Object")
	{
	var in;
	in = event[event_count][| cevent+1]
	if(in.active = 0)in.active = 1
	cevent += 2
	}
#endregion
#region Instance Lock Direction
if(event[event_count][| cevent] == "Instance Lock Direction")
	{
	var inst,dir;
	inst = event[event_count][| cevent+1]
	
	if(inst == LAST)inst = created
	with(inst)
		{
		direction_locked = true
		lockdir = direction
		}
	cevent += 2
	}
#endregion
#region Instance Unlock Direction
if(event[event_count][| cevent] == "Instance Unlock Direction")
	{
	var inst,dir;
	inst = event[event_count][| cevent+1]
	
	if(inst == LAST)inst = created
	with(inst)
		{
		direction_locked = false
		lockdir = direction
		}
	cevent += 2
	}
#endregion
if(skip_direct())
	{
	wait -= 1
	}
