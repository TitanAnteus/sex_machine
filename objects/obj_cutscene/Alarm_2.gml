///@ description start cutscene
if(isdone == true)
and(norepeat == true)
	{
	instance_destroy()
	exit
	}

ini_open(global.saveloc)
ini_write_real("SceneCount",cut_id,event_count)
ini_close()

start = 0
cevent = 0
cname = ""
oname = ""
ctext = ""
tpos = 0
tspd = .75
tspd_start = tspd

offset = 200
mspd = 8

candraw = 0

darken = 0
d_speed = 0
darken_alpha = 0

wait_count = 0
play_sfx = 1

if(start_condition == cut_auto)
	{
	cutscene_start()
	}