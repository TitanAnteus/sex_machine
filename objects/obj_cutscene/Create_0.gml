event_inherited()
depth = room_height+1000

cut_id = room_get_name(room)+"_"+name


for(i=0;i<event_count;i+=1)
	{
	event[i] = ds_list_create()
	}
	
tomenu = false

cevent = 0
event_count = 0
created = noone
cname = ""
oname = ""
ctext = ""
tpos = 0
tspd = .75
tspd_start = tspd

hpad = 40
vpad = 8

lpad = 10

boxwidth = view_wport-(hpad*2)
boxheight = 180

offset = 200
start_offset = offset
mspd = 8

candraw = 0

mxwait = global.mxwait
wait = mxwait

draw_continue = false
text_bob = 0
text_bob = 0
text_bdir = 1
text_bspd = .2
mx_text_bob = 5

parent = noone
darken = 0
d_speed = 0
darken_alpha = 0

wait_count = 0
tofreeze = 0

start = 0
action = cutscene_start
blursurf = -1
tosave = false

ini_open(global.saveloc)
isdone = ini_read_real("Scene",cut_id,0)
event_count = ini_read_real("SceneCount",cut_id,event_count)
ini_close()


event_perform(ev_alarm,2)