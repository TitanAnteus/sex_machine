if(start == 0)exit
draw_set_alpha(darken_alpha)
draw_set_color(c_black)
draw_rectangle(0,0,view_wport,view_hport,0)
draw_set_alpha(1)

var xx,yy,hh;
xx = round(hpad)
yy = round((view_hport-vpad-boxheight)+offset)



if(event[event_count][| cevent] == "Talk")
	{
	if(offset > 0)
		{
		candraw = 1
		offset += clamp((0-offset)/mspd,-20,-1)
		}
	else
		{
		offset = 0
		candraw = 1
		}
	}

draw_set_alpha(.8)
draw_set_color(c_black)
draw_rectangle(xx,yy,xx+boxwidth,yy+boxheight,0)

draw_set_alpha(1)
draw_set_color(c_white)

draw_rectangle(xx,yy,xx+boxwidth,yy+boxheight,1)
var scl,off,port,citem;
scl = (boxheight-2)/sprite_get_height(spr_rougehead)
off = round(scl*sprite_get_width(spr_rougehead))
xx += off

if(candraw)
	{
	draw_set_halign(fa_left)
	draw_set_valign(fa_bottom)
	draw_set_font(global.big_font)
	citem = oname
	if(citem != undefined)
		{
		var fr,spos;
		spos = string_pos("(",citem)
		if(spos == 0){fr = 0}
		else{fr = real(string_char_at(citem,spos+1))}
		
		port = get_portrait(cname)
		if(sprite_exists(port))draw_sprite_ext(port,fr,xx-off+2,yy+2,scl,scl,0,c_white,image_alpha)
		}
	
	hh = string_height("H")+4
	draw_text(xx+lpad,yy+hh,string_split(cname))
	draw_line(xx+lpad,yy+hh+4,xx+boxwidth-(lpad*2)-off,yy+hh+4)
	yy = yy+hh+12
	draw_set_valign(fa_top)
	draw_set_font(global.main_font)
	hh = string_height("H")+2
	var str;
	tpos = clamp(tpos,0,string_length(string_split(ctext)))
	str = string_copy(string_split(ctext),0,tpos)
	draw_text_width(xx+lpad,yy,str,boxwidth-(lpad*2)-off,hh)
	if(tpos >= string_length(string_split(ctext)))
	and(event[event_count][| cevent] == "Talk")
		{
		draw_continue = true
		}
	else
		{
		draw_continue = false
		}
	if(draw_continue = true)
		{
		var symb;
		symb = keyname(global.k_select)
		draw_set_halign(fa_right)
		draw_set_valign(fa_bottom)
		if(draw_get_color() != c_white)draw_set_color(c_white)
		draw_text(round(hpad+boxwidth-5),round((view_hport-vpad-boxheight)+offset)+boxheight-5-text_bob,symb)
		}
	if(text_bdir == true)
		{
		text_bob += text_bspd
		if(text_bob > mx_text_bob)text_bdir = !text_bdir
		}
	else
		{
		text_bob -= text_bspd
		if(text_bob < 0)text_bdir = !text_bdir
		}
	}
