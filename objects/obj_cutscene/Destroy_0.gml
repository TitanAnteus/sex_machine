if(tofreeze = 2)
	{
	if(! instance_exists(obj_battle))
		{
		with(obj_canfreeze)
			{
			unfreeze_char()
			}
		}
	with(obj_player)
		{
		state = prevstate
		direction_locked = false
		}
	with(obj_enemy)
		{
		state = prevstate
		timer[0] = decision_timer+irandom(20)-irandom(20)
		direction_locked = false
		}
	}
if(tomenu = true)
	{
	var ordina;
	ordina = instance_create_depth(0,0,0,obj_ordina_menu)
	ordina.itemshop_list = parent.itemshop_list
	ordina.equipshop_list = parent.equipshop_list
	}
	
if(norepeat == true)
	{
	ini_open(global.saveloc)
	ini_write_real("Scene",cut_id,1)
	ini_close()
	}
if(tosave = true)
	{
	save_game()
	}