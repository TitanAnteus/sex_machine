event_inherited()

//Setup Sprites
stand_south = spr_ordina_south
stand_east = spr_ordina_east
stand_west = spr_ordina_west
stand_north = spr_ordina_north

walk_south = spr_ordina_south_walk
walk_east = spr_ordina_east_walk
walk_west = spr_ordina_west_walk
walk_north = spr_ordina_north_walk

direction_locked = false
move_speed = 0

action = ordina_action

name = "Ordina | オルディーナ"

itemshop_list = ds_list_create()
function add_item(item_name){
	ds_list_add(itemshop_list,full_itemname(item_name))
}
add_item("Passion Plus | パッションプラス")
add_item("Energizer | エナジャイザー")
add_item("Libido Control | 性欲管理")

equipshop_list = ds_list_create()
function add_equip(equip_name){
	ds_list_add(equipshop_list,full_itemname(equip_name))
}
add_equip("Iron Resistance | 鉄の抵抗")
