if(surface_exists(blursurf))
	{
	surface_copy(blursurf,0,0,application_surface)
	var offset;
	offset = 2
	draw_set_alpha(.4)
	draw_surface(blursurf,-offset,-offset)
	draw_surface(blursurf,offset,offset)
	draw_surface(blursurf,offset,-offset)
	draw_surface(blursurf,-offset,offset)
	draw_set_alpha(1)
	}
else
	{
	blursurf = surface_create(view_wport[0],view_hport[0])
	}
	
if(surface_exists(menusurf))
	{
	surface_set_target(menusurf)
	draw_set_color(c_black)
	draw_set_alpha(.8)
	draw_roundrect(1,1,menuwidth-1,menuheight-1,0)
	
	draw_set_color(c_white)
	draw_set_alpha(1)
	draw_roundrect(2,2,menuwidth-2,menuheight-2,1)	
	
	draw_set_halign(fa_center)
	draw_set_valign(fa_center)
	draw_set_font(global.big_font)
	var citem,tx,ty,theight,tcol;
	tx = text_x
	theight = array_length_2d(menu,cmenu)*text_pad
	ty = (menuheight/2)-(theight/2)+(text_pad/2)
	tcol = c_white
	for(i=0;i<array_length_2d(menu,cmenu);i+=1)
		{
		tcol = c_white
		if(i = cpos)
			{
			if(selecting = false)tcol = c_red
			if(selecting = true)tcol = c_yellow
			}
		if(draw_get_color() != tcol)
			{
			draw_set_color(tcol)
			}
		citem = menu[cmenu,i]
		draw_text(tx,ty+(i*text_pad),string_split(citem))
		}
	if(draw_get_color() != c_white)draw_set_color(c_white)
	draw_line(menuwidth-rightwidth,25,menuwidth-rightwidth,menuheight-25)
	
	draw_set_halign(fa_left)
	draw_set_valign(fa_bottom)
	draw_set_font(global.big_font)
	if(draw_get_color() != c_white)draw_set_color(c_white)
	draw_set_alpha(1)
	draw_text(10,menuheight-3,string_split("Bits:"+string(global.bits)))
	
	if(surface_exists(rightsurf))
		{
		citem = menu[cmenu,cpos]
		surface_set_target(rightsurf)
		draw_clear_alpha(c_white,0)
		if(string_count("Status",citem))draw_status_menu()
		if(string_count("Item",citem))draw_item_menu()
		if(string_count("Assist",citem))draw_assist_menu()
		if(string_count("Tutorial",citem))draw_tutorial_menu()
		if(string_count("Meters",citem))draw_meter_menu()
		if(string_count("Battle 1",citem))draw_battle1_menu()
		if(string_count("Battle 2",citem))draw_battle2_menu()
		if(string_count("Defense",citem))draw_defense_menu()
		if(string_count("Library",citem))draw_library_menu()
		if(string_count("Passive",citem))draw_passive_menu()
		if(string_count("Extra",citem))draw_extra_menu()
		if(string_count("Enem",citem))draw_enemy_menu()
		if(string_count("Title",citem))draw_title_menu()
		if(string_count("Back",citem))draw_back_menu()
		surface_reset_target()
		
		draw_surface(rightsurf,menuwidth-rightwidth,0)
		}
	else
		{
		rightsurf = surface_create(rightwidth,rightheight)
		}
	
	surface_reset_target()
	
	var lx,ly;
	lx = menu_centerx-(menuwidth/2)
	ly = menu_centery-(menuheight/2)
	draw_surface(menusurf,lx,ly);
	}
else
	{
	menusurf = surface_create(menuwidth,menuheight)
	}
	
if(show_scene != 0)
and(sprite_exists(scene))
	{
	draw_set_alpha(.8)
	draw_set_color(c_black)
	draw_rectangle(0,0,view_wport,view_hport,0)
	var lx,ly,sw,sh;
	sw = sprite_get_width(scene)*scene_scale
	sh = sprite_get_height(scene)*scene_scale
	lx = round((view_wport/2)-(sw/2))
	ly = ((view_hport/2)-(sh/2))
	draw_set_color(c_white)
	draw_set_alpha(1)
	draw_rectangle(lx-1,ly-1,lx+sw,ly+sh,1)
	var extra;
	extra = asset_get_index(sprite_get_name(sprite_index)+"extra")
	if(sprite_exists(extra)){draw_sprite_ext(extra,image_index,lx,ly,scene_scale,scene_scale,image_angle,image_blend,image_alpha)}
	draw_sprite_ext(sprite_index,image_index,lx,ly,scene_scale,scene_scale,image_angle,image_blend,image_alpha)
	}