var citem;
citem = menu[cmenu,cpos]

if(show_scene = 0)
	{
	if(selecting = false)
		{
		if(down_pressed())
		or(right_pressed())
			{
			cpos += 1
			dpos = 0
			audio_play_sound(sfx_menumove,1,0)
			}
		if(up_pressed())
		or(left_pressed())
			{
			cpos -= 1
			dpos = 0
			audio_play_sound(sfx_menumove,1,0)
			}
		if(cpos = -1)cpos = array_length_2d(menu,cmenu)-1
		cpos = cpos mod array_length_2d(menu,cmenu)
		}
	else
		{
		if(down_pressed())
		or(right_pressed())
			{
			if(down_pressed() = true and string_count("Item",citem) > 0)
			and(dpos > 0)dpos += itemhor-1
			dpos += 1
			audio_play_sound(sfx_menumove,1,0)
			}
		if(up_pressed())
		or(left_pressed())
			{
			if(up_pressed() = true and string_count("Item",citem) > 0)
			and(dpos > 1)dpos -= itemhor-1
			dpos -= 1
			audio_play_sound(sfx_menumove,1,0)
			}
		dpos = clamp(dpos,-1,dmax+1)
		if(dpos < 0)dpos = dmax
		if(equip_select = true)and(dpos > dmax)dpos+=1
		dpos = dpos mod (dmax+1)
	
		if(dpos = 0)
		and(equip_select = true)
			{
			item_menu = global.item_list
			dmax = ds_list_size(item_menu)
			audio_stop_sound(sfx_menumove)
			audio_play_sound(sfx_back,1,0)
			equip_select = false
			}
		}
	}
if(select_pressed())
	{
	if(string_count("Status",citem) > 0)
	and(selecting = false)
		{
		selecting = true
		dmax = 1
		dpos = 0
		audio_play_sound(sfx_select,1,0)
		}
	if(string_count("Enem",citem) > 0)
		{
		if(selecting = false)
		and(ds_list_size(global.enemy_list) > 0)
			{
			selecting = true
			dpos = 0
			dmax = ds_list_size(global.enemy_list)-1
			audio_play_sound(sfx_select,1,0)
			}
		else
			{
			selecting = false
			audio_play_sound(sfx_back,1,0)
			}
		}
	if(string_count("Assist",citem) > 0)
		{
		if(selecting = false)
			{
			if(ds_list_size(global.assist_list) > 0)
				{
				selecting = true
				dpos = 0
				dmax = ds_list_size(global.assist_list)-1
				audio_play_sound(sfx_select,1,0)
				}
			else
				{
				audio_play_sound(sfx_fail,1,0)
				}
			}
		else
			{
			var cass,asval;
			cass = global.assist_list[| dpos]
			if(show_scene = 2)
				{
				show_scene = 0
				scene = -1
				sound = -1
				exit
				}
			if(show_scene = 1)
				{
				show_scene = 2
				scene = get_assist_anim(english_split(cass),1)
				sound = get_assist_sound(english_split(cass),1)
				}
			if(show_scene = 0)
				{
				asval = ds_map_find_value(global.assist_cooldown,cass)
				if(is_undefined(asval))asval = 0
				if(asval <= 0)
					{
					ds_map_replace(global.assist_cooldown,cass,get_assist_cost(cass))
					perform_assist(cass)
					scene = get_assist_anim(english_split(cass),0)
					sound = get_assist_sound(english_split(cass),0)
					if(sprite_exists(scene))
						{
						show_scene = 1
						}
					audio_play_sound(sfx_select,1,0)
					}
				else
					{
					audio_play_sound(sfx_fail,1,0)
					}
				}
			}
		}
	if(string_count("Item",citem) > 0)
		{
		if(selecting = false)
			{
			selecting = true
			item_menu = global.item_list
			dmax = ds_list_size(item_menu)
			dpos = 0
			audio_play_sound(sfx_select,1,0)
			}
		else
			{
			if(dpos != 0)
			and(equip_select = false)
				{
				consume_item(dpos-1)
				}
			if(dpos != 0)
			and(equip_select = true)
				{
				if(global.equipment != "")
					{
					var titem;
					titem = global.equipment
					global.equipment = item_menu[| dpos-1]
					ds_list_replace(item_menu,dpos-1,titem)
					dmax = ds_list_size(global.item_list)
					dpos = clamp(dpos,0,dmax)
					}
				else
					{
					global.equipment = item_menu[| dpos-1]
					ds_list_delete(item_menu,dpos-1)
					dmax = ds_list_size(global.equip_list)
					dpos = clamp(dpos,0,dmax)
					}
				audio_play_sound(sfx_select,1,0)
				if(dpos = 0)
				and(equip_select = true)
					{
					item_menu = global.item_list
					dmax = ds_list_size(item_menu)
					equip_select = false
					exit
					}
				}
			if(dpos = 0)
			and(equip_select = false)
				{
				if(ds_list_size(global.equip_list) > 0)
					{
					equip_select = true
					item_menu = global.equip_list
					dmax = ds_list_size(item_menu)
					audio_play_sound(sfx_select,1,0)
					dpos = 1
					}
				else
					{
					audio_play_sound(sfx_fail,1,0)
					}
				}
			}
		}
	if(string_count("Tutorial",citem) > 0)
		{
		cmenu = 2
		cpos = 0
		audio_play_sound(sfx_select,1,0)
		}
	if(string_count("Library",citem) > 0)
		{
		cmenu = 1
		cpos = 0
		dpos = 0
		audio_play_sound(sfx_select,1,0)
		}
	if(string_count("Title",citem) > 0)
		{
		room_goto(rm_menu)
		instance_destroy()
		}
	if(string_count("Back",citem) > 0)
		{
		if(cmenu = 0)
		and(selecting = false)
			{
			if(alarm[0] = -1)alarm[0] = 2
			exit
			}
		else if(cmenu = 2)
			{
			cmenu = 1
			cpos = 0
			audio_play_sound(sfx_back,1,0)
			exit
			}
		else
			{
			cmenu = 0
			cpos = 0
			audio_play_sound(sfx_back,1,0)
			exit
			}
		}
	}
if(back_pressed())
	{
	if(show_scene > 0)
		{
		exit
		}
	if(selecting = false)
		{
		if(cmenu = 0)
		and(selecting = false)
			{
			if(alarm[0] = -1)alarm[0] = 2
			exit
			}
		else if(cmenu = 2)
			{
			cmenu = 1
			cpos = 0
			audio_play_sound(sfx_back,1,0)
			exit
			}
		else
			{
			cmenu = 0
			cpos = 0
			audio_play_sound(sfx_back,1,0)
			exit
			}
		}
	if(selecting = true)
		{
		if(equip_select = true)
			{
			dpos = 0
			equip_select = false
			item_menu = global.item_list
			dmax = ds_list_size(item_menu)
			}
		else
			{
			selecting = false
			}
		audio_play_sound(sfx_back,1,0)
		}
	}