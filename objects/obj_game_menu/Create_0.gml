blursurf = -1
menusurf = -1
rightsurf = -1
itemsurf = -1
enemysurf = -1

menuwidth = 1000
menuheight = 700
rightwidth = menuwidth*.7
rightheight = menuheight

menu_centerx = view_wport/2
menu_centery = view_hport/2

text_x = (menuwidth-rightwidth)/2
text_y = menuheight/2
text_pad = font_get_size(global.big_font)+32

menu[0,0] = "Status | 状態"
menu[0,1] = "Items | アイテム"
menu[0,2] = "Assist | 味方"
menu[0,3] = "Library | 図鑑"
menu[0,4] = "Title | 題名"
menu[0,5] = "Back | 戻る"

menu[1,0] = "Tutorial | チュートリアル"
menu[1,1] = "Enemies | 敵"
menu[1,2] = "Back | 戻る"

menu[2,0] = "Meters | メーター"
menu[2,1] = "Battle 1 | バトル1"
menu[2,2] = "Battle 2 | バトル2"
menu[2,3] = "Defense | 防衛"
menu[2,4] = "Passives | パッシブ"
menu[2,5] = "Extra | 追加"
menu[2,6] = "Back | 戻る"

cmenu = 0
cpos = 0

item_menu = global.item_list

selecting = false
equip_select = false

dpos = 0
dmax = 0

itemhor = 2

show_scene = 0
scene = -1
sound = -1
scene_scale = 8

with(obj_canfreeze)
	{
	freeze_char(-1)
	}