event_inherited()

//Setup Sprites
icon = spr_rougehead
stand_south = spr_red_south
stand_east = spr_red_east
stand_west = spr_red_west
stand_north = spr_red_north

walk_south = spr_red_south_walk
walk_east = spr_red_east_walk
walk_west = spr_red_west_walk
walk_north = spr_red_north_walk

battle[0] = spr_rouge_battle
battle[1] = spr_rouge_creampie
sound[0] = tm_rouge_battle
sound[1] = tm_rouge_creampie

assist[0] = spr_rougeassist
assist[1] = spr_rougeassistcum

//Main Variables
name = rouge_name
state = "Wander"


//Editable Variables
random_set_seed(global.seed)
walk_speed = 1.2
run_speed = 1.75
decision_timer = 80
wander_dist = 120
follow_time = 0
mxfollow_time = 120
follow_dist = 120
mybits = 10+floor(random(5))

//Game Variables
move_speed = 0
tmove_speed = 0
tdirection = direction

timer[0] = decision_timer+irandom(20)-irandom(20)

//Battle Variables
habit[0][0] = "L"
habit[0][1] = habit[0][0]
habit[0][2] = "R"
habit[0][3] = habit[0][2]

/*habit[1][0] = "U"
habit[1][1] = habit[1][0]
habit[1][2] = "D"
habit[1][3] = habit[1][2]*/
randomize()
//chab = floor(random(array_height_2d(habit)))
chab = 0
lhab = chab

//STATS
min_speed = array_length_2d(habit,0)

mxwillpower = 140
willpower = mxwillpower
mxtimeline = 0
cream = 4
free_answer = 2

battle_speed = array_length_2d(habit,0)+1
max_speed = 10
	
strength = 3
skill = 8
turn_time = 35

chab = 0
boss = 1

//Skills
enemy_action = ds_map_create()
enemy_text = ds_map_create()
enemy_mod = ds_map_create()
//ds_map_add(enemy_action,"Energetic | 元気",20+irandom(15))
ds_map_add(enemy_action,"Wait | 待つ",20+irandom(5))

//ds_map_add(enemy_text,"Energetic | 元気","She's getting excited.\nThe speed of battle increases by <col @c_yellow>2<col @c_white>. | 彼女は興奮している。\n戦闘速度が<col @c_yellow>2増加します。")
ds_map_add(enemy_text,"Wait | 待つ","She watches you as she moves her hips. | 彼女は腰を動かしながらあなたを見ている。")

//ds_map_add(enemy_mod,"Energetic | 元気",energetic_mod)
ds_map_add(enemy_mod,"Wait | 待つ",wait_mod)

start = ds_list_create()
ds_list_add(start,"I'm putting it in. | あぁ、熱いのが中に・・・。")
ds_list_add(start,"Don't cum too soon. | すぐに射精したら許さないわよ。")

win = ds_list_create()
ds_list_add(win,"Hahahaha. Lame. | ハハハッ！ もう出ちゃったの？")
ds_list_add(win,"<wave>Let's play some more.</wave> | <wave>もっと遊びましょうよ。</wave>")
ds_list_add(win,"<shake strong=2>Let it out! Let it out!</shake> \nI know you've got more in there! | <shake strong=2>ほら！ほら！ほら！</shake> \nもっと精子が出せるはずよ！")

loss = ds_list_create()
ds_list_add(loss,"<shake strong=1>No. I can't.</shake> | <shake strong=1>ウソッ！？</shake>")
ds_list_add(loss,"<shake strong=2>I won't lose to you!</shake> | <shake strong=1>私がイかされる！？</shake>")
ds_list_add(loss,"<shake strong=3>I'm cumming!</shake> | <shake strong=1>あぁああぁぁッ!!!!</shake>")