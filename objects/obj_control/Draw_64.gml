if(show_debug)
	{
	draw_set_alpha(1)
	draw_set_color(c_white)
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)
	draw_text(5,15,"FPS: "+string(fps))
	}
if(global.pause)
	{
	draw_set_alpha(.5)
	draw_set_color(c_black)
	draw_rectangle(0,0,view_wport,view_hport,0)
	draw_set_halign(fa_center)
	draw_set_valign(fa_center)
	draw_set_font(global.big_font)
	draw_set_color(c_white)
	draw_set_alpha(1)
	draw_text(view_wport/2,view_hport/2,string_split("Game Paused | 一時停止"))
	}
	
if(instance_exists(obj_player))
and(global.battle = false)
	{
	if(draw_gui = true)
		{
		draw_set_halign(fa_left)
		draw_set_valign(fa_top)
		draw_text_outline(5,15,
		"Press L to switch languages | Lを押して言語を切り替えます"+
		string_split("\nPress Z to Toggle Lights | \nZを押してライトを切り替えます")+
		string_split("\nPress D to show Debug information | \nDキーを押して、デバッグ情報を表示します")+
		string_split("\nPress R to restart the room | \nRキーを押して部屋を再起動します")+
		string_split("\nPress Q to hide the user interface | \nQを押してユーザーインターフェイスを非表示にします")+
		string_split("\nPress Delete to reset save data | \n削除を押して保存データをリセットします")+
		string_split("\nClick On The Screen To Move | \n移動するには画面をクリックしてください")+
		"\nX : "+string(obj_player.x)+"\nY: "+string(obj_player.y)+"\nMoveSpeed: "+string(obj_player.move_speed)+"\nKeyList: "+string(ds_list_size(obj_player.key_list))
		,c_white,c_black)
		}
	}

if(trans_alpha > 0)
	{
	trans_alpha -= .06
	draw_set_alpha(trans_alpha)
	if(draw_get_color() != c_black)draw_set_color(c_black)
	draw_rectangle(0,0,view_wport,view_hport,0)
	}