equip_check()
if(keyboard_check_pressed(vk_space))
and(keyboard_check(vk_control) or keyboard_check(vk_shift))
	{
	global.trans_pos = 1
	room_restart()
	}


if(back_pressed())
and(keyboard_check(vk_control) or global.gameover == true)
	{
	global.gameover = false
	global.willpower = global.mxwillpower
	global.stamina = global.mxstamina
	global.cum = 0
	room_goto(rm_menu)
	}

if(keyboard_check_pressed(ord("T")))
and(keyboard_check(vk_control) or global.gameover == true)
	{
	global.gameover = false
	global.willpower = global.mxwillpower
	global.stamina = global.mxstamina
	global.cum = 0
	room_goto(rm_menu)
	}
	
if(global.cancontrol)
or(keyboard_check(vk_shift))
	{
	if(keyboard_check_pressed(ord("P")))
		{
		screen_save(get_save_filename("PNG|*.png;","Screen"))
		}
	if(keyboard_check_pressed(ord("Q")))
		{
		draw_gui = !draw_gui
		ini_open("settings.ini")
		ini_write_real("Settings","DrawGui",draw_gui)
		ini_close()
		}
	if(keyboard_check_pressed(ord("D")))
		{
		show_debug = !show_debug
		ini_open("settings.ini")
		ini_write_real("Settings","Debug",show_debug)
		ini_close()
		show_debug_overlay(show_debug)
		}
	if(keyboard_check_pressed(ord("Z")))
		{
		global.light_visible = !global.light_visible
		ini_open("settings.ini")
		ini_write_real("Settings","Light",global.light_visible)
		ini_close()
		}
	}
var canfreeze;
canfreeze = true
with(obj_canfreeze){if(frozen = true)canfreeze = false}
if(back_pressed())
or(keyboard_check_pressed(vk_escape))
	{
	if(! instance_exists(obj_game_menu))
		{
		if(keyboard_check_pressed(vk_escape))
			{
			if(room != rm_menu){
				room_goto(rm_menu)
				exit
			}
			else{game_end()}
			}
		}
	if(canfreeze = true)
	and(! instance_exists(obj_main_menu))
	and(! instance_exists(obj_battle))
		{
		if(! instance_exists(obj_game_menu))
			{
			with(obj_canfreeze){freeze_char(-1)}
			instance_create_depth(0,0,room_height,obj_game_menu)
			audio_play_sound(sfx_select,1,0)
			}
		}
	}
if(instance_number(obj_hero) > 0)
	{
	if(global.light_visible == false)
		{
		instance_deactivate_object(FG_light_control)
		}
	else
		{
		instance_activate_object(FG_light_control)
		}
	}
if(language_pressed())
	{
	if(instance_exists(obj_main_menu))
	and(obj_main_menu.control_edit = true){}
	else
		{
		global.language = !global.language
		ini_open("settings.ini")
		ini_write_real("Settings","Language",global.language)
		ini_close()
		}
	}
if(keyboard_check_pressed(vk_delete))
	{
	file_delete("settings.ini")
	file_delete(global.saveloc)
	file_delete(global.fsaveloc)
	file_delete("save.png")
	}
if(keyboard_check(vk_alt))
and(keyboard_check_pressed(vk_enter))
	{
	global.fullscreen = !global.fullscreen
	ini_open("settings.ini")
	ini_write_real("Settings","Fullscreen",global.fullscreen)
	ini_close()
	
	if(can_fullscreen)
		{
		window_set_fullscreen(global.fullscreen)
		can_fullscreen = 0
		alarm[0] = FULL_TIME
		}
	exit
	}
