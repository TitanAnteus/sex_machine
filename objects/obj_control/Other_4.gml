
ini_open("settings.ini")
draw_gui = ini_read_real("Settings","DrawGui",false)
show_debug = ini_read_real("Settings","Debug",false)
global.language = ini_read_real("Settings","Language",global.os)
global.light_visible = ini_read_real("Settings","Light",true)
global.fullscreen = ini_read_real("Settings","Fullscreen",false)
ini_close()

global.light_visible = true
if(window_get_fullscreen() != global.fullscreen)window_set_fullscreen(global.fullscreen)

show_debug_overlay(show_debug)

if(! instance_exists(obj_camera))
and(instance_exists(obj_player))
	{
	instance_create_layer(obj_player.x,obj_player.y,"Characters",obj_camera)
	}
	
var ly;
ly = layer_get_id("Background")
if(layer_exists(ly))layer_depth(ly,room_height+1000)
ly = layer_get_id("Foreground")
if(layer_exists(ly))layer_depth(ly,-room_height-1000)
if(layer_exists("Foreground2"))
	{
	ly = layer_get_id("Foreground2")
	if(layer_exists(ly))layer_depth(ly,-room_height-990)
	}
	
play_music()
if(instance_number(obj_hero) > 0)instance_create_layer(0,0,"Light",FG_light_control)
if(instance_number(obj_hero) > 0)instance_create_layer(0,0,"Light",obj_global_light)

var ly,el;
ly = layer_get_id("Tree")
if(layer_exists(ly))
	{
	el = layer_get_all_elements(ly)
	new_layers = array_create(array_length(el))
	for(i=0;i<array_length(el);i+=1)
		{
		var tm,ty,th;
		tm = el[i]
		ty = tilemap_get_y(tm)
		th = tilemap_get_height(tm)
		
		new_layers[i] = layer_create(-(ty+th),i)
		layer_element_move(tm,new_layers[i])
		}
	}
	
if(room != rm_menu)
	{
	trans_alpha = 1
	}