//Setup Controls
ATEX_init()
FG_light_init()
initialize_states()
init_data()
initialize_skills()
can_fullscreen = 1
new_layers[0] = -1

global.saveloc = temp_directory+"save.ini"
global.fsaveloc = working_directory+"save\\save.ini"

trans_alpha = 0
global.trans_pos = 0
global.trans_dir = 0

global.bits = 0

global.enemy_list = ds_list_create()
global.assist_list = ds_list_create()
global.assist_cooldown = ds_map_create()
global.assist_battle = ds_list_create()

ini_open("settings.ini")
global.k_left = ini_read_real("Controls","Left",vk_left)
global.k_right = ini_read_real("Controls","Right",vk_right)
global.k_up = ini_read_real("Controls","Up",vk_up)
global.k_down = ini_read_real("Controls","Down",vk_down)
global.k_select = ini_read_real("Controls","Select",ord("Z"))
global.k_back = ini_read_real("Controls","Back",ord("X"))
global.k_skip = ini_read_real("Controls","Skip",vk_control)
global.k_language = ini_read_real("Controls","Language",ord("L"))
ini_close()

//Setup Main Variables
global.grid = 16
global.port_width = 1280
global.port_height = 720
global.lastbgm = -1
global.bgm = -1
global.cancontrol = false

audio_group_load(audiogroup_music)

gpu_set_tex_mip_filter(tf_point)
display_set_gui_size(global.port_width,global.port_height)

//global.bright = false
global.pause = false

global.main_font = -1
global.big_font = -1

global.tspd = .6
if(global.main_font = -1)
	{
	global.main_font = font_add("PixelMPlus.ttf",18,false,false,32,128)
	global.big_font = font_add("PixelMPlus.ttf",34,false,false,32,128)
	draw_set_font(global.main_font)
	}


//global.mxspeed = 10
global.seed = current_second
global.mxwait = 4

#macro ENGLISH 0
#macro JAPANESE 1
#macro EASY 0
#macro NORMAL 1
#macro c_pink make_color_rgb(255,105,179)
#macro FULL_TIME 15
#macro LAST -1000
#macro CURRENT -1000
#macro PLAYER -2000

global.pink = c_pink

if(os_get_language() != "ja"){global.os = ENGLISH}
else{global.os = JAPANESE}

randomize()

global.difficulty = NORMAL

ini_open("settings.ini")
global.bgm_gain = (ini_read_real("Settings","BGM",70)/100)
global.sfx_gain = (ini_read_real("Settings","SFX",100)/100)
ini_close()
audio_group_set_gain(audiogroup_music,global.bgm_gain,0)
audio_group_set_gain(audiogroup_default,global.sfx_gain,0)

global.item_list = ds_list_create()
global.equip_list = ds_list_create()
global.equipment = ""

global.battle = false

//Setup Character Variables
global.gameover = false
global.mxwillpowerbase = 150
global.mxwillpower = global.mxwillpowerbase

global.willpower = global.mxwillpower

global.mxstaminabase = 100
global.mxstamina = global.mxstaminabase
global.stamina = global.mxstamina
global.stamina_regenbase = 4
global.stamina_regen = global.stamina_regenbase

global.exhaustbase = global.mxstamina/3
global.exhaust = global.exhaustbase

global.mxcumbase = 100
global.mxcum = global.mxcumbase
global.cum = 0

global.strengthbase = 4
global.strength = global.strengthbase

global.restcost = 5
global.mxitems = 8