if(active = false)exit
if(surface_exists(darksurf))
	{
	surface_set_target(darksurf)
	draw_set_alpha(dark_alph)
	draw_set_color(c_black)
	draw_rectangle(0,0,view_wport,view_hport,0)
	draw_set_alpha(1)
	
	focus += clamp((0-focus)/20,-4,0)
	
	if(cdialogue = start_text and ctext >= 3)
	or(cdialogue = second_text and ctext != 7)
	or(cdialogue = third_text and ctext != 1)
	or(cdialogue = fourth_text)
		{
		gpu_set_blendmode(bm_subtract)
		draw_set_color(c_black)
		draw_roundrect(obj_battle.command_x-1-focus,obj_battle.command_y-1-focus,obj_battle.command_x+obj_battle.command_width+1+focus,
		obj_battle.command_y+obj_battle.command_height+1+focus,0)
		gpu_set_blendmode(bm_normal)
		}
	if(cdialogue = first_text)
		{
		gpu_set_blendmode(bm_subtract)
		draw_set_color(c_black)
		if(ctext < 5)draw_rectangle(obj_battle.meter_x-1-focus,obj_battle.meter_y-1-focus,obj_battle.meter_x+obj_battle.meter_width+1+focus,
		obj_battle.meter_y+(obj_battle.meter_height*3)+1+focus,0)
		if(ctext = 5)
			{
			draw_rectangle(view_wport,0,view_wport-obj_battle.emwidth-4-focus,obj_battle.emheight+4+focus,0)
			}
		gpu_set_blendmode(bm_normal)
		}
	if(cdialogue = second_text)
		{
		if(ctext == 7)
			{
			gpu_set_blendmode(bm_subtract)
			draw_set_color(c_black)
			draw_rectangle(obj_battle.line_x-focus,obj_battle.command_y+obj_battle.command_height-obj_battle.text_height-2-focus
			,obj_battle.line_x+string_width(string(obj_battle.action_timer))+8+focus,obj_battle.command_y+obj_battle.command_height+focus,0)
			gpu_set_blendmode(bm_normal)
			}
		if(ctext == 2)
			{
			/*
			var lx,ly,lw;
			lx = obj_battle.line_x +8
			ly = obj_battle.command_y+1
			lw = 18*array_height_2d(obj_battle.cenemy.habit)
			gpu_set_blendmode(bm_subtract)
			draw_set_color(c_black)
			draw_rectangle(lx-focus,ly-focus
			,lx+lw+focus,ly+25+focus,0)
			gpu_set_blendmode(bm_normal)*/
			}
		}
	if(cdialogue = third_text)
		{
		if(ctext == 1)
			{
			var lx,ly,lw;
			lx = obj_battle.line_x +8
			ly = obj_battle.command_y+1
			lw = 18*array_height_2d(obj_battle.cenemy.habit)
			gpu_set_blendmode(bm_subtract)
			draw_set_color(c_black)
			draw_rectangle(lx-focus,ly-focus
			,lx+lw+focus,ly+25+focus,0)
			gpu_set_blendmode(bm_normal)
			}
		}
		
		
	surface_reset_target()
	}
else
	{
	darksurf = surface_create(view_wport[0],view_hport[0])
	draw_set_alpha(dark_alph)
	draw_set_color(c_black)
	draw_rectangle(0,0,view_wport,view_hport,0)
	}
draw_surface(darksurf,0,0)
draw_set_color(c_black)
draw_set_alpha(.8)


bx1 += (box_x1-bx1)/spd
bx2 += (box_x2-bx2)/spd
by1 += (box_y1-by1)/spd
by2 += (box_y2-by2)/spd
if(abs(bx1-box_x1) < .2)
and(box_open = false)
	{
	bx1 = box_x1
	bx2 = box_x2
	by1 = box_y1
	by2 = box_y2
	box_open = true
	}
draw_roundrect(bx1,by1,bx2,by2,0)
draw_set_color(c_white)
draw_set_alpha(1)
draw_roundrect(bx1,by1,bx2,by2,1)

if(box_open = true)
	{
	draw_set_font(global.main_font)
	if(tpos <= string_length(string_split(dtext)))
		{
		tpos += tspd
		tpos = skip_color(string_split(dtext),tpos)
		draw_continue = false
		}
	if(tpos >= string_length(string_split(dtext)))
		{
		draw_continue = true
		}
	var ww;
	draw_set_halign(fa_center)
	draw_set_valign(fa_center)
	ww = (box_width)-(padding*2)
	tpos = clamp(tpos,0,string_length(string_split(dtext)))
	draw_text_width(center_x,center_y,string_copy(string_split(dtext),0,tpos),ww,text_height)
	
	if(draw_continue = true)
		{
		var symb;
		symb = keyname(global.k_select)
		draw_set_halign(fa_right)
		draw_set_valign(fa_bottom)
		if(draw_get_color() != c_white)draw_set_color(c_white)
		draw_text(box_x2-padding,box_y2-padding-text_bob,symb)
		}
	if(text_bdir == true)
		{
		text_bob += text_bspd
		if(text_bob > mx_text_bob)text_bdir = !text_bdir
		}
	else
		{
		text_bob -= text_bspd
		if(text_bob < 0)text_bdir = !text_bdir
		}
	}