active = true

center_x = view_wport/4
center_y = view_hport/2

box_open = false

box_width = 400
box_height = 300

box_x1 = center_x-(box_width/2)
box_x2 = center_x+(box_width/2)
box_y1 = center_y-(box_height/2)
box_y2 = center_y+(box_height/2)

bx1 = center_x
bx2 = center_x
by1 = center_y
by2 = center_y
text_padding = 8
text_height = font_get_size(global.main_font)+text_padding
padding = 8

spd = 8

pink_col = c_pink

start_text = ds_list_create()
ds_list_add(start_text,"Welcome to the tutorial for Sex Machine.\n\nAll information in these tutorials can be reviewed in the Library. | Sex Machineのチュートリアルへようこそ。\n\nこれらのチュートリアルのすべての情報は、ライブラリで確認できます。")
//ds_list_add(start_text,"This is a Reverse Rape, BattleFuck RPG. | これは逆レイプによるバトルファックRPGです。")
//ds_list_add(start_text,"All information in these tutorials can be reviewed in the Library. | これらのチュートリアルのすべての情報は、ライブラリで確認できます。 ")
//ds_list_add(start_text,"Before combat starts, the enemy will say something. | 戦闘が始まる前に敵は何かを言います。")

first_text = ds_list_create()
ds_list_add(first_text,"While doing battle there are 3 meters you need to keep in mind. | 戦闘中は3つのゲージの長さに注意する必要があります。")
ds_list_add(first_text,"<col @c_red>Willpower - Red\n<col @c_lime>Stamina - Green\n<col @c_white>Lust - White | <col @c_red>意志力-赤\n<col @c_lime>スタミナ-緑\n<col @c_white>欲望-白")
ds_list_add(first_text,"Willpower is how willing you are to continue resisting. \n\n<col @c_red>If it reaches 0 you will lose. | 意志力は、戦い続ける意欲を表します。 \n\n<col @c_red>0になると負けになります。")
//ds_list_add(first_text,"Stamina is how energetic you are. You can expend it to use skills. \n\n<col @c_red>YOU WILL BE EXHAUSTED IF YOUR STAMINA IS TOO LOW. WHILE EXHAUSTED YOU TAKE MORE DAMAGE. | スタミナは元気です。 スキルを使う際に消費します。 \n\n<col @c_red>スタミナが低すぎるとあなたは疲れきってしまいスキルを使用できません。")
//ds_list_add(first_text,"Lust is how close you are to cumming. \n\n<col @c_red>Cumming does good damage to your Willpower, ignores all defenses, and heals the enemy. | 欲望はあなたが射精にどれだけ近いかを表します。 \n\n<col @c_red>射精はすべての防御を無視してあなたの意志力に大きなダメージを与え、敵を回復させます。")
//ds_list_add(first_text,"In order to win you must <col @c_red>lower the enemy's Willpower to 0. | 勝つためには<col @c_red>敵の意志力を0にする必要があります。")
//ds_list_add(first_text,"Battles in Sex Machine are a bit different. <col @c_lime>For now, press Prepare. | Sex Machineでの戦闘は少し特殊です。 <col @c_lime>まずは備えるを押します。")

second_text = ds_list_create()
ds_list_add(second_text,"This is the battle screen. | これがバトル画面です。")
ds_list_add(second_text,"Here you must use the arrow keys, to match the enemy's arrows. | ここでは、敵の矢印に合わせて矢印キーを使用する必要があります。")
ds_list_add(second_text,"White diamonds show the enemy's habits. | 白いひし形は敵の習慣を示しています。")
//ds_list_add(second_text,"The white diamonds on top are where the habits show.\nThe <col @pink_col>pink <col @c_white>diamonds are completely random. | 特定のパターンがあるのはホワイトダイヤモンドです。<col @pink_col>ピンク<col @c_white>ダイヤモンドは完全にランダムです。")
//ds_list_add(second_text,"The diamonds on the bottom are your actions. Some might be filled in. These are free arrows given to you by Igrec's own assessment of the situation. | 下部のひし形はあなたの行動です。 いくつかは記入されるかもしれません。これらはIgrec自身の状況の評価によってあなたに与えられた無料の矢印です。 ")
//ds_list_add(second_text,"The faster the battle, the more diamonds there are. Each attack does slightly less damage the more diamonds there are, to compensate. | 戦闘が速度が速いほどより多くのダイヤモンドが出現します。 各攻撃は、補償するためにダイヤモンドが多いほどわずかに少ないダメージを与えます。")
//ds_list_add(second_text,"If you match an arrow, <col @c_lime>you block all damage to your Willpower, <col @c_white>but your Lust will still increase. | 矢印を合わせると意志力へのダメージをすべてブロックしますが、欲望は増加します。")
//ds_list_add(second_text,"Keep in mind. \nYou must make an action before time runs out. | そして注意してください。 \n制限時間以内にアクションを実行する必要があります。")

third_text = ds_list_create()
//ds_list_add(third_text,"You can hover and look at the pattern you just completed for as long as you want. | 完了したばかりのパターンにカーソルを合わせて、好きなだけ見ることができます。")
//ds_list_add(third_text,"When the pattern is back at this square, the white arrows will be the same. | パターンがこの正方形に戻ると、白い矢印は同じになります。")
ds_list_add(third_text,"<col @pink_col>Pink<col @c_white> arrows are always random.\nThere are skills you can use to deal with that. | <col @pink_col>ピンク<col @c_white>の矢印は常にランダムですが.\nそれに対処するために使用できるスキルがあります。")

fourth_text = ds_list_create()
ds_list_add(fourth_text,"These are the skills you can use. They all cost <col @c_lime>Stamina<col @c_white> to use. | これらはあなたが使用できるスキルです。 それらはすべて<col @c_lime>スタミナ<col @c_white>の費用がかかります。")
ds_list_add(fourth_text,"<col @c_lime>Stamina regenerates<col @c_white> for every arrow you guess. \n\nYou get more for correct arrows. | <col @c_lime>スタミナは<col @c_white>、あなたが推測する正しい矢印ごとに再生します。 \n\nあなたは正しい矢のためにより多くを得る。")
//ds_list_add(fourth_text,"Don't hesitate to use your skills! \n\nStamina replenishes as you resist the enemy! | あなたのスキルを使うことを躊躇しないでください！ \n\n敵に抵抗するとスタミナが補充されます！ ")
//ds_list_add(fourth_text,"Good luck in your battles.\nYou can review this information in the Library. | あなたの戦いに幸運を。\n図書館でこの情報を確認できます。")

cdialogue = start_text
ctext = 0
dtext = cdialogue[| ctext]
tpos = 0
tspd = global.tspd
draw_continue = false

text_bob = 0
text_bdir = 1
text_bspd = .2
mx_text_bob = 5

darksurf = -1
dark_alph = .9
focus = 10

mxwait = global.mxwait
wait = mxwait