cmenu = 0
cpos = 0
menu[0,0] = "New Game | 開始"
menu[0,1] = "Continue | 続ける"
menu[0,2] = "Gallery | ギャラリー"
menu[0,3] = "Settings | 設定"
menu[0,4] = "Exit | 終わり"

menu[1,0] = "Normal | 正常"
menu[1,1] = "Easy | かんたん"
menu[1,2] = "Back | バック"

menu[2,0] = "BGM Volume | BGMボリューム"
menu[2,1] = "SFX Volume | SFXボリューム"
menu[2,2] = "Controls | コントロール"
menu[2,3] = "Fullscreen: {full} | 全画面表示: {full}"
menu[2,4] = "Language: {lang} | 言語: {lang}"
menu[2,5] = "Default | 既定"
menu[2,6] = "Back | バック"

menu[3,0] = "Start Edit | 編集を開始"
menu[3,1] = "Confirm | 確認"
menu[3,2] = "Back | バック"

menu[4,0] = ""

keylist[0] = "Left"
keylist[1] = "Right"
keylist[2] = "Up"
keylist[3] = "Down"
keylist[4] = "Select"
keylist[5] = "Back"
keylist[6] = "Skip"
keylist[7] = "Change Lang"

con_pos = 0

bgm_edit = false
sfx_edit = false
control_edit = false

bwidth = 200
bheight = 20

change = .02
change_l = .1
toload = false