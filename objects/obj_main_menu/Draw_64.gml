draw_set_halign(fa_center)
draw_set_valign(fa_center)
if(draw_get_color() != c_white)draw_set_color(c_white)
if(draw_get_alpha() != 1)draw_set_alpha(1)

var mlength,cx,cy,pad,theight,col;
mlength = array_length_2d(menu,cmenu)
pad = 3
theight = font_get_size(global.big_font)+pad
cx = view_wport/2
cy = ((view_hport*.6)-((mlength*(theight))/2))+80
col = c_white
if(draw_get_color() != c_white)draw_set_color(c_white)
draw_set_font(global.big_font)
//if(cmenu != 4)draw_text_outline(cx,100,"Sex Machine",c_white,c_black)
draw_set_font(global.main_font)
if(cmenu != 3)
	{
	for(i=0;i<mlength;i+=1)
		{
		var lx,ly,str,tr;
		lx = cx-(bwidth/2)
		ly = (cy+(i*theight))-(bheight/2)
		tr = "True"
		if(window_get_fullscreen() == false)tr = "False"
		str = string_replace(menu[cmenu,i],"{full}",tr)
		str = string_replace(str,"{full}",tr)
		
		tr = "English"
		if(global.language == JAPANESE)tr = "日本語"
		str = string_replace(str,"{lang}",tr)
		str = string_replace(str,"{lang}",tr)
		
		if(i = cpos)col = c_red
		else{col = c_white}
		if(bgm_edit = true and i = 0)
			{
			draw_rectangle(lx,ly,lx+bwidth,ly+bheight,1)
			draw_rectangle(lx,ly,lx+(bwidth*global.bgm_gain),ly+bheight,0)
			}
		else if(sfx_edit = true and i =1)
			{
			draw_rectangle(lx,ly,lx+bwidth,ly+bheight,1)
			draw_rectangle(lx,ly,lx+(bwidth*global.sfx_gain),ly+bheight,0)
			}
		else
			{
			draw_text_outline(cx,cy+(i*theight),string_split(str),col,c_black)
			}
		}
	}
else
	{
	mlength = array_length_1d(keylist)
	cy = (view_hport*.6)-((mlength*(theight))/2)
	var col;
	col = c_red
	if(control_edit = 1)col = c_yellow
	if(cpos = 0)draw_text_outline(cx,cy-42,string_split(menu[cmenu,0]),col,c_black)
	else{draw_text_outline(cx,cy-42,string_split(menu[cmenu,0]),c_white,c_black)}
	
	if(cpos = 1)draw_text_outline(cx,cy+((mlength+1)*theight),string_split(menu[cmenu,1]),c_red,c_black)
	else{draw_text_outline(cx,cy+((mlength+1)*theight),string_split(menu[cmenu,1]),c_white,c_black)}
	if(cpos = 2)draw_text_outline(cx,(cy+((mlength+1)*theight))+theight,string_split(menu[cmenu,2]),c_red,c_black)
	else{draw_text_outline(cx,(cy+((mlength+1)*theight))+theight,string_split(menu[cmenu,2]),c_white,c_black)}
	
	draw_set_halign(fa_left)
	cwidth = 350
	cheight = mlength*theight
	var lx,ly;
	lx = (view_wport/2)-(cwidth)/2
	ly = cy-24
	draw_set_color(c_dkgray)
	draw_set_alpha(.8)
	draw_rectangle(lx,ly,lx+cwidth,ly+cheight+20,0)
	draw_set_color(c_white)
	draw_set_alpha(1)
	draw_line(view_wport/2,cy+5,view_wport/2,ly+cheight-5)
	draw_rectangle(lx-1,ly-1,lx+cwidth,ly+cheight+20,1)
	
	for(i=0;i<mlength;i+=1)
		{
		col = c_white
		if(control_edit = true and i = con_pos)col = c_red
		draw_set_color(col)
		
		draw_set_halign(fa_left)
		draw_text(lx+10,cy+(i*theight),keylist[i])
		draw_text_outline(lx+cwidth+10,cy+(i*theight),keyname(nk[i]),col,c_black)
		draw_set_halign(fa_right)
		draw_text(lx+cwidth-10,cy+(i*theight),keyname(tk[i]))
		}
	}
draw_set_halign(fa_left)
draw_set_valign(fa_bottom)
draw_set_font(global.main_font)
draw_set_color(c_white)

Lname = keyname(global.k_language)
if(cmenu != 4)draw_text_width(5,view_hport-30,"<return @Lname>を押して言語を交換します | Press <return @Lname> to Swap Languages",room_width,-1)