if(control_edit = true)
	{
	if(keyboard_check_pressed(vk_anykey))
		{
		var can_change;
		can_change = true
		for(i=0;i<con_pos;i+=1)
			{
			if(tk[i] = keyboard_lastkey)
				{
				can_change = false
				audio_play_sound(sfx_fail,1,0)
				break
				}
			}
		if(can_change)
			{
			tk[con_pos] = keyboard_lastkey
			con_pos += 1
			audio_play_sound(sfx_select,1,0)
			}
		if(con_pos = array_length(tk))
			{
			cpos = 1
			control_edit = false
			}
		exit
		}
	}

if(bgm_edit = false)
and(sfx_edit = false)
and(control_edit = false)
and(cmenu != 4)
	{
	if(left_pressed())
	or(up_pressed())
		{
		cpos -= 1
		if(cpos < 0)cpos = array_length_2d(menu,cmenu)-1
		audio_play_sound(sfx_menumove,1,0)
		}
	if(right_pressed())
	or(down_pressed())
		{
		cpos += 1
		audio_play_sound(sfx_menumove,1,0)
		}
	}
else
	{
	if(bgm_edit)
		{
		if(left_pressed())global.bgm_gain -= change
		if(down_pressed())global.bgm_gain -= change_l
		if(right_pressed())global.bgm_gain += change
		if(up_pressed())global.bgm_gain += change_l
		global.bgm_gain = clamp(global.bgm_gain,0,1)
		audio_group_set_gain(audiogroup_music,global.bgm_gain,0)
		
		ini_open("settings.ini")
		ini_write_real("Settings","BGM",round(global.bgm_gain*100))
		ini_close()
		}
	if(sfx_edit)
		{
		if(left_pressed())global.sfx_gain -= change
		if(down_pressed())global.sfx_gain -= change_l
		if(right_pressed())global.sfx_gain += change
		if(up_pressed())global.sfx_gain += change_l
		global.sfx_gain = clamp(global.sfx_gain,0,1)
		audio_group_set_gain(audiogroup_default,global.sfx_gain,0)
		ini_open("settings.ini")
		ini_write_real("Settings","SFX",round(global.sfx_gain*100))
		ini_close()
		}
	}
if(back_pressed())
	{
	if(cmenu = 1)
		{
		cmenu = 0
		cpos = 0
		audio_play_sound(sfx_back,1,0)
		}
	if(cmenu = 2)
		{
		if(bgm_edit = true)
		or(sfx_edit = true)
			{
			bgm_edit = false
			sfx_edit = false
			audio_play_sound(sfx_back,1,0)
			}
		else
			{
			cmenu = 0
			cpos = 0
			audio_play_sound(sfx_back,1,0)
			}
		}
	if(cmenu = 3)
		{
		if(control_edit = false)
			{
			cmenu = 2
			cpos = 0
			audio_play_sound(sfx_back,1,0)
			}
		}
	}
if(select_pressed())
	{
	var snd,ci;
	snd = sfx_select
	ci = menu[cmenu,cpos]
	if(cmenu = 0)
		{
		if(string_count("New Game",ci) > 0)
			{
			cmenu = 1
			audio_play_sound(snd,1,0)
			exit
			}
		if(string_count("Continue",ci) > 0)
			{
			if(load_game() = false)
				{
				cmenu = 1
				cpos = 0
				audio_play_sound(snd,1,0)
				}
			exit
			}
		if(string_count("Settings",ci) > 0)
			{
			cmenu = 2
			cpos = 0
			audio_play_sound(snd,1,0)
			exit
			}
		if(string_count("Exit",ci) > 0)
			{
			game_end()
			}
		if(string_count("Gallery",ci) > 0)
			{
			cmenu = 4
			cpos = 0
			audio_play_sound(snd,1,0)
			instance_create_depth(0,0,0,obj_gallery)
			exit
			}
		}
	if(cmenu = 1)
		{
		if(string_count("Normal",ci) > 0)
			{
			new_game(NORMAL)
			}
		if(string_count("Easy",ci) > 0)
			{
			new_game(EASY)
			}
		if(string_count("Back",ci) > 0)
			{
			cmenu = 0
			cpos = 0
			snd = sfx_back
			}
		}
	if(cmenu = 2)
		{
		if(string_count("BGM",ci) > 0)
			{
			bgm_edit = !bgm_edit
			if(bgm_edit = false)snd = sfx_back
			}
		if(string_count("SFX",ci) > 0)
			{
			sfx_edit = !sfx_edit
			if(sfx_edit = false)snd = sfx_back
			}
		if(string_count("Control",ci) > 0)
			{
			cmenu = 3
			cpos = 0
			snd = sfx_select
			audio_play_sound(snd,1,0)
			exit
			}
		if(string_count("Fullscreen",ci) > 0)
		and(can_fullscreen = 1)
			{
			can_fullscreen = 0
			alarm[0] = FULL_TIME
			global.fullscreen = !global.fullscreen
			
			window_set_fullscreen(global.fullscreen)
			ini_open("settings.ini")
			ini_write_real("Settings","Fullscreen",global.fullscreen)
			ini_close()
			}
		if(string_count("Language",ci) > 0)
			{
			global.language = !global.language
			
			window_set_fullscreen(global.fullscreen)
			ini_open("settings.ini")
			ini_write_real("Settings","Language",global.language)
			ini_close()
			}
		if(string_count("Default",ci) > 0)
			{
			global.bgm_gain = .7
			global.sfx_gain = 1
			global.fullscreen = false
			global.language = global.os
			
			audio_group_set_gain(audiogroup_music,global.bgm_gain,0)
			audio_group_set_gain(audiogroup_default,global.sfx_gain,0)
			window_set_fullscreen(global.fullscreen)
			default_controls()
			temp_key_init()
			ini_open("settings.ini")
			ini_write_real("Settings","Fullscreen",global.fullscreen)
			ini_write_real("Settings","BGM",round(global.bgm_gain*100))
			ini_write_real("Settings","SFX",round(global.sfx_gain*100))
			ini_write_real("Settings","Language",global.language)
			ini_close()
			}
		if(string_count("Back",ci) > 0)
			{
			cmenu = 0
			cpos = 0
			snd = sfx_back
			}
		}
	if(cmenu = 3)
		{
		if(string_count("Start Edit",ci) > 0)
			{
			if(control_edit = false)
				{
				control_edit = true
				con_pos = 0
				temp_key_init()
				snd = sfx_select
				}
			}
		if(string_count("Confirm",ci) > 0)
			{
			control_edit = false
			temp_key_write()
			temp_key_init()
			exit
			}
		if(string_count("Back",ci) > 0)
		and(control_edit = false)
			{
			cmenu = 2
			cpos = 0
			snd = sfx_back
			}
		}
	if(cmenu != 4)audio_play_sound(snd,1,0)
	}
cpos = cpos mod array_length_2d(menu,cmenu)