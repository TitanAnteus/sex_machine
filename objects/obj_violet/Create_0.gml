event_inherited()

//Setup Sprites
icon = spr_violethead
stand_south = spr_violet_south
stand_east = spr_violet_east
stand_west = spr_violet_west
stand_north = spr_violet_north

walk_south = spr_violet_south_walk
walk_east = spr_violet_east_walk
walk_west = spr_violet_west_walk
walk_north = spr_violet_north_walk

battle[0] = spr_violet_battle
battle[1] = spr_violet_creampie
sound[0] = tm_violet_battle
sound[1] = tm_violet_creampie

assist[0] = spr_vertassist
assist[1] = spr_vertassistcum

//Main Variables
name = violet_name
state = "Wander"

//Editable Variables
random_set_seed(global.seed)
walk_speed = 1.05
run_speed = 1.6
decision_timer = 60
wander_dist = 240
follow_time = 0
mxfollow_time = 80
follow_dist = 90

//Game Variables
move_speed = 0
tmove_speed = 0
tdirection = direction

//Battle Variables
habit[0][0] = choose("L","R")
habit[0][1] = opposite_dir(habit[0][0])
habit[0][2] = choose("R","L")
habit[0][3] = opposite_dir(habit[0][2])

habit[1][0] = opposite_dir(habit[0][0])
habit[1][1] = opposite_dir(habit[0][1])
habit[1][2] = opposite_dir(habit[0][2])
habit[1][3] = opposite_dir(habit[0][3])

habit[2][0] = side_dir(habit[0][0])
habit[2][1] = side_dir(habit[0][1])
habit[2][2] = side_dir(habit[0][2])
habit[2][3] = side_dir(habit[0][3])

mybits = 12+floor(random(3))
timer[0] = decision_timer+irandom(20)-irandom(20)
randomize()
//chab = floor(random(array_height_2d(habit)))
chab = 0
lhab = chab
//STATS
min_speed = array_length_2d(habit,0)
if(global.difficulty = EASY)
	{
	mxwillpower = 100+irandom(10)
	willpower = mxwillpower
	mxtimeline = 0
	cream = 5
	free_answer = 1

	battle_speed = array_length_2d(habit,0)+choose(0,2)
	max_speed = 8
	min_speed = array_length_2d(habit,0)
	strength = 3
	skill = 9
	turn_time = 30
	}
else
	{
	mxwillpower = 120+irandom(10)
	//mxwillpower = 1
	willpower = mxwillpower
	mxtimeline = 0
	cream = 5
	free_answer = 0

	battle_speed = array_length_2d(habit,0)+choose(0,1,2,3)
	max_speed = 10
	strength = 3
	skill = 11
	turn_time = 25
	}

//Skills
enemy_action = ds_map_create()
enemy_text = ds_map_create()
enemy_mod = ds_map_create()

add_skill("Wait | 待つ",5+irandom(5),
"She's obsessed with kissing. | 彼女はイグレックの足をしっかり握りながら腰を動かしている。",
wait_mod)
add_skill("Heavy | 重い",5+irandom(5),
"She showers you with single-minded affection without caring about your circumstance!\nFor <col @c_yellow>4<col @c_white> turns, her power is increased, and stamina regen is greatly reduced. | 彼女はあなたの状況を気にせずにひたむきな愛情であなたにシャワーを浴びます！\n<col @c_yellow>4<col @c_white>ターンの間、彼女のパワーは増加し、スタミナ再生は大幅に減少します。",
heavy_mod)

add_skill("Lewd Kiss | ベロチュー",5+irandom(5),
"Her tongue pries it's way into Igrec's mouth as she steals even his breath!\nIgrec can't think properly!\nShe uses the opportunity to move faster! | 彼女が彼の息さえも盗むとき、彼女の舌はそれがイグレックの口に入る方法をこじ開けます！\nIgrecは正しく考えることができません！\n彼女はより速く動く機会を利用しています！",
lewd_kiss_mod)

add_skill("Stamina Pit | スタミナピット",5+irandom(25),
"Reduces Igrec's stamina at the start of the turn for <color @c_yellow>3<color @c_white> turns. | <color @c_yellow>3<color @c_white>ターンの間、ターンの開始時にプレイヤーのスタミナを減らす",
stamina_pit_mod)

add_skill("Concern | 懸念",5+irandom(40),
"She moves in a way that brings the greatest pleasure to Igrec! \nIgrec can see all of her arrows, but the pressure gives him little time to act! | 彼女はイグレックに最大の喜びをもたらす方法で動きます！\nイグレックは彼女の矢をすべて見ることができますが、プレッシャーは彼に行動する時間をほとんど与えません！",
concern_mod)

add_skill("Rising Intensity | 強度の上昇",5+irandom(15),
"For <col @c_yellow>5<col @c_white> turns, the battle speed will increase by <col @c_yellow>1<col @c_white>. \nThe action timer will also be halved. | <col @c_yellow>5<col @c_yellow>ターンの間、戦闘速度は<col @c_yellow>1<col @c_white>ずつ増加します。\nアクションタイマーも半分になります。",
rising_intensity_mod)




start = ds_list_create()
ds_list_add(start,"All I want to do is have some fun. | 私と楽しいことをしましょう。")
ds_list_add(start,"Let's feel good together. | 一緒に気持ちよくなるの。")

win = ds_list_create()
ds_list_add(win,"<wave>Your face is so cute when you cum.</wave> | <wave>イってるあなたの顔はとってもかわいいわ。</wave>")
ds_list_add(win,"I'm not letting go...\nI haven't had enough yet. | 手放さないわよ...。\n私はまだイってないもの。")
ds_list_add(win,"Did you see this coming? \nIs this why you wanted to run? | あぁ・・・アナタの精液が溢れてくるわ \n見えるでしょう？もっと出していいのよ？")

loss = ds_list_create()
ds_list_add(loss,"<shake strong=.5>This is it!</shake> | <shake strong=.5>これよ！</shake>")
ds_list_add(loss,"<shake strong=1>This pleasure is making me numb!!!</shake> | <shake strong=1>この快感が私を痺れさせるわ!!!</shake>")
ds_list_add(loss,"<shake strong=1.5>I wish I could feel this forever!</shake> | <shake strong=1.5>これをずっと感じることができたらいいのに！</shake>")
ds_list_add(loss,"<shake strong=2.5>It's here!!!</shake> | <shake strong=2.5>来たぁ!!!!</shake>")

//Starting Actions
snap_x_center()
snap_y_center()