event_inherited()

//Setup Sprites
icon = spr_jaunehead
stand_south = spr_jaune_south
stand_east = spr_jaune_east
stand_west = spr_jaune_west
stand_north = spr_jaune_north

walk_south = spr_jaune_south_walk
walk_east = spr_jaune_east_walk
walk_west = spr_jaune_west_walk
walk_north = spr_jaune_north_walk

battle[0] = spr_jaune_battle
battle[1] = spr_jaune_creampie
sound[0] = tm_jaune_battle
sound[1] = tm_jaune_creampie

assist[0] = spr_jauneassist
assist[1] = spr_jauneassistcum

//Main Variables
name = jaune_name
state = "Wander"

//Editable Variables
random_set_seed(global.seed)
walk_speed = 1.05
run_speed = 1.6
decision_timer = 60
wander_dist = 240
follow_time = 0
mxfollow_time = 80
follow_dist = 90

//Game Variables
move_speed = 0
tmove_speed = 0
tdirection = direction

//Battle Variables
habit[0][0] = choose("U","L","R","D")
habit[0][1] = habit[0][0]
habit[0][2] = habit[0][0]

habit[1][0] = clockwise_dir(habit[0][0])
habit[1][1] = clockwise_dir(habit[0][1])
habit[1][2] = clockwise_dir(habit[0][2])

habit[2][0] = clockwise_dir(habit[1][0])
habit[2][1] = clockwise_dir(habit[1][1])
habit[2][2] = clockwise_dir(habit[1][2])

habit[3][0] = clockwise_dir(habit[2][0])
habit[3][1] = clockwise_dir(habit[2][1])
habit[3][2] = clockwise_dir(habit[2][2])

mybits = 12+floor(random(3))
timer[0] = decision_timer+irandom(20)-irandom(20)
randomize()
//chab = floor(random(array_height_2d(habit)))
chab = 0
lhab = chab
//STATS
min_speed = array_length_2d(habit,0)
if(global.difficulty = EASY)
	{
	mxwillpower = 100+irandom(10)
	willpower = mxwillpower
	mxtimeline = 0
	cream = 5
	free_answer = 1

	battle_speed = array_length_2d(habit,0)+choose(0,2)
	max_speed = 7
	min_speed = array_length_2d(habit,0)
	strength = 3
	skill = 9
	turn_time = 30
	}
else
	{
	mxwillpower = 120+irandom(10)
	//mxwillpower = 1
	willpower = mxwillpower
	mxtimeline = 0
	cream = 5
	free_answer = 0

	battle_speed = array_length_2d(habit,0)+choose(0,1,2,3)
	max_speed = 8
	strength = 3
	skill = 11
	turn_time = 25
	}

//Skills
enemy_action = ds_map_create()
enemy_text = ds_map_create()
enemy_mod = ds_map_create()

add_skill("Wait | 待つ",5+irandom(5),
"She's obsessed with kissing. | 彼女はイグレックの足をしっかり握りながら腰を動かしている。",
wait_mod)

add_skill("Self Control | 自粛",5+irandom(5),
"She realizes she's been in a rhythm!\nShe moves to another habit!\nHer willpower slightly increases! | 彼女は自分がリズムに乗っていることに気づきます！\n彼女は別の習慣に移ります！\n彼女の意志力はわずかに増加します！",
self_control_mod)

add_skill("Vaginal Control | 膣の制御",5+irandom(5),
"Her vagina moves in any way it can to bring out semen!\nFor <col @c_yellow>4<col @c_white> turns, lust damage is increased! | 彼女の膣は精液を引き出すためにそれができるあらゆる方法で動きます！ \n<col @c_yellow>4<col @c_white>ターンの間、欲望のダメージが増加します！",
vaginal_control_mod)

add_skill("Passionate Reaction | 情熱的な反応",5+irandom(5),
"She watches for how her movements affect Igrec!\nFor <col @c_yellow>6<col @c_white> turns, speed increases based on lust! | 彼女は自分の動きがどのように影響するかを見守っています、Igrec！ \n<col @c_yellow>6<col @c_white>ターンの場合、欲望に基づいて速度が上がります！",
passionate_reaction_mod)

add_skill("Taunt | 挑発",0+irandom(0),
"Igrec is provoked. \nUnless the Speed is above <color @c_yellow>4<color @c_white>, Igrec's power is decreased considerably. | イグレックは挑発されます。 \n速度が<color @c_yellow>4<color @c_white>を超えない限り、イグレックのダメージはかなり減少します。",
taunt_mod)

start = ds_list_create()
ds_list_add(start,"All I want to do is have some fun. | 私と楽しいことをしましょう。")
ds_list_add(start,"Let's feel good together. | 一緒に気持ちよくなるの。")

win = ds_list_create()
ds_list_add(win,"<wave>Your face is so cute when you cum.</wave> | <wave>イってるあなたの顔はとってもかわいいわ。</wave>")
ds_list_add(win,"I'm not letting go...\nI haven't had enough yet. | 手放さないわよ...。\n私はまだイってないもの。")
ds_list_add(win,"Did you see this coming? \nIs this why you wanted to run? | あぁ・・・アナタの精液が溢れてくるわ \n見えるでしょう？もっと出していいのよ？")

loss = ds_list_create()
ds_list_add(loss,"<shake strong=.5>This is it!</shake> | <shake strong=.5>これよ！</shake>")
ds_list_add(loss,"<shake strong=1>This pleasure is making me numb!!!</shake> | <shake strong=1>この快感が私を痺れさせるわ!!!</shake>")
ds_list_add(loss,"<shake strong=1.5>I wish I could feel this forever!</shake> | <shake strong=1.5>これをずっと感じることができたらいいのに！</shake>")
ds_list_add(loss,"<shake strong=2.5>It's here!!!</shake> | <shake strong=2.5>来たぁ!!!!</shake>")

//Starting Actions
snap_x_center()
snap_y_center()