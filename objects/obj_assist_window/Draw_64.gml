var x1,y1,port,scle,sw;
port = get_portrait(assist)
scle = 3
draw_set_font(global.big_font)
height = sprite_get_height(port)*scle
sw = sprite_get_width(port)*scle
x1 = (view_wport/2)-(width/2)
y1 = (view_hport/2)-(height/2)

if(draw_get_color() != c_black)draw_set_color(c_black)
if(draw_get_alpha() != 1)draw_set_alpha(1)
draw_rectangle(x1,y1,x1+width,y1+height,0)
draw_set_color(c_white)
draw_rectangle(x1,y1,x1+width,y1+height,1)

draw_set_halign(fa_center)
draw_set_valign(fa_top)
draw_text_width(x1+sw+((width-sw)/2),y1+1,"New Ally | 新しい友達",width,-1)
draw_line(x1+4,y1+string_height("H")+3,x1+width-4,y1+string_height("H")+3)

var lx,rx,ly,cy,h,txt,port;
lx = x1+(width/4)
rx = x1+width-(width/4)
h = string_height("H")+3
ly = (y1+(height/2)+h)

draw_sprite_ext(port,0,x1+1,y1+1,scle,scle,0,c_white,1)

draw_set_valign(fa_middle)
draw_set_font(global.main_font)
draw_text_width(x1+sw+((width-sw)/2),ly-(string_height("H")),string_split(assist)+string_split(" has joined!|参加しました "),width,-1)