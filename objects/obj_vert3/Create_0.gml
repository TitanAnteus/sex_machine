event_inherited()

//Setup Sprites
icon = spr_verthead
stand_south = spr_green_south
stand_east = spr_green_east
stand_west = spr_green_west
stand_north = spr_green_north

walk_south = spr_green_south_walk
walk_east = spr_green_east_walk
walk_west = spr_green_west_walk
walk_north = spr_green_north_walk

battle[0] = spr_vert_battle
battle[1] = spr_vert_creampie
sound[0] = tm_vert_battle
sound[1] = tm_vert_creampie

assist[0] = spr_vertassist
assist[1] = spr_vertassistcum

//Main Variables
name = vert_name
state = "Wander"

//Editable Variables
random_set_seed(global.seed)
walk_speed = 1.05
run_speed = 1.6
decision_timer = 60
wander_dist = 240
follow_time = 0
mxfollow_time = 80
follow_dist = 90


//Game Variables
move_speed = 0
tmove_speed = 0
tdirection = direction



//Battle Variables
habit[0][0] = choose("L","R")
habit[0][1] = choose("D","U")
habit[0][2] = choose("R","L")

habit[1][0] = opposite_dir(habit[0][0])
habit[1][1] = opposite_dir(habit[0][1])
habit[1][2] = opposite_dir(habit[0][2])

mybits = 12+floor(random(3))
timer[0] = decision_timer+irandom(20)-irandom(20)
randomize()
//chab = floor(random(array_height_2d(habit)))
chab = 0
lhab = chab
//STATS
min_speed = array_length_2d(habit,0)
if(global.difficulty = EASY)
	{
	mxwillpower = 100+irandom(10)
	willpower = mxwillpower
	mxtimeline = 0
	cream = 5
	free_answer = 1

	battle_speed = array_length_2d(habit,0)+choose(0,2)
	max_speed = 8
	min_speed = array_length_2d(habit,0)
	strength = 3
	skill = 9
	turn_time = 30
	}
else
	{
	mxwillpower = 120+irandom(10)
	willpower = mxwillpower
	mxtimeline = 0
	cream = 5
	free_answer = 0

	battle_speed = array_length_2d(habit,0)+choose(0,2,4)
	max_speed = 10
	strength = 3
	skill = 11
	turn_time = 25
	}

//Skills
enemy_action = ds_map_create()
enemy_text = ds_map_create()
enemy_mod = ds_map_create()
ds_map_add(enemy_action,"Playful Spirit | 遊び心",5+irandom(30))
ds_map_add(enemy_action,"Merciless | 無慈悲",5+irandom(20))
ds_map_add(enemy_action,"Compassion | 思いやり",5+irandom(20))
ds_map_add(enemy_action,"Stamina Pit | スタミナピット",5+irandom(25))
ds_map_add(enemy_action,"Powerful Grip | 強力なグリップ",5+irandom(10))
ds_map_add(enemy_action,"Taunt | 挑発",0+irandom(0))
ds_map_add(enemy_action,"Wait | 待つ",5+irandom(5))

var ptext;
ptext = "Set speed to a random value between <return @cenemy.min_speed> and <return @cenemy.max_speed>. | 速度を<return @cenemy.min_speed>〜<return @cenemy.max_speed>のランダムな値に設定します。"

ds_map_add(enemy_text,"Playful Spirit | 遊び心","Set speed to a random value between <color @c_yellow><return @cenemy.min_speed><color @c_white> and <color @c_yellow><return @cenemy.max_speed><color @c_white>. | 速度を<color @c_yellow><return @cenemy.min_speed><color @c_white>〜<color @c_yellow><return @cenemy.max_speed><color @c_white>のランダムな値に設定します。")
ds_map_add(enemy_text,"Merciless | 無慈悲","Ignore speed penalty on damage for <color @c_yellow>1<color @c_white> turn. | <color @c_yellow>1<color @c_white>ターンの間、ダメージに対する速度のペナルティを無視します。")
ds_map_add(enemy_text,"Compassion | 思いやり","Heal based on speed. \nThe slower the speed, the greater the heal. | 速度に基づいて回復します。 \n速度が遅いほど、回復は大きくなります。")
ds_map_add(enemy_text,"Stamina Pit | スタミナピット","Reduces Igrec's stamina at the start of the turn for <color @c_yellow>3<color @c_white> turns. | <color @c_yellow>3<color @c_white>ターンの間、ターンの開始時にプレイヤーのスタミナを減らす")
ds_map_add(enemy_text,"Powerful Grip | 強力なグリップ","Igrec cannot escape, and the action timer is halved for <color @c_yellow>3<color @c_white> turns. | イグレックは逃げることができず、\nアクションタイマーは<color @c_yellow>3<color @c_white>ターンの間半分になります。")
ds_map_add(enemy_text,"Taunt | 挑発","Igrec is provoked! \nUnless the Speed is above <color @c_yellow>4<color @c_white>, Igrec's power is decreased considerably. | イグレックは挑発されます。 \n速度が<color @c_yellow>4<color @c_white>を超えない限り、イグレックのダメージはかなり減少します。")
ds_map_add(enemy_text,"Wait | 待つ","She moves while holding on to Igrec's legs tightly. | 彼女はイグレックの足をしっかり握りながら腰を動かしている。")

ds_map_add(enemy_mod,"Playful Spirit | 遊び心",playful_spirit_mod)
ds_map_add(enemy_mod,"Merciless | 無慈悲",merciless_mod)
ds_map_add(enemy_mod,"Compassion | 思いやり",compassion_mod)
ds_map_add(enemy_mod,"Stamina Pit | スタミナピット",stamina_pit_mod)
ds_map_add(enemy_mod,"Powerful Grip | 強力なグリップ",powerful_grip_mod)
ds_map_add(enemy_mod,"Taunt | 挑発",taunt_mod)
ds_map_add(enemy_mod,"Wait | 待つ",wait_mod)

start = ds_list_create()
ds_list_add(start,"All I want to do is have some fun. | 私と楽しいことをしましょう。")
ds_list_add(start,"Let's feel good together. | 一緒に気持ちよくなるの。")

win = ds_list_create()
ds_list_add(win,"<wave>Your face is so cute when you cum.</wave> | <wave>イってるあなたの顔はとってもかわいいわ。</wave>")
ds_list_add(win,"I'm not letting go...\nI haven't had enough yet. | 手放さないわよ...。\n私はまだイってないもの。")
ds_list_add(win,"Did you see this coming? \nIs this why you wanted to run? | あぁ・・・アナタの精液が溢れてくるわ \n見えるでしょう？もっと出していいのよ？")

loss = ds_list_create()
ds_list_add(loss,"<shake strong=.5>This is it!</shake> | <shake strong=.5>これよ！</shake>")
ds_list_add(loss,"<shake strong=1>This pleasure is making me numb!!!</shake> | <shake strong=1>この快感が私を痺れさせるわ!!!</shake>")
ds_list_add(loss,"<shake strong=1.5>I wish I could feel this forever!</shake> | <shake strong=1.5>これをずっと感じることができたらいいのに！</shake>")
ds_list_add(loss,"<shake strong=2.5>It's here!!!</shake> | <shake strong=2.5>来たぁ!!!!</shake>")

//Starting Actions
snap_x_center()
snap_y_center()