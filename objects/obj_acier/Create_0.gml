event_inherited()

//Setup Sprites
icon = spr_acierhead
stand_south = spr_acier_south
stand_east = spr_acier_east
stand_west = spr_acier_west
stand_north = spr_acier_north

walk_south = spr_acier_south_walk
walk_east = spr_acier_east_walk
walk_west = spr_acier_west_walk
walk_north = spr_acier_north_walk

battle[0] = spr_acier_battle1
battle[1] = spr_acier_creampie1
sound[0] = tm_acier_battle1
sound[1] = tm_acier_creampie1

battle_offset = 0

battle[2] = spr_acier_battle2
battle[3] = spr_acier_creampie2
sound[2] = tm_acier_battle2
sound[3] = tm_acier_creampie2

assist[0] = spr_acierassist
assist[1] = spr_acierassistcum

//Main Variables
name = acier_name
state = "Wander"

//Editable Variables
random_set_seed(global.seed)
walk_speed = 1.25
run_speed = 1.8
decision_timer = 60
wander_dist = 240
follow_time = 0
mxfollow_time = 80
follow_dist = 90
mybits = 1000

//Game Variables
move_speed = 0
tmove_speed = 0
tdirection = direction

battle_bgm = bgm_battle2

//Battle Variables
habit[0][0] = choose("L","R")
habit[0][1] = choose("D","U")
habit[0][2] = opposite_dir(habit[0][0])
habit[0][3] = opposite_dir(habit[0][1])


habit[1][0] = choose("L","R","U","D")
if(habit[1][0] == "U" or habit[1][0] == "D")
	{
	habit[1][1] = choose("R","L")
	}
else
	{
	habit[1][1] = choose("U","D")
	}
habit[1,2] = opposite_dir(habit[1][1])
habit[1,3] = opposite_dir(habit[1][0])

habit[2][0] = choose("L","R","U","D")
habit[2][1] = habit[2][0]
habit[2][2] = habit[2][0]
habit[2][3] = opposite_dir(habit[2][0])

//Habit2
habit2[0][0] = choose("L","R")
habit2[0][1] = choose("D","U")
habit2[0][2] = opposite_dir(habit2[0][0])
habit2[0][3] = opposite_dir(habit2[0][1])

habit2[1][0] = choose("L","R","U","D")
if(habit2[1][0] == "U" or habit2[1][0] == "D")
	{
	habit2[1][1] = choose("R","L")
	}
else
	{
	habit2[1][1] = choose("U","D")
	}
habit2[1][2] = opposite_dir(habit2[1][1])
habit2[1][3] = opposite_dir(habit2[1][0])


habit2[2][0] = choose("L","R","U","D")
habit2[2][1] = habit2[2][0]
habit2[2][2] = habit2[2][0]
habit2[2][3] = opposite_dir(habit2[2][0])

timer[0] = decision_timer+irandom(20)-irandom(20)
randomize()

//chab = floor(random(array_height_2d(habit)))
chab = 0
lhab = chab
//STATS
min_speed = array_length_2d(habit,0)
if(global.difficulty = EASY)
	{
	mxwillpower = 550+irandom(25)
	willpower = mxwillpower
	mxtimeline = 0
	cream = 5
	free_answer = 2

	battle_speed = array_length_2d(habit,0)+1
	max_speed = 8
	min_speed = array_length_2d(habit,0)
	strength = 3
	skill = 9
	turn_time = 30
	}
else
	{
	mxwillpower = 650+irandom(50)
	willpower = mxwillpower
	mxtimeline = 0
	cream = 5
	free_answer = 0

	battle_speed = array_length_2d(habit,0)+1
	max_speed = 10
	strength = 3
	skill = 11
	turn_time = 25
	}

//Skills
enemy_action = ds_map_create()
enemy_text = ds_map_create()
enemy_mod = ds_map_create()
ds_map_add(enemy_action,"Overheat | オーバーヒート",10+irandom(10))
ds_map_add(enemy_action,"Sadist | サディスト",20+irandom(40))
ds_map_add(enemy_action,"Merciless | 無慈悲",5+irandom(20))
ds_map_add(enemy_action,"Rising Intensity | 強度の上昇",5+irandom(15))
ds_map_add(enemy_action,"Decouple | デカップリング",0)


ds_map_add(enemy_text,"Overheat | オーバーヒート","For <col @c_yellow>1<col @c_white> turn, damage given increased, and damage \nreceived increased. Increases battle speed to <col @c_yellow>8<col @c_white>.\nEnds after guarding succesfully <col @c_yellow>5<col @c_white> times. | <col @c_yellow>1<col @c_white>ターンの間、与えられるダメージが増加し、\n受けるダメージが増加しす。 ターン数を<col @c_yellow>8<col @c_white>に増やします。\n<col @c_yellow>5<col @c_white>回ガードした後終了します。")
ds_map_add(enemy_text,"Sadist | サディスト","The battle speed will increase \nfor every perfect guard for <col @c_yellow><return @sadist_count><col @c_white> turns. | <col @c_yellow><return @sadist_count><col @c_white>ターンの間に完璧なガードをするたびに、\n戦闘速度が上がります。")
ds_map_add(enemy_text,"Merciless | 無慈悲","Ignore speed penalty on damage for <col @c_yellow>1<col @c_white> turn. | <col @c_yellow>1<col @c_white>ターンの間、ダメージに対する速度のペナルティを無視します。")
ds_map_add(enemy_text,"Rising Intensity | 強度の上昇","For <col @c_yellow>5<col @c_white> turns, the battle speed will increase by <col @c_yellow>1<col @c_white>. \nThe action timer will also be halved. | <col @c_yellow>5<col @c_white>ターンの間、戦闘速度は<col @c_yellow>1<col @c_white>ずつ増加します。\nアクションタイマーも半分になります。")
ds_map_add(enemy_text,"Decouple | デカップリング","Something's happening... \nAcier has switched positions! \nShe has new skills and a different pattern! | 何かが起こっています... アシエは形態を変化した。 彼女は新しいスキルと異なるパターンで攻めてくる！")

ds_map_add(enemy_mod,"Overheat | オーバーヒート",overheat_mod)
ds_map_add(enemy_mod,"Sadist | サディスト",sadist_mod)
ds_map_add(enemy_mod,"Merciless | 無慈悲",merciless_mod)
ds_map_add(enemy_mod,"Rising Intensity | 強度の上昇",rising_intensity_mod)
ds_map_add(enemy_mod,"Decouple | デカップリング",decouple_mod)
	

//Phase 2
enemy_action2 = ds_map_create()
enemy_text2 = ds_map_create()
enemy_mod2 = ds_map_create()

ds_map_add(enemy_action2,"Exhausting Pound | 排気ポンド",25+irandom(10))
ds_map_add(enemy_action2,"Tightening | 締め付け",10+irandom(5))
ds_map_add(enemy_action2,"Leeway | 余裕",5+irandom(8))
ds_map_add(enemy_action2,"Unpredictable | 予測不可能な",10+irandom(20))

ds_map_add(enemy_text2,"Exhausting Pound | 排気ポンド","For <col @c_yellow>1<col @c_white> turn, all Arrows reduce stamina and speed. \nCorrect arrows reduce less stamina. | <col @c_yellow>1<col @c_white>ターンの間、すべての矢印でスタミナが減少します。 \n正しい矢印もスタミナを減らします。")
ds_map_add(enemy_text2,"Tightening | 締め付け","Increase Lust. \nHigher increase at low speeds. | ラストを増やします。 \n低速でより高い増加。")
ds_map_add(enemy_text2,"Leeway | 余裕","Reduces speed to minimum. \nHeal based amount the difference in speed. | 速度を最小にします。 \n回復に基づいて速度に差が出ます。")
ds_map_add(enemy_text2,"Unpredictable | 予測不可能な","She gives in to the pleasure. \n<col @global.pink>There's no telling what she'll do!<col @c_white> | 彼女は喜びに屈します。 \n<col @global.pink>彼女が何をするか予測できない！<col @c_white>")
ds_map_add(enemy_text2,"Decouple | デカップリング","Something's happening... \nAcier has switched positions! \nShe has new skills and a different pattern! | 何かが起こっています... アシエは形態を変化した。 彼女は新しいスキルと異なるパターンで攻めてくる！")

ds_map_add(enemy_mod2,"Exhausting Pound | 排気ポンド",exhausting_pound_mod)
ds_map_add(enemy_mod2,"Tightening | 締め付け",tightening_mod)
ds_map_add(enemy_mod2,"Leeway | 余裕",leeway_mod)
ds_map_add(enemy_mod2,"Unpredictable | 予測不可能な",unpredictable_mod2)

start = ds_list_create()
ds_list_add(start,"I'm not letting this chance go. | 私はこのチャンスを逃すつもりは無いわ。")
ds_list_add(start,"Just give up, \nthere's no chance you'll beat me. | あきらめなさい。\nあなたが私に勝つ可能性は無いわ。")

win = ds_list_create()
ds_list_add(win,"<shake strong=2>CUM! DON'T EVEN THINK OF LEAVING\n BEHIND A SINGLE DROP!</shake> | <shake strong=2>射精た！ このまま私の中で\n射精し続けなさい！</shake>")
ds_list_add(win,"Not yet...\nI haven't had enough. | まだ...\nまだ足りない。")
ds_list_add(win,"I don't think I can live without this. | やっぱりセックスはやめられない！")


loss = ds_list_create()
ds_list_add(loss,"<shake strong=.5>I... I won't lose!</shake> | <shake strong=.5>私は...負けない！</shake>")
ds_list_add(loss,"<shake strong=1>I won't lose to this dick!!!</shake> | <shake strong=1>セックスでは絶対に負けない!!!!</shake>")
ds_list_add(loss,"<shake strong=2>Guh... Why does it feel so good!</shake> | <shake strong=2>ぐぅ...なんでこんなに気持ちいいの！？</shake>")
ds_list_add(loss,"<shake strong=3>Nooo!!!</shake> | <shake strong=3>ダメェ!!!</shake>")

//Starting Actions
snap_x_center()
snap_y_center()