if(! instance_exists(FG_light_control))instance_deactivate_object(object_index)

var cam = view_camera[0];
var x0 = camera_get_view_x(cam), y0 = camera_get_view_y(cam),
	cw = camera_get_view_width(cam), ch = camera_get_view_height(cam);
	
if(! surface_exists(surf_extra))
	{
	surf_extra = surface_create(cw,ch)
	}
if(! surface_exists(surf_walls))
	{
	surf_walls = surface_create(cw,ch)
	}

gpu_set_blendmode(bm_subtract)

surface_set_target(surf_extra)
var back_alph;
back_alph = 1
if(instance_exists(obj_global_light))back_alph = obj_global_light.d
draw_clear_alpha(c_black,back_alph)

with(obj_character_light)
	{
	draw_sprite_ext(light_mask_hole,0,x-x0,y-y0,1,1,0,c_white,1)
	}

surface_reset_target()

gpu_set_blendmode(bm_normal)
