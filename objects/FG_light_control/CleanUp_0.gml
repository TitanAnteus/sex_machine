/// @description clean up
// You can write your code in this editor

if surface_exists(surf_light)
	surface_free(surf_light)
	
if surface_exists(surf_shadow)
	surface_free(surf_shadow)