//Do my door function
image_speed = 0
image_index = 0

event_inherited()
alpha = 0
alpha_start = 0

action = function(){
	if(locked == 0)
		{
		if(alpha_start = 0)
			{
			with(obj_player){state = "Cutscene"}
			alpha_start = 1
			image_speed = 1
			audio_play_sound(door_sound,1,0)
			}
		}
	else
		{
		audio_play_sound(sfx_fail,1,0)
		instance_create_depth(0,0,0,obj_locked_door)
		}
}