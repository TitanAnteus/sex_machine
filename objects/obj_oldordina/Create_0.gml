event_inherited()

//Setup Sprites
stand_south = spr_oldordina_south
stand_east = spr_oldordina_east
stand_west = spr_oldordina_west
stand_north = spr_oldordina_north

walk_south = spr_oldordina_south_walk
walk_east = spr_oldordina_east_walk
walk_west = spr_oldordina_west_walk
walk_north = spr_oldordina_north_walk

direction_locked = false
move_speed = 0

function oldordina_action(){

	direction = point_direction(x,y,other.x,other.y)
	freeze_char(-1)
		
	var cut;
	cut = instance_create_depth(0,0,0,obj_cutscene)
	cut.mysfx = sfx_ordina
	cut.parent = id
	
	with(cut)
		{
		event_talk(android_name,"IF YOU WISH TO ESCAPE, YOU MUST REACH GROUND LEVEL. | 脱出したい場合は、地上レベルに到達する必要があります。",0)
		event_talk(android_name,"THERE ARE QUITE A FEW ANDROIDS THAT WILL HINDER YOU IN THAT GOAL HOWEVER. | ただし、その目標であなたの邪魔になるAndroidはかなりあります。",0)
		event_talk(android_name,"TAKE CARE. | 気を付けて。",0)
		}
	
}

action = oldordina_action

start_dir = 270

name = "Ordina | オルディーナ"

itemshop_list = ds_list_create()
function add_item(item_name){
	ds_list_add(itemshop_list,full_itemname(item_name))
}
add_item("Passion Plus | パッションプラス")
add_item("Energizer | エナジャイザー")
add_item("Libido Control | 性欲管理")

equipshop_list = ds_list_create()
function add_equip(equip_name){
	ds_list_add(equipshop_list,full_itemname(equip_name))
}
add_equip("Iron Resistance | 鉄の抵抗")
