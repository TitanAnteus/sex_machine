event_inherited()

if(state = "Cutscene")
and(image_alpha < 1)
	{
	image_alpha += .02
	}

if(check_battle())
and(global.battle = false)
and(escape = false)
and(can_battle = true)
	{
	if(instance_exists(stopid))with(stopid){instance_destroy()}
	start_battle(battle,sound)
	}
	
if(point_distance(obj_player.x,obj_player.y,x,y) < active_dist)
and(active = false)active = true

depth = depth_start+(-(y+16))