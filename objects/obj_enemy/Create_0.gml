event_inherited()

state = ""
stopid = noone
escape = false
escape_time = 80

active = false
active_dist = 600

mybits = 0
battle_offset = 0
boss = 0
battle_bgm = bgm_battle

direction_locked = false
lockdir = direction
mycut = noone

var createlight;
createlight = 1
with(obj_character_light)
	{
	//if(par == other.id)createlight = 0
	}
if(createlight == 1)
	{
	followlight = instance_create_depth(x,y,room_height+100,obj_character_light)
	with(followlight)
		{
		par = other.id
		event_perform(ev_step,ev_step_normal)
		}
	}
if(direction = 359)direction = choose(0,90,180,270)

//show_debug_message("Direction: "+string(direction))

//Starting Actions
snap_x_center()
snap_y_center()

//For Text Display
hyper_speed = 0
slow_speed = 0