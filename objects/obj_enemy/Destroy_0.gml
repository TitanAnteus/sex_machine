if(instance_exists(followlight))with(followlight){instance_destroy()}
if(instance_exists(mycut))
	{
	with(mycut){instance_destroy()}
	}
mycut = instance_place(x,y,obj_stop)
if(instance_exists(mycut))
	{
	with(mycut){instance_destroy()}
	}
if(respawn = false)
	{
	ini_open(global.saveloc)
	ini_write_real("EnemySpawn",room_get_name(room)+"_"+english_split(name),1)
	ini_close()
	}