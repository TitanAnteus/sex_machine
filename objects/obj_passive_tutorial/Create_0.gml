active = true

center_x = view_wport/4
center_y = view_hport/2

box_open = false

box_width = 400
box_height = 300

box_x1 = center_x-(box_width/2)
box_x2 = center_x+(box_width/2)
box_y1 = center_y-(box_height/2)
box_y2 = center_y+(box_height/2)

bx1 = center_x
bx2 = center_x
by1 = center_y
by2 = center_y
text_padding = 8
text_height = font_get_size(global.main_font)+text_padding
padding = 8

spd = 8

start_text = ds_list_create()
ds_list_add(start_text,"Igrec has abilities that <col @c_lime>activate automatically <col @c_white>if certain conditions are met. | イグレックには特定の条件が満たされた場合に<col @c_lime>自動的にアクティブ<col @c_white>になるスキルがあります。")
ds_list_add(start_text,"You can check the conditions with the <col @c_lime>Passive<col @c_white> command on the command select screen. | コマンド選択画面の<col @c_lime>パッシブコマンド<col @c_white>で条件を確認できます。")

cdialogue = start_text
ctext = 0
dtext = cdialogue[| ctext]
tpos = 0
tspd = global.tspd
draw_continue = false

text_bob = 0
text_bdir = 1
text_bspd = .2
mx_text_bob = 5

focus = 0
darksurf = -1
dark_alph = .6

mxwait = global.mxwait
wait = mxwait