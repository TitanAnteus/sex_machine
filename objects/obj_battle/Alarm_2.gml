/// @description Start Enemy Turn
if(state = "Victory")exit
reveal = 0
cmenu = 0
cpos = 0
state = "Enemy"
draw_continue = true

ds_list_clear(exclude)
clear_enemy_state()

var rand,item,ea,scr;
eskill = ""
ds_map_copy(temp_enemy_action,cenemy.enemy_action)
ea = temp_enemy_action

item = ds_map_find_first(cenemy.enemy_mod)
//show_debug_message("First Item: "+item+"\n\nSize: "+string(ds_map_size(cenemy.enemy_mod)))
for(md=0;md<ds_map_size(cenemy.enemy_mod);md+=1)
	{
	scr = ds_map_find_value(cenemy.enemy_mod,item)
	if(! is_undefined(scr))
		{
		script_execute(scr,item)
		if(ds_map_find_value(temp_enemy_action,item) >= 1000)eskill = item
		}
	else
		{
		show_debug_message("ERROR: Could Not Find Skill Mod")
		}
	item = ds_map_find_next(cenemy.enemy_mod,item)
	}


while(eskill == "")
	{
	item = ds_map_find_first(ea)
	for(md=0;md<ds_map_size(ea);md+=1)
		{
		rand = floor(random(1000))
		if(rand < ds_map_find_value(ea,item))
			{
			eskill = item
			}
		item = ds_map_find_next(ea,item)
		}
	}
	
enemy_skill(eskill)
etext = string(cenemy.enemy_text[? eskill])

check_passive()