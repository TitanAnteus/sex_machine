name = ""

battle[0] = -1
battle[1] = -1

sound[0] = -1
sound[1] = -1

cenemy = noone
cbat = 0

//Location Variables
battle_width = 480
battle_height = 480
battle_x = (view_wport/2)-(battle_width/2)
battle_y = 60

meter_width = 500
meter_height = 8

emwidth = battle_width
emheight = 24

meter_x = (view_wport/2)-(meter_width/2)
meter_y = battle_y+battle_height

command_width = 1150
command_height = 145

state_s = sprite_get_height(spr_focus_icon)+2

estate_x = battle_x+battle_width+1
estate_y = battle_y+4

pstate_x = battle_x-state_s-1
pstate_y = battle_y+4

text_padding = 8
text_height = font_get_size(global.main_font)+text_padding

command_x = (view_wport/2)-(command_width/2)
command_y = meter_y+(meter_height*3)+2

line_x = command_x+250
pline_x = command_x+(command_width/2)
heart_x = view_wport*.7

text_x = command_x+((line_x-command_x)/2)
text_y = command_y+20

timer_x = line_x+5
timer_y = command_y+command_height-2

passive_x = 45
passive_y = 10
passive_wid = 250


//Battle Variables
battle_bgm = bgm_battle
battle_start = 60
cream_count = 0
max_cream_count = 3
temp_enemy_action = ds_map_create()

twillpower = global.willpower
tstamina = global.stamina
tcum = global.cum
meter_speed = 1

old_speed = 0
speed_increase = 0

draw_continue = false
free_answer = 2
action_timer = 0

reveal = 0
guardcount = 0
misscount = 0
failcount = 0

select_color = c_red

state = "Start"
cmenu = 0
cpos = 0
dpos = 0
advantage = "Normal"

command[0][0] = "Prepare | 備える"
command[0][1] = "Skill | スキル"
command[0][2] = "Passive | パッシブコマンド"
command[0][3] = "Item | 項目"
command[0][4] = "Run | 逃げる"

for(i=0;i<ds_list_size(global.skill_list);i+=1)
	{
	command[1][i] = global.skill_list[| i]
	}

passive_map = global.equip_passive
cpassive = ds_map_create()

cpas = 0
cost_text = "Stamina | スタミナ"
current_cost = 0
use_skill = false
use_item = false
can_run = true
skill_cost = 0

text_bob = 0
text_bdir = 1
text_bspd = .2
mx_text_bob = 5

exclude = ds_list_create()
eskill = ""
pskill = ""
etext = ""
ptext = ""

player_state = ds_list_create()
enemy_state = ds_list_create()
toadd_state = ds_list_create()
//state_reduce = ds_list_create()

ctext = 0
dtext = ""
tpos = 0
tspd = global.tspd
empty_cum = 0
cdialogue = -1
start_mxpattern = 0

//Run Code
alarm[0] = battle_start

audio_play_sound(sfx_battle,1,0)
audio_stop_sound(global.bgm)

global.battle = true
depth = (room_height+1000)

blursurf = -1
command_surf = -1
passive_surf = -1
item_surf = -1

image_speed = 0
firstrun = true
tutorial = false
prepare_only = false
skill_only = false

if(global.battleposition = 0)
	{
	instance_create_depth(0,0,0,obj_battle_tutorial)
	prepare_only = true
	}

run_chance = 50+irandom(40)-irandom(40);

run_fail = ds_list_create()
ds_list_add(run_fail,"... | ...")
ds_list_add(run_fail,"Igrec could not escape! | イグレックは逃げられなかった！")

run_success = ds_list_create()
ds_list_add(run_success,"... | ...")
ds_list_add(run_success,"Igrec managed to escape! | イグレックはなんとか脱出できた！")

ambush = "Igrec was <col @c_red>surprised! \n<col @c_white>The enemy has the advantage!  | イグレックは<col @c_red>驚いた！ \n<col @c_white>この戦いは敵に有利だ！"
surprise = "Igrec was <col @c_aqua>prepared <col @c_white>for this battle!  | イグレックはこの戦いに<col @c_aqua>備えていた！"
final_text = "_"

mybits = 0
highlight = 0

mxwait = global.mxwait
wait = mxwait

toend = false