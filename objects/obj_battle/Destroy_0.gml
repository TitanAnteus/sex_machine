if(surface_exists(blursurf))surface_free(blursurf)
if(surface_exists(command_surf))surface_free(command_surf)
if(surface_exists(passive_surf))surface_free(passive_surf)
if(surface_exists(item_surf))surface_free(item_surf)
audio_play_sound(global.bgm,1,1)

audio_stop_sound(bgm_victory)
audio_stop_sound(bgm_loss)
audio_stop_sound(battle_bgm)

if(global.gameover = 0)
and(toend = false)
	{
	audio_stop_sound(bgm_victory)
	global.battle = false
	if(cdialogue != run_success)
		{
		global.bits += mybits
		}
		
	ini_open("settings.ini")
	ini_write_real("Enemy",english_split(name),true)
	ini_close()
	ini_open(global.saveloc)
	ini_write_real("Enemy",english_split(name),true)
	ini_close()
	populate_enemy_list()
	
	if(global.battleposition = 0)
		{
		global.battleposition = 1
		instance_create_depth(0,0,0,obj_event_tutorialend)
		}
	ini_open(global.saveloc)
	ini_write_real("FirstCheck","BattlePosition",global.battleposition)
	ini_close()
	
	assist_countdown()
	with(obj_canfreeze)
		{
		unfreeze_char()
		}
	}
	
with(obj_enemy)
	{
	ds_map_destroy(enemy_action)
	ds_map_destroy(enemy_text)
	ds_map_destroy(enemy_mod)
	
	ds_list_destroy(start)
	ds_list_destroy(win)
	ds_list_destroy(loss)
	
	event_perform(ev_create,0)
	}
	
ds_list_destroy(player_state)
ds_list_destroy(enemy_state)
ds_list_destroy(toadd_state)
ds_list_destroy(exclude)

if(instance_exists(cenemy))
	{
	with(cenemy)
		{
		chab = 0
		willpower = mxwillpower
		escape = true
		image_alpha = .5
		timer[0] = decision_timer*2
		timer[1] = escape_time
		}
	if(cenemy.remain = true)
		{
		if(room = rm_area2_2)instance_create_depth(x,y,depth,obj_rouge_defeat)
		if(room = rm_area2_bleuboss)instance_create_depth(x,y,depth,obj_bleu_defeat)
		if(room = rm_area2_3)instance_create_depth(x,y,depth,obj_vert_defeat)
		}
	}
