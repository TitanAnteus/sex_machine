tutorial = false
if(instance_exists(obj_battle_tutorial))
and(obj_battle_tutorial.active = true)
	{
	tutorial = true
	}
if(instance_exists(obj_cream_tutorial))
and(obj_cream_tutorial.active = true)
	{
	tutorial = true
	}
if(instance_exists(obj_passive_tutorial))
and(obj_passive_tutorial.active = true)
	{
	tutorial = true
	}
if(instance_exists(obj_multipattern_tutorial))
and(obj_multipattern_tutorial.active = true)
	{
	tutorial = true
	}

if(state = "Command Select")
	{
	if(! check_estate("Powerful Grip"))
	and(! check_estate("Commanding Presence"))
	and(cenemy.boss = 0)
		{
		can_run = true
		}
	else
		{
		can_run = false
		}
	}


if(cenemy.willpower <= 0)
and(global.gameover = false)
	{
	if(state == "Test")
		{
		global.cum = clamp(global.cum,0,global.mxcum-.01)
		empty_cum = global.mxcum-global.cum
		state = "Victory"
		
		if(check_pstate("Victory Cry"))
			{
			mybits += (2+floor(random(10)))*timeline_size(sound[cenemy.battle_offset+1])
			
			final_text = string_replace("Igrec has gained <col @c_yellow><b> <col @c_white>bits!\nHe now has <col @c_yellow><tb><col @c_white> bits in total! | イグレックは<col @c_yellow><b><col @c_white>ビットを得ました!\n彼は今<col @c_yellow><tb><col @c_white>ビットを持っています！","<b>",string(mybits))
			final_text = string_replace(final_text,"<b>",string(mybits))
			final_text = string_replace(final_text,"<tb>",string(global.bits+mybits))
			final_text = string_replace(final_text,"<tb>",string(global.bits+mybits))
			with(cenemy)
				{
				var lt;
				lt = loss[| ds_list_size(loss)-1]
				if(string_count("Igrec has gained",lt) == 0)
					{
					ds_list_add(loss,other.final_text)
					}
				else
					{
					ds_list_replace(loss,ds_list_size(loss)-1,other.final_text)
					}
				}
			
			}
		
		ctext = 0
		tpos = 0
		cdialogue = cenemy.loss
		dtext = ds_list_find_value(cdialogue,ctext)
		set_speed(cenemy.min_speed+1)
		exit
		}
	}
	
if(global.cum >= global.mxcum)
and(cbat = 0)
and(state != "Enemy")
and(state != "Victory")
	{
	global.cum = global.mxcum
	
	if(state != "Creampie")
		{
		cbat = 1
		sprite_index = battle[cbat+cenemy.battle_offset]
		image_index = 0
		state = "Creampie"
		clear_enemy_state()
		
		cmenu = 0
		cpos = 0
		draw_continue = false
	
		old_speed = mxpattern
		set_speed(cenemy.min_speed+1)
	
		ctext = 0
		tpos = 0
		cdialogue = cenemy.win
		dtext = ds_list_find_value(cdialogue,ctext)
		//show_message("dtext: "+dtext+"\n\nsplit: "+string_split(dtext))
						
		alarm[1] = -1
		alarm[2] = -1
		alarm[3] = -1
		}
	}

timeline_index = sound[cbat+cenemy.battle_offset]
timeline_position = image_index
if(state != "Start")
	{
	var tospeed;
	tospeed = mxpattern-array_length_2d(cenemy.habit,0)
	image_speed = 1+(tospeed*.275)
	
	if(instance_exists(obj_battle_tutorial))
	and(obj_battle_tutorial.cdialogue == obj_battle_tutorial.start_text)
		{
		with(obj_battle_tutorial)
			{
			active = true
			cdialogue = first_text
			ctext = 0
			tpos = 0
			dtext = cdialogue[| ctext]
			
			focus = 20
			}
		}
	}
else
	{
	if(firstrun = true)
	and(tutorial = false)
		{
		firstrun = false
		image_speed = 0
		ctext = 0
		tpos = 0
		cdialogue = cenemy.start
		
		if(advantage == "Ambush")
			{
			ds_list_insert(cdialogue,0,ambush)
			audio_play_sound(sfx_surprise,1,0)
			ds_list_add(player_state,"Blind")
			
			global.cum += 25
			
			if(check_pstate("Blind"))
				{
				for(i=0;i<array_length_2d(pattern,1);i+=1)
					{
					pattern[3][i] = "?"
					}
				}
			}
		if(advantage == "Surprise")
			{
			ds_list_insert(cdialogue,0,surprise)
			audio_play_sound(sfx_powerup,1,0)
			ds_list_add(player_state,"Vision")
			
			global.stamina += 25
			global.willpower += 10
			
			if(check_pstate("Vision"))
				{
				for(i=0;i<array_length_2d(pattern,1);i+=1)
					{
					pattern[3][i] = pattern[1][i]
					}
				}
			}
		var cass;
		for(i=0;i<ds_list_size(global.assist_battle);i+=1)
			{
			cass = global.assist_battle[| i]
			ds_list_add(player_state,cass)
			}
		if(ds_list_size(global.assist_battle) > 0)
			{
			audio_play_sound(sfx_powerup,1,0)
			ds_list_clear(global.assist_battle)
			}
			dtext = ds_list_find_value(cdialogue,ctext)
		exit
		}
	}
if(state = "Test")
	{
	if(instance_exists(obj_battle_tutorial))
	and(reveal >= mxpattern)
		{
		skill_only = true
		with(obj_battle_tutorial)
			{
			if(cdialogue = second_text)
				{
				active = true
				cdialogue = third_text
				ctext = 0
				tpos = 0
				dtext = cdialogue[| ctext]
			
				focus = 20
				}
			}
		}
	}
if(cmenu == 1)
	{
	if(instance_exists(obj_battle_tutorial))
		{
		with(obj_battle_tutorial)
			{
			if(cdialogue = third_text)
				{
				active = true
				cdialogue = fourth_text
				ctext = 0
				tpos = 0
				dtext = cdialogue[| ctext]
			
				focus = 20
				}
			}
		}
	}
if(tutorial == true)exit
if(global.gameover == false)
	{
	if(right_pressed())
	or(down_pressed())
		{
		if(state = "Command Select")
			{
			if(cpos < array_length_2d(command,cmenu)-1)
				{
				cpos += 1
				audio_play_sound(sfx_menumove,1,0)
				}
			else
				{
				cpos = 0
				audio_play_sound(sfx_menumove,1,0)
				}
			}
		if(state = "Passive")
			{
			if(right_pressed())
				{
				var k;
				k = (cpos div 2)*2
				if(cpos < ds_map_size(passive_map))
					{
					cpos += 1
					if(cpos = ds_map_size(passive_map))cpos -= 2
					if((cpos div 2)*2 > k)cpos = k
					}
				audio_play_sound(sfx_menumove,1,0)
				}
			else
				{
				if(cpos < ds_map_size(passive_map)-2)
					{
					cpos += 2
					}
				audio_play_sound(sfx_menumove,1,0)
				}
			cpos = clamp(cpos,0,ds_map_size(passive_map)-1)
			}	
		if(state = "Item")
			{
			if(right_pressed())
				{
				var k;
				k = (cpos div 2)*2
				if(cpos < ds_list_size(global.item_list))
					{
					cpos += 1
					if(cpos = ds_list_size(global.item_list))cpos -= 2
					if((cpos div 2)*2 > k)cpos = k
					}
				audio_play_sound(sfx_menumove,1,0)
				}
			else
				{
				if(cpos < ds_list_size(global.item_list)-2)
					{
					cpos += 2
					}
				audio_play_sound(sfx_menumove,1,0)
				}
			cpos = clamp(cpos,0,ds_list_size(global.item_list)-1)
			}	
		}
	if(left_pressed())
	or(up_pressed())
		{
		if(state = "Command Select")
			{
			if(cpos > 0)
				{
				cpos -= 1
				audio_play_sound(sfx_menumove,1,0)
				}
			else
				{
				cpos = array_length_2d(command,cmenu)-1
				audio_play_sound(sfx_menumove,1,0)
				}
			}
		if(state = "Passive")
			{
			if(left_pressed())
				{
				var k;
				k = (cpos div 2)*2
				if(cpos >= 0)
					{
					cpos -= 1
					if(cpos = -1)cpos = 1
					if((cpos div 2)*2 < k)cpos = k+1
					}
				audio_play_sound(sfx_menumove,1,0)
				}
			else
				{
				if(cpos > 1)
					{
					cpos -= 2
					}
				audio_play_sound(sfx_menumove,1,0)
				}
			cpos = clamp(cpos,0,ds_map_size(passive_map)-1)
			}
		if(state = "Item")
			{
			if(left_pressed())
				{
				var k;
				k = (cpos div 2)*2
				if(cpos >= 0)
					{
					cpos -= 1
					if(cpos = -1)cpos = 1
					if((cpos div 2)*2 < k)cpos = k+1
					}
				audio_play_sound(sfx_menumove,1,0)
				}
			else
				{
				if(cpos > 1)
					{
					cpos -= 2
					}
				audio_play_sound(sfx_menumove,1,0)
				}
			cpos = clamp(cpos,0,ds_list_size(global.item_list)-1)
			}
		}
	}
if(skip_direct())
	{
	wait -= 1
	}
if(select_pressed())
or(wait = 0)
	{
	wait = mxwait
	if(state = "Test")
		{
		if(reveal >= mxpattern)
			{
			draw_continue = false
			event_perform(ev_alarm,2)
			exit
			}
		}
	if(state = "Creampie")
	or(global.gameover = true)
		{
		if(tpos < string_length(string_split(dtext)))
			{
			tpos = string_length(string_split(dtext))
			draw_continue = true
			}
		else
			{
			if(ctext < ds_list_size(cdialogue)-1)
				{
				tpos = 0
				ctext += 1
				draw_continue = false
				dtext = ds_list_find_value(cdialogue,ctext)
				
				if(ctext >= 2)
				and(cbat != 0)
					{
					cbat = 0
					sprite_index = battle[cbat+cenemy.battle_offset]
					set_speed(cenemy.min_speed+2+floor(random(3)))
					if(global.gameover = true)
					and(! audio_is_playing(bgm_loss))audio_play_sound(bgm_loss,1,1)
					cream_count = clamp(cream_count+1,0,max_cream_count)
					if(global.firstloss = true)
						{
						instance_create_depth(0,0,0,obj_cream_tutorial)
						}
					}
				}
			else
				{
				if(cbat = 0)
					{
					state = "Game Over"
					if(global.gameover == false)
						{
						state = "Command Select"
						global.cum = 0
						set_speed(old_speed)
						event_perform(ev_alarm,3)
						exit
						}
					}
				}
			}
		}
	else
		{
		if(cbat == 1)
		and(image_index > image_number-2)
		and(global.gameover = false)
			{
			if(alarm[4] < 60)
			and(tpos >= string_length(string_split(dtext)))
			and(dtext = final_text)
				{
				with(cenemy){
					if(remain = false)
						{
						instance_destroy()
						}
					else
						{
						can_battle = false
						}
					}
				instance_destroy()
				exit
				}
			}
		}
	if(state = "Victory")
		{
		if(tpos < string_length(string_split(dtext)))
			{
			tpos = string_length(string_split(dtext))
			draw_continue = true
			}
		else
			{
			if(ctext < ds_list_size(cdialogue)-1)
				{
				tpos = 0
				ctext += 1
				draw_continue = false
				dtext = ds_list_find_value(cdialogue,ctext)
				if(ctext < ds_list_size(cdialogue)-1)
					{
					global.cum += empty_cum/(ds_list_size(cdialogue)-2)
					global.cum = clamp(global.cum,0,global.mxcum-.01)
					set_speed(mxpattern+1)
					}
				}
			if(ctext > ds_list_size(cdialogue)-2)
				{
				if(cbat = 0)
					{
					set_speed(cenemy.min_speed+1)
					audio_sound_gain(battle_bgm,0,20)
					global.cum = global.mxcum
					cbat = 1
					sprite_index = battle[cbat+cenemy.battle_offset]
					image_index = 0
				
					audio_play_sound(sfx_victory,1,0)
					alarm[4] = 280
					}
				}
			}
		}
	if(state = "Start")
		{
		if(tpos < string_length(string_split(dtext)))
			{
			tpos = string_length(string_split(dtext))
			draw_continue = true
			}
		else
			{
			if(ctext < ds_list_size(cdialogue)-1)
				{
				tpos = 0
				ctext += 1
				draw_continue = false
				dtext = ds_list_find_value(cdialogue,ctext)
				if(ctext = 1)
					{
					var tospeed;
					tospeed = mxpattern-array_length_2d(cenemy.habit,0)
					image_speed = 1+(tospeed*.275)
					}
				}
			else
				{
				state = "Command Select"
				draw_continue = false
				exit
				}
			}
		}
	if(state = "Run")
		{
		if(tpos < string_length(string_split(dtext)))
			{
			tpos = string_length(string_split(dtext))
			draw_continue = true
			}
		else
			{
			if(ctext < ds_list_size(cdialogue)-1)
				{
				tpos = 0
				ctext += 1
				draw_continue = false
				dtext = ds_list_find_value(cdialogue,ctext)
				}
			else
				{
				if(cdialogue == run_fail)
					{
					state = "Command Select"
					draw_continue = false
					event_perform(ev_alarm,1)
					exit
					}
				else
					{
					instance_destroy()
					exit
					}
				}
			}
		}
	if(state = "Item")
		{
		if(consume_item(cpos))
			{
			use_item = true
			cmenu = 0
			cpos = 0
			dpos = 0
			reveal = 0
			state = "Command Select"
			exit
			}
		}
	if(state = "Command Select")
	and(select_pressed())
		{
		if(cmenu = 1)
			{
			if(command_stamina(command[cmenu,cpos]))
				{
				skill_cost = current_cost
				use_skill = true
				execute_skill()
				cmenu = 0
				cpos = 0
				dpos = 0
				reveal = 0
				exit
				}
			else
				{
				audio_play_sound(sfx_fail,1,0)
				}
			}
		if(string_count("Prepare",command[cmenu,cpos]) > 0)
		and(cmenu = 0)
			{
			if(skill_only = false)
				{
				state = "Pattern"
				dpos = 0
				audio_play_sound(sfx_menumove,1,0)
				
				if(global.battleposition == 1)
				and(! instance_exists(obj_multipattern_tutorial))
					{
					instance_create_depth(0,0,0,obj_multipattern_tutorial)
					}
				if(prepare_only = true)
					{
					prepare_only = false					
					if(instance_exists(obj_battle_tutorial))
						{
						with(obj_battle_tutorial)
							{
							active = true
							cdialogue = second_text
							ctext = 0
							tpos = 0
							dtext = cdialogue[| ctext]
							}
						}
					}
				}
			else
				{
				audio_play_sound(sfx_fail,1,0)
				}
			}
			
		if(string_count("Passive",command[cmenu,cpos]) > 0)
		and(cmenu = 0)
			{
			if(ds_map_size(global.equip_passive) > 0)
			and(prepare_only = false)
			and(skill_only = false)
				{
				state = "Passive"
				cpos = 0
				audio_play_sound(sfx_menumove,1,0)
				}
			else
				{
				audio_play_sound(sfx_fail,1,0)
				}
			}
		if(string_count("Item",command[cmenu,cpos]) > 0)
		and(cmenu = 0)
			{
			if(ds_list_size(global.item_list) > 0)
			and(use_item = false)
				{
				state = "Item"
				cpos = 0
				audio_play_sound(sfx_menumove,1,0)
				}
			else
				{
				audio_play_sound(sfx_fail,1,0)
				}
			}
		
		if(string_count("Skill",command[cmenu,cpos]) > 0)
		and(cmenu = 0)
			{
			if(prepare_only = false)
			and(use_skill = false)
				{
				cmenu = 1
				cpos = 0
				if(skill_only = true)skill_only = false
				}
			else
				{
				audio_play_sound(sfx_fail,1,0)
				}
			}
		if(string_count("Run",command[cmenu,cpos]) > 0)
		and(cmenu = 0)
			{
			if(prepare_only == false)
			and(can_run == true)
			and(skill_only = false)
				{
				state = "Run"
				if(floor(random(100)) < run_chance)
					{
					cdialogue = run_success
					ctext = 0
					tpos = 0
					}
				else
					{
					cdialogue = run_fail
					ctext = 0
					tpos = 0
					run_chance += 10
					}
				dtext = cdialogue[| ctext]
				//instance_destroy()
				}
			else
				{
				audio_play_sound(sfx_fail,1,0)
				}
			}
		}
	if(state = "Enemy")
		{
		event_perform(ev_alarm,3)
		exit
		}
	if(state = "Passive Skill")
		{
		if(cpas < ds_map_size(cpassive))
			{
			pskill = ds_map_find_next(cpassive,pskill)
			ptext = ds_map_find_value(passive_map,pskill)
			perform_passive()
			cpas += 1
			}
		else
			{
			end_turn()
			ds_map_clear(cpassive)
			exit
			}
		}
	}
if(back_pressed())
	{
	if(cmenu != 0)
	or(state == "Pattern" and dpos = 0)
		{
		state = "Command Select"
		cmenu = 0
		cpos = 0
		}
	if(state = "Passive")
	or(state = "Item")
		{
		state = "Command Select"
		cmenu = 0
		cpos = 0
		}
	if(state = "Pattern")
		{
		if(dpos > 0)
			{
			pattern[0][dpos-1] = ""
			dpos -= 1
			audio_play_sound(sfx_back,1,0)
			}
		}
	}

if(state = "Pattern")
	{
	var sfx_play;
	sfx_play = 0
	if(up_pressed())
	and(cpos < array_length_1d(pattern))
		{
		if(check_exclude("U"))
			{
			pattern[0][dpos] = "U"
			dpos += 1
			sfx_play = 1
			}
		}
	if(left_pressed())
	and(cpos < array_length_1d(pattern))
		{
		if(check_exclude("L"))
			{
			pattern[0][dpos]  = "L"
			dpos += 1
			sfx_play = 1
			}
		}
	if(right_pressed())
	and(cpos < array_length_1d(pattern))
		{
		if(check_exclude("R"))
			{
			pattern[0][dpos]  = "R"
			dpos += 1
			sfx_play = 1
			}
		}
	if(down_pressed())
	and(cpos < array_length_1d(pattern))
		{
		if(check_exclude("D"))
			{
			pattern[0][dpos]  = "D"
			dpos += 1
			sfx_play = 1
			}
		}

	if(dpos = mxpattern)
		{
		if(select_pressed())
		or(wait = 0)dpos += 1
		draw_continue = true
		}
	else
		{
		draw_continue = false
		}
	if(dpos > mxpattern)
		{
		if(up_pressed())or(left_pressed())
		or(down_pressed())or(right_pressed())
		or(select_pressed() or wait = 0)
			{
			sfx_play = 0
			if(alarm[1] == -1)alarm[1] = 1
			}
		}
	if(sfx_play == 1)audio_play_sound(sfx_menumove,1,0)
	}
if(wait = 0)wait=mxwait

//if(keyboard_check_pressed(vk_space))show_message(ds_map_size(cpassive))