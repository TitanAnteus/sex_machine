/// @description End Enemy Turn
if(state = "Victory")exit
clear_player_state()
for(var i=0;i<ds_list_size(toadd_state);i+=1)
	{
	ds_list_add(player_state,toadd_state[| i])
	}
ds_list_clear(toadd_state)

if(ds_map_size(cpassive) > 0)
	{
	state = "Passive Skill"
	cpas = 0
	pskill = ds_map_find_first(cpassive)
	ptext = ds_map_find_value(passive_map,pskill)
	perform_passive()
	
	cpas += 1
	}
else
	{
	end_turn()
	}