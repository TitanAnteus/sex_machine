if(global.battle = false)
	{
	global.battle = true
	state = "GameOver"
	}


global.willpower = clamp(global.willpower,0,global.mxwillpower)
global.stamina = clamp(global.stamina,0,global.mxstamina)
global.cum = clamp(global.cum,0,global.mxcum)
cenemy.willpower = clamp(cenemy.willpower,0,cenemy.mxwillpower)

if(global.battle = true)
	{
	if(surface_exists(blursurf))
		{
		surface_copy(blursurf,0,0,application_surface)
		var offset;
		offset = 2
		draw_set_alpha(.4)
		draw_surface(blursurf,-offset,-offset)
		draw_surface(blursurf,offset,offset)
		draw_surface(blursurf,offset,-offset)
		draw_surface(blursurf,-offset,offset)
		draw_set_alpha(1)
		}
	else
		{
		blursurf = surface_create(view_wport[0],view_hport[0])
		}
		
	//Draw Battle
	var scl;
	scl = battle_width/sprite_get_width(sprite_index)
	draw_set_color(c_white)
	draw_rectangle(battle_x,battle_y,battle_x+battle_width,battle_y+battle_height,1)
	draw_sprite_ext(sprite_index,image_index,battle_x,battle_y,scl,scl,0,c_white,1)
	if(highlight > 0)
		{
		draw_set_alpha(highlight)
		draw_rectangle(battle_x-1,battle_y-1,battle_x+battle_width,battle_y+battle_height,0)
		highlight -= .02
		}
	if(draw_get_alpha() != 1)draw_set_alpha(1)

	//Draw Name
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)
	draw_set_font(global.big_font)
	draw_text_outline(10,4,string_split(cenemy.name),c_white,c_red)
	draw_set_font(global.main_font)
	
	//Draw States
	var smap,item,sval,scl,sts;
	smap = state_map_create(enemy_state)
	item = ds_map_find_first(smap)
	draw_set_halign(fa_left)
	draw_set_valign(fa_bottom)
	scl = 1.5
	sts = state_s*scl
	for(i=0;i<ds_map_size(smap);i+=1)
		{
		var ic;
		ic = get_icon(item)
		sval = ds_map_find_value(smap,item)
		if(ic != -1)draw_sprite_ext(ic,0,estate_x,estate_y+(sts*i),scl,scl,0,c_white,1)
		if(sval > 1)draw_text_outline(round(estate_x+sts),round(estate_y+(sts-3)+(sts*i)),"x "+string(sval),c_white,c_black)
		item = ds_map_find_next(smap,item)
		}
	ds_map_destroy(smap)
	
	
	smap = state_map_create(player_state)
	item = ds_map_find_first(smap)
	draw_set_halign(fa_right)
	
	pstate_x = battle_x-sts-1
	for(i=0;i<ds_map_size(smap);i+=1)
		{
		sval = ds_map_find_value(smap,item)
		draw_sprite_ext(get_icon(item),0,pstate_x,pstate_y+(sts*i),scl,scl,0,c_white,1)
		if(sval > 1)draw_text_outline(round(pstate_x),round(pstate_y+(sts-3)+(sts*i)),string(sval)+" x",c_white,c_black)
		item = ds_map_find_next(smap,item)
		}
	ds_map_destroy(smap)
	//Draw Meters
	var wil,stm,cm;
	twillpower = clamp(twillpower+(meter_speed*sign(global.willpower-twillpower)),0,global.willpower)
	tstamina = clamp(tstamina+(meter_speed*sign(global.stamina-tstamina)),0,global.stamina) 
	tcum = clamp(tcum+(meter_speed*sign(global.cum-tcum)),0,global.cum)
	if(global.cum = global.mxcum)tcum = global.mxcum
	
	wil = meter_width*(twillpower/global.mxwillpower);
	stm = meter_width*(tstamina/global.mxstamina);
	cm = meter_width*(tcum/global.mxcum);
	
	var stmcol;
	stmcol = c_lime
	if(global.stamina < global.exhaust)stmcol = c_green
	draw_meter(0,wil,c_red)
	draw_meter(meter_height,stm,stmcol)
	
	//Draw Exhaust Point
	draw_set_color(c_blue)
	draw_line(meter_x+(meter_width*(global.exhaust/global.mxstamina)),meter_y+meter_height,meter_x+(meter_width*(global.exhaust/global.mxstamina)),meter_y+(meter_height*2))
	draw_line(meter_x+(meter_width*(global.exhaust/global.mxstamina))+1,meter_y+meter_height,meter_x+(meter_width*(global.exhaust/global.mxstamina))+1,meter_y+(meter_height*2))
	
	draw_meter((meter_height*2),cm,c_white)
	draw_enemy_meter(emwidth*(cenemy.willpower/cenemy.mxwillpower))
	
	//Draw Hearts
	var hcol,hwidth,scl;
	scl = 2
	hwidth = (sprite_get_width(spr_heart)*scl)+3
	for(i=0;i<max_cream_count;i+=1)
		{
		hcol = c_dkgray
		if(i < cream_count)hcol = c_white
		draw_sprite_ext(spr_heart,0,heart_x+(hwidth*i),command_y-3,scl,scl,0,hcol,1)
		}
	
	//Draw Commands
	// -- Draw Command Window
	draw_set_alpha(.8)
	draw_set_color(c_black)
	draw_roundrect(command_x,command_y,command_x+command_width,command_y+command_height,0)
	draw_set_alpha(1)
	draw_set_color(c_white)
	draw_roundrect(command_x,command_y,command_x+command_width,command_y+command_height,1)
	
	if(state = "Victory")
	or(state = "Start")
	or(state = "Run")
	or(state = "Creampie")
		{
		draw_set_halign(fa_center)
		draw_set_valign(fa_center)
		var tx,ty,col;
		tx = command_x+(command_width/2)
		ty = command_y+(command_height/2)
		
		//draw_circle(tx,ty,2,0)
		if(tpos <= string_length(string_split(dtext)))
			{
			tpos += tspd
			tpos = skip_color(string_split(dtext),tpos)
			draw_continue = false
			}
		if(tpos >= string_length(string_split(dtext)))
			{
			draw_continue = true
			}
		col = c_white
		draw_set_font(global.big_font)
		tpos = clamp(tpos,0,string_length(string_split(dtext)))
		
		if(dtext == ambush)col = c_white
		if(dtext == surprise)col = c_white
		if(dtext == final_text)col = c_white
		if(draw_get_color() != col)draw_set_color(col)
		draw_text_width(round(tx),round(ty),string_copy(string_split(dtext),0,tpos),command_width,-1)
		
		/*
		if(firstrun = false)
			{
			draw_set_halign(fa_left)
			draw_set_valign(fa_top)
			if(col = c_white)draw_text_width(command_x+8,command_y+8,string_split(cenemy.name)+":",command_width)
			}*/
		draw_set_font(global.main_font)
		}
	else if(state = "Passive")
		{
		//Draw Passive
		//  --Draw Line
		draw_line(pline_x,command_y+5,pline_x,command_y+command_height-5)
		// -- Draw Timer
		draw_set_halign(fa_left)
		draw_set_valign(fa_bottom)
		var tcol;
		tcol = c_white
		if(action_timer <= mxpattern*2)tcol = c_yellow
		if(action_timer <= mxpattern)tcol = c_red
		if(draw_get_color() != tcol)draw_set_color(tcol)
		if(action_timer > 0)
		and(global.battleposition >= 2)draw_text(round(pline_x+4),round(timer_y),action_timer)
		
		draw_set_halign(fa_left)
		draw_set_valign(fa_top)
		
		if(! surface_exists(passive_surf))
			{
			passive_surf = surface_create(pline_x-command_x,command_height)
			}
		else
			{
			surface_set_target(passive_surf)
			draw_clear_alpha(c_white,0)
			var hor,h,k,col,cskl,lsize,isize,cpos_y;
			hor = 2
			col = c_white
			
			lsize = ds_map_size(passive_map)
			isize = (ds_map_size(passive_map)+1) div hor
			cpos_y = -2
			if(cpos > 0)and(isize > 4)
				{
				cpos_y += (cpos-2) div hor
				cpos_y = clamp(cpos_y,0,max(isize-4,0))
				}
			else
				{
				cpos_y = 0
				}
			
			cskl = ds_map_find_first(passive_map)
			for(i=0;i<ds_map_size(passive_map);i+=1)
				{
				h = i mod hor
				k = i div hor
				col = c_white
				if(i = cpos)
					{
					col = c_red
					pskill = cskl
					}
				if(draw_get_color() != col)draw_set_color(col)
				draw_text(round(passive_x+(h*passive_wid)),round(passive_y+(k*text_height))-(cpos_y*text_height),string_split(cskl))
				
				cskl = ds_map_find_next(passive_map,cskl)
				}
			surface_reset_target()
			draw_surface(passive_surf,command_x,command_y)
			}
		var mx,my,ty,txt,ww;
		ww = command_x+command_width-pline_x
		mx = pline_x+((ww)/2)
		draw_set_halign(fa_center)
		draw_set_valign(fa_center)
		txt = string(ds_map_find_value(passive_map,pskill))
		//txt = ""
			
		ty = command_y
		my = ty+(command_height/2)
		if(draw_get_color() != c_white)draw_set_color(c_white)
		draw_text_width(round(mx),round(my),string_split(txt),ww,text_height)
		}
	else if(state = "Item")
		{
		//Draw Passive
		//  --Draw Line
		draw_line(pline_x,command_y+5,pline_x,command_y+command_height-5)
		// -- Draw Timer
		draw_set_halign(fa_left)
		draw_set_valign(fa_bottom)
		var tcol;
		tcol = c_white
		if(action_timer <= mxpattern*2)tcol = c_yellow
		if(action_timer <= mxpattern)tcol = c_red
		if(draw_get_color() != tcol)draw_set_color(tcol)
		if(action_timer > 0)
		and(global.battleposition >= 2)draw_text(round(pline_x+4),round(timer_y),action_timer)
		
		draw_set_halign(fa_left)
		draw_set_valign(fa_top)
		
		if(! surface_exists(item_surf))
			{
			item_surf = surface_create(pline_x-command_x,command_height)
			}
		else
			{
			surface_set_target(item_surf)
			draw_clear_alpha(c_white,0)
			var hor,h,k,col,citem,lsize,isize,cpos_y;
			hor = 2
			col = c_white
			
			lsize = ds_list_size(global.item_list)
			isize = (ds_list_size(global.item_list)+1) div hor
			cpos_y = -2
			if(cpos > 0)and(isize > 4)
				{
				cpos_y += (cpos-2) div hor
				cpos_y = clamp(cpos_y,0,max(isize-4,0))
				}
			else
				{
				cpos_y = 0
				}

			
			for(i=0;i<ds_list_size(global.item_list);i+=1)
				{
				citem = global.item_list[| i]
				h = i mod hor
				k = i div hor
				col = c_white
				if(i = cpos)
					{
					col = c_red
					}
				if(draw_get_color() != col)draw_set_color(col)
				draw_text(round(passive_x+(h*passive_wid)),round(passive_y+(k*text_height))-(cpos_y*text_height),string_split(citem))
				}
			surface_reset_target()
			draw_surface(item_surf,command_x,command_y)
			}
		var mx,my,ty,txt,ww;
		ww = command_x+command_width-pline_x
		mx = pline_x+((ww)/2)
		draw_set_halign(fa_center)
		draw_set_valign(fa_center)
		txt = string_split(get_item_description(global.item_list[| cpos]))
		//txt = ""
			
		ty = command_y
		my = ty+(command_height/2)
		if(draw_get_color() != c_white)draw_set_color(c_white)
		draw_text_width(round(mx),round(my),string_split(txt),ww,text_height)
		}
	else
		{
		/*if(state = "Pattern")
	or(state = "Test")
	or(state = "Enemy")
	or(state = "Command Select")
	or(state = "Passive Skill")*/
		if(global.gameover == false)
			{
			//  --Draw Line
			draw_line(line_x,command_y+5,line_x,command_y+command_height-5)
			// -- Draw Timer
			draw_set_halign(fa_left)
			draw_set_valign(fa_bottom)
			var tcol;
			tcol = c_white
			if(action_timer <= mxpattern*2)tcol = c_yellow
			if(action_timer <= mxpattern)tcol = c_red
			if(draw_get_color() != tcol)draw_set_color(tcol)
			if(action_timer > 0)
			and(state != "Enemy")
			and(state != "Passive Skill")
			and(global.battleposition >= 2)draw_text(round(timer_x),round(timer_y),action_timer)
			
			if(! surface_exists(command_surf))
				{
				command_surf = surface_create(line_x-command_x,command_height)
				}
			else
				{
				// -- Draw Commands
				var surfwidth,surfheight,dcol,cpos_y,mixcolor;
				surfwidth = surface_get_width(command_surf)
				surfheight = surface_get_height(command_surf)
				surface_set_target(command_surf)
				if(draw_get_color() != c_white)draw_set_color(c_white)
				draw_clear_alpha(c_white,0)
				draw_set_halign(fa_center)
				draw_set_valign(fa_top)
				text_x = surfwidth/2
				if(array_length_2d(command,cmenu) > 5)
					{
					if(cpos <= 2)
						{
						cpos_y = 0
						draw_text(round(surfwidth-14),round(surfheight-text_height),"▼")
						}
					if(cpos > 2)
					and(cpos < array_length_2d(command,cmenu)-2)
						{
						cpos_y = ((cpos-2)*text_height)
						draw_text(round(surfwidth-14),round(surfheight-text_height),"▼")
						draw_text(surfwidth-14,2,"▲")
						}
					if(cpos >= array_length_2d(command,cmenu)-2)
						{
						cpos_y = ((array_length_2d(command,cmenu)-5)*text_height)
						draw_text(round(surfwidth-14),2,"▲")
						}
					}
				else
					{
					cpos_y = 0
					}
				dcol = c_white
				text_y = clamp((surfheight/2)-(array_length_2d(command,cmenu)*text_height)/2,5,5000)-cpos_y
				mixcolor = c_white
				if(state != "Command Select")mixcolor = c_black
				for(i=0;i<array_length_2d(command,cmenu);i+=1)
					{
					if(state = "Command Select")
						{
						mixcolor = c_white
						dcol = c_white
						}
					if(command_stamina(command[cmenu][i]) = false)
						{
						mixcolor = c_green
						}
					if(cpos == i)
						{
						dcol = merge_color(select_color,mixcolor,.2)
						if(state = "Command Select")
							{
							if(string_count("Prepare",command[cmenu][i]) > 0)and(skill_only = true)dcol = merge_color(select_color,c_black,.5)
							if(string_count("Run",command[cmenu][i]) > 0)and((can_run = false) or (prepare_only = true) or (skill_only = true))dcol = merge_color(select_color,c_black,.5)
							if(string_count("Skill",command[cmenu][i]) > 0)and((use_skill = true) or (prepare_only = true))dcol = merge_color(select_color,c_black,.5)
							if(string_count("Item",command[cmenu][i]) > 0)and((ds_list_size(global.item_list) == 0) or (use_item = true) or (prepare_only = true) or (skill_only = true))dcol = merge_color(select_color,c_black,.5)
							if(string_count("Passive",command[cmenu][i]) > 0)and((ds_map_size(passive_map) == 0) or (prepare_only = true) or (skill_only = true))dcol = merge_color(select_color,c_black,.5)
							if(string_count("Run",command[cmenu][i]) > 0)and((can_run == false) or (skill_only = true))dcol = merge_color(select_color,c_black,.5)
							}
						}
					else
						{
						dcol = merge_color(c_white,mixcolor,.5)
						if(state = "Command Select")
							{
							if(string_count("Prepare",command[cmenu][i]) > 0)and(skill_only = true)dcol = merge_color(c_white,c_black,.5)
							if(string_count("Run",command[cmenu][i]) > 0)and((can_run = false) or (prepare_only = true) or (skill_only = true))dcol = merge_color(c_white,c_black,.5)
							if(string_count("Skill",command[cmenu][i]) > 0)and((use_skill = true) or (prepare_only = true))dcol = merge_color(c_white,c_black,.5)
							if(string_count("Item",command[cmenu][i]) > 0)and((ds_list_size(global.item_list) == 0) or (use_item = true) or (prepare_only = true) or (skill_only = true))dcol = merge_color(c_white,c_black,.5)
							if(string_count("Passive",command[cmenu][i]) > 0)and((ds_map_size(passive_map) == 0) or (prepare_only = true) or (skill_only = true))dcol = merge_color(c_white,c_black,.5)
							if(string_count("Run",command[cmenu][i]) > 0)and((can_run == false) or (skill_only = true))dcol = merge_color(c_white,c_black,.5)
							}
						}
					if(draw_get_color() != dcol)draw_set_color(dcol)
					draw_text(round(text_x),round(text_y+(text_height*i)),string_split(command[cmenu][i]))
					}
				surface_reset_target()
				draw_surface(command_surf,command_x,command_y+1)
				}
			}
		}
	if(global.gameover = false)
		{
		//Draw Diamonds
		var dx,dy,dw;
		dw = sprite_get_width(spr_diamond)
		dx = line_x+((command_x+command_width-line_x)/2)-((dw*mxpattern)/2)
		dy = command_y+(command_height/2)-dw
	
		
		if(draw_get_color() != c_white)draw_set_color(c_white)
		if(state = "Pattern")
			{
			draw_set_halign(fa_top)
			draw_set_valign(fa_left)
			var lx,ly,lw,pa,cl;
			lx = line_x +10
			ly = command_y+10
			lw = 10
			pa = 15
			for(i=0;i<array_height_2d(cenemy.habit);i+=1)
				{
				cl = c_dkgray
				if(i = cenemy.lhab)cl = c_white
				if(draw_get_color() != cl)draw_set_color(cl)
				draw_rectangle(lx+(i*pa),ly,lx+(i*pa)+lw,ly+lw,0)
				}
			
			draw_set_halign(fa_center)
			draw_set_valign(fa_center)
			for(i=0;i<mxpattern;i+=1)
				{
				var pat,dir;
				pat = pattern[0][i]
				dir = 0
				if(pat = "U")dir = 90
				if(pat = "L")dir = 180
				if(pat = "R")dir = 0
				if(pat = "D")dir = 270
				draw_sprite_ext(spr_diamond,0,dx+(dw*i)+(dw/2),dy+(dw/2),1,1,0,pattern[2][i],1)
				draw_sprite_ext(spr_diamond,0,dx+(dw*i)+(dw/2),dy+dw+(dw/2),1,1,0,pattern[2][i],1)
				
				if(pat = "?")
				or(pat = "")
					{
					if(draw_get_color() != c_white)draw_set_color(c_white)
					draw_text(round(dx+(dw*i)+(dw/2)),round(dy+(dw/2)),pattern[0][i])
					}
				else
					{
					draw_sprite_ext(spr_diamond,1,dx+(dw*i)+(dw/2),dy+(dw/2),1,1,dir,pattern[2][i],1)
					}
				
				pat = pattern[3][i]
				dir = 0
				if(pat = "U")dir = 90
				if(pat = "L")dir = 180
				if(pat = "R")dir = 0
				if(pat = "D")dir = 270
				
				if(pat = "?")
				or(pat = "")
					{
					if(draw_get_color() != c_white)draw_set_color(c_white)
					draw_text(round(dx+(dw*i)+(dw/2)),round((dy+dw)+(dw/2)),pattern[3][i])
					}
				else
					{
					draw_sprite_ext(spr_diamond,1,dx+(dw*i)+(dw/2),dy+dw+(dw/2),1,1,dir,pattern[2][i],1)
					}
				}
			}
		if(state = "Test")
			{
			draw_set_halign(fa_top)
			draw_set_valign(fa_left)
			var lx,ly,lw,pa,cl;
			lx = line_x +10
			ly = command_y+10
			lw = 10
			pa = 15
			for(i=0;i<array_height_2d(cenemy.habit);i+=1)
				{
				cl = c_dkgray
				if(i = cenemy.lhab)cl = c_white
				if(draw_get_color() != cl)draw_set_color(cl)
				draw_rectangle(lx+(i*pa),ly,lx+(i*pa)+lw,ly+lw,0)
				}
			
			draw_set_halign(fa_center)
			draw_set_valign(fa_center)
			for(i=0;i<mxpattern;i+=1)
				{
				var pat,dir;
				pat = pattern[0][i]
				dir = 0
				draw_sprite_ext(spr_diamond,0,dx+(dw*i)+(dw/2),dy+(dw/2),1,1,0,pattern[2][i],1)
				draw_sprite_ext(spr_diamond,0,dx+(dw*i)+(dw/2),dy+dw+(dw/2),1,1,0,pattern[2][i],1)
				
				if(i < reveal)
					{
					pat = pattern[0][i]
					dir = 0
					if(pat = "U")dir = 90
					if(pat = "L")dir = 180
					if(pat = "R")dir = 0
					if(pat = "D")dir = 270
					
					if(pat = "?")
					or(pat = "")
						{
						if(draw_get_color() != c_white)draw_set_color(c_white)
						draw_text(round(dx+(dw*i)+(dw/2)),round(dy+(dw/2)),pattern[0][i])
						}
					else
						{
						draw_sprite_ext(spr_diamond,1,dx+(dw*i)+(dw/2),dy+(dw/2),1,1,dir,pattern[2][i],1)
						}
					}
				if(i < reveal)
					{
					pat = pattern[1][i]
					dir = 0
					if(pat = "U")dir = 90
					if(pat = "L")dir = 180
					if(pat = "R")dir = 0
					if(pat = "D")dir = 270
					
					if(pat = "?")
					or(pat = "")
						{
						if(draw_get_color() != c_white)draw_set_color(c_white)
						draw_text(round(dx+(dw*i)+(dw/2)),round((dy+dw)+(dw/2)),pattern[1][i])
						}
					else
						{
						draw_sprite_ext(spr_diamond,1,dx+(dw*i)+(dw/2),dy+dw+(dw/2),1,1,dir,pattern[2][i],1)
						}
					}
				}
			}
		if(state = "Enemy")
			{
			draw_set_halign(fa_center)
			var mx,my,ty,ww;
			mx = line_x+((command_x+command_width-line_x)/2)
			ww = (command_x+command_width)-line_x
		
			draw_set_valign(fa_top)
			if(draw_get_color() != c_white)draw_set_color(c_white)
			draw_text(round(mx),round(command_y+5),string_split(eskill))
			draw_line(line_x+5,command_y+8+text_height,command_x+command_width-5,command_y+8+text_height)
		
			//Draw Skill Description
			draw_set_valign(fa_center)
			ty = command_y+8+text_height
			my = ty+((command_height-(8+text_height))/2)
			if(draw_get_color() != c_white)draw_set_color(c_white)
			draw_text_width(round(mx),round(my),etext,ww,text_height)
			}
		if(state = "Passive Skill")
			{
			draw_set_halign(fa_center)
			var mx,my,ty,in,ww;
			mx = line_x+((command_x+command_width-line_x)/2)
			ww = (command_x+command_width)-line_x
			if(draw_get_color() != c_white)draw_set_color(c_white)
			draw_line(line_x+5,command_y+8+text_height,command_x+command_width-5,command_y+8+text_height)
		
			//Draw Skill Description
			
			ty = command_y+8+text_height
			my = ty+((command_height-(8+text_height))/2)
			if(draw_get_color() != c_orange)draw_set_color(c_orange)
			draw_set_valign(fa_top)
			draw_text(round(mx),round(command_y+5),string_split(pskill))
			in = ptext
			draw_set_valign(fa_center)
			if(! is_undefined(in))
				{
				draw_text_width(round(mx),round(my),string_split(in),ww,text_height)
				}
			}
	
		if(state = "Command Select")
			{
			draw_set_font(global.main_font)
			draw_set_halign(fa_center)
			var mx,my,ty,txt,cm,ww;
			ww = (command_x+command_width)-line_x
			cm = command[cmenu,cpos]
			mx = line_x+((command_x+command_width-line_x)/2)
			my = command_y+(command_height/2)
		
			//Draw Command Cost
			txt = string(global.skill_cost[? cm])
			if(txt == "-1")
				{
				txt = string_split("All - While Not Exhausted | すべて  - 使い果たされていない間")
				}
			else
				{
				txt += " / "+string(floor(global.stamina))
				}
			if(string_count("undefined",txt) == 0)
				{
				draw_set_valign(fa_top)
				if(draw_get_color() != c_white)draw_set_color(c_white)
				
				draw_text_width(round(mx),round(command_y+5),string_split(cost_text)+": "+string(txt),ww,text_height)
				draw_line(line_x+5,command_y+8+text_height,command_x+command_width-5,command_y+8+text_height)
				}
			//Draw Skill Description
			txt = string(global.skill_map[? cm])
			//if(string_count(txt,"Slightly increase defense") > 0)show_message("What?")
			if(string_count("undefined",txt) == 0)
				{
				draw_set_valign(fa_center)
				ty = command_y+8+text_height
				my = ty+((command_height-(8+text_height))/2)
				if(draw_get_color() != c_white)draw_set_color(c_white)
				draw_text_width(round(mx),round(my),txt,ww,text_height)
				}
			else
				{
				//if(cmenu = 1)show_message("WHAT!")
				}
			}
		}
	if(draw_continue = true)
		{
		var symb;
		symb = keyname(global.k_select)

		if(state = "Victory")
		and(ctext = ds_list_size(cenemy.loss)-2)
			{
			symb = "^"
			}
		draw_set_halign(fa_right)
		draw_set_valign(fa_bottom)
		if(draw_get_color() != c_white)draw_set_color(c_white)
		draw_text(round(command_x+command_width-5),command_y+command_height-5-text_bob,symb)
		}
	if(text_bdir == true)
		{
		text_bob += text_bspd
		if(text_bob > mx_text_bob)text_bdir = !text_bdir
		}
	else
		{
		text_bob -= text_bspd
		if(text_bob < 0)text_bdir = !text_bdir
		}
	}
	
if(global.gameover = true)
	{
	/*draw_set_alpha(.5)
	draw_set_color(c_white)
	draw_rectangle(0,0,view_wport,view_hport,0)
	draw_set_alpha(1)*/
		
	draw_set_halign(fa_center)
	draw_set_valign(fa_center)
	draw_set_font(global.big_font)
	draw_text_outline(round(battle_x/2),round(battle_y+(battle_height/2)),"Game Over",c_red,c_black)
	draw_set_font(global.main_font)
	draw_text_outline(round(battle_x/2),round(battle_y+(battle_height/2)+34),"(Press Back To Restart)",c_white,c_black)
	}