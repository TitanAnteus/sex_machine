function temp_key_init() {
	ini_open("settings.ini")
	tleft = ini_read_real("Controls","Left",global.k_left)
	tright = ini_read_real("Controls","Right",global.k_right)
	tdown = ini_read_real("Controls","Down",global.k_down)
	tup = ini_read_real("Controls","Up",global.k_up)
	tsel = ini_read_real("Controls","Select",global.k_select)
	tback = ini_read_real("Controls","Back",global.k_back)
	tskip = ini_read_real("Controls","Skip",global.k_skip)
	tlanguage = ini_read_real("Controls","Language",global.k_language)
	ini_close()

	tk[0] = tleft
	tk[1] = tright
	tk[2] = tup
	tk[3] = tdown
	tk[4] = tsel
	tk[5] = tback
	tk[6] = tskip
	tk[7] = tlanguage
	
	nk[0] = global.k_left
	nk[1] = global.k_right
	nk[2] = global.k_up
	nk[3] = global.k_down
	nk[4] = global.k_select
	nk[5] = global.k_back
	nk[6] = global.k_skip
	nk[7] = global.k_language
}
