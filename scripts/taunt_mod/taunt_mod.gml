///@function tauntmod()
///@description Returns a high value when Taunt is a good skill to use
function taunt_mod(argument0) {
	var ea,chk,skl;
	ea = temp_enemy_action
	skl = argument0

	chk = ds_map_find_value(ea,skl)
	if(chk = undefined)return false

	if(mxpattern+speed_increase < 5)
		{
		chk += 40
		}
	if(mxpattern+speed_increase < 4)
		{
		chk += 90
		}

	if(state_count(enemy_state,"Taunt") > 0)
		{
		chk = -10
		}
	
	if(mxpattern+speed_increase >= 5)
		{
		chk = -10
		}
	ds_map_replace(ea,skl,clamp(chk,-10,500))
	show_debug_message(skl+" : "+string(chk)+"/"+string(ds_map_find_value(cenemy.enemy_action,skl)))
	return true


}
