function default_controls() {
	ini_open("settings.ini")
	ini_write_real("Controls","Left",vk_left)
	ini_write_real("Controls","Right",vk_right)
	ini_write_real("Controls","Up",vk_up)
	ini_write_real("Controls","Down",vk_down)
	ini_write_real("Controls","Select",ord("Z"))
	ini_write_real("Controls","Back",ord("X"))
	ini_write_real("Controls","Skip",vk_control)
	ini_write_real("Controls","Language",ord("L"))

	global.k_left = ini_read_real("Controls","Left",vk_left)
	global.k_right = ini_read_real("Controls","Right",vk_right)
	global.k_up = ini_read_real("Controls","Up",vk_up)
	global.k_down = ini_read_real("Controls","Down",vk_down)
	global.k_select = ini_read_real("Controls","Select",ord("Z"))
	global.k_back = ini_read_real("Controls","Back",ord("X"))
	global.k_skip = ini_read_real("Controls","Skip",vk_control)
	global.k_language = ini_read_real("Controls","Language",ord("L"))
	ini_close()


}
