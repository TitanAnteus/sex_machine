/// @function skip_pressed()
/// @description Checks if the skip key is pressed
function control_pressed() {
	if(keyboard_check_pressed(global.k_skip))return 1
	return 0


}
