/// @function left_direct()
/// @description Checks if left key is being pressed
function left_direct() {
	if(keyboard_check(global.k_left))return 1
	return 0


}
