/// @function freeze_char(time)
/// @description Checks inputs, and decides move speed
/// @param {real} time - amount of time in ticks to freeze character
function freeze_char(argument0) {

	if(object_is_ancestor(object_index,obj_canfreeze))
	and(frozen = false)
		{
		frozen = true
	
		img_save = image_speed
		image_speed = 0
	
		pth_speed = path_speed
		path_speed = 0
	
		alarm[0] = argument0
		}


}
