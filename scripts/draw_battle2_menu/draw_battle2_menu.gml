/// @function draw_battle2_menu()
/// @description Draws the item menu
function draw_battle2_menu() {

	var boxwidth,boxheight,topy,mpad,descheight,txth;
	boxwidth = rightwidth
	boxheight = rightheight
	descheight = 250
	topy = 20
	mpad = 50

	draw_set_font(global.big_font)
	draw_set_halign(fa_center)
	draw_set_valign(fa_top)
	draw_text(boxwidth/2,topy,string_split("Battle 2 | バトル2"))

	draw_line(10,topy+mpad,boxwidth-10,topy+mpad)
	topy = topy+mpad+2
	draw_set_font(global.main_font)
	draw_set_halign(fa_left)
	txth = string_height("H")+2

	if(draw_get_color() != c_white)draw_set_color(c_white)

	draw_text_width(5,topy,"Next, let's talk about battle speed: | 次は戦闘速度について説明します。",boxwidth-10,txth)
	topy += txth*2
	draw_text_width(5,topy,"Battle speed is the amount of diamonds per battle. | 戦闘速度とは女の子の腰振りの速さのことで、表示されるダイヤモンドの数に影響を与えます。",boxwidth-10,txth)
	topy += txth*2
	draw_text_width(5,topy,"The higher the battle speed the more actions you have to worry about per turn. | 戦闘速度が速いほどターンごとに入力する矢印の数が増加してしまいます。",boxwidth-10,txth)

	topy += txth*3
	draw_text_width(5,topy,"You'll also have to worry about the action timer. | また、タイマーについても注意する必要があります。",boxwidth-10,txth)
	topy += txth*2
	draw_text_width(5,topy,"The action timer is the amount of time you have to put in your arrows. | タイマーは矢印を入力することができる時間です。",boxwidth-10,txth)

	topy += txth*3
	draw_text_width(5,topy,"Be careful. The timer goes down per thrust, so at high battle speeds you won't have much time. | 注意してください。 タイマーは女の子が腰振りをするたびにダウンするため、戦闘速度が速い場合は時間がすぐに0になります。",boxwidth-10,txth)
	draw_set_color(c_white)
	topy += txth*4
	draw_text_width(5,topy,"Overall damage received is lower at higher battle speeds to compensate for the increase in speed. | 戦闘速度が速いほど受けるダメージは全体的に少なくなり、速度の増加を補います。",boxwidth-10,txth)


	draw_set_color(c_white)
	draw_set_font(global.big_font)


}
