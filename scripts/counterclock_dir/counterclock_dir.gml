///@function counterclock_dir(string_dir)
///@description Gets the counter-clockwise direction of the string
///@param string_dir - The direction string
function counterclock_dir(argument0) {
	var string_dir;
	string_dir = argument0
	if(string_dir = "U")return "L"
	if(string_dir = "L")return "D"
	if(string_dir = "D")return "R"
	if(string_dir = "R")return "U"
	return ""



}
