///@function side_dir(string_dir)
///@description Gets the side direction of the string
///@param string_dir - The direction string
function side_dir(argument0) {
	var string_dir;
	string_dir = argument0
	if(string_dir = "U")return "L"
	if(string_dir = "L")return "U"
	if(string_dir = "D")return "R"
	if(string_dir = "R")return "D"
	return ""



}
