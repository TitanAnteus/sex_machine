/// @func ATEX_mode_is_enabled(mode_index)
/// @arg {real} mode_index Index of current mode
function ATEX_mode_is_enabled(argument0) {

	return argument0 & ATEX_mode > 0


}
