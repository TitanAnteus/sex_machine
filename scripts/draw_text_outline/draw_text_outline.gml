/// @function draw_text_outline(x,y,string,color,outline_color)
/// @description Draws a string at a color with an outline
/// @param x x position of string
/// @param y y position of string
/// @param string string to draw
/// @param color inner color of text
/// @param outline_color outline color of text
function draw_text_outline(argument0, argument1, argument2, argument3, argument4) {

	_oldcolor = draw_get_color();

	// Draw outline
	draw_set_color(argument4);
	draw_text(argument0-1,argument1,argument2);
	draw_text(argument0+1,argument1,argument2);
	draw_text(argument0,argument1+1,argument2);
	draw_text(argument0,argument1-1,argument2);

	// Draw text
	draw_set_color(argument3);
	draw_text(argument0,argument1,argument2);

	draw_set_color(_oldcolor)




}
