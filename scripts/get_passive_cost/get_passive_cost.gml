/// @function get_passice_cost(passivename)
/// @description returns the cost of the passive
/// @param passivename - The name of the passive
function get_passive_cost(argument0) {
	var pname;
	pname = string(argument0)
	if(string_count("Slow Lover",pname) > 0)return 3
	if(string_count("Triple Stamina",pname) > 0)return 1
	if(string_count("Unbreakable",pname) > 0)return 3
	if(string_count("Odd Calm",pname) > 0)return 2
	if(string_count("Stamina Speed",pname) > 0)return 1
	if(string_count("Sensitive",pname) > 0)return -1
	if(string_count("Panic Attack",pname) > 0)return -2
	if(string_count("Strong Will",pname) > 0)return 2
	if(string_count("Rhythm",pname) > 0)return 2
	if(string_count("Keen Eye",pname) > 0)return 4
	if(string_count("Shaky Hips",pname) > 0)return -2
	if(string_count("Shaky Hands",pname) > 0)return -2
	if(string_count("Conviction",pname) > 0)return 3
	return 0



}
