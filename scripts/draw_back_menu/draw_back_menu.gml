/// @function draw_title_menu()
/// @description Draws the item menu
function draw_back_menu() {

	var boxwidth,boxheight,topy,mpad,descheight,txth;
	boxwidth = rightwidth
	boxheight = rightheight
	descheight = 250
	topy = 20
	mpad = 50

	draw_set_font(global.big_font)
	draw_set_halign(fa_center)
	draw_set_valign(fa_top)
	draw_text(boxwidth/2,topy,string_split("Back | 戻る"))
	draw_set_valign(fa_center)
	draw_line(10,topy+mpad,boxwidth-10,topy+mpad)
	topy = topy+mpad+2
	txth = string_height("H")+2

	if(draw_get_color() != c_white)draw_set_color(c_white)

	draw_text_width(boxwidth/2,topy+((boxheight-topy)/2),string_split("Go back a screen | 画面に戻る"),boxwidth-10,txth)


}
