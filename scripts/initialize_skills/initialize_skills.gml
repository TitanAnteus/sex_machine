///@function initialize_skills()
///@description Database for all the player skills
function initialize_skills() {
	global.skill_list = ds_list_create()

	global.skill_map = ds_map_create()
	ds_map_add(global.skill_map,"Slow | 鈍化","Slows the battle speed by 1-2. | 戦闘速度を1-2遅くします。")
	ds_map_add(global.skill_map,"Calm | 落ち着く","Reduces battle speed in the next turn by \n1 for every perfect guard. | 完璧なガードごとに、次のターンの戦闘速度を1ずつ減らします。")
	ds_map_add(global.skill_map,"Focus | 集中","Take less damage in battle for 1 turn. | 戦闘中のダメージが少なくなります。")
	ds_map_add(global.skill_map,"Meditate | 瞑想","Lowers Lust by 35% | 欲望を35％下げる")
	ds_map_add(global.skill_map,"Psych Up | サイケアップ","Restores 15-50% of Willpower, and increases battle speed. \nHeals more with lower hearts. | 意志力の15-50％を回復し、速度を上げます。\nより低い心でより多くを癒します。")
	ds_map_add(global.skill_map,"Ready | 角郷","For 1 turn, all damage to the enemy \nincreases stamina instead. Slightly increase defense. | 1ターンの間、敵へのすべてのダメー\nジは代わりにスタミナを増加させます。\n防御力を少し上げます。")
	ds_map_add(global.skill_map,"Pressure | 圧力","Deal 1.5x damage for 1 turn. \nIncrease battle speed by 2. | 1ターンに1.5倍のダメージを与えます。 \n戦闘速度を2増加させる。")
	ds_map_add(global.skill_map,"Victory Cry | 勝利の叫ぶ","If you win in the next turn, \nyou will slightly heal willpower during the ejaculation, regain all stamina and gain more bits.\nSlightly Increase Damage Dealt. | 次のターンで勝った場合、\n射精中に意志力をわずかに回復し、すべてのスタミナを取り戻し、より多くのビットを獲得します。\nすべてのスタミナが回復します。")
	ds_map_add(global.skill_map,"Surrender | 降伏","Give up. | あきらめる。")

	global.skill_cost = ds_map_create()
	ds_map_add(global.skill_cost,"Slow | 鈍化",10)
	ds_map_add(global.skill_cost,"Calm | 落ち着く",25)
	ds_map_add(global.skill_cost,"Focus | 集中",15)
	ds_map_add(global.skill_cost,"Meditate | 瞑想",50)
	ds_map_add(global.skill_cost,"Psych Up | サイケアップ",50)
	ds_map_add(global.skill_cost,"Ready | 角郷",5)
	ds_map_add(global.skill_cost,"Pressure | 圧力",20)
	ds_map_add(global.skill_cost,"Victory Cry | 勝利の叫ぶ",-1)
	ds_map_add(global.skill_cost,"Surrender | 降伏",0)



}
