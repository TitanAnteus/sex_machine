/// @function draw_meter(yoffset,width,color)
/// @description Draws a battle meter
/// @param yoffset - The offset to draw the meter
/// @param width - The width of the meter
/// @param color - The color of the meter
function draw_meter(argument0, argument1, argument2) {
	var my;
	my = meter_y+argument0
	draw_set_color(c_dkgray)
	draw_rectangle(round(meter_x),round(my),round(meter_x+meter_width),round(my+meter_height),0)

	if(argument1 > .1)
		{
		draw_set_color(argument2)
		draw_rectangle(round(meter_x),round(my),round(meter_x+argument1),round(my+meter_height),0)
		}
	
	draw_set_color(c_white)
	draw_rectangle(round(meter_x),round(my),round(meter_x+meter_width),round(my+meter_height),1)


}
