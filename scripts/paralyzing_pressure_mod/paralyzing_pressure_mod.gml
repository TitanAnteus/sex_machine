///@function paralyzing_pressure_mod()
///@description Returns a high value when Paralyzing Pressure is a good skill to use.
function paralyzing_pressure_mod(argument0) {
	var ea,chk,skl;
	ea = temp_enemy_action
	skl = argument0

	chk = ds_map_find_value(ea,skl)
	if(chk = undefined)return false


	if(mxpattern+speed_increase <= 4)
		{
		chk += 80
		}

	if(global.willpower < global.mxwillpower/2)chk +=20



	if(mxpattern+speed_increase > 5)
		{
		chk = -10
		}
	if(state_count(enemy_state,"Paralyzing Pressure") > 0)
		{
		chk = -100
		}
	
	ds_map_replace(ea,skl,clamp(chk,-10,500))
	show_debug_message(skl+" : "+string(chk)+"/"+string(ds_map_find_value(cenemy.enemy_action,skl)))
	return true


}
