/// @function draw_passive_menu()
/// @description Draws the item menu
function draw_passive_menu() {

	var boxwidth,boxheight,topy,mpad,descheight,txth;
	boxwidth = rightwidth
	boxheight = rightheight
	descheight = 250
	topy = 20
	mpad = 50

	draw_set_font(global.big_font)
	draw_set_halign(fa_center)
	draw_set_valign(fa_top)
	draw_text(boxwidth/2,topy,string_split("Passives | パッシブ"))

	draw_line(10,topy+mpad,boxwidth-10,topy+mpad)
	topy = topy+mpad+2
	draw_set_font(global.main_font)
	draw_set_halign(fa_left)
	txth = string_height("H")+2

	if(draw_get_color() != c_white)draw_set_color(c_white)

	draw_text_width(5,topy,"Passives are abilities that activate automatically. | パッシブとは自動的にアクティブになる能力です。",boxwidth-10,txth)
	topy += txth*3
	draw_text_width(5,topy,"They all have unique activation conditions. | それらはすべて固有のアクティブ条件を持っています。",boxwidth-10,txth)
	topy += txth*3
	draw_set_color(c_red)
	draw_text_width(5,topy,"Not all of them help you however. Be wary of meeting the activation conditions of negative Passives. | ただし、中にはこちらにとって不利になるネガティブパッシブが存在します。 ネガティブパッシブのアクティブ条件に注意してください。",boxwidth-10,txth)
	draw_set_color(c_white)

	topy += txth*4
	draw_text_width(5,topy,"You can check which Passives you have access to using the Passive command in the command menu. | コマンドメニューの[パッシブ]コマンドを使用してアクセスできるパッシブを確認できます。",boxwidth-10,txth)
	topy += txth*4
	draw_text_width(5,topy,"Do your best to meet the positive activation conditions to give yourself an edge in battle. | 積極的に有利なパッシブのアクティブ条件を満たして戦闘で優位に立ちましょう。",boxwidth-10,txth)


	draw_set_color(c_white)
	draw_set_font(global.big_font)


}
