/// @function draw_enemy_menu()
/// @description Draws the enemy menu
function draw_enemy_menu() {

	var boxwidth,boxheight,topy,mpad,descheight,scl;
	boxwidth = rightwidth
	boxheight = rightheight
	descheight = 300
	topy = 20
	mpad = 50
	scl = 1.5

	draw_set_halign(fa_center)
	draw_set_valign(fa_top)
	draw_text(boxwidth/2,topy,string_split("Enemies | 敵"))

	draw_line(10,topy+mpad,boxwidth-10,topy+mpad)

	var icw,ich,leftmost;
	icw = sprite_get_width(spr_sadist_icon)
	ich = icw
	leftmost = 120

	draw_set_halign(fa_left)
	draw_set_valign(fa_center)
	topy = topy+mpad
	if(draw_get_color() != c_white)draw_set_color(c_white)

	var citem,h,k,ww,hh,hor,leftx,lsize,cpos_y,isize;
	hor = itemhor
	ww = boxwidth/hor
	hh = font_get_size(global.big_font)+8
	leftx = 20
	draw_set_halign(fa_left)
	draw_set_valign(fa_center)

	lsize = ds_list_size(global.enemy_list)
	var mxh,sh;
	mxh = 3
	sh = 0
	cpos_y = -(mxh/2)
	if(dpos > 0)and(lsize > mxh)
		{
		cpos_y += dpos
		cpos_y = clamp(cpos_y,0,lsize-mxh)
		}
	else
		{
		cpos_y = 0
		}

	if(! surface_exists(enemysurf))
		{
		enemysurf = surface_create(boxwidth,boxheight-topy-descheight)
		}
	else
		{
		surface_set_target(enemysurf)
		draw_clear_alpha(c_white,0)
		var col;
		col = c_white
		for(i=0;i<ds_list_size(global.enemy_list);i+=1)
			{
			citem = get_portrait(english_split(ds_list_find_value(global.enemy_list,i)))
			if(sprite_exists(citem))
				{
				sh = sprite_get_height(citem)*scl
				draw_sprite_ext(citem,0,5,3+(sh*i)-(cpos_y*sh),scl,scl,0,c_white,1)
				col = c_white
				if(selecting = true)
				and(dpos = i)col = c_red
				draw_set_color(col)
				draw_text(8+(sprite_get_width(citem)*scl),3+(sh*(i+.5))-(cpos_y*sh)," - "+string_split(global.enemy_list[| i]))
				}
			}
		surface_reset_target()
		draw_surface(enemysurf,0,topy)
		}

	if(draw_get_color() != c_white)draw_set_color(c_white)
	topy = boxheight-descheight
	draw_line(10,topy,boxwidth-10,topy)
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)
	draw_set_font(global.main_font)
	hh = string_height("H")
	if(ds_list_size(global.enemy_list) > 0)
	and(selecting = true)
		{
		citem = global.enemy_list[| dpos]
		draw_text_width(8,topy+8,get_enemy_description(citem),boxwidth-16,hh)
		}



}
