function save_game() {
	ini_open(global.saveloc)
	ini_write_string("Main","location",room_get_name(room))
	ini_write_real("Main","MaxPP",global.maxPP)
	ini_write_real("Main","MaxItems",global.mxitems)

	ini_write_real("Main","Willpower",global.willpower)
	ini_write_real("Main","MXWillpower",global.mxwillpower)
	ini_write_real("Main","MXWillpowerbase",global.mxwillpowerbase)

	ini_write_real("Main","Stamina",global.stamina)
	ini_write_real("Main","MXStamina",global.mxstamina)
	ini_write_real("Main","MXStaminabase",global.mxstaminabase)

	ini_write_real("Main","Lust",0)
	ini_write_real("Main","MXLust",global.mxcum)
	ini_write_real("Main","MXLustbase",global.mxcumbase)

	ini_write_real("Main","Strength",global.strength)
	ini_write_real("Main","Strengthbase",global.strengthbase)

	ini_write_real("Main","STMR",global.stamina_regen)
	ini_write_real("Main","STMRbase",global.stamina_regenbase)

	ini_write_real("Main","Bits",global.bits)
	
	ini_write_real("FirstCheck","BattlePosition",global.battleposition)
	ini_write_real("FirstCheck","FirstLoss",global.firstloss)
	ini_write_real("FirstCheck","FirstPassive",global.firstpassive)
	ini_write_real("FirstCheck","FirstOrdina",global.firstordina)
	
	ini_write_real("Main","Random",global.seed)
	
	with(obj_player)
		{
		//show_debug_message(object_get_name(id.object_index)+" | "+string(id)+"|X| "+string(id.x)+"|Y| "+(string(id.y)))
		var saveid;
		saveid = "Igrec"
		ini_write_real(saveid,"X",x)
		ini_write_real(saveid,"Y",y)
		ini_write_real(saveid,"D",direction)
		}
	ini_write_string("Item","Equip",global.equipment)

	ini_write_real("Item","total",ds_list_size(global.item_list))
	for(i=0;i<ds_list_size(global.item_list);i+=1)
		{
		ini_write_string("Item","item "+string(i),global.item_list[| i])
		}
	
	ini_write_real("Active Assist","total",ds_list_size(global.assist_battle))
	for(i=0;i<ds_list_size(global.assist_battle);i+=1)
		{
		ini_write_string("Active Assist","assist "+string(i),global.assist_battle[| i])
		}

	ini_write_real("Equip","total",ds_list_size(global.equip_list))
	for(i=0;i<ds_list_size(global.equip_list);i+=1)
		{
		ini_write_string("Equip","equip "+string(i),global.equip_list[| i])
		}
	
	ini_write_real("Skill","total",ds_list_size(global.skill_list))
	for(i=0;i<ds_list_size(global.skill_list);i+=1)
		{
		ini_write_string("Skill","skill "+string(i),global.skill_list[| i])
		}
	
	var cskl;
	cskl = ds_map_find_first(global.all_passive)
	for(i=0;i<ds_map_size(global.all_passive);i+=1)
		{
		ini_write_real(english_split(cskl),ds_map_find_value(global.all_passive,cskl),ds_map_exists(global.equip_passive,cskl))
		cskl = ds_map_find_next(global.all_passive,cskl)
		}
	
	var ename,cool;
	for(i=0;i<ds_list_size(global.assist_list);i+=1)
		{
		ename = global.assist_list[| i]
		cool = ds_map_find_value(global.assist_cooldown,ename)
		if(! is_undefined(cool))
			{
			ini_write_real("Cooldown",english_split(ename),cool)
			}
		}
	
	ini_close()
	
	
	file_copy(global.saveloc,global.fsaveloc)
	audio_play_sound(sfx_save,1,0)

	if(surface_exists(blursurf))
		{
		surface_save(blursurf,working_directory+"save.png")
		if(sprite_exists(save_sprite))sprite_delete(save_sprite)
		save_sprite = sprite_add(working_directory+"save.png",1,0,0,0,0)
		}
	else
		{
		surface_save(application_surface,working_directory+"save.png")
		}

}
