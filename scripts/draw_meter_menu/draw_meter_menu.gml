/// @function draw_item_menu()
/// @description Draws the item menu
function draw_meter_menu() {

	var boxwidth,boxheight,topy,mpad,descheight,txth;
	boxwidth = rightwidth
	boxheight = rightheight
	descheight = 250
	topy = 20
	mpad = 50

	draw_set_font(global.big_font)
	draw_set_halign(fa_center)
	draw_set_valign(fa_top)
	draw_text(boxwidth/2,topy,string_split("Meters | メーター"))

	draw_line(10,topy+mpad,boxwidth-10,topy+mpad)
	topy = topy+mpad+2
	draw_set_font(global.main_font)
	draw_set_halign(fa_left)
	txth = string_height("H")+2

	draw_text_width(5,topy,"There are 3 meters in the game: | ゲームには3つのメーターがあります：",boxwidth-10,txth)
	draw_set_color(c_red)
	topy += txth
	draw_text_width(5,topy,"The Red Meter is WILLPOWER | 赤いメーターは意志力です",boxwidth-10,txth)
	draw_set_color(c_white)
	topy += txth
	draw_text_width(5,topy,"This is how willing Igrec is to fight. If it is reduced to 0, Igrec loses the battle. | これがイグレックの体力です。 0になるとイグレックは戦闘で敗北します。",boxwidth-10,txth)


	draw_set_color(c_lime)
	topy += txth*3
	draw_text_width(5,topy,"The Green Meter is STAMINA | 緑のメーターはスタミナです",boxwidth-10,txth)
	draw_set_color(c_white)
	topy += txth
	draw_text_width(5,topy,"Stamina is used for skills. Igrec regains some stamina for every diamond guessed. You gain more by getting the arrows correct. | スタミナはスキルで使用します。 戦闘で矢印を入力することでスタミナは回復します。 矢印を正しく入力することでより多く回復できます。",boxwidth-10,txth)

	draw_set_color(c_ltgray)
	topy += txth*4
	draw_text_width(5,topy,"The White Meter is LUST | 白いメーターは欲望です",boxwidth-10,txth)
	draw_set_color(c_white)
	topy += txth
	draw_text_width(5,topy,"Lust is how close you are to cumming. This will go up for every diamond guessed. You gain more by getting the arrows incorrect. | 欲望はあなたが射精にどれだけ近いかを表します。 戦闘で矢印を入力することで上昇していきます。 矢印を間違えると正解した際よりも早く上昇してしまいます。",boxwidth-10,txth)

	draw_set_color(c_white)
	topy += txth*5
	draw_text_width(5,topy,"In short, keep the Red Bar high. Don't let the Green Bar be too high or too low. Keep the White Bar low. | 赤いバーは高い状態を維持してください。 緑のバーが低すぎるとスキルが使用できません。 白いバーは低い状態にして射精を抑えてください。これらを意識して戦いましょう。",boxwidth-10,txth)


	draw_set_color(c_white)
	draw_set_font(global.big_font)


}
