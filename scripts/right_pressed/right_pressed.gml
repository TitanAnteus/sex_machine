/// @function right_pressed()
/// @description Checks if right key has been pressed
function right_pressed() {
	if(keyboard_check_pressed(global.k_right))return 1
	return 0


}
