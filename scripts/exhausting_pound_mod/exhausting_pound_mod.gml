///@function exhausting_pound_mod()
function exhausting_pound_mod(argument0) {
	var ea,chk,skl;
	ea = temp_enemy_action
	skl = argument0

	chk = ds_map_find_value(ea,skl)
	if(chk = undefined)return false

	if(mxpattern > cenemy.min_speed+2)
		{
		chk += 50
		}
	if(global.stamina > global.mxstamina*.5)
		{
		chk += 20
		}
	if(global.stamina < global.mxstamina*.6)
	and(global.stamina > global.mxstamina*.3)
		{
		chk += 25
		}

	ds_map_replace(ea,skl,clamp(chk,0,500))
	show_debug_message(skl+" : "+string(chk)+"/"+string(ds_map_find_value(cenemy.enemy_action,skl)))
	return true


}
