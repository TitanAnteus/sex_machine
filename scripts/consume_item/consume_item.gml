/// @function consume_item(position)
/// @description Consumes an item and runs its code
/// @param position - The position in the itemlist
function consume_item(argument0) {
	var pos,nme,scr;
	pos = argument0
	nme = "item_"+string_replace(english_split(string_lower(global.item_list[| pos]))," ","_")
	scr = asset_get_index(nme)
	if(script_exists(scr))
		{
		if(script_execute(scr))
			{
			ds_list_delete(global.item_list,pos)
			dmax = ds_list_size(global.item_list)
			dpos = clamp(dpos,0,dmax)
			global.willpower = clamp(global.willpower,0,global.mxwillpower)
			global.stamina = clamp(global.stamina,0,global.mxstamina)
			global.cum = clamp(global.cum,0,global.mxcum)
			return true
			}
		else
			{
			audio_play_sound(sfx_fail,1,0)
			return false
			}
		}
	else
		{
		audio_play_sound(sfx_fail,1,0)
		return false
		}


}
