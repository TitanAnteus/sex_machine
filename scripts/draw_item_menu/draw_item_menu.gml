/// @function draw_item_menu()
/// @description Draws the item menu
function draw_item_menu() {

	var boxwidth,boxheight,topy,mpad,descheight;
	boxwidth = rightwidth
	boxheight = rightheight
	descheight = 250
	topy = 20
	mpad = 50

	draw_set_halign(fa_center)
	draw_set_valign(fa_top)
	draw_text(boxwidth/2,topy,string_split("Items | アイテム"))

	draw_line(10,topy+mpad,boxwidth-10,topy+mpad)

	var icw,ich,leftmost;
	icw = sprite_get_width(spr_sadist_icon)
	ich = icw
	topy = topy+mpad
	leftmost = 120
	draw_set_halign(fa_right)
	draw_set_valign(fa_center)
	if(draw_get_color() != c_white)draw_set_color(c_white)
	draw_text(leftmost-1,topy+10+(ich/2),"Mod:")
	draw_rectangle(leftmost,topy+10,leftmost+icw,topy+10+ich,1)
	var equip_icon;
	equip_icon = asset_get_index(string_lower("spr_"+string_replace(english_split(global.equipment)," ","_")+"_icon"))
	if(sprite_exists(equip_icon))draw_sprite(equip_icon,0,leftmost+1,topy+11)

	if(draw_get_color() != c_white)draw_set_color(c_white)
	if(dpos = 0)
	and(selecting = true)draw_set_color(c_red)
	if(equip_select = true)draw_set_color(c_yellow)

	draw_set_halign(fa_left)
	draw_set_valign(fa_center)
	if(global.equipment = ""){draw_text(leftmost+icw+10,topy+10+(ich/2),string_split("<Empty> | <無し>"))}
	else{draw_text(leftmost+icw+10,topy+10+(ich/2),string_split(global.equipment))}
	topy = topy+mpad
	if(draw_get_color() != c_white)draw_set_color(c_white)
	draw_line(10,topy,boxwidth-10,topy)

	var citem,h,k,ww,hh,hor,leftx,lsize,cpos_y,isize;
	hor = itemhor
	ww = boxwidth/hor
	hh = font_get_size(global.big_font)+8
	leftx = 20
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)

	lsize = ds_list_size(item_menu)
	isize = (ds_list_size(item_menu)+1) div hor
	cpos_y = -5
	if(dpos > 0)and(isize > 7)
		{
		cpos_y += (dpos-1) div hor
		cpos_y = clamp(cpos_y,0,isize-7)
		}
	else
		{
		cpos_y = 0
		}

	if(! surface_exists(itemsurf))
		{
		itemsurf = surface_create(boxwidth,boxheight-topy-descheight)
		}
	else
		{
		surface_set_target(itemsurf)
		draw_clear_alpha(c_white,0)
		var col,clower;
		col = c_white
		for(i=0;i<ds_list_size(item_menu);i+=1)
			{
			citem = string_split(ds_list_find_value(item_menu,i))
			clower = string_lower(english_split(ds_list_find_value(item_menu,i)))
			h = i mod hor
			k = i div hor
			col = c_white
			if(asset_get_index("item_"+string_replace(clower," ","_")) == -1)
			and(equip_select = false)col = c_ltgray
		
			if(dpos = i+1)
			and(selecting = true)
				{
				col = c_red
				if(asset_get_index("item_"+string_replace(clower," ","_")) == -1)
				and(equip_select = false)col = merge_color(c_black,c_red,.5)
				}
			if(draw_get_color() != col)draw_set_color(col)
			draw_text(leftx+(h*ww),14+(k*hh)-(cpos_y*hh),citem)
			}
			
		surface_reset_target()
		draw_surface(itemsurf,0,topy)
		}

	if(draw_get_color() != c_white)draw_set_color(c_white)
	topy = boxheight-descheight
	draw_line(10,topy,boxwidth-10,topy)
	draw_set_halign(fa_center)
	draw_set_valign(fa_center)
	if(dpos > 0)
		{
		citem = ds_list_find_value(item_menu,dpos-1)
		}
	else
		{
		citem = "<Empty> | <無し>"
		}
	draw_text_width(boxwidth/2,boxheight-(descheight/2),get_item_description(citem),boxwidth-3,hh)

	draw_set_halign(fa_right)
	draw_set_valign(fa_top)
	draw_text_width(boxwidth-95,boxheight-descheight,string(ds_list_size(global.item_list))+"/"+string(global.mxitems),boxwidth-4,32)



}
