/// @function draw_battle1_menu()
/// @description Draws the item menu
function draw_battle1_menu() {

	var boxwidth,boxheight,topy,mpad,descheight,txth;
	boxwidth = rightwidth
	boxheight = rightheight
	descheight = 250
	topy = 20
	mpad = 50

	draw_set_font(global.big_font)
	draw_set_halign(fa_center)
	draw_set_valign(fa_top)
	draw_text(boxwidth/2,topy,string_split("Battle 1 | バトル1"))

	draw_line(10,topy+mpad,boxwidth-10,topy+mpad)
	topy = topy+mpad+2
	draw_set_font(global.main_font)
	draw_set_halign(fa_left)
	txth = string_height("H")+2

	draw_text_width(5,topy,"Battles in Sex Machine are a bit different. | Sex Machineでの戦闘は少し特殊です。",boxwidth-10,txth)
	topy += txth*2
	draw_text_width(5,topy,"In order to do well in battle, you have to match the enemy's arrows with your own. The arrows appear inside two different types of diamonds. | 戦闘でうまく戦うためには、自分の入力する矢印を敵の矢印と合わせる必要があります。 矢印は2種類のダイヤモンドの中に表示されます。",boxwidth-10,txth)

	draw_set_color(c_pink)
	topy += txth*5
	draw_text_width(5,topy,"Pink Diamonds are random diamonds | ピンクのダイヤモンドは矢印の表示がランダムです",boxwidth-10,txth)
	draw_set_color(c_white)
	topy += txth
	draw_text_width(5,topy,"These diamonds are dangerous, as you can never predict what direction they'll be. | このダイヤモンドはどの方向に矢印が向くかを予測することはできないため注意が必要です。",boxwidth-10,txth)

	draw_set_color(c_ltgray)
	topy += txth*3
	draw_text_width(5,topy,"White Diamonds are habits | 白のダイヤモンドには女の子ごとにパターンが存在します。",boxwidth-10,txth)
	draw_set_color(c_white)
	topy += txth
	draw_text_width(5,topy,"These diamonds show a pattern. Every girl reuses the same directions in a pattern. | すべての女の子は独自のパターンで矢印を表示し、全て表示し終えると再び最初から同じパターンを繰り返します。",boxwidth-10,txth)

	topy += txth*3
	draw_text_width(5,topy,"The current pattern is shown by a small square in the top left of the action window. | 現在のパターンはアクションウィンドウの左上に小さな正方形で表示されます。",boxwidth-10,txth)
	draw_set_color(c_white)


	draw_set_color(c_white)
	draw_set_font(global.big_font)


}
