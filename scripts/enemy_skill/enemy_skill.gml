/// @function enemy_skill(skill)
/// @description Executes the enemy skill
function enemy_skill(argument0) {
	var skl,csp;
	skl = argument0
	csp = mxpattern+speed_increase
	if(string_count("Wait",skl) > 0)
		{
		//Do nothing
		}
	if(string_count("Hyper",skl) > 0)
		{
		audio_play_sound(sfx_powerup,1,0)
		hyper_speed = 1+floor(random(3))
		speed_increase += hyper_speed
		}
	if(string_count("Energetic",skl) > 0)
		{
		audio_play_sound(sfx_powerup,1,0)
		speed_increase += 2
		}
	if(string_count("Overheat",skl) > 0)
		{
		audio_play_sound(sfx_overheat,1,0)
		if(csp <= 8)
			{
			speed_increase = 8-mxpattern
			}
		if(check_estate("Overheat") == false)
			{
			ds_list_add(enemy_state,"Overheat")
			}
		}
	if(string_count("Superheat",skl) > 0)
		{
		audio_play_sound(sfx_overheat,1,0)
		if(csp <= 10)
			{
			speed_increase = 10-mxpattern
			}
		if(check_estate("Superheat") == false)
			{
			repeat(2){ds_list_add(enemy_state,"Superheat")}
			}
		}
	if(string_count("Unpredictable",skl) > 0)
		{
		audio_play_sound(sfx_random,1,0)
		ds_list_add(enemy_state,"Unpredictable")
		}
	if(string_count("Sadist",skl) > 0)
		{
		ds_list_add(enemy_state,"Sadist")
		sadist_count = 1
		if(floor(random(100)) < 10)
			{
			ds_list_add(enemy_state,"Sadist")
			sadist_count = 2
			}
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Heart Piercing Smile",skl) > 0)
		{
		audio_play_sound(sfx_powerdown,1,0)
		global.stamina -= (35)+floor(random(15))	
		}
	if(string_count("Loving Caress",skl) > 0)
		{
		ds_list_add(enemy_state,"Loving Caress")
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Rising Intensity",skl) > 0)
		{
		var rp;
		rp = clamp(5-state_count(enemy_state,"Rising Intensity"),0,5)
		repeat(rp){ds_list_add(enemy_state,"Rising Intensity")}
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Paralyzing Pressure",skl) > 0)
		{
		ds_list_add(enemy_state,"Paralyzing Pressure")
		audio_play_sound(sfx_powerup,1,0)
		}
	
	
	if(string_count("Playful Spirit",skl) > 0)
		{
		speed_increase = clamp(floor(random(cenemy.max_speed))-floor(random(cenemy.max_speed)),array_length_2d(cenemy.habit,0)-mxpattern,cenemy.max_speed-mxpattern)
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Merciless",skl) > 0)
		{
		ds_list_add(enemy_state,"Merciless")
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Compassion",skl) > 0)
		{
		cenemy.willpower += (cenemy.max_speed-(csp))*3.75
		audio_play_sound(sfx_heal,1,0)
		}
	if(string_count("Stamina Pit",skl) > 0)
		{
		var rp,nm;
		nm = 3
		rp = clamp(nm-state_count(enemy_state,"Stamina Pit"),0,nm)
		repeat(rp){ds_list_add(enemy_state,"Stamina Pit")}
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Powerful Grip",skl) > 0)
		{
		var rp;
		rp = clamp(3-state_count(enemy_state,"Powerful Grip"),0,3)
		repeat(rp){ds_list_add(enemy_state,"Powerful Grip")}
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Taunt",skl) > 0)
		{
		audio_play_sound(sfx_surprise,1,0)
		if(ds_list_find_index(enemy_state,"Taunt") == -1)
			{
			ds_list_add(enemy_state,"Taunt")
			}
		}
	
	
	if(string_count("Decouple",skl) > 0)
		{
		cenemy.battle_offset = 2
		sprite_index = battle[cbat+cenemy.battle_offset]
		highlight = 1
		with(cenemy)
			{
			habit[0,0] = habit2[0,0]
			habit[0,1] = habit2[0,1]
			habit[0,2] = habit2[0,2]
			habit[0,3] = habit2[0,3]

			habit[1,0] = habit2[1,0]
			habit[1,1] = habit2[1,1]
			habit[1,2] = habit2[1,2]
			habit[1,3] = habit2[1,3]
	
			habit[2,0] = habit2[2,0]
			habit[2,1] = habit2[2,1]
			habit[2,2] = habit2[2,2]
			habit[2,3] = habit2[2,3]
		
			enemy_action = enemy_action2
			enemy_text = enemy_text2
			enemy_mod = enemy_mod2
			}
	
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Exhausting Pound",skl) > 0)
		{
		ds_list_add(enemy_state,"Exhausting Pound")
		audio_play_sound(sfx_powerup,1,0)
		}
	
	if(string_count("Tightening",skl) > 0)
		{
		var dif;
		dif = max(0,csp-cenemy.min_speed)*7
		global.cum += 35-dif
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Leeway",skl) > 0)
		{
		var dif;
		dif = max(0,csp-cenemy.min_speed)
		speed_increase = -100
		cenemy.willpower += dif*10
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Self Control",skl) > 0)
		{
		cenemy.chab = floor(random(array_height_2d(cenemy.habit)))
		cenemy.willpower += 7+floor(random(3))-floor(random(3))
		audio_play_sound(sfx_random,1,0)
		}
	if(string_count("Heavy",skl) > 0)
		{
		ds_list_add(enemy_state,"Heavy")
		ds_list_add(enemy_state,"Heavy")
		ds_list_add(enemy_state,"Heavy")
		ds_list_add(enemy_state,"Heavy")
		audio_play_sound(sfx_drop_cloth,1,0)
		}
	if(string_count("Lewd Kiss",skl) > 0)
		{
		ds_list_add(toadd_state,"Blind")
		speed_increase += 2+floor(random(2))
		audio_play_sound(sfx_kiss,1,0)
		}
	if(string_count("Concern",skl) > 0)
		{
		ds_list_add(toadd_state,"Vision")
		ds_list_add(enemy_state,"Concern")
		audio_play_sound(sfx_concern,1,0)
		}
	if(string_count("Vaginal Control",skl) > 0)
		{
		repeat(4){ds_list_add(enemy_state,"Vaginal Control")}
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Passionate Reaction",skl) > 0)
		{
		repeat(6){ds_list_add(enemy_state,"Passionate Reaction")}
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Heavy Hips",skl) > 0)
		{
		ds_list_add(enemy_state,"Pressure")
		if(floor(random(5)) < 3)
			{
			ds_list_add(player_state,"Silence")
			}
		}
}
