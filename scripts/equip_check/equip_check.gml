/// @function equip_check()
/// @description Checks the currently equipped equipment and raises stats.
function equip_check() {
	var equipname,willadd,staminadd,lustadd,strengthadd,energyadd;
	equipname = string(global.equipment)
	willadd = 0  //To add onto mxwillpower
	staminadd = 0 //To add onto mxstamina
	lustadd = 0 //To add onto mxcum
	strengthadd = 0//To add onto strength
	energyadd = 0 //To add onto stamina_regen
	if(string_count("Iron Resistance",equipname) > 0)willadd += global.mxwillpowerbase*.5

	global.mxwillpower = global.mxwillpowerbase+willadd
	global.mxstamina = global.mxstaminabase+staminadd
	global.mxcum = global.mxcumbase+lustadd
	global.strength = global.strengthbase+strengthadd
	global.stamina_regen = global.stamina_regenbase+energyadd



}
