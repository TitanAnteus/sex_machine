/// @function add_skill(skill_name,base_mod,description,script_id)
/// @description Adds a skill to a character
/// @param {string} skill_name - String of skill name
/// @param {real} base_mod - Base number that decides whether skill is selected or not
/// @param {string} description - Description of Skill
/// @param {string} script_id - ID of the skill mod

function add_skill(skill_name,base_mod,description,script_id) {
	ds_map_add(enemy_action,skill_name,base_mod)
	ds_map_add(enemy_text,skill_name,description)
	ds_map_add(enemy_mod,skill_name,script_id)
}
