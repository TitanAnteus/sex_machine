/// @function draw_status_menu()
/// @description Draws the status menu
function draw_status_menu() {

	var boxwidth,boxheight,leftx,topy,mpad;
	boxwidth = rightwidth
	boxheight = rightheight

	topy = 200
	mpad = 50

	draw_set_halign(fa_center)
	draw_set_valign(fa_center)
	draw_text(boxwidth/2,boxheight*.2,string_split("Igrec | イグレック"))

	draw_line(10,topy-2,boxwidth-10,topy-2)
	draw_set_halign(fa_right)
	draw_set_valign(fa_top)

	leftx = boxwidth*.3

	draw_text(leftx,topy,"HP: ")
	draw_text(leftx,topy+mpad,"STM: ")
	draw_text(leftx,topy+(mpad*2),"LST: ")

	draw_line(10,topy+(mpad*3)+2,boxwidth-10,topy+(mpad*3)+2)

	var barx,barwidth;
	barx = leftx+4
	barwidth = boxwidth-barx-10

	draw_set_color(c_red)
	draw_rectangle(barx,topy+5,barx+(barwidth*(global.willpower/global.mxwillpower)),-5+topy+mpad,0)
	draw_set_color(c_lime)
	draw_rectangle(barx,topy+(mpad)+5,barx+(barwidth*(global.stamina/global.mxstamina)),-5+topy+(mpad*2),0)
	draw_set_color(c_white)
	draw_rectangle(barx,topy+(mpad*2)+5,barx+(barwidth*(global.cum/global.mxcum)),-5+topy+(mpad*3),0)

	draw_rectangle(barx,topy+5,barx+barwidth,-5+topy+mpad,1)
	draw_rectangle(barx,topy+(mpad)+5,barx+barwidth,-5+topy+(mpad*2),1)
	draw_rectangle(barx,topy+(mpad*2)+5,barx+barwidth,-5+topy+(mpad*3),1)

	var staty,dcol;
	staty = topy+(mpad*3)+15
	leftx = boxwidth*.5
	dcol = c_white
	if(selecting = true)dcol = c_red
	if(dpos = 0)
		{
		if(draw_get_color() != dcol)draw_set_color(dcol)
		draw_set_halign(fa_right)
		draw_text(leftx,staty,string_split("Strength : | パワー :"))
		draw_set_halign(fa_left)
		draw_text(leftx+2,staty,string(global.strength))
		}
	else
		{
		if(draw_get_color() != c_white)draw_set_color(c_white)
		draw_set_halign(fa_right)
		draw_text(leftx,staty,string_split("Strength : | パワー :"))
		draw_set_halign(fa_left)
		draw_text(leftx+2,staty,string(global.strength))
		}
	if(dpos = 1)
		{
		if(draw_get_color() != dcol)draw_set_color(dcol)
		draw_set_halign(fa_right)
		draw_text(leftx,staty+mpad,string_split("Energy : | エネルギー :"))
		draw_set_halign(fa_left)
		draw_text(leftx+2,staty+mpad,string(global.stamina_regen))
		}
	else
		{
		if(draw_get_color() != c_white)draw_set_color(c_white)
		draw_set_halign(fa_right)
		draw_text(leftx,staty+mpad,string_split("Energy : | エネルギー :"))
		draw_set_halign(fa_left)
		draw_text(leftx+2,staty+mpad,string(global.stamina_regen))
		}

	if(draw_get_color() != c_white)draw_set_color(c_white)
	draw_line(10,staty+(mpad*2)+8,boxwidth-10,staty+(mpad*2)+8)
	draw_set_halign(fa_center)
	draw_set_valign(fa_top)

	if(selecting = true)
		{
		var description;
		if(dpos = 0)description = "The base damage Igrec does to the enemy per action. | イグレックがアクションごとに敵に与える基本ダメージ。"
		if(dpos = 1)description = "The base amount of stamina Igrec recovers per action. | スタミナの基本量はアクションごとに回復します。"
		draw_text_width(boxwidth*.5,staty+(mpad*2)+14,string_split(description),boxwidth-10,font_get_size(global.big_font)+8)
		}


}
