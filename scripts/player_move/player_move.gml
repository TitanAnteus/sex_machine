/// @function player_move()
/// @description Checks inputs, and decides move speed
function player_move() {
	var key_index;
	if(state != "Cutscene")
		{
		if(left_direct())
			{
			tdirection = 180
			if(ds_list_find_index(key_list,tdirection) == -1)ds_list_add(key_list,tdirection)
			}
		if(! left_direct())
			{
			key_index = ds_list_find_index(key_list,180)
			if(key_index != -1)ds_list_delete(key_list,key_index)
			}
		if(right_direct())
			{
			tdirection = 0
			if(ds_list_find_index(key_list,tdirection) == -1)ds_list_add(key_list,tdirection)
			}
		if(! right_direct())
			{
			key_index = ds_list_find_index(key_list,0)
			if(key_index != -1)ds_list_delete(key_list,key_index)
			}
		if(up_direct())
			{
			tdirection = 90
			if(ds_list_find_index(key_list,tdirection) == -1)ds_list_add(key_list,tdirection)
			}
		if(! up_direct())
			{
			key_index = ds_list_find_index(key_list,90)
			if(key_index != -1)ds_list_delete(key_list,key_index)
			}
		if(down_direct())
			{
			tdirection = 270
			if(ds_list_find_index(key_list,tdirection) == -1)ds_list_add(key_list,tdirection)
			}
		if(! down_direct())
			{
			key_index = ds_list_find_index(key_list,270)
			if(key_index != -1)ds_list_delete(key_list,key_index)
			}
		}
	if(check_snapx_center())
	and(check_snapy_center())
		{
		
		if(state = "Cutscene")
			{
			move_speed = 0
			ds_list_clear(key_list)
			}
		
		if(ds_list_size(key_list) > 0)
		and(path_index == -1)
			{
			var newdir;
			newdir = key_list[| (ds_list_size(key_list)-1)]
			if(newdir != direction)
				{
				direction = newdir
				snap_x_center()
				snap_y_center()
				}
			}
		
		
		
		if(! left_direct())
		and(! right_direct())
		and(! down_direct())
		and(! up_direct())
			{
			move_speed = 0
			}
		else
			{
			var xto,yto;
			xto = x+lengthdir_x(global.grid,direction)
			yto = y+lengthdir_y(global.grid,direction)+(global.grid/2)

			if(! position_meeting(xto,yto,obj_wall))
			and(! position_meeting(xto,yto,obj_stop))
			and(! position_meeting(xto,yto,obj_block))
			and(ds_list_size(key_list) > 0)
				{
				move_speed = walk_speed
				}
			else
				{
				move_speed = 0
				}
		
			if(direction = 0)
				{
				if(position_meeting(x,y+(global.grid/2),obj_wall_r))
				or(position_meeting(x,y+(global.grid/2),obj_wall_ur))
				or(position_meeting(xto,y+(global.grid/2),obj_wall_l))
				or(position_meeting(xto,y+(global.grid/2),obj_wall_ul))move_speed = 0
				}
			if(direction = 180)
				{
				if(position_meeting(x,y+(global.grid/2),obj_wall_l))
				or(position_meeting(x,y+(global.grid/2),obj_wall_ul))
				or(position_meeting(xto,y+(global.grid/2),obj_wall_r))
				or(position_meeting(xto,y+(global.grid/2),obj_wall_ur))move_speed = 0
				}
			if(direction = 90)
				{
				if(position_meeting(x+2,y-(global.grid/2),obj_wall_u))
				or(position_meeting(x+2,y-(global.grid/2),obj_wall_ur))
				or(position_meeting(x+2,y-(global.grid/2),obj_wall_ul))move_speed = 0
				}
			if(direction = 270)
				{
				if(position_meeting(x+2,yto,obj_wall_u))
				or(position_meeting(x+2,yto,obj_wall_ur))
				or(position_meeting(x+3,yto,obj_wall_ul))move_speed = 0
				}
			}
		}
	if(move_speed = 0)
	and(path_index == -1)
		{
		snap_x_center()
		snap_y_center()
		}

}
