/// @function set_timer()
/// @description Sets the action timer for the turn
function set_timer(){
	var ttime;
	ttime = cenemy.turn_time
	
	if(check_estate("Paralyzing Pressure"))ttime = 12
	if(check_estate("Concern"))ttime = 12
	
	if(check_estate("Powerful Grip"))ttime /= 2
	if(check_estate("Rising Intensity"))
		{
		speed_increase += 1
		ttime /= 2
		}
	return round(ttime)
}