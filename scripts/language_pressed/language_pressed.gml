/// @function language_pressed()
/// @description Checks if the language key is pressed
function language_pressed() {
	if(keyboard_check_pressed(global.k_language))return 1
	return 0


}
