/// @function togrid(number)
/// @description Snaps a number to a grid
/// @param number - The number to snap
function togrid(argument0) {
	return (round(argument0) div global.grid)*global.grid


}
