function play_music() {
	if(global.bgm == -1)
		{
		audio_stop_sound(global.lastbgm)
		exit
		}
	if(! audio_is_playing(global.bgm))
		{
		audio_stop_sound(global.lastbgm)
		audio_play_sound(global.bgm,1,1)
		}
}
