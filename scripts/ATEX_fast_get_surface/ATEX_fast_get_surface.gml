/// @arg body
function ATEX_fast_get_surface(argument0) {

	var surf=argument0[? "surf"]
	if !surface_exists(surf)
	{
		ATEX_fast_update(argument0, false)
		surf=argument0[? "surf"]
	}
	return surf


}
