/// @function path_to(targetx, targety, speed)
/// @description This script creates a path to the target using the create_path function
/// @param targetx The final X position
/// @param targety The final Y position
/// @param speed The speed of the path
function path_to(argument0, argument1, argument2) {

	var cpath;
	cpath = create_path(x,y,argument0,argument1,global.grid,1,false,obj_wall,false)
	path_shift(cpath,-global.grid/2,-global.grid/2)
	path_change_point(cpath,0,x,y,100)
	path_change_point(cpath,path_get_number(cpath)-1,argument0,argument1,100)
	
	if(path_get_number(cpath) > 1)
		{
		path_start(cpath,argument2,path_action_stop,true)
		}
}
