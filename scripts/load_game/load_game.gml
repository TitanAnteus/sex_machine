function load_game() {
	var toroom,total;
	toroom = rm_area1_1
	if(file_exists(global.fsaveloc))
		{
		global.battle = 0
		global.gameover = 0
		file_copy(global.fsaveloc,global.saveloc)
		ds_map_clear(global.all_passive)
		ds_map_clear(global.equip_passive)
	
		ini_open(global.fsaveloc)
		toroom = ini_read_string("Main","location",room_get_name(rm_area1_1))
		toroom = asset_get_index(toroom)
		
		global.maxPP = ini_read_real("Main","MaxPP",global.maxPP)
		global.mxitems = ini_read_real("Main","MaxItems",global.mxitems)
	
		global.willpower = ini_read_real("Main","Willpower",global.willpower)
		global.mxwillpower = ini_read_real("Main","MXWillpower",global.mxwillpower)
		global.mxwillpowerbase = ini_read_real("Main","MXWillpowerbase",global.mxwillpowerbase)
	
		global.stamina = ini_read_real("Main","Stamina",global.stamina)
		global.mxstamina = ini_read_real("Main","MXStamina",global.mxstamina)
		global.mxstaminabase = ini_read_real("Main","MXStaminabase",global.mxstaminabase)
	
		global.cum = ini_read_real("Main","Lust",global.cum)
		global.mxcum = ini_read_real("Main","MXLust",global.mxcum)
		global.mxcumbase = ini_read_real("Main","MXLustbase",global.mxcumbase)

		global.strength = ini_read_real("Main","Strength",global.strength)
		global.stamina_regen = ini_read_real("Main","STMR",global.stamina_regen)
		global.strengthbase = ini_read_real("Main","Strengthbase",global.strengthbase)
		global.stamina_regenbase = ini_read_real("Main","STMRbase",global.stamina_regenbase)
	
	
		global.seed = ini_read_real("Main","Random",global.seed)
		global.equipment = ini_read_string("Item","Equip",global.equipment)
		
		global.bits = ini_read_real("Main","Bits",global.bits)
		
		global.battleposition = ini_read_real("FirstCheck","BattlePosition",0)
		global.firstloss = ini_read_real("FirstCheck","FirstLoss",true)
		global.firstpassive = ini_read_real("FirstCheck","FirstPassive",true)
		global.firstordina = ini_read_real("FirstCheck","FirstOrdina",true)
		global.difficulty = ini_read_real("Main","Difficulty",NORMAL)
	
		global.exhaust = global.mxstamina/3
	
	
		total = ini_read_real("Item","total",0)
		ds_list_clear(global.item_list)
		var citem;
		for(i=0;i<total;i+=1)
			{
			citem = ini_read_string("Item","item "+string(i),"")
			if(citem != "")ds_list_add(global.item_list,citem)
			}
		
		total = ini_read_real("Active Assist","total",0)
		ds_list_clear(global.assist_battle)
		var citem;
		for(i=0;i<total;i+=1)
			{
			citem = ini_read_string("Active Assist","assist "+string(i),"")
			if(citem != "")ds_list_add(global.assist_battle,citem)
			}
	
		total = ini_read_real("Equip","total",0)
		ds_list_clear(global.equip_list)
		var citem;
		for(i=0;i<total;i+=1)
			{
			citem = ini_read_string("Equip","equip "+string(i),"")
			if(citem != "")ds_list_add(global.equip_list,citem)
			}
		
		total = ini_read_real("Skill","total",0)
		ds_list_clear(global.skill_list)
		var citem;
		for(i=0;i<total;i+=1)
			{
			citem = ini_read_string("Skill","skill "+string(i),"")
			if(citem != "")ds_list_add(global.skill_list,citem)
			}
	
		var cskl;
		cskl = ds_map_find_first(global.total_passive)
		for(i=0;i<ds_map_size(global.total_passive);i+=1)
			{
			if(ini_key_exists(english_split(cskl),ds_map_find_value(global.total_passive,cskl)))
				{
				citem = ini_read_real(english_split(cskl),ds_map_find_value(global.total_passive,cskl),0)
				ds_map_add(global.all_passive,cskl,ds_map_find_value(global.total_passive,cskl))
				if(citem)ds_map_add(global.equip_passive,cskl,ds_map_find_value(global.total_passive,cskl))
				}
			cskl = ds_map_find_next(global.total_passive,cskl)
			}
		persistent = 1
		room_goto(toroom)
		ini_close()
		
		populate_enemy_list()
		populate_assist_list()
		
		ds_map_clear(global.assist_cooldown)
		ini_open(global.fsaveloc)
		var ename;
		for(i=0;i<ds_list_size(global.assist_list);i+=1)
			{
			ename = global.assist_list[| i]
			if(ini_key_exists("Cooldown",english_split(ename)))
				{
				citem = ini_read_real("Cooldown",english_split(ename),0)
				if(citem != 0)ds_map_add(global.assist_cooldown,ename,citem)
				}
			}
		ini_close()	
		
		audio_play_sound(sfx_save,1,0)
		toload = true
		}
	else
		{
		return false
		}


}
