/// @function draw_text_outline(x,y,string,color,outline_color,scale)
/// @description Draws a string at a color with an outline
/// @param x x position of string
/// @param y y position of string
/// @param string string to draw
/// @param color inner color of text
/// @param outline_color outline color of text
/// @param scale scale of text
function draw_text_outline_size(argument0, argument1, argument2, argument3, argument4, argument5) {

	_oldcolor = draw_get_color();

	// Draw outline
	draw_set_color(argument4);
	draw_text_transformed(argument0-1,argument1,argument2,argument5,argument5,0);
	draw_text_transformed(argument0+1,argument1,argument2,argument5,argument5,0);
	draw_text_transformed(argument0,argument1+1,argument2,argument5,argument5,0);
	draw_text_transformed(argument0,argument1-1,argument2,argument5,argument5,0);

	// Draw text
	draw_set_color(argument3);
	draw_text_transformed(argument0,argument1,argument2,argument5,argument5,0);

	draw_set_color(_oldcolor)




}
