///@function get_portrait(name)
///@description Gets the icon of the character for cutscene
///@param name - The name of the character
function get_portrait(argument0) {

	var fin,ste;
	ste = english_split(argument0)
	fin = string_lower(ste)
	fin = string_replace(fin," ","")
	fin = "spr_"+fin+"head"
	return asset_get_index(fin)


}
