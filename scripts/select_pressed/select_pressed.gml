/// @function select_pressed()
/// @description Checks if the select key has been pressed
function select_pressed() {
	if(keyboard_check_pressed(global.k_select))return 1
	return 0


}
