/// @function unfreeze_char()
/// @description UnFreezes a frozen character
function unfreeze_char() {

	if(object_is_ancestor(object_index,obj_canfreeze))
	and(frozen == true)
		{
		frozen = false
		image_speed = img_save
		path_speed = pth_speed
		alarm[0] = -1
		}

}
