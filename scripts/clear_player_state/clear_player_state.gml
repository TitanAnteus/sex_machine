///@function clear_player_state()
///@description Clears the first instance of all states for the player
function clear_player_state() {
	var st,in,ct;
	for(ct=0;ct<ds_list_size(global.states);ct+=1)
		{
		st = global.states[| ct]
		in = ds_list_find_index(player_state,st)
		
		if(in != -1)
			{
			ds_list_delete(player_state,ds_list_find_index(player_state,st))
			}
		}


}
