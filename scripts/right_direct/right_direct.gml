/// @function right_direct()
/// @description Checks if right key is being pressed
function right_direct() {
	if(keyboard_check(global.k_right))return 1
	return 0


}
