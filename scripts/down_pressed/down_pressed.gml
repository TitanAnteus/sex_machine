/// @function down_pressed()
/// @description Checks if down key has been pressed
function down_pressed() {
	if(keyboard_check_pressed(global.k_down))return 1
	return 0


}
