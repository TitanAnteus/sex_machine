/// @function check_exclude(direction)
/// @description returns false and plays a fail sound effect if direction is in exclusion list
/// @param direction - Up, Down, Left, or Right
function check_exclude(argument0) {
	var dir;
	dir = argument0
	if(ds_list_find_index(exclude,dir) != -1)
		{
		audio_play_sound(sfx_fail,1,0)
		return false
		}
	else
		{
		return true
		}


}
