///@function opposite_dir(string_dir)
///@description Gets the opposite direction of the string
///@param string_dir - The direction string
function opposite_dir(argument0) {
	var string_dir;
	string_dir = argument0
	if(string_dir = "U")return "D"
	if(string_dir = "L")return "R"
	if(string_dir = "D")return "U"
	if(string_dir = "R")return "L"
	return ""



}
