/// @function draw_item_menu()
/// @description Draws the item menu
function draw_pequip_menu() {

	var boxwidth,boxheight,topy,mpad,descheight;
	boxwidth = rightwidth
	boxheight = rightheight
	descheight = 285
	topy = 20
	mpad = 50

	draw_set_halign(fa_center)
	draw_set_valign(fa_top)
	draw_text(boxwidth/2,topy,string_split("Passive Equip | パッシブ装備"))
	draw_line(10,topy+mpad,boxwidth-10,topy+mpad)

	var icw,ich,leftmost;
	icw = sprite_get_width(spr_sadist_icon)
	ich = icw
	leftmost = 120

	draw_set_halign(fa_left)
	draw_set_valign(fa_center)
	topy = topy+mpad
	if(draw_get_color() != c_white)draw_set_color(c_white)

	var citem,h,k,ww,hh,hor,leftx,lsize,cpos_y,isize;
	hor = 1
	ww = boxwidth/hor
	hh = font_get_size(global.big_font)+8
	leftx = 20
	peskill = ""
	draw_set_halign(fa_left)
	draw_set_valign(fa_center)

	lsize = ds_map_size(global.all_passive)
	var mxh,sh;
	mxh = 7
	sh = 0
	cpos_y = -(mxh/2)
	if(dpos > 0)and(lsize > mxh)
		{
		cpos_y += dpos
		cpos_y = clamp(cpos_y,0,lsize-mxh)
		}
	else
		{
		cpos_y = 0
		}

	if(! surface_exists(passivesurf))
		{
		passivesurf = surface_create(boxwidth,boxheight-topy-descheight)
		}
	else
		{
		surface_set_target(passivesurf)
		draw_clear_alpha(c_white,0)
		var col,cskl,ccost;
		col = c_white
		cskl = ds_map_find_first(global.all_passive)
		ccost = get_passive_cost(cskl)
		draw_set_font(global.main_font)
		for(i=0;i<ds_map_size(global.all_passive);i+=1)
			{
			sh = string_height("H")+4
			col = c_white
			if(ds_map_exists(global.equip_passive,cskl))
				{
				col = c_aqua
				}
			if(selecting = true)
				{
				if(dpos = i){
					col = c_red
					peskill = cskl
					}
				else{col = c_white}
				if(ds_map_exists(global.equip_passive,cskl))
					{
					if(dpos = i){col = c_lime}
					else{col = c_aqua}
					}
				}
			if(draw_get_color() != col)draw_set_color(col)
			draw_set_halign(fa_left)
			draw_text(8,3+(sh*(i+.5))-(cpos_y*sh),""+string_split(cskl))
			draw_set_halign(fa_right)
			draw_text(boxwidth-65,3+(sh*(i+.5))-(cpos_y*sh),"-")
			var sgn;
			sgn = ""
			if(ccost > 0)sgn = "+"
			draw_text(boxwidth-8,3+(sh*(i+.5))-(cpos_y*sh),sgn+string(ccost))
			cskl = ds_map_find_next(global.all_passive,cskl)
			ccost = get_passive_cost(cskl)
			/*
			citem = get_portrait(english_split(ds_list_find_value(global.enemy_list,i)))
			if(sprite_exists(citem))
				{
				sh = sprite_get_height(citem)*scl
				draw_sprite_ext(citem,0,5,3+(sh*i)-(cpos_y*sh),scl,scl,0,c_white,1)
				col = c_white
				if(selecting = true)
				and(dpos = i)col = c_red
				draw_set_color(col)
				draw_text(8+(sprite_get_width(citem)*scl),3+(sh*(i+.5))-(cpos_y*sh)," - "+string_split(global.enemy_list[| i]))
				}*/
			}
		draw_set_font(global.big_font)
		surface_reset_target()
		draw_surface(passivesurf,0,topy)
		}

	if(draw_get_color() != c_white)draw_set_color(c_white)
	topy = boxheight-descheight
	draw_line(10,topy,boxwidth-10,topy)
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)

	cskl = ds_map_find_first(global.equip_passive)
	global.cPP = 0
	for(i=0;i<ds_map_size(global.equip_passive);i+=1)
		{
		global.cPP += get_passive_cost(cskl)
		cskl = ds_map_find_next(global.equip_passive,cskl)
		}
	draw_text(8,topy+8,string_split("PP Cost: "+string(global.cPP)+"/"+string(global.maxPP)))

	topy += string_height("H")+8
	draw_line(10,topy,boxwidth-10,topy)
	draw_set_font(global.main_font)
	hh = string_height("H")
	var txt;
	txt = ds_map_find_value(global.all_passive,peskill)
	if(! is_undefined(txt))draw_text_width(8,topy+4,txt,boxwidth-16,hh)




}
