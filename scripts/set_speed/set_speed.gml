/// @function set_speed(battlespeed)
/// @description Changes the battle speed
/// @param battlespeed - This is the amount of diamonds that show
function set_speed(argument0) {
	mxpattern = clamp(argument0,cenemy.min_speed,cenemy.max_speed)
	for(i=0;i<mxpattern;i+=1)
		{
		pattern[0][i] = ""			//Player's Choice
		pattern[1][i] = ""			//Correct Answer
		pattern[2][i] = c_red		//Diamond Color
		pattern[3][i] = ""			//Free Answer
		}


}
