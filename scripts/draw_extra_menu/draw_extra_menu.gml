/// @function draw_passive_menu()
/// @description Draws the item menu
function draw_extra_menu() {

	var boxwidth,boxheight,topy,mpad,descheight,txth;
	boxwidth = rightwidth
	boxheight = rightheight
	descheight = 250
	topy = 20
	mpad = 50

	draw_set_font(global.big_font)
	draw_set_halign(fa_center)
	draw_set_valign(fa_top)
	draw_text(boxwidth/2,topy,string_split("Extra | 追加"))

	draw_line(10,topy+mpad,boxwidth-10,topy+mpad)
	topy = topy+mpad+2
	draw_set_font(global.main_font)
	draw_set_halign(fa_left)
	txth = string_height("H")+2

	if(draw_get_color() != c_white)draw_set_color(c_white)

	draw_text_width(5,topy,"In order to do well, there are a few extra things you need to be aware of. | 注意する必要があるいくつかの追加事項があります。",boxwidth-10,txth)
	topy += txth*4

	draw_set_color(c_aqua)
	draw_text_width(5,topy,"Every turn you will be given a few free diamonds. The lower the speed the less you'll get. | 毎ターンいくつかのダイヤモンドが与えられます。 速度が遅いほど得られるものは少なくなります。",boxwidth-10,txth)
	topy += txth*4
	draw_set_color(c_yellow)
	draw_text_width(5,topy,"The enemies have patterns within their patterns. For instance, Rouge will only use horizontal arrows in her first pattern. | 敵は個別のパターンを持っています。 たとえばルージュは最初のパターンで水平矢印のみを使用します。",boxwidth-10,txth)
	draw_set_color(c_white)

	topy += txth*5
	draw_text_width(5,topy,"If you find the game too challenging, it is recommended to start a New Game on Easy mode. | ゲームが難しすぎると感じた場合はイージーモードで新しいゲームを開始することをお勧めします。",boxwidth-10,txth)
	topy += txth*4
	draw_text_width(5,topy,"Good luck and do your best! | クリアを目指して頑張ってください！",boxwidth-10,txth)


	draw_set_color(c_white)
	draw_set_font(global.big_font)


}
