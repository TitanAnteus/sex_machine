/// @function draw_passive_menu()
/// @description Draws the item menu
function draw_defense_menu() {

	var boxwidth,boxheight,topy,mpad,descheight,txth;
	boxwidth = rightwidth
	boxheight = rightheight
	descheight = 250
	topy = 20
	mpad = 50

	draw_set_font(global.big_font)
	draw_set_halign(fa_center)
	draw_set_valign(fa_top)
	draw_text(boxwidth/2,topy,string_split("Defense | 防衛"))

	draw_line(10,topy+mpad,boxwidth-10,topy+mpad)
	topy = topy+mpad+2
	draw_set_font(global.main_font)
	draw_set_halign(fa_left)
	txth = string_height("H")+2

	if(draw_get_color() != c_white)draw_set_color(c_white)

	draw_text_width(5,topy,"Keeping your Willpower high and your Lust low will help you achieve victory. | 意志力を高く保ち、欲望を低く保つことは勝利を達成するのに役立ちます。",boxwidth-10,txth)
	topy += txth*3
	draw_set_color(c_lime)
	draw_text_width(5,topy,"However, do not ignore Stamina. If Stamina gets too low, Igrec will be exhausted and take more damage. | ただし、スタミナを無視しないでください。 スタミナが低くなりすぎるとイグレックは疲れ果てて、より多くのダメージを受けます。",boxwidth-10,txth)
	topy += txth*4
	draw_set_color(c_white)
	draw_text_width(5,topy,"Don't be discouraged if you cum. Damage is reduced for every heart you obtain. | 射精しても落胆しないでください。 射精によって得るハートはダメージを減少させます。",boxwidth-10,txth)

	topy += txth*3
	draw_text_width(5,topy,"Every heart increases the damage you take the next time you cum however. | ただし、次に受けるダメージが増加してしまいます。",boxwidth-10,txth)
	topy += txth*3
	draw_text_width(5,topy,"Remember, at high speeds damage is slightly reduced as well. | 高速でのダメージもわずかに減少することを忘れないでください。",boxwidth-10,txth)


	draw_set_color(c_white)
	draw_set_font(global.big_font)


}
