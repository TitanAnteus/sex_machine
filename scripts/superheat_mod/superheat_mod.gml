///@function superheat_mod()
///@description Returns a high value when Superheat is a good skill to use.
function superheat_mod(argument0) {
	var ea,chk,skl;
	ea = temp_enemy_action
	skl = argument0

	chk = ds_map_find_value(ea,skl)
	if(chk = undefined)return false

	if(mxpattern+speed_increase <= 5)
		{
		chk += 25
		}
	else
		{
		chk -= 5
		}
	if(cenemy.willpower < cenemy.mxwillpower/2)chk += 10
	if(check_estate("Superheat"))chk -= 1000
	ds_map_replace(ea,skl,clamp(chk,0,500))

	show_debug_message(skl+" : "+string(chk)+"/"+string(ds_map_find_value(cenemy.enemy_action,skl)))
	return true


}
