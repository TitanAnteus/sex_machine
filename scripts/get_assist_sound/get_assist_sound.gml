/// @function get_assist_sound(enemyname)
/// @description returns the assist sprite
/// @param enemyname - The name of the enemy
function get_assist_sound(argument0, argument1) {
	var enemyname,finish;
	enemyname = string(argument0)
	finish = argument1
	if(finish = 0){return asset_get_index("tm_"+string_lower(enemyname)+"assist")}
	else{return asset_get_index("tm_"+string_lower(enemyname)+"assistcum")}

}
