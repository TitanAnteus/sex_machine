/// @function start_battle(skill_name,sound)
/// @description Starts a battle
/// @param battle - Array that includes battle sprites
/// @param sound - Array that includes sound sprites
function start_battle(argument0, argument1) {

	var bat;
	bat = instance_create_depth(0,0,0,obj_battle)
	bat.battle = argument0
	bat.sound = argument1
	bat.cenemy = id
	bat.battle_bgm = battle_bgm
	move_speed = 0
	tmove_speed = move_speed

	//show_message(sprite_get_name(argument0[0])+"\n"+timeline_get_name(argument1[0]))

	with(obj_canfreeze)
		{
		freeze_char(-1)
		}

	if(round(obj_player.direction) = round(direction))
		{
		if(state = "Follow")
			{
			bat.advantage = "Ambush"
			}
		else
			{
			bat.advantage = "Surprise"
			bat.run_chance = 90
			}
		}

	with(bat)
		{
		sprite_index = battle[0]
		timeline_index = sound[0]
		timeline_running = true
		timeline_speed = 0
		name = other.name
	
		set_speed(other.battle_speed)
		rand_pattern()
		
		
		action_timer = cenemy.turn_time
		mybits = other.mybits
		}
	bat.final_text = string_replace("Igrec has gained <col @c_yellow><b><col @c_white> bits!\nHe now has <col @c_yellow><tb><col @c_white> bits in total! | イグレックは<col @c_yellow><b><col @c_white>ビットを得ました!\n彼は今<col @c_yellow><tb><col @c_white>ビットを持っています！","<b>",string(mybits))
	bat.final_text = string_replace(bat.final_text,"<b>",string(mybits))
	bat.final_text = string_replace(bat.final_text,"<tb>",string(global.bits+mybits))
	bat.final_text = string_replace(bat.final_text,"<tb>",string(global.bits+mybits))
	var lt;
	lt = loss[| ds_list_size(loss)-1]
	if(string_count("Igrec has gained",lt) == 0)
		{
		ds_list_add(loss,bat.final_text)
		}
	else
		{
		ds_list_replace(loss,ds_list_size(loss)-1,bat.final_text)
		}



}
