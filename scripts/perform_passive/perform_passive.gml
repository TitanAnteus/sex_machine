///@function perform_passive()
///@description Perform's a passive skill
function perform_passive() {
	var cskl;
	cskl = pskill
	if(global.firstpassive = true)
		{
		global.firstpassive = false
		ini_open("settings.ini")
		ini_write_real("Settings","FirstPassive",global.firstpassive)
		ini_close()
	
		instance_create_depth(0,0,0,obj_passive_tutorial)
		}

	if(string_count("Triple Stamina",cskl) > 0)
		{
		global.stamina += global.mxstamina*.2
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Slow Lover",cskl) > 0)
		{
		global.cum -= global.mxcum*.10
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Panic Attack",cskl) > 0)
		{
		cenemy.chab += array_height_2d(cenemy.habit)-1
		set_speed(mxpattern+1)
		rand_pattern()
		audio_play_sound(sfx_surprise,1,0)
		}
	if(string_count("Unbreakable",cskl) > 0)
		{
		cenemy.willpower -= (global.strength*mxpattern)*.1
		global.cum -= global.mxcum*.10
		audio_play_sound(sfx_strong_power,1,0)
		}
	if(string_count("Odd Calm",cskl) > 0)
		{
		global.cum -= global.mxcum*.10
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Stamina Speed",cskl) > 0)
		{
		global.stamina += global.mxstamina*.15
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Sensitive",cskl) > 0)
		{
		ds_list_add(player_state,"Sensitive")
		audio_play_sound(sfx_surprise,1,0)
		}
	if(string_count("Strong Will",cskl) > 0)
		{
		global.stamina += global.mxstamina*.15
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Rhythm",cskl) > 0)
		{
		if(speed_increase > 0)speed_increase = 0
		audio_play_sound(sfx_powerdown,1,0)
		}
	if(string_count("Keen Eye",cskl) > 0)
		{
		ds_list_add(player_state,"Vision")
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Shaky Hips",cskl) > 0)
		{
		ds_list_add(player_state,"Silence")
		audio_play_sound(sfx_surprise,1,0)
		}
	if(string_count("Shaky Hands",cskl) > 0)
		{
		ds_list_add(player_state,"Itemless")
		audio_play_sound(sfx_surprise,1,0)
		}
	if(string_count("Conviction",cskl) > 0)
		{
		ds_list_add(player_state,"Pressure")
		audio_play_sound(sfx_powerup,1,0)
		}
}
