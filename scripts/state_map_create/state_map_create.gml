///@function state_map_create(state_list)
///@description creates a map for state list
///@param state_list - The list index of the state
function state_map_create(argument0) {

	var tmap,slist,cval;
	tmap = ds_map_create()
	slist = argument0
	for(i=0;i<ds_list_size(slist);i+=1)
		{
		cval = ds_map_find_value(tmap,slist[| i])
		if(cval == undefined)
			{
			cval = 1
			if(ds_list_find_index(global.limit_state,slist[| i]) != -1)
				{
				cval = 0
				}
			ds_map_add(tmap,slist[| i],cval)
			}
		else
			{
			if(ds_list_find_index(global.limit_state,slist[| i]) == -1)
				{
				ds_map_replace(tmap,slist[| i],cval+1)
				}
			}
		}
	return tmap


}
