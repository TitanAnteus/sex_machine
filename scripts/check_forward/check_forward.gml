/// @function check_forward(direction)
/// @description Checks if the area in front is free from obstacles
/// @param {real} direction - Direction to check if collision exists
function check_forward(argument0) {

	var dr,xcheck,ycheck,obst;
	dr = argument0
	xcheck = (x+(global.grid/2))+lengthdir_x(global.grid,dr)
	ycheck = (y+(global.grid/2))+lengthdir_y(global.grid,dr)
	obst = noone
	if(object_is_ancestor(object_index,obj_enemy))obst = obj_enemy
	if(instance_position(xcheck,ycheck,obst) == id)obst = noone

	if(position_meeting(xcheck,ycheck,obj_wall))
	or(position_meeting(xcheck,ycheck,obst))
	or(position_meeting(xcheck,ycheck,obj_block))
	or(xcheck >= room_width)or(xcheck <= 0)or(ycheck >= room_height)
	or(ycheck <= 0)
		{
		return false
		}
	else
		{
		return true
		}


}
