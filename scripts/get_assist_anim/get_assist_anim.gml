/// @function get_assist_anim(enemyname)
/// @description returns the assist sprite
/// @param enemyname - The name of the enemy
/// @param finish - boolean whether or not to get cum animation
function get_assist_anim(argument0, argument1) {
	var enemyname,finish;
	enemyname = string(argument0)
	finish = argument1
	if(finish = 0){return asset_get_index("spr_"+string_lower(enemyname)+"assist")}
	else{return asset_get_index("spr_"+string_lower(enemyname)+"assistcum")}
	/*
	if(string_count("Rouge",enemyname) > 0)return spr_rougeassist
	if(string_count("Bleu",enemyname) > 0)return spr_bleuassist
	if(string_count("Vert",enemyname) > 0)return spr_vertassist
	if(string_count("Acier",enemyname) > 0)return spr_acierassist*/


}
