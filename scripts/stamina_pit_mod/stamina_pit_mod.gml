///@function stamina_pit_mod()
///@description Returns a high value when Stamina Pit is a good skill to use.
function stamina_pit_mod(argument0) {
	var ea,chk,skl;
	ea = temp_enemy_action
	skl = argument0

	chk = ds_map_find_value(ea,skl)
	if(chk = undefined)return false

	if(state_count(enemy_state,"Stamina Pit") > 1)
		{
		chk -= 40
		}
	if(global.stamina > global.mxstamina*.2)
		{
		chk += 30
		}
	else
		{
		chk += 10
		}
	if(cenemy.willpower < cenemy.mxwillpower*.4)chk -= 20
	ds_map_replace(ea,skl,clamp(chk,0,500))
	show_debug_message(skl+" : "+string(chk)+"/"+string(ds_map_find_value(cenemy.enemy_action,skl)))
	return true


}
