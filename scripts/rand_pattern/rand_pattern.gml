/// @function rand_pattern()
/// @description Replaces pattern[2 variable with random values
function rand_pattern() {
	var hlength,ilast,k,habit,chab,odd;
	cenemy.chab = cenemy.chab mod array_height_2d(cenemy.habit)
	habit = cenemy.habit
	chab = cenemy.chab
	cenemy.lhab = chab

	hlength = array_length_2d(habit,chab)
	if(check_estate("Unpredictable"))hlength = 0
	ilast = 0

	odd = -1
	if((hlength mod 2) = 1)odd = floor(mxpattern/2)

	for(i=0;i<mxpattern;i+=1)
		{
		pattern[0][i] = ""
		pattern[3][i] = "?"
		k = floor(random(4))
		if(k = 0)pattern[1][i] = "U"
		if(k = 1)pattern[1][i] = "L"
		if(k = 2)pattern[1][i] = "R"
		if(k = 3)pattern[1][i] = "D"
		pattern[2][i] = c_pink
		if(i < floor(hlength/2))
			{
			pattern[1][i] = habit[chab,ilast]
			ilast += 1
			pattern[2][i] = c_white
			}
		if(i == odd)
			{
			pattern[1][i] = habit[chab,ilast]
			ilast += 1
			pattern[2][i] = c_white
			}
		if(i > (mxpattern-(floor(hlength/2)))-1)
			{
			pattern[1][i] = habit[chab,ilast]
			ilast += 1
			pattern[2][i] = c_white
			}
		}

	chab += 1
	chab = chab mod array_height_2d(cenemy.habit)
	cenemy.chab = chab

	free_answer = 4
	if(mxpattern <= 9)free_answer = 3
	if(mxpattern <= 6)free_answer = 2
	if(mxpattern <= 3)free_answer = 1
	free_answer += cenemy.free_answer

	repeat(free_answer-floor(random(2))+floor(random(2)))
		{
		var le,ir;
		le = mxpattern-1
		ir = irandom(le)
		pattern[3][ir] = pattern[1][ir] 
		}

	if(check_pstate("Blind"))
		{
		for(i=0;i<array_length_2d(pattern,1);i+=1)
			{
			pattern[3][i] = "?"
			}
		}
	
	if(check_pstate("Vision"))
		{
		for(i=0;i<array_length_2d(pattern,1);i+=1)
			{
			pattern[3][i] = pattern[1][i]
			}
		}



}
