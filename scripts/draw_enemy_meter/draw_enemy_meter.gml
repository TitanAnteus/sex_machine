/// @function draw_enemy_meter(width)
/// @description Draws the enemy meter
function draw_enemy_meter(argument0) {
	var mx,my,foc;
	foc = 0
	if(instance_exists(obj_battle_tutorial))foc = 0

	mx = round((view_wport[0]/2)-(emwidth/2)-foc)
	my = battle_y-emheight

	draw_set_color(c_dkgray)
	draw_rectangle(mx,my,round(mx+emwidth+foc),round(my+emheight+foc),0)

	if(argument0 > .1)
		{
		draw_set_color(c_red)
		draw_rectangle(mx,my,round(mx+argument0+foc),round(my+emheight+foc),0)
		}
	
	draw_set_color(c_white)
	draw_rectangle(mx,my,round(mx+emwidth+foc),round(my+emheight+foc),1)


}
