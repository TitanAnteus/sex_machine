///@function check_passive()
///@description Check's if passive skill is to be used or not.
function check_passive() {
	var cskl,cval,csp;
	csp = clamp(mxpattern+speed_increase,array_length_2d(cenemy.habit,0),cenemy.max_speed)
	cskl = ds_map_find_first(passive_map)
	cval = ds_map_find_value(passive_map,cskl)
	for(i=0;i<ds_map_size(passive_map);i+=1)
		{
		if(string_count("Triple Stamina",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
			{
			if(guardcount >= 3)
			and(floor(random(100)) < 30)ds_map_add(cpassive,cskl,cval)
			}
		if(string_count("Slow Lover",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
			{
			if(csp <= cenemy.min_speed)
			and(floor(random(100)) < 50)ds_map_add(cpassive,cskl,cval)
			}
		if(string_count("Panic Attack",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
			{
			if(floor(random(100)) < 8)ds_map_add(cpassive,cskl,cval)
			}
		if(string_count("Unbreakable",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
			{
			if(guardcount >= start_mxpattern)ds_map_add(cpassive,cskl,cval)
			}
		if(string_count("Odd Calm",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
			{
			if((csp mod 2) = 1)
			and(floor(random(100)) < 50)ds_map_add(cpassive,cskl,cval)
			}
		if(string_count("Stamina Speed",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
			{
			var pink;
			pink = (csp-cenemy.min_speed)-1
			if(floor(random(100)) < (7*pink))ds_map_add(cpassive,cskl,cval)
			}
		if(string_count("Sensitive",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
			{
			var pink,tpink;
			pink = csp-cenemy.min_speed
			tpink = cenemy.max_speed-cenemy.min_speed
			if(floor(random(100)) < 50*((tpink-pink)/(tpink+1)))ds_map_add(cpassive,cskl,cval)
			}
		if(string_count("Strong Will",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
			{
			if(floor(random(global.mxwillpower*1.5)) < global.willpower)ds_map_add(cpassive,cskl,cval)
			}
		if(string_count("Rhythm",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
		and(speed_increase > 0)
		and(check_estate("Overheat") == false)
		and(check_estate("Superheat") == false)
			{
			if(floor(random(10)) < 3)ds_map_add(cpassive,cskl,cval)
			}
		if(string_count("Keen Eye",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
			{
			if(floor(random(100)) < 16)ds_map_add(cpassive,cskl,cval)
			}
		if(string_count("Shaky Hips",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
			{
			if(floor(random(30-(csp*2))) < 5)ds_map_add(cpassive,cskl,cval)
			}
		if(string_count("Shaky Hands",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
			{
			if(floor(random(30-(csp*2))) < 5)ds_map_add(cpassive,cskl,cval)
			}	
		if(string_count("Conviction",cskl) > 0)
		and(ds_map_exists(cpassive,cskl) == false)
			{
			if(guardcount >= start_mxpattern)ds_map_add(cpassive,cskl,cval)
			}
		cskl = ds_map_find_next(passive_map,cskl)
		cval = ds_map_find_value(passive_map,cskl)
		}


}
