/// @function skip_direct()
/// @description Checks if skip key is being pressed
function skip_direct() {
	if(keyboard_check(global.k_skip))return 1
	return 0


}
