/// @function no_color(text)
/// @description removes coloring from text
/// @param text - text to skip colors of
function no_color(cstr) {
	var color_list,length_list,full_color,color_string;
	color_list = ds_list_create()
	length_list = ds_list_create()
	full_color = ds_list_create()
	for(var st=0;st<string_length(cstr);st+=1)
		{
		if(string_copy(cstr,st,1) == "<")
			{
			ds_list_add(color_list,st)
			for(var ed=0;ed<1000;ed+=1)
				{
				if(string_copy(cstr,st+ed,1) == ">")
					{
					ds_list_add(length_list,ed)
					break
					}
				}
			color_string = string_copy(cstr,st,ed)
			if(string_char_at(color_string,string_length(color_string)) != ">")color_string += ">"
			ds_list_add(full_color,color_string)
			st=st+ed
			}
		}
	for(var st=0;st<ds_list_size(full_color);st+=1)
		{
		cstr = string_replace(cstr,full_color[| st],"")
		}
		
	ds_list_destroy(color_list)
	ds_list_destroy(length_list)
	ds_list_destroy(full_color)
	return cstr
}
