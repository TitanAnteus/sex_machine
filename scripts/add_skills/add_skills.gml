///@function add_skills(skills)
///@description Add Skills to the skill list in order
///@param skills	-	array of skills
function add_skills(ar) {
	for(i=0;i<array_length(ar);i+=1)
		{
		ds_list_add(global.skill_list,ar[i])
		}
		
	var newlist;
	newlist = ds_list_create()
	
	addtolist("Slow",newlist)
	addtolist("Calm",newlist)
	addtolist("Focus",newlist)
	addtolist("Meditate",newlist)
	addtolist("Psych Up",newlist)
	addtolist("Ready",newlist)
	addtolist("Pressure",newlist)
	addtolist("Victory Cry",newlist)
	addtolist("Surrender",newlist)
	
	
	ds_list_copy(global.skill_list,newlist)
}

///@function addtolist(item,newlist)
///@param item  -  item to check
///@param newlist - newlist
function addtolist(itm,nlist){
	for(i=0;i<ds_list_size(global.skill_list);i+=1)
		{
		var skl;
		skl = global.skill_list[| i]
		if(string_count(itm,global.skill_list[| i]) >= 1)ds_list_add(nlist,skl)
		}
}

///@function add_passives(passives)
///@description Add Passives to the passive list in order
///@param passives	-	array of passives
function add_passives(ar) {
	for(i=0;i<array_length(ar);i+=1)
		{
		ds_map_add(global.all_passive,ar[i],ds_map_find_value(global.total_passive,ar[i]))
		}
}

///@function add_assist(assist)
///@description Add Assist to the assist list
///@param assist	-	Singular name of assist character
function add_assist(assist) {
	ini_open("settings.ini")
	ini_write_real("Assist",english_split(assist),true)
	ini_close()
	ini_open(global.saveloc)
	ini_write_real("Assist",english_split(assist),true)
	ini_close()
	populate_enemy_list()
	populate_assist_list()
	assist_countdown()
}

///@function increase_pp(amount)
///@description Increase the total amount of passive points
///@param amount	-	amount to increase by
function increase_pp(amount) {
	global.maxPP += amount
}

///@function increase_carry(amount)
///@description Increase the total carry weight
///@param amount	-	amount to increase by
function increase_carry(amount) {
	global.mxitems += amount
}