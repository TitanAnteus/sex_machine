///@function heart_pierce_mod()
///@description Returns a high value when Heart Piercing Smile is a good skill to use.
function heart_pierce_mod(argument0) {
	var ea,chk,skl;
	ea = temp_enemy_action
	skl = argument0

	chk = ds_map_find_value(ea,skl)
	if(chk = undefined)return false

	if(global.cum > global.mxcum/2)
		{
		chk += 40
		}
	else
		{
		chk -= 10
		}
	if(global.stamina < global.mxstamina/2)
		{
		chk += 20
		}
	else
		{
		chk -= 5
		}
	
	if(floor(random(10)) < 2)chk += 20
	ds_map_replace(ea,skl,clamp(chk,0,500))


	show_debug_message(skl+" : "+string(chk)+"/"+string(ds_map_find_value(cenemy.enemy_action,skl)))
	return true


}
