/// @description Initializes functions for cutscenes

/// @function					event_talk(character,dialogue)
/// @param {string} character	Name of character talking
/// @param {string} dialogue	Dialogue to say
function event_talk(character,dialogue,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Talk")
ds_list_add(event[count],character)
ds_list_add(event[count],dialogue)
}

/// @function					event_darken(steps)
/// @param {real} steps			How long until screen is dark
function event_darken(steps,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Darken")
ds_list_add(event[count],steps)
}

/// @function					event_brighten(steps)
/// @param {real} steps			How long until screen is normal
function event_brighten(steps,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Brighten")
ds_list_add(event[count],steps)
}

/// @function					event_wait(steps)
/// @param {real} steps			How long to do nothing for
function event_wait(steps,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Wait")
ds_list_add(event[count],steps)
}

/// @function					event_save()
function event_save(evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Save")
}

/// @function					event_bgm(bgm)
/// @param bgm					Play Background Music
function event_bgm(bgm,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"BGM")
ds_list_add(event[count],bgm)
}

/// @function					event_text_speed(spd)
/// @param {real} spd			How fast the text moves
function event_text_speed(spd,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Text Speed")
ds_list_add(event[count],spd)
}

/// @function					event_play_sound(snd)
/// @param snd					Sound to play
/// @param {bool} loop			Whether or not to loop
function event_play_sound(snd,loop,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Play Sound")
ds_list_add(event[count],snd)
ds_list_add(event[count],loop)
}

/// @function					event_stop_sound(snd)
/// @param snd					Sound to stop
function event_stop_sound(snd,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Stop Sound")
ds_list_add(event[count],snd)
}
/// @function					event_instance_face(instance,direction)
/// @param instance				Instance to change
/// @param {real} direction		Direction to face
function event_instance_face(inst,dir,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Instance Face")
ds_list_add(event[count],inst)
ds_list_add(event[count],dir)
}

/// @function					event_instance_lock_direction(instance)
/// @param instance				Instance to change
function event_instance_lock_direction(inst,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Instance Lock Direction")
ds_list_add(event[count],inst)
}

/// @function					event_instance_unlock_direction(instance)
/// @param instance				Instance to change
function event_instance_unlock_direction(inst,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Instance Unlock Direction")
ds_list_add(event[count],inst)
}

/// @function					event_instance_create(instance,x,y,direction)
/// @param instance				Instance to change
/// @param {real} x				X position
/// @param {real} y				Y position
/// @param {real} direction		Direction
function event_instance_create(inst,xx,yy,dir,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Instance Create")
ds_list_add(event[count],inst)
ds_list_add(event[count],xx)
ds_list_add(event[count],yy)
ds_list_add(event[count],dir)
}

/// @function							event_instance_move(instance,x,y)
/// @param instance						Instance to change
/// @param {real} x						X position
/// @param {real} y						Y position
function event_instance_move(inst,xx,yy,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
if(xx > CURRENT)xx = (xx div global.grid) * global.grid
if(yy > CURRENT)yy = (yy div global.grid) * global.grid
ds_list_add(event[count],"Instance Move")
ds_list_add(event[count],inst)
ds_list_add(event[count],xx)
ds_list_add(event[count],yy)
}

/// @function							event_instance_move_relative(instance,x,y)
/// @param instance						Instance to change
/// @param {real} x						X position
/// @param {real} y						Y position
function event_instance_move_relative(inst,xx,yy,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
xx = (xx div global.grid) * global.grid
yy = (yy div global.grid) * global.grid
ds_list_add(event[count],"Instance Move Relative")
ds_list_add(event[count],inst)
ds_list_add(event[count],xx)
ds_list_add(event[count],yy)
}

/// @function							event_instance_destroy(instance)
/// @param instance						Instance to destroy
function event_instance_destroy(inst,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Instance Destroy")
ds_list_add(event[count],inst)
}

/// @function							event_state_change(instance,state)
/// @param instance						Instance to change state
/// @param {string} state				state to change into
function event_state_change(inst,st,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"State Change")
ds_list_add(event[count],inst)
ds_list_add(event[count],st)
}

/// @function							event_room_change(room,start)
/// @param room							room to go to
/// @param {integer} start				starting number inside room
function event_room_change(rm,st,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Room Change")
ds_list_add(event[count],rm)
ds_list_add(event[count],st)
}

/// @function							event_learn_skills(skill_array)
/// @param skills						Array of skills
function event_learn_skills(skills,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Learn Skills")
ds_list_add(event[count],skills)
}

/// @function							event_learn_passives(passive_array)
/// @param passives						Array of passives
function event_learn_passives(passives,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Learn Passive")
ds_list_add(event[count],passives)
}

/// @function							event_gain_assist(assist)
/// @param assist						Name of assist
function event_gain_assist(assist,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Gain Assist")
ds_list_add(event[count],assist)
}

/// @function							event_increase_pp(increase)
/// @param increase						Amount to increase passive points by
function event_increase_pp(increase,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Increase PP")
ds_list_add(event[count],increase)
}

/// @function							event_increase_carry(increase)
/// @param increase						Amount to increase carry weight by
function event_increase_carry(increase,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Increase Carry")
ds_list_add(event[count],increase)
}



/// @function							event_activate_object(object)
/// @param object						Object to activate
function event_activate_object(in,evcount){
var count;
count = evcount
if(is_undefined(count))count = 0
ds_list_add(event[count],"Activate Object")
ds_list_add(event[count],in)
}

/// @function					cutscene_start()
/// @description				Freezes all characters and starts the cutscene
function cutscene_start(){
	start = 1
	tofreeze = 1
}