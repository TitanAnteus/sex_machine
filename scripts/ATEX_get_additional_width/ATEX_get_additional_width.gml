/// @arg body
/// @arg index
/// @arg nW
/// @arg cH
/// @arg cW
function ATEX_get_additional_width(argument0, argument1, argument2, argument3, argument4) {

	var body = argument0, index = argument1, nW = argument2, cH = argument3, cW = argument4, active = 0, addW = 0;

	for(var j = index + 1; j < array_length_1d(body); j+=1) {
		var nextTag = body[j];

		if (is_array(nextTag)) 
		{
			var tagCurrent = nextTag, tagContainer = tagCurrent[0], tagScript = tagContainer[1], tagMode = tagContainer[3], tagType = tagCurrent[3];
			if (tagScript == ATEX_tag_joint) {
				active = 1;
				continue;
			} else
			{
				if (active < 0)
					break;
				if (tagType != 1 and tagMode != ATEX.drawer and tagMode != ATEX.effect) {
					var output=script_execute(tagScript, tagCurrent[1], tagCurrent[2], [nW, cH, cW - addW], "position");
					if is_array(output) {
						addW += output[0]
						if output[0] != 0
							active --;
					} else
						nextTag = string(output);
				}
			}
		}
		if is_string(nextTag) {
			var s = "", symb = "", pchar = "";
			for (var char = 1; char <= string_length(nextTag); char ++) {
				symb = string_char_at(nextTag, char);
					
				if (symb == " " or symb == "\n" or symb == "\r" or ATEX_in(pchar, global.ATEX_separating_symbols)) {
					active = -1;
					break;
				}
				s += symb;
					
				pchar = symb;
			}
			if ATEX_in(symb, global.ATEX_separating_symbols)
				active = -1;
			addW += string_width(s);
		}

		if (active < 0)
			break;
	}

	return addW;


}
