///@function opposite_overworld(direction)
///@description Gets the opposite direction
///@param direction - The direction
function opposite_overworld(argument0) {
	var dir;
	dir = argument0
	if(dir = 0)return 180
	if(dir = 90)return 270
	if(dir = 270)return 90
	if(dir = 180)return 180
	return -1



}
