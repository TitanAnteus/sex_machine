function ATEX_tag_joint(argument0, argument1, argument2, argument3, argument4, argument5) {
	var args=argument0, props=argument1, sizes=argument2, type=argument3;
	var W=sizes[0], H=sizes[1];

#region tag data

	if type=="depth" 
		return 0;
	if type=="names"
		return ["~"];
	if type=="type"
		return ATEX.ext;
	
#endregion

	if type=="position" {
		return [0, 0] // first value - width, second - height. You can write your values
	}
	else
	if type=="draw" {
		//var text=argument4, X=argument5, Y=argument6; - for "drawer" tags
		var X=argument4, Y=argument5;
		/*
				...DOING SOMETHING...
		*/
		return true
	}
	else
	if type=="start" {
		var poslist=argument4, textlist=argument5;
		/*
				start of the tag, it calling when "position" event return text
		*/
	}
	else
	if type=="end" {
		var poslist=argument4, textlist=argument5; // array with text coordinates. You can use it to detect collision with cursor
		/*
				end of the tag, it calling when text returned in "position" event already drawn
		*/
	}
	else
	if type=="init" {
		/*
			initialization, usually in CREATE event
		*/
	}


}
