/// @function search_hero()
/// @description Look for the hero
function search_hero() {
	if(! instance_exists(obj_hero))
		{
		return false
		}
	if(point_distance(x,y,obj_hero.x,obj_hero.y) > follow_dist+40)
		{
		return false
		}

	var cx,cy,tx,ty;
	cx = x+(global.grid/2)
	cy = y+(global.grid/2)
	for(i=-70;i<70;i+=10)
		{
		for(k=20;k<follow_dist;k+=10)
			{
			tx = cx+lengthdir_x(k,direction+i)
			ty = cy+lengthdir_y(k,direction+i)
			if(position_meeting(tx,ty,obj_player))
				{
				return true
				}
			if(position_meeting(tx,ty,obj_wall))
				{
				break
				}
			}
		}
	return false


}
