/// @arg body
function ATEX_fast_destroy(argument0) {

	ATEX_content_destroy(argument0[? "body"], true)
	surface_free(argument0[? "surf"])
	ds_map_destroy(argument0)


}
