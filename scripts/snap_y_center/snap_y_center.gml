/// @function snap_y_center()
/// @description Snaps a centered object on the grid
function snap_y_center() {
	y = (round(y) div global.grid) * global.grid
}
