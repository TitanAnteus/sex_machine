function ATEX_init() {
	global.ATEX_macros=ds_map_create()
	global.ATEX_macros_list=ds_map_create()
	global.ATEX_scripts=ds_map_create()
	global.ATEX_scripts_list=ds_list_create()
	global.ATEX_font=0
	global.ATEX_trigger_start="<"				/// start and end of the tag
	global.ATEX_trigger_end	 =">"
	global.ATEX_modes=0
	global.ATEX_separating_symbols=[";", ","]
	global.ATEX_symbol_number = 0
	global.ATEX_symbol_draw = -1
	global.ATEX_settings = ds_map_create()		// default settings
		global.ATEX_settings[? "back-color"]=c_orange
		global.ATEX_settings[? "back-alpha"]=0
		global.ATEX_settings[? "content-color"]=c_orange
		global.ATEX_settings[? "content-alpha"]=c_white
		global.ATEX_settings[? "content-font"]=-1
		global.ATEX_settings[? "content-halign"]=fa_left
		global.ATEX_settings[? "symbol-width"]=-1
		global.ATEX_settings[? "line-height"] = 0;
		global.ATEX_settings[? "line-distance"] = 0;
	global.ATEX_variables=ds_map_create()		// variables
	global.ATEX_variables_list=ds_list_create()
		ATEX_variable_set("left", fa_left)
		ATEX_variable_set("right", fa_right)
		ATEX_variable_set("middle", fa_middle)
		ATEX_variable_set("center", fa_center)
	global.ATEX_settings_default = global.ATEX_settings
	global.ATEX_fast_index=-1

#macro ATEX_mode global.ATEX_modes

	global.ATEX_mouse_dx=0
	global.ATEX_mouse_dy=0
	global.ATEX_isfast=0
	global.ATEX_current_version="0.6 - alpha"

	global.ATEX_modes_count=7

	enum ATEX
	{
		notags		=1,	// this mode makes drawing faster, but tags and macros will not work
		gui			=2,	// use it when you draw text in GUI event
		nomacros	=4,	// this mode makes drawing faster, but macros and variables will not work
		wrapping	=8,	// enable wordwrapping
		alignment	=16,// enable alignment
		debug		=32,// enable debug mode
		cutwords	=64,// enable cut words
		text		=0,
		tag			=1,
		standart	=-1,
		drawer		=0,
		element		=1,
		part		=1,
		effect		=2,
		ext			=3
	}

	/*
					You can create your own modes, just write:
						global.my_mode = ATEX_mode_add()
					
					And you can check it with function 
						ATEX_mode_is_enabled( global.my_mode )
				
					Example:
							global.smooth_mode = ATEX_mode_add()
							ATEX_mode = global.my_mode | ATEX.wrapping
				
					"|" - summ of tags, you can use "+" if you know that current mode didn't use
					Example:
							DO: ATEX_mode = global.my_mode | global.wrapping | global.my_mode
							DO: ATEX_mode = global.my_mode + global.wrapping
							NOT TO DO: ATEX_mode = global.my_mode + global.wrapping + global.my_mode
				
					To turn off all modes just write 
												ATEX_mode = 0
	*/

	ATEX_add_script(ATEX_tag_colour)
	ATEX_add_script(ATEX_tag_font)
	ATEX_add_script(ATEX_tag_text)
	ATEX_add_script(ATEX_tag_link)
	ATEX_add_script(ATEX_tag_shadow)
	ATEX_add_script(ATEX_tag_outline)
	ATEX_add_script(ATEX_tag_picture)
	ATEX_add_script(ATEX_tag_underline)
	ATEX_add_script(ATEX_effect_wave)
	ATEX_add_script(ATEX_effect_rainbow)
	ATEX_add_script(ATEX_effect_shake)
	ATEX_add_script(ATEX_tag_runline)
	ATEX_add_script(ATEX_tag_halign)
	ATEX_add_script(ATEX_tag_props)
	ATEX_add_script(ATEX_tag_style)
	ATEX_add_script(ATEX_tag_space)
	ATEX_add_script(ATEX_tag_symbol_width)
	ATEX_add_script(ATEX_tag_shake)
	ATEX_add_script(ATEX_tag_joint)

	show_debug_message("ATEX: initialization successfull")
	global.constants = ds_map_create();
	global.constants[?"vk_nokey"] = vk_nokey;
	global.constants[?"vk_anykey"] = vk_anykey;
	global.constants[?"vk_left"] = vk_left;
	global.constants[?"vk_right"] = vk_right;
	global.constants[?"vk_up"] = vk_up;
	global.constants[?"vk_down"] = vk_down;
	global.constants[?"vk_enter"] = vk_enter;
	global.constants[?"vk_escape"] = vk_escape;
	global.constants[?"vk_space"] = vk_space;
	global.constants[?"vk_shift"] = vk_shift;
	global.constants[?"vk_control"] = vk_control;
	global.constants[?"vk_alt"] = vk_alt;
	global.constants[?"vk_backspace"] = vk_backspace;
	global.constants[?"vk_tab"] = vk_tab;
	global.constants[?"vk_home"] = vk_home;
	global.constants[?"vk_end"] = vk_end;
	global.constants[?"vk_delete"] = vk_delete;
	global.constants[?"vk_insert"] = vk_insert;
	global.constants[?"vk_pageup"] = vk_pageup;
	global.constants[?"vk_pagedown"] = vk_pagedown;
	global.constants[?"vk_pause"] = vk_pause;
	global.constants[?"vk_printscreen"] = vk_printscreen;
	global.constants[?"vk_f1"] = vk_f1;
	global.constants[?"vk_f2"] = vk_f2;
	global.constants[?"vk_f3"] = vk_f3;
	global.constants[?"vk_f4"] = vk_f4;
	global.constants[?"vk_f5"] = vk_f5;
	global.constants[?"vk_f6"] = vk_f6;
	global.constants[?"vk_f7"] = vk_f7;
	global.constants[?"vk_f8"] = vk_f8;
	global.constants[?"vk_f9"] = vk_f9;
	global.constants[?"vk_f10"] = vk_f10;
	global.constants[?"vk_f11"] = vk_f11;
	global.constants[?"vk_f12"] = vk_f12;
	global.constants[?"vk_numpad0"] = vk_numpad0;
	global.constants[?"vk_numpad1"] = vk_numpad1;
	global.constants[?"vk_numpad2"] = vk_numpad2;
	global.constants[?"vk_numpad3"] = vk_numpad3;
	global.constants[?"vk_numpad4"] = vk_numpad4;
	global.constants[?"vk_numpad5"] = vk_numpad5;
	global.constants[?"vk_numpad6"] = vk_numpad6;
	global.constants[?"vk_numpad7"] = vk_numpad7;
	global.constants[?"vk_numpad8"] = vk_numpad8;
	global.constants[?"vk_numpad9"] = vk_numpad9;
	global.constants[?"vk_multiply"] = vk_multiply;
	global.constants[?"vk_divide"] = vk_divide;
	global.constants[?"vk_add"] = vk_add;
	global.constants[?"vk_subtract"] = vk_subtract;
	global.constants[?"vk_decimal"] = vk_decimal;
	global.constants[?"vk_lshift"] = vk_lshift;
	global.constants[?"vk_lcontrol"] = vk_lcontrol;
	global.constants[?"vk_lalt"] = vk_lalt;
	global.constants[?"vk_rshift"] = vk_rshift;
	global.constants[?"vk_rcontrol"] = vk_rcontrol;
	global.constants[?"vk_ralt"] = vk_ralt;
	
	global.constants[?"c_aqua"] = c_aqua;
	global.constants[?"c_black"] = c_black;
	global.constants[?"c_blue"] = c_blue;
	global.constants[?"c_dkgray"] = c_dkgray;
	global.constants[?"c_fuchsia"] = c_fuchsia;
	global.constants[?"c_gray"] = c_gray;
	global.constants[?"c_green"] = c_green;
	global.constants[?"c_lime"] = c_lime;
	global.constants[?"c_ltgray"] = c_ltgray;
	global.constants[?"c_maroon"] = c_maroon;
	global.constants[?"c_navy"] = c_navy;
	global.constants[?"c_olive"] = c_olive;
	global.constants[?"c_orange"] = c_orange;
	global.constants[?"c_purple"] = c_purple;
	global.constants[?"c_red"] = c_red;
	global.constants[?"c_silver"] = c_silver;
	global.constants[?"c_teal"] = c_teal;
	global.constants[?"c_white"] = c_white;
	global.constants[?"c_yellow"] = c_yellow;
	
	global.constants[?"fa_top"] = fa_top;
	global.constants[?"fa_middle"] = fa_middle;
	global.constants[?"fa_bottom"] = fa_bottom;
	global.constants[?"fa_left"] = fa_left;
	global.constants[?"fa_center"] = fa_center;
	global.constants[?"fa_right"] = fa_right;

}
