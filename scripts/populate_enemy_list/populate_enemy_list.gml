function populate_enemy_list() {
	ds_list_clear(global.enemy_list)
	ini_open(global.saveloc)
	if(ini_read_real("Enemy","Ordina",0))ds_list_add(global.enemy_list,ordina_name)
	if(ini_read_real("Enemy","Rouge",0))ds_list_add(global.enemy_list,rouge_name)
	if(ini_read_real("Enemy","Bleu",0))ds_list_add(global.enemy_list,bleu_name)
	if(ini_read_real("Enemy","Vert",0))ds_list_add(global.enemy_list,vert_name)
	if(ini_read_real("Enemy","Acier",0))ds_list_add(global.enemy_list,acier_name)
	if(ini_read_real("Enemy","Violet",0))ds_list_add(global.enemy_list,violet_name)
	if(ini_read_real("Enemy","Jaune",0))ds_list_add(global.enemy_list,jaune_name)
	if(ini_read_real("Enemy","Tilleul",0))ds_list_add(global.enemy_list,tilleul_name)
	if(ini_read_real("Enemy","Ciel",0))ds_list_add(global.enemy_list,ciel_name)
	ini_close()


}
