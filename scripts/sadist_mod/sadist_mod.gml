///@function sadist_mod()
///@description Returns a high value when Sadist is a good skill to use.
function sadist_mod(argument0) {
	var ea,chk,skl;
	ea = temp_enemy_action
	skl = argument0

	chk = ds_map_find_value(ea,skl)
	if(chk = undefined)return false

	if(state_count(enemy_state,"Sadist") > 0)
		{
		chk -= 100
		}
	else
		{
		chk += 25
		}

	if(mxpattern+speed_increase > 1)
	and(mxpattern+speed_increase < 6)
		{
		chk += 45
		}
	else
		{
		chk -= 25
		}
	if(global.cum < global.mxcum/2)
		{
		chk += 10
		}
	ds_map_replace(ea,skl,clamp(chk,0,500))

	show_debug_message(skl+" : "+string(chk)+"/"+string(ds_map_find_value(cenemy.enemy_action,skl)))
	return true


}
