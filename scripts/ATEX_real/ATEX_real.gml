/// @arg string
function ATEX_real(argument0) {

	var s = argument0;

	if is_string(s) {
		s = string_replace_all(s, ",", ".")
		var m = 1;
		if string_count(".", s) > 1 return 0;
		if string_char_at(s, 1) == "-" {
			if string_length(s) == 1 return 0;
			s = string_delete(s, 1, 1);
			m = -1;
		}
		for(var i = 1; i <= string_length(s); i++) {
			if (string_pos(string_char_at(s, i), "1234567890.") < 1) 
			{
				return 0;
			}
		}
		return real(s) * m;
	}

	if !is_real(s) {
		if s == undefined return 0;
		return real(s);	
	}

	return s;


}
