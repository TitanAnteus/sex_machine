///@function state_change(dmg,cdm,str,reg)
///@parm dmg - Total willpower damage to modify
///@parm cdm - Total cum damage to modify
///@parm str - Total hero power to modify
///@parm reg - Total energy regen to modify
///@description Runs at the end of a turn
function state_change(dmg,cdm,str,reg) {
	var tret;
	tret[0] = dmg
	tret[1] = cdm
	tret[2] = str
	tret[3] = reg
	
	//Enemy State Changes
	if(check_estate("Overheat"))
		{
		dmg *= 1.05
		cdm *= 1.3
		str *= 1.15
		}
	if(check_estate("Paralyzing Pressure"))
		{
		str *= .6
		}
	if(check_estate("Taunt"))
		{
		str *= .5
		}
	if(check_estate("Heavy"))
		{
		dmg *= 1.05
		reg *=.02
		}
	if(check_estate("Vaginal Control"))
		{
		cdm *= 1.5
		}
	if(check_estate("Pressure"))
		{
		dmg *= 1.1
		cdm *= 1.1
		}
		
	//Player State Changes
	if(check_pstate("Victory Cry"))
		{
		str *= 1.2
		}
	if(check_pstate("Ready"))
		{
		dmg *= .88
		cdm *= .88
		str *= 1.2
		}
	if(check_pstate("Pressure"))
		{
		str *= 1.5
		}
		
	if(check_pstate("Attack Boost"))
		{
		str *= 1.5
		}
	if(check_pstate("Stamina Boost"))
		{
		reg *= 1.5
		}
	if(check_pstate("Sensitive"))
		{
		dmg *= 1.5
		cdm *= 1.5
		}
		
	if(check_pstate("Focus"))
		{
		dmg *= .4
		cdm *= .4
		}
	
	tret[0] = dmg
	tret[1] = cdm
	tret[2] = str
	tret[3] = reg
	return tret
}
