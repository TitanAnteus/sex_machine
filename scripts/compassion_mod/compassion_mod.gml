///@function compassion_mod()
///@description Returns a high value when Compassion is a good skill to use.
function compassion_mod(argument0) {
	var ea,chk,skl;
	ea = temp_enemy_action
	skl = argument0

	chk = ds_map_find_value(ea,skl)
	if(chk = undefined)return false

	if(mxpattern+speed_increase < 6)
	and(cenemy.willpower < cenemy.mxwillpower*.8)
		{
		chk += 30
		}
	else
		{
		chk -= 25
		}

	if(cenemy.willpower < cenemy.mxwillpower*.3)chk += 30
	if(cenemy.willpower < cenemy.mxwillpower*.15)chk += 30
	ds_map_replace(ea,skl,clamp(chk,0,500))
	show_debug_message(skl+" : "+string(chk)+"/"+string(ds_map_find_value(cenemy.enemy_action,skl)))
	return true


}
