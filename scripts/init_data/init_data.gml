function init_data() {
	global.maxPP = 6
	global.cPP = 0
	global.all_passive = ds_map_create()
	global.total_passive = ds_map_create()
	
	//All the Passives that can be added to the game.
	ds_map_add(global.total_passive,"Triple Stamina | トリプルスタミナ","Perfect guarding 3 times has a 30% chance to restore 20% of stamina. | 3回パーフェクトガードすると、20％の確率でスタミナの30％を回復します。")
	ds_map_add(global.total_passive,"Slow Lover | 遅い恋人","Ending a turn at minimum speed has a 50% chance to reduce lust by 10%. | 最低速度でターンを終了すると、50％の確率で欲望が10％減少します。")
	ds_map_add(global.total_passive,"Unbreakable | 割れない","Getting all perfect guards deals an additional 10% damage, and decreases lust by 10%. | 完全なガードをすべて取得すると、追加の10％のダメージが与えられ、欲望が10％減少します。")
	ds_map_add(global.total_passive,"Odd Calm | 奇妙な穏やかさ","50% Chance to reduce Lust by 10% if the turn speed is odd. | ターン速度が奇数の場合、10％の確率で欲望を50％削減します。")
	ds_map_add(global.total_passive,"Stamina Speed | スタミナスピード","Chance to increase stamina by 15% every turn. \nChance increases with higher speeds. | ターンごとにスタミナを15％増やすチャンス。 \nチャンスは、速度が上がると増加します。")
	ds_map_add(global.total_passive,"Sensitive | 敏感","Chance to take 1.5x damage for the turn. \nChance increases with lower speeds. | ターン中に1.5倍のダメージを受けるチャンス。 \nチャンスは速度が遅くなると増加します。")
	ds_map_add(global.total_passive,"Panic Attack | パニック発作","Very small chance to increase battle speed every turn. | 毎ターン戦闘速度を上げる非常に小さなチャンス。")
	
	ds_map_add(global.total_passive,"Strong Will | 強い意志","The higher the willpower the higher the chance that stamina will slightly recover. | 意志力が高いほど、スタミナがわずかに回復する可能性が高くなります。")
	ds_map_add(global.total_passive,"Rhythm | リズム","Chance to nullify speed increases. | 速度を無効にするチャンスが増えます。")
	ds_map_add(global.total_passive,"Keen Eye | するどいめ","Small chance to see all enemy arrows for the next turn. | 次のターンにすべての敵の矢を見る小さなチャンス。")
	ds_map_add(global.total_passive,"Shaky Hips | 不安定な腰","Small chance to disable use of skills for 1 turn.\nChance increases at higher speeds. | 1ターンの間スキルの使用を無効にする小さなチャンス。\n高速になるとチャンスが増えます。")
	ds_map_add(global.total_passive,"Shaky Hands | 手ぶれ","Small chance to disable use of items for 1 turn.\nChance increases at higher speeds. | 1ターンの間アイテムの使用を無効にする小さなチャンス。\n高速になるとチャンスが増えます。")
	ds_map_add(global.total_passive,"Conviction | 信念","Getting all perfect guards increases strength for 1 turn. | すべての完璧なガードを取得すると、1ターンの間強度が増加します。")
	
	//These Passives appear in the pequip menu.
	ds_map_add(global.all_passive,"Triple Stamina | トリプルスタミナ","Perfect guarding 3 times has a 30% chance to restore 20% of stamina. | 3回パーフェクトガードすると、20％の確率でスタミナの30％を回復します。")
	ds_map_add(global.all_passive,"Slow Lover | 遅い恋人","Ending a turn at minimum speed has a 50% chance to reduce lust by 10%. | 最低速度でターンを終了すると、50％の確率で欲望が10％減少します。")
	ds_map_add(global.all_passive,"Unbreakable | 割れない","Getting all perfect guards deals an additional 10% damage, and decreases lust by 10%. | 完全なガードをすべて取得すると、追加の10％のダメージが与えられ、欲望が10％減少します。")
	ds_map_add(global.all_passive,"Odd Calm | 奇妙な穏やかさ","50% Chance to reduce Lust by 10% if the turn speed is odd. | ターン速度が奇数の場合、10％の確率で欲望を50％削減します。")
	ds_map_add(global.all_passive,"Stamina Speed | スタミナスピード","Chance to increase stamina by 15% every turn. \nChance increases with higher speeds. | ターンごとにスタミナを15％増やすチャンス。 \nチャンスは、速度が上がると増加します。")
	ds_map_add(global.all_passive,"Sensitive | 敏感","Chance to take 1.5x damage for the turn. \nChance increases with lower speeds. | ターン中に1.5倍のダメージを受けるチャンス。 \nチャンスは速度が遅くなると増加します。")
	ds_map_add(global.all_passive,"Panic Attack | パニック発作","Very small chance to increase battle speed every turn. | 毎ターン戦闘速度を上げる非常に小さなチャンス。")
	
	//ds_map_copy(global.all_passive,global.total_passive)

	global.equip_passive = ds_map_create()
	//ds_map_add(global.equip_passive,"Slow Lover | 遅い恋人","Ending a turn at minimum speed has a 50% chance to reduce lust by 10%. | 最低速度でターンを終了すると、50％の確率で欲望が10％減少します。")
	#macro igrec_name "Igrec | イグレック"
	#macro igrec_name1 "Igrec | イグレック (1)"
	#macro igrec_name2 "Igrec | イグレック (2)"
	#macro rouge_name "Rouge | ルージュ"
	#macro bleu_name "Bleu | ブルー"
	#macro vert_name "Vert | ヴェール"
	#macro acier_name "Acier | アシエ"
	#macro violet_name "Violet | バイオレット"
	#macro jaune_name "Jaune | ジョーン"
	#macro argent_name "Argent | アージェント"
	#macro doree_name "Doree | ドリー"
	#macro electrum_name "Electrum | エレクトラム"
	#macro tilleul_name "Tilleul | ティルウル"
	#macro ciel_name "Ciel | シエル"
	#macro mercure_name "Mercure | メルキュール"
	#macro ordina_name "Ordina | オルディーナ"
	#macro android_name "Android | アンドロイド"
	
	//Cutscene Macros
	#macro cut_auto 0
	#macro cut_examine 1
	#macro cut_collide 2
}
