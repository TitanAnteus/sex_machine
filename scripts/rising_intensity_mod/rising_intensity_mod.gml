///@function rising_intensity_mod()
///@description Returns a high value when Rising Intensity is a good skill to use.
function rising_intensity_mod(argument0) {
	var ea,chk,skl;
	ea = temp_enemy_action
	skl = argument0

	chk = ds_map_find_value(ea,skl)
	if(chk = undefined)return false

	if(state_count(enemy_state,"Rising Intensity") > 1)
		{
		chk -= 100
		}
	else
		{
		chk += 10
		}
	if(mxpattern+speed_increase <= 5)
		{
		chk += 50
		}
	else
		{
		chk -= 25
		}
	ds_map_replace(ea,skl,clamp(chk,0,500))
	show_debug_message(skl+" : "+string(chk)+"/"+string(ds_map_find_value(cenemy.enemy_action,skl)))
	return true


}
