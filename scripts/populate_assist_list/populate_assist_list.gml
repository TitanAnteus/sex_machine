function populate_assist_list() {
	ds_list_clear(global.assist_list)
	ini_open(global.saveloc)
	if(ini_read_real("Assist","Rouge",0) != 0)ds_list_add(global.assist_list,rouge_name)
	if(ini_read_real("Assist","Bleu",0) != 0)ds_list_add(global.assist_list,bleu_name)
	if(ini_read_real("Assist","Vert",0) != 0)ds_list_add(global.assist_list,vert_name)
	if(ini_read_real("Assist","Acier",0) != 0)ds_list_add(global.assist_list,acier_name)
	if(ini_read_real("Assist","Violet",0) != 0)ds_list_add(global.assist_list,violet_name)
	if(ini_read_real("Assist","Jaune",0) != 0)ds_list_add(global.assist_list,jaune_name)
	if(ini_read_real("Assist","Tilleul",0) != 0)ds_list_add(global.assist_list,tilleul_name)
	if(ini_read_real("Assist","Ciel",0) != 0)ds_list_add(global.assist_list,ciel_name)
	ini_close()


}
