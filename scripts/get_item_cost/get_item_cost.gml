/// @function get_item_cost(passivename)
/// @description returns the cost of the passive
/// @param passivename - The name of the passive
function get_item_cost(argument0) {
	var itemname;
	itemname = string(argument0)
	if(string_count("Passion Plus",itemname) > 0)return 15
	if(string_count("Energizer",itemname) > 0)return 10
	if(string_count("Libido Control",itemname) > 0)return 20
	if(string_count("Iron Resistance",itemname) > 0)return 80
	if(string_count("Scope",itemname) > 0)return 100
	return 0
}
