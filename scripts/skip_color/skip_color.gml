/// @function skip_color(text,pos)
/// @description Draws a string at a width
/// @param text - text to skip colors of
/// @param pos - current position in text
function skip_color(text, pos) {
	var cp,skp;
	cp = 0
	skp = 0
	for(cp=pos;cp<string_length(text);cp+=1)
		{
		if(string_copy(text,cp,1) == "<")
			{
			for(skp=0;skp<string_length(text);skp+=1)
				{
				if(string_copy(text,cp+skp,1) == ">")
					{
					return pos+skp
					}
				}
			return pos
			}
		return pos
		}
	return pos
}
