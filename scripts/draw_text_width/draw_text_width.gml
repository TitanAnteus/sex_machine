/// @function draw_text_width(x,y,string,width,height)
/// @description Draws a string at a width
/// @param x - x position of string
/// @param y - y position of string
/// @param string - string to draw
/// @param width - is the maximum width it'll draw at
/// @param height - is the height of the string
function draw_text_width(argument0, argument1, argument2, argument3, argument4) {
	var xx,yy,txt,ww,ustr,th,nocol;
	xx = argument0
	yy = argument1
	txt = string_split(argument2)
	ww = argument3
	th = argument4
	th = -1
	
	var hal,val;
	hal = draw_get_halign()
	val = draw_get_valign()
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)
	
	if(global.language = JAPANESE)
		{
		ustr = newline(txt,ww-10)
		nocol = no_color(ustr)
		if(hal = fa_center)xx = xx-(string_width(nocol)/2)
		if(val = fa_center)yy = yy-(string_height(nocol)/2)
		
		ATEX_draw(xx,yy,ustr,-1,hal)
		}
	else
		{
		ustr = newline_en(txt,ww-10)
		nocol = no_color(ustr)
		if(hal = fa_center)xx = xx-(string_width(nocol)/2)
		if(val = fa_center)yy = yy-(string_height(nocol)/2)
		
		ATEX_draw(xx,yy,ustr,-1,hal)
		}
	draw_set_halign(hal)
	draw_set_valign(val)

}
