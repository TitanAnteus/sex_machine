/// @function camera_center()
/// @description Centers the camera around current x and y location
function camera_center() {
	var acamera,viewxview,viewyview,viewwview,viewhview,newx,newy;

	acamera = view_camera[0]
	viewxview = camera_get_view_x(acamera)
	viewyview = camera_get_view_y(acamera)
	viewwview = camera_get_view_width(acamera)
	viewhview = camera_get_view_height(acamera)


	newx = -(viewwview/2)+x
	newy = -(viewhview/2)+y

	newx = clamp(newx,0,room_width-viewwview)
	newy = clamp(newy,0,room_height-viewhview)

	newx += random_range(-shake,shake)
	newy += random_range(-shake,shake)

	camera_set_view_pos(acamera,newx,newy)


}
