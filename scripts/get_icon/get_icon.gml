///@function get_icon(state)
///@description Gets the icon of the state
///@param state - The name of the state
function get_icon(argument0) {

	var fin,ste;
	ste = argument0
	fin = string_lower(ste)
	fin = string_replace(fin," ","_")
	fin = "spr_"+fin+"_icon"
	return asset_get_index(fin)


}
