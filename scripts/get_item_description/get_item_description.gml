/// @function get_item_description(itemstring)
/// @description Returns a description text from the name of the item
/// @param itemstring - The name of the item
function get_item_description(argument0) {
	var itemname;
	itemname = full_itemname(argument0)
	if(string_count("Passion Plus",itemname) > 0)return "Increases Willpower by 50% | 意志力が50％増加。"
	if(string_count("Energizer",itemname) > 0)return "Completely restores Stamina, and reduces Lust by 15%. | スタミナを完全に回復し欲望を15％減らします。"
	if(string_count("Libido Control",itemname) > 0)return "Reduce Lust by 75% | 欲望を75％減らす。"
	if(string_count("Iron Resistance",itemname) > 0)return "Increases Maximum Willpower by 50%. | 最大意志力が50%増加します。"
	if(string_count("Scope",itemname) > 0)return "Grants insight. \nThe current turn is completely visible. | 洞察を与えます。 \n現在のターンは完全に見えます。"
	return ""
}
