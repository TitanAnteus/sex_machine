///@function playful_spirit_mod()
///@description Returns a high value when Playful Spirit is a good skill to use.
function playful_spirit_mod(argument0) {
	var ea,chk,skl;
	ea = temp_enemy_action
	skl = argument0

	chk = ds_map_find_value(ea,skl)
	if(chk = undefined)return false

	if(mxpattern+speed_increase <= 4)
		{
		chk += 45
		}
	if(mxpattern+speed_increase >= 7)
		{
		chk += 45
		}
	if(cenemy.willpower > cenemy.mxwillpower-10)chk += 200
	ds_map_replace(ea,skl,clamp(chk,0,500))
	show_debug_message(skl+" : "+string(chk)+"/"+string(ds_map_find_value(cenemy.enemy_action,skl)))
	return true


}
