/// @function up_direct()
/// @description Checks if up key is being pressed
function up_direct() {
	if(keyboard_check(global.k_up))return 1
	return 0


}
