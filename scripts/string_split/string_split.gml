/// @function string_split(string)
/// @description splits a string and returns the current language
/// @param string - the string to split
function string_split(argument0) {
	var split,cent;
	split = string(argument0)
	if(string_count("|",split) > 0)
		{
		cent = string_pos("|",split)
		if(global.language == JAPANESE)
			{
			if(string_char_at(split,cent+1) == " ")return string_copy(split,cent+2,string_length(split))
			else{return string_copy(split,cent+1,string_length(split))}
			}
		else
			{
			if(string_char_at(split,cent-1) == " ")return string_copy(split,0,cent-2)
			else{return string_copy(split,0,cent-1)}
			}
		}
	else
		{
		return split
		}


}
