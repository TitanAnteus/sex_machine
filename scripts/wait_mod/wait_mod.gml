///@function wait_mod()
///@description Returns a high value when Wait is a good skill to use.
function wait_mod(argument0) {
	var ea,chk,skl,mn;
	ea = temp_enemy_action
	skl = argument0

	mn = -1

	chk = ds_map_find_value(ea,skl)
	if(chk = undefined)return false

	if(ds_map_size(cenemy.enemy_action) <= 2)mn = 2

	if(cenemy.willpower > cenemy.mxwillpower*.85+random(.15))chk += irandom_range(2,10)
	if(cenemy.willpower < cenemy.mxwillpower*.85)chk -= 50

	ds_map_replace(ea,skl,clamp(chk,mn,500))

	show_debug_message(skl+" : "+string(chk)+"/"+string(ds_map_find_value(cenemy.enemy_action,skl)))
	return true


}
