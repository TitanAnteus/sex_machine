/// @function enemy_damage()
/// @description Code that runs every thrust
function enemy_damage() {
	audio_play_sound(sfx_slap,1,0)
	if(room = rm_menu)exit
	if(reveal != mxpattern)
		{
		if(action_timer > 0)
		and(! instance_exists(obj_battle_tutorial))
		and(state = "Pattern")
			{
			if(global.battleposition >= 2)action_timer -= 1
			}
		if(action_timer == 0)
			{
			action_timer = -1
			cmenu = 0
			cpos = 0
			event_perform(ev_alarm,1)
			exit
			}
		if(global.willpower <= 0)
			{
			if(global.gameover = false)
			and(state != "Victory")
				{
				global.gameover = true
				audio_sound_gain(battle_bgm,0,60*15)
				audio_play_sound(sfx_defeat,1,0)
				}
			}

		var dmg,cdm,str,reg;
		dmg = cenemy.strength
		cdm = cenemy.skill
		str = global.strength
		reg = global.stamina_regen
	
		if(! check_estate("Merciless"))
			{
			dmg *= clamp(1-(.085*(mxpattern-array_length_2d(cenemy.habit,0))),.3,1)
			cdm *= clamp(1-(.085*(mxpattern-array_length_2d(cenemy.habit,0))),.3,1)
			}

		str *= clamp(1-(.05*(mxpattern-array_length_2d(cenemy.habit,0))),.3,1)
	
		dmg *= 1-(cream_count/12)
		cdm *= 1-(cream_count/6)

		global.exhaust = global.mxstamina/3
		if(global.stamina < global.exhaust)
			{
			dmg *= 1.02
			cdm *= 1.2
			str *= .8
			}

		var ret;
		ret = state_change(dmg,cdm,str,reg)
		dmg = ret[0]
		cdm = ret[1]
		str = ret[2]
		reg = ret[3]
		
		if(global.gameover = true)
			{
			if(global.cum < global.mxcum)
				{
				global.cum += cdm*.38
				exit
				}
			}
		if(state = "Test")
			{
			if(reveal < mxpattern)
				{
				if(check_estate("Exhausting Pound"))
					{
					global.stamina -= 10
					speed_increase -= 1
					}
				reveal += 1
				var match,pat1,pat2;
				pat1 = pattern[0,reveal-1]
				pat2 = pattern[1,reveal-1]
				
				match = 1
				if(pat1 = pat2)match = 0
				if(pat1 = opposite_dir(pat2))match = 2
				if(pat1 = "")match = 2
		
				if(match = 0)
					{
					guardcount += 1
					audio_play_sound(sfx_correct,1,0)
					dmg = 0
					if(global.difficulty = EASY)
						{
						str *= 1.75
						cdm *= .2
						}
					else
						{
						str *= 1.7
						cdm *= .25
						}
				
					global.stamina += reg*.6
			
					if(check_estate("Sadist"))
						{
						speed_increase += 1
						}
					if(check_pstate("Calm"))
						{
						speed_increase -= 1
						}
					}
				if(match = 1)
					{
					misscount += 1
					audio_play_sound(sfx_null,1,0)
					if(global.difficulty = EASY)
						{
						str *= .5
						cdm *= 1.05
						}
					else
						{
						str *= .45
						cdm *= 1.1
						}
					global.stamina += reg*.4
				
					if(check_estate("Loving Caress"))
						{
						audio_play_sound(sfx_heal,1,0)
						cenemy.willpower += str*4
						}
					}
				if(match = 2)
					{
					misscount += 1
					failcount += 1
					if(global.difficulty = EASY)
						{
						dmg *= 1.1
						str *= .25
						cdm *= 1.3
						}
					else
						{
						dmg *= 1.15
						str *= .2
						cdm *= 1.45
						}
					audio_play_sound(sfx_fail,1,0)
					global.stamina += reg*.05
				
				
					if(check_estate("Loving Caress"))
						{
						audio_play_sound(sfx_heal,1,0)
						cenemy.willpower += str*8
						}
					}
				}
			if(cdm < 0)cdm = 0
			if(dmg < 0)dmg = 0
			if(check_pstate("Ready") == false)
				{
				cenemy.willpower -= str
				}
			else
				{
				global.stamina += str*1.8
				}
			if(global.willpower > 0)global.willpower -= dmg
			if(global.cum < global.mxcum)global.cum += cdm
			}
		}
	else
		{
		draw_continue = true
		}


}
