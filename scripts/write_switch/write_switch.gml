/// @function write_switch(section,key,value)
/// @description Writes a switch to the savefile
/// @param section - The section of the ini file, usually "Extra"
/// @param key - The id of the switch
/// @param value - Turn it off or on
function write_switch(section,key,value){
	ini_open(global.saveloc)
	ini_write_real(section,key,value)
	ini_close()
}