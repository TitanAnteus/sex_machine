/// @function back_pressed()
/// @description Checks if the select key has been pressed
function back_pressed() {
	if(keyboard_check_pressed(global.k_back))return 1
	return 0


}
