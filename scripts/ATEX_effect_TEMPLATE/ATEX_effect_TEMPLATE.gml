function ATEX_effect_TEMPLATE(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8) {
	var type=argument0, args=argument1, props=argument2, cpos=argument3, lineH=argument4, h=argument5, symb=argument6, text=argument7, symb_index=argument8;

#region tag data

	if type=="depth" 
		return 10
	if type=="names"
		return ["shadow"]
	if type=="type"
		return ATEX.drawer
	
#endregion

	return true


}
