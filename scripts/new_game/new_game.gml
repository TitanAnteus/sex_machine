function new_game(argument0) {
	file_delete(global.saveloc)
	//file_delete(global.fsaveloc)
	//file_delete(working_directory+"save.png")
	ini_open(global.saveloc)
	ini_write_real("Main","Difficulty",argument0)
	
	global.trans_pos = 0
	global.trans_dir = 0
	
	global.seed = current_second
	ini_write_real("Main","Random",global.seed)
	ds_map_clear(global.equip_passive)
	ds_list_clear(global.item_list)
	ds_list_clear(global.equip_list)
	global.equipment = ""

	ds_list_clear(global.enemy_list)
	ds_list_clear(global.assist_list)
	ds_list_clear(global.assist_battle)
	ds_map_clear(global.assist_cooldown)
	global.battle = 0
	global.gameover = 0
	global.bits = 0
	ini_close()

	global.mxwillpowerbase = 150
	global.mxwillpower = global.mxwillpowerbase

	global.willpower = global.mxwillpower

	global.mxstaminabase = 100
	global.mxstamina = global.mxstaminabase
	global.stamina = global.mxstamina
	global.stamina_regenbase = 4
	global.stamina_regen = global.stamina_regenbase

	global.exhaustbase = global.mxstamina/3
	global.exhaust = global.exhaustbase

	global.mxcumbase = 100
	global.mxcum = global.mxcumbase
	global.cum = 0

	global.strengthbase = 4
	global.strength = global.strengthbase
	
	
	//global.firstbattle = true
	//global.firstloss = true
	//global.firstpassive = true
	//global.firstordina = true
	
	global.battleposition = 0
	global.firstloss = true
	global.firstpassive = true
	global.firstordina = true
	
	global.difficulty = argument0
	if(global.difficulty = EASY)
		{
		global.mxitems = 15
		}
	else
		{
		global.mxitems = 8
		}

	global.skill_list = ds_list_create()
	ds_list_add(global.skill_list,"Slow | 鈍化")
	/*
	ds_list_add(global.skill_list,"Calm | 落ち着く")
	ds_list_add(global.skill_list,"Focus | 集中")
	ds_list_add(global.skill_list,"Meditate | 瞑想")
	ds_list_add(global.skill_list,"Psych Up | サイケアップ")
	ds_list_add(global.skill_list,"Ready | 角郷")
	ds_list_add(global.skill_list,"Pressure | 圧力")
	ds_list_add(global.skill_list,"Victory Cry | 勝利の叫ぶ")
	ds_list_add(global.skill_list,"Surrender | 降伏")*/


	populate_assist_list()
	
	/*ini_write_real("Assist","Rouge",true)
	ini_write_real("Assist","Bleu",true)
	ini_write_real("Assist","Vert",true)
	ini_write_real("Assist","Acier",true)*/
	
	room_goto(rm_area1_1)


}
