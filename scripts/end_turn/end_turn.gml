///@function end_turn()
///@description Runs at the end of a turn
function end_turn() {
	if(check_estate("Rising Intensity"))
		{
		audio_play_sound(sfx_rise,1,0)
		speed_increase += 1
		}
	if(check_estate("Stamina Pit"))
		{
		global.stamina -= global.mxstamina*(.05+random(.2))
		audio_play_sound(sfx_powerdown,1,0)
		}
	if(check_estate("Passionate Reaction"))
		{
		audio_play_sound(sfx_rise,1,0)
		speed_increase += round(global.cum/20)
		}

	if(check_pstate("Willpower Regen"))
		{
		global.willpower += global.mxwillpower*.1
		}
	if(check_pstate("Lust Control"))
		{
		global.cum -= global.mxcum*.05
		}


	cmenu = 0
	cpos = 0
	use_skill = false
	use_item = false
	draw_continue = false
	state = "Command Select"
	if(check_pstate("Itemless"))use_item = true
	if(check_pstate("Silence"))use_skill = true

	guardcount = 0
	failcount = 0
	misscount = 0

	global.stamina += global.stamina_regen
	if(speed_increase != 0)
		{
		if(check_estate("Superheat") = true)
		or(check_estate("Overheat") = true)
			{
			if(speed_increase < 0)speed_increase = 0
			}
		set_speed(mxpattern+speed_increase)	
		speed_increase = 0
		}
	rand_pattern()
	clear_special_enemystate()

	action_timer = set_timer()
}
