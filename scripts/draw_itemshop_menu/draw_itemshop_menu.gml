/// @function draw_item_menu()
/// @description Draws the item menu
function draw_itemshop_menu() {

	var boxwidth,boxheight,topy,mpad,descheight;
	boxwidth = rightwidth
	boxheight = rightheight
	descheight = 285
	topy = 20
	mpad = 50

	draw_set_halign(fa_center)
	draw_set_valign(fa_top)
	draw_text(boxwidth/2,topy,string_split("Item Shop | アイテムショップ"))
	draw_line(10,topy+mpad,boxwidth-10,topy+mpad)

	var icw,ich,leftmost;
	icw = sprite_get_width(spr_sadist_icon)
	ich = icw
	leftmost = 120

	draw_set_halign(fa_left)
	draw_set_valign(fa_center)
	topy = topy+mpad
	if(draw_get_color() != c_white)draw_set_color(c_white)

	var citem,h,k,ww,hh,hor,leftx,lsize,cpos_y,isize;
	hor = 1
	ww = boxwidth/hor
	hh = font_get_size(global.big_font)+8
	leftx = 20
	peskill = ""
	draw_set_halign(fa_left)
	draw_set_valign(fa_center)

	lsize = ds_list_size(itemshop_list)
	var mxh,sh;
	mxh = 7
	sh = 0
	cpos_y = -(mxh/2)
	if(dpos > 0)and(lsize > mxh)
		{
		cpos_y += dpos
		cpos_y = clamp(cpos_y,0,lsize-mxh)
		}
	else
		{
		cpos_y = 0
		}

	if(! surface_exists(shopsurf))
		{
		shopsurf = surface_create(boxwidth,boxheight-topy-descheight)
		}
	else
		{
		surface_set_target(shopsurf)
		draw_clear_alpha(c_white,0)
		var col,ccost;
		col = c_white
		draw_set_font(global.main_font)
		for(i=0;i<ds_list_size(itemshop_list);i+=1)
			{
			citem = itemshop_list[| i]
			ccost = get_item_cost(citem)
			sh = string_height("H")+4
			col = c_white
			if(selecting = true)
				{
				if(dpos = i){
					col = c_red
					if(ccost > global.bits)col = merge_colour(col,c_black,.5)
					if(ds_list_size(global.item_list) >= global.mxitems)col = merge_colour(c_yellow,c_black,.5)
					}
				else{
					col = c_white
					if(ccost > global.bits)col = merge_colour(col,c_black,.5)
					if(ds_list_size(global.item_list) >= global.mxitems)col = merge_colour(c_red,c_black,.5)
					}
				}
			else
				{
				col = c_white
				if(ccost > global.bits)col = merge_colour(col,c_black,.5)
				}
			
			if(draw_get_color() != col)draw_set_color(col)
			draw_set_halign(fa_left)
			draw_text(8,3+(sh*(i+.5))-(cpos_y*sh),""+string_split(citem))
			draw_set_halign(fa_right)
			//draw_text(boxwidth-65,3+(sh*(i+.5))-(cpos_y*sh),"-")
			draw_text(boxwidth-8,3+(sh*(i+.5))-(cpos_y*sh),string(ccost)+" Bits")
			}
		draw_set_font(global.main_font)
		surface_reset_target()
		draw_surface(shopsurf,0,topy)
		}
	if(draw_get_color() != c_white)draw_set_color(c_white)
	topy = boxheight-descheight

	draw_line(10,topy,boxwidth-10,topy)
	var staty,dcol,description;
	staty = topy+((boxheight-topy)/2)
	leftx = boxwidth*.5
	dcol = c_white
	draw_set_halign(fa_center)
	draw_set_valign(fa_center)

	//staty = (staty)+((boxheight-staty)/2)
	if(selecting = true)
		{
		description = get_item_description(itemshop_list[| dpos])
		draw_text_width(boxwidth*.5,staty,string_split(description),boxwidth-10,font_get_size(global.big_font)+8)
		}


}
