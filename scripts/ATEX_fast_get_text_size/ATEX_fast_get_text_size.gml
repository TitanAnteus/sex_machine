/// @arg body
function ATEX_fast_get_text_size(argument0) {

	ATEX_fast_get_surface(argument0)
	var body=argument0[? "body"];

	return [body[1], body[2]]


}
