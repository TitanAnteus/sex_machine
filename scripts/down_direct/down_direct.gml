/// @function down_direct()
/// @description Checks if down key is being pressed
function down_direct() {
	if(keyboard_check(global.k_down))return 1
	return 0


}
