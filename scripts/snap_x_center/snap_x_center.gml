/// @function snap_x_center()
/// @description Snaps a centered object on the grid
function snap_x_center() {
	x = (round(x) div global.grid) * global.grid
}
