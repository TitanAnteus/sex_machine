///@function command_stamina(cmenu,cpos)
///@description returns true if you have the require stamina to perform an action
///@param cmenu - command[cmenu,cpos]
///@param cpos - command[cmenu,cpos[
function command_stamina(argument0) {

	current_cost = global.skill_cost[? argument0]
	if(current_cost = undefined)current_cost = 0
	if(current_cost = -1)
		{
		if(global.stamina >= global.exhaust)current_cost = global.stamina
		else{current_cost = global.mxstamina+1}
		}
	if(global.stamina >= current_cost)
		{
		return true
		}
	else
		{
		return false
		}


}
