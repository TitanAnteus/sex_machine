///@function clear_special_enemystate()
///@description Clears the first instance of all states for the enemy
function clear_special_enemystate() {
	var st,in;
	for(i=0;i<ds_list_size(global.states);i+=1)
		{
		st = global.states[| i]
		in = ds_list_find_index(enemy_state,st)
		if(in != -1)
			{
			if(st = "Taunt")
				{
				if(mxpattern+speed_increase > 4)ds_list_delete(enemy_state,ds_list_find_index(enemy_state,st))
				}
			else if(st = "Paralyzing Pressure")
				{
				if(mxpattern+speed_increase > 5)ds_list_delete(enemy_state,ds_list_find_index(enemy_state,st))
				}
			else
				{
				//
				}
			}
		}



}
