/// @function draw_status_menu()
/// @description Draws the status menu
function draw_restore_menu() {

	var boxwidth,boxheight,leftx,topy,mpad;
	boxwidth = rightwidth
	boxheight = rightheight

	topy = 200
	mpad = 50

	draw_set_halign(fa_center)
	draw_set_valign(fa_center)
	draw_text(boxwidth/2,boxheight*.2,string_split("Restore | 休息"))

	draw_line(10,topy-2,boxwidth-10,topy-2)
	draw_set_halign(fa_right)
	draw_set_valign(fa_top)

	leftx = boxwidth*.4

	draw_text(leftx,topy,"HP: ")
	draw_text(leftx,topy+mpad,"STM: ")

	draw_line(10,topy+(mpad*2)+2,boxwidth-10,topy+(mpad*2)+2)

	var barx,barwidth;
	barx = leftx+4
	barwidth = boxwidth-barx-10

	draw_set_color(c_red)
	draw_rectangle(barx,topy+5,barx+(barwidth*(global.willpower/global.mxwillpower)),-5+topy+mpad,0)
	draw_set_color(c_lime)
	draw_rectangle(barx,topy+(mpad)+5,barx+(barwidth*(global.stamina/global.mxstamina)),-5+topy+(mpad*2),0)
	draw_set_color(c_white)
	draw_rectangle(barx-1,topy+5,barx+barwidth,-5+topy+mpad,1)
	draw_rectangle(barx-1,topy+(mpad)+5,barx+barwidth,-5+topy+(mpad*2),1)

	topy += (mpad*2)+5
	var col;
	col = c_lime
	if(global.bits < global.restcost)col = c_red
	draw_set_color(col)
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)

	draw_text(10,topy+1,string_split("Cost: | 費用: ")+string(global.restcost))
	topy += mpad
	draw_set_color(c_white)
	draw_line(10,topy,boxwidth-10,topy)

	var staty,dcol,description;
	staty = topy+((boxheight-topy)/2)
	leftx = boxwidth*.5
	dcol = c_white
	if(draw_get_color() != c_white)draw_set_color(c_white)
	draw_set_halign(fa_center)
	draw_set_valign(fa_center)

	//staty = (staty)+((boxheight-staty)/2)
	description = "Restore Igrec to Peak Condition | イグレックを最高の状態に戻す"
	draw_text_width(boxwidth*.5,staty,string_split(description),boxwidth-10,font_get_size(global.big_font)+8)


}
