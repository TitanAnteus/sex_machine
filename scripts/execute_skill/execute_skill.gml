/// @function execute_skill()
/// @description Executes the currently selected skill
function execute_skill() {
	var skl;
	skl = command[cmenu,cpos]
	if(string_count("Slow",skl) > 0)
		{
		audio_play_sound(sfx_slow,1,0)
		cenemy.chab += array_height_2d(cenemy.habit)-1
		set_speed(mxpattern-(1+floor(random(2))))
		rand_pattern()
		//ds_list_add(exclude,"D")
		}
	if(string_count("Focus",skl) > 0)
		{
		ds_list_add(player_state,"Focus")
		audio_play_sound(sfx_powerup,1,0)
		//ds_list_add(exclude,"U")
		}
	if(string_count("Meditate",skl) > 0)
		{
		global.cum -= global.mxcum*.35
		audio_play_sound(sfx_powerup,1,0)
		//ds_list_add(exclude,"U")
		}
	if(string_count("Victory Cry",skl) > 0)
		{
		ds_list_add(player_state,"Victory Cry")
		audio_play_sound(sfx_powerup,1,0)
		//ds_list_add(exclude,"U")
		}
	if(string_count("Psych Up",skl) > 0)
		{
		var psych,tops,high,low;
		high = .5
		low = .05
	
		psych = high
		tops = (high-low)/(max_cream_count-cream_count)
		psych -= tops*(cream_count-0)
	
		psych = clamp(psych,low,high)
	
		global.willpower += global.mxwillpower*psych
		audio_play_sound(sfx_heal,1,0)
		cenemy.chab += array_height_2d(cenemy.habit)-1
		set_speed(mxpattern+1)
		rand_pattern()
		}
	if(string_count("Calm",skl) > 0)
		{
		ds_list_add(player_state,"Calm")
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Ready",skl) > 0)
		{
		ds_list_add(player_state,"Ready")
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Pressure",skl) > 0)
		{
		ds_list_add(player_state,"Pressure")
		cenemy.chab += array_height_2d(cenemy.habit)-1
		set_speed(mxpattern+2)
		rand_pattern()
		audio_play_sound(sfx_powerup,1,0)
		}
	if(string_count("Surrender",skl) > 0)
		{
		global.willpower = -1
		global.cum = global.mxcum-1
		audio_play_sound(sfx_powerdown,1,0)
		}
	global.stamina -= current_cost


}
