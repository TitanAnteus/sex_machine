/// @function get_assist_cost(enemyname)
/// @description returns the cost of the passive
/// @param enemyname - The name of the passive
function get_assist_cost(argument0) {
	var enemyname;
	enemyname = string(argument0)
	if(string_count("Ordina",enemyname) > 0)return 0
	if(string_count("Rouge",enemyname) > 0)return 3
	if(string_count("Bleu",enemyname) > 0)return 3
	if(string_count("Vert",enemyname) > 0)return 4
	if(string_count("Acier",enemyname) > 0)return 5
	return 0



}
