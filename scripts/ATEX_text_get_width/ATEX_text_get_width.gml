/// @arg sdist
/// @arg text
function ATEX_text_get_width(argument0, argument1) {

	if argument0==-1 return string_width(argument1)
	else return string_length(argument1)*argument0


}
