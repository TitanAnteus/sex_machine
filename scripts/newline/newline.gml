/// @function newline(string,width)
/// @description Adds newlines to a string if it goes over a certain width
/// @param string - the string to split
/// @param width - the width to check for
function newline(argument0, argument1) {

	var cstr,width;
	cstr = argument0
	width = argument1
	
	var color_list,length_list,full_color,color_string;
	color_list = ds_list_create()
	length_list = ds_list_create()
	full_color = ds_list_create()
	for(var st=0;st<string_length(cstr);st+=1)
		{
		if(string_copy(cstr,st,1) == "<")
			{
			ds_list_add(color_list,st)
			for(var ed=0;ed<1000;ed+=1)
				{
				if(string_copy(cstr,st+ed,1) == ">")
					{
					ds_list_add(length_list,ed)
					break
					}
				}
			color_string = string_copy(cstr,st,ed)
			if(string_char_at(color_string,string_length(color_string)) != ">")color_string += ">"
			ds_list_add(full_color,color_string)
			st=st+ed
			}
		}
	for(var st=0;st<ds_list_size(full_color);st+=1)
		{
		cstr = string_replace(cstr,full_color[| st],"")
		}
	for(var st=0;st<string_length(cstr);st+=1)
		{
		if(string_width(string_copy(cstr,0,st)) > width)
			{
			cstr = string_insert("\n",cstr,st-1)
			}
		}
	var newcount;
	newcount = 0
	for(var st=0;st<ds_list_size(color_list);st+=1)
		{
		newcount = string_count("\n",string_copy(cstr,0,color_list[| st]+2))-string_count("\n",string_copy(argument0,0,color_list[| st]+2))
		cstr = string_insert(full_color[| st],cstr,color_list[| st]+newcount)
		}
	ds_list_destroy(color_list)
	ds_list_destroy(length_list)
	ds_list_destroy(full_color)
	return cstr
}

function newline_en(argument0, argument1){
	var cstr,width;
	cstr = argument0
	width = argument1
	
	var color_list,length_list,full_color,color_string;
	color_list = ds_list_create()
	length_list = ds_list_create()
	full_color = ds_list_create()
	for(var st=0;st<string_length(cstr);st+=1)
		{
		if(string_copy(cstr,st,1) == "<")
			{
			ds_list_add(color_list,st)
			for(var ed=0;ed<1000;ed+=1)
				{
				if(string_copy(cstr,st+ed,1) == ">")
					{
					ds_list_add(length_list,ed)
					break
					}
				}
			color_string = string_copy(cstr,st,ed)
			if(string_char_at(color_string,string_length(color_string)) != ">")color_string += ">"
			ds_list_add(full_color,color_string)
			st=st+ed
			}
		}
		
	for(var st=0;st<ds_list_size(full_color);st+=1)
		{
		cstr = string_replace(cstr,full_color[| st],"")
		}

	for(var st=0;st<string_length(cstr);st+=1)
		{
		if(string_width(string_copy(cstr,0,st)) > width)
			{
			for(var j=st;j>0;j-=1)
				{
				if(string_char_at(cstr,j) == "\n")
					{
					break
					}
				if(string_char_at(cstr,j) == " ")
					{
					cstr = string_copy(cstr,0,j-1)+"\n"+string_copy(cstr,j+1,string_length(cstr)-j)
					break
					}
				}
			}
		}
		
	for(var st=0;st<ds_list_size(color_list);st+=1)
		{
		cstr = string_insert(full_color[| st],cstr,color_list[| st])
		}
	ds_list_destroy(color_list)
	ds_list_destroy(length_list)
	ds_list_destroy(full_color)
	return cstr
}