/// @function full_itemname(itemstring)
/// @description Returns the full item name, with Japanese included
/// @param itemstring - The name of the item
function full_itemname(argument0) {
	var itemname;
	itemname = argument0
	if(string_count("Passion Plus",itemname) > 0)return "Passion Plus | パッションプラス"
	if(string_count("Energizer",itemname) > 0)return "Energizer | エナジャイザー"
	if(string_count("Libido Control",itemname) > 0)return "Libido Control | 性欲管理"
	if(string_count("Iron Resistance",itemname) > 0)return "Iron Resistance | 鉄の抵抗"
	if(string_count("Scope",itemname) > 0)return "Scope | スコープ"
	return ""
}
