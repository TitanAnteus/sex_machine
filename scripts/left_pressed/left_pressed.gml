/// @function left_pressed()
/// @description Checks if left key has been pressed
function left_pressed() {
	if(keyboard_check_pressed(global.k_left))
		{
		return 1
		}
	return 0


}
