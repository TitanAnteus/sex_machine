/// @function up_pressed()
/// @description Checks if up key has been pressed
function up_pressed() {
	if(keyboard_check_pressed(global.k_up))return 1
	return 0


}
