///@function state_count(list,string)
///@param state_list - The list to check
///@param string - The state to check
function state_count(argument0, argument1) {
	var lst,itm,cnt;
	lst = argument0
	cnt = 0
	for(i=0;i<ds_list_size(lst);i+=1)
		{
		itm = ds_list_find_value(lst,i)
		if(itm == argument1)cnt += 1
		}
	return cnt
}
