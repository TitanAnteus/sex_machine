/// @function get_enemy_description(enemystring)
/// @description Returns a description text from the name of the enemy
/// @param enemystring - The name of the enemy
function get_enemy_description(argument0) {
	var enmname;
	enmname = argument0
	if(string_count("Rouge",enmname) > 0){
		return string_split("Rouge is a Generation 1 android designed with aggressiveness in mind.\nMost real girls are generally passive when it comes to sex, so her model is quite popular.\nShe gets antsy when she can't move her hips so she's the perfect model for the dominant and the people who want to be dominated. | "+
		"ルージュは、セックスへの積極性を考慮して設計された第1世代のアンドロイドです。 \n人間の女の子はセックスに関して受動的であることが多く、彼女のモデルは非常に人気があります。 \n彼女はセックスができないと不安になるので、彼女を調教したい人や彼女に性的に支配されたい人に最適なモデルです。")
	}
	if(string_count("Bleu",enmname) > 0){
		return string_split("Bleu is a Generation 1 android designed to soothe the heart.\nMeticulous detail was given to her face. Her Smile can make anyone fall head over heels for her.\nMost of her energy goes to her emotional VI supplement. She's the best model for those who want a caring partner. | "+
		"ブルーは、心を落ち着かせることを目的に設計された第1世代のアンドロイドです。 \n彼女の顔には細心の注意が払われて作られました。 彼女の笑顔に誰もが癒されることでしょう。 \n彼女のエネルギーの大部分は感情に消費されます。 思いやりのあるパートナーが欲しい人に最適なモデルです。")
	}
	if(string_count("Vert",enmname) > 0){
		return string_split("Vert is a Generation 1 Android designed with mature sex appeal.\nShe has stunning curves and is quite large in general.\nShe can do many various tasks at high proficiency. She has the most overall control over her body and surroundings. Her movements are accurate to the millimeter, which makes her quite the responsive unit in bed. | "+
		"ヴェールは、成熟した性的魅力を持つ女性として設計された第1世代のアンドロイドです。 \n彼女の体は見事な曲線を描いており胸も大きいです。 高い能力を持ち、多くのタスクを実行できます。 彼女は自分の体と周囲をバランス良くコントロールしています。 彼女の動きはミリメートル単位で正確であるため、ベッドの中では非常にテクニカルなユニットになります。")
	}
	if(string_count("Acier",enmname) > 0){
		return string_split("Acier is one of the early Luxury models.\n\nFans of our machines might want more than what a normal woman can offer. Acier is an extremely flexible machine, and can even split apart her body for some normally unthinkable positions.\n\n She is aggressive, and loves sex above all else. | "+
		"アシエは初期の高コストモデルの1つです。 \n我々のアンドロイドの愛好家は人間の女性には出来ないことを求めている事が多いです。 アシエは非常に柔軟なマシンであり、体を分離する機能も有しています。 彼女はセックスに積極的で、何よりもセックスが大好きです。")
	}
	if(string_count("Ordina",enmname) > 0){
		return string_split("Our Androids could not be made possible without the groundwork laid for us by various computer scientists.\nIn collaboration with these groups we've made a support machine with extremely high processing ability. Very little of this processing ability will go to her VI however, so users can focus on making her do various technical tasks. She's willing to receive you at any moment. | "+
		"私たちのアンドロイドは、多くの科学者の協力無しには実現できませんでした。 \nこれらのグループと協力して非常に高い処理能力を備えたコンピューターを作成しています。 この処理能力のほとんどは彼女たちの収集したデータの処理に使用されるため、ユーザーのさまざまなプレイに彼女たちは柔軟に対応できます。 彼女たちはいつでも喜んであなた方を迎えてくれることでしょう。")
	}
	return ""



}
