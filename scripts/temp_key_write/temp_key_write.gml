function temp_key_write() {
	ini_open("settings.ini")
	ini_write_real("Controls","Left",tk[0])
	ini_write_real("Controls","Right",tk[1])
	ini_write_real("Controls","Up",tk[2])
	ini_write_real("Controls","Down",tk[3])
	ini_write_real("Controls","Select",tk[4])
	ini_write_real("Controls","Back",tk[5])
	ini_write_real("Controls","Skip",tk[6])
	ini_write_real("Controls","Language",tk[7])
	ini_close()

	global.k_left = tk[0]
	global.k_right = tk[1]
	global.k_up = tk[2]
	global.k_down = tk[3]
	global.k_select = tk[4]
	global.k_back = tk[5]
	global.k_skip = tk[6]
	global.k_language = tk[7]

	tk[0] = tleft
	tk[1] = tright
	tk[2] = tup
	tk[3] = tdown
	tk[4] = tsel
	tk[5] = tback
	tk[6] = tskip
	tk[7] = tlanguage


}
