{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_mercureassistcum",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "DynamicTexturePage": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Assist",
    "path": "texturegroups/Assist",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"1621d548-e3e7-4cc0-a33a-4bceaa01cf48",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"38e53174-3747-4f2f-b655-a8b1e3428e6d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a907050f-61d3-47d3-ad40-4a45a4417748",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e2a7c1ab-bc03-449b-a4dd-6a9b9dbc8de5",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2fcffe4d-3d34-4c8e-b246-0c5453044d69",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ef280d8b-7fcd-4d8a-a472-009fc1864d6b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"84427af0-9054-468b-807d-65ed56275dc7",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"86b5bfff-9cf0-478e-96bf-07a6380b4180",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"893639c5-070d-4b73-a663-3376e789692e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9fdb8636-b39c-4633-925e-4603fafb04c2",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"dbe05a8a-0a3b-4793-a81b-ff71a75833d4",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"b4bc2514-5c43-4756-a5a5-786c22f14642",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"21b56ced-4ebd-4ae8-8aaa-c851620064b8",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"fdcd1179-2b5b-4877-9a29-168273c8ea9f",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"3ebaffa1-35b6-4c13-9634-0dcae91120c4",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"91bbfa94-a0a6-42e4-95c9-c985b100d875",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2a3d939e-7b44-48b2-94ca-69796523e776",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"29f5c09d-2448-43c0-8d4f-0ae618102972",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"33f6cd48-3cb5-48c1-85f0-ed8cd1fcc774",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"1b784729-3c27-4084-bfad-940d77729b08",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"dfcf3947-c60a-4e10-9132-a11ca00b45d9",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"5fa3f49d-ab0a-462c-b5b8-267a6ec2cf49",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "spr_mercureassistcum",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 22.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a1ed8ab3-a830-470b-8877-7e90ed5f3cdf","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1621d548-e3e7-4cc0-a33a-4bceaa01cf48","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"577908e0-0f6e-4325-89bd-a7a7c3166d38","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"38e53174-3747-4f2f-b655-a8b1e3428e6d","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"825f738f-01b3-423e-8177-301773b0817b","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a907050f-61d3-47d3-ad40-4a45a4417748","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a7f590b5-16f9-4f06-b15e-1e35340286b5","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e2a7c1ab-bc03-449b-a4dd-6a9b9dbc8de5","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1def4cb1-497f-4136-a3ae-508eb300cd7f","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2fcffe4d-3d34-4c8e-b246-0c5453044d69","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"68c106a2-cb0e-4071-8656-201f8f99b8d0","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ef280d8b-7fcd-4d8a-a472-009fc1864d6b","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2470ec73-bf5c-4d58-85d7-2e302843594e","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"84427af0-9054-468b-807d-65ed56275dc7","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5e11b872-ec11-418f-9f48-5c0ac86420c5","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"86b5bfff-9cf0-478e-96bf-07a6380b4180","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7811c4b3-07a7-4d9f-8b7b-d7a1208283ec","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"893639c5-070d-4b73-a663-3376e789692e","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4d6d6426-86a6-4543-98d0-22a59833a00b","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9fdb8636-b39c-4633-925e-4603fafb04c2","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"98360844-9153-489a-a093-738e805969e7","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dbe05a8a-0a3b-4793-a81b-ff71a75833d4","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"76adcba6-681c-4019-bfe0-46fdd866b986","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b4bc2514-5c43-4756-a5a5-786c22f14642","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4e80576d-b7fd-40b1-82cc-f8bbfbf65c86","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"21b56ced-4ebd-4ae8-8aaa-c851620064b8","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"78e1c08d-c69e-48c7-9fda-2ab7d088227e","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fdcd1179-2b5b-4877-9a29-168273c8ea9f","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f8f60dd7-4fc3-49af-a8ef-702730f8275b","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3ebaffa1-35b6-4c13-9634-0dcae91120c4","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bf5c4700-779d-4bf8-acc7-35f10354f2b0","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"91bbfa94-a0a6-42e4-95c9-c985b100d875","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6e33751d-b88a-4f4e-b1c9-a57a1ac29d6f","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2a3d939e-7b44-48b2-94ca-69796523e776","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"508b56c4-9f55-4ffb-b58f-6a1d7212ec3f","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"29f5c09d-2448-43c0-8d4f-0ae618102972","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b735ed58-a507-4be1-94e8-d5fb262922fb","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"33f6cd48-3cb5-48c1-85f0-ed8cd1fcc774","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a16062f2-3197-448a-bdee-dd5345f0f504","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1b784729-3c27-4084-bfad-940d77729b08","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e2a06d6e-ce4f-4718-8ffc-1229f704513e","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dfcf3947-c60a-4e10-9132-a11ca00b45d9","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5abdec4f-2c2e-44e6-84a3-db0635b1c6aa","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5fa3f49d-ab0a-462c-b5b8-267a6ec2cf49","path":"sprites/spr_mercureassistcum/spr_mercureassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"b43081a7-8e3e-44f1-950e-82eb092f4c1d","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Assist",
    "path": "folders/Sprites/Assist.yy",
  },
}