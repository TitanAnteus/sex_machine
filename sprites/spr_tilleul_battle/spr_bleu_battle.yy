{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Battle",
    "path": "texturegroups/Battle",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"ab58ec0c-eb97-467a-993f-31797cbd7eb6","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ab58ec0c-eb97-467a-993f-31797cbd7eb6","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"LayerId":{"name":"12c56a55-cf7c-452d-ab4e-26b48370f88a","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bleu_battle","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","name":"ab58ec0c-eb97-467a-993f-31797cbd7eb6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6d26b76d-8311-4677-9de5-c4ec3b2f2a58","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6d26b76d-8311-4677-9de5-c4ec3b2f2a58","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"LayerId":{"name":"12c56a55-cf7c-452d-ab4e-26b48370f88a","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bleu_battle","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","name":"6d26b76d-8311-4677-9de5-c4ec3b2f2a58","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0db28638-67c1-485d-b71a-7281fe73e780","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0db28638-67c1-485d-b71a-7281fe73e780","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"LayerId":{"name":"12c56a55-cf7c-452d-ab4e-26b48370f88a","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bleu_battle","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","name":"0db28638-67c1-485d-b71a-7281fe73e780","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e8ffcde8-9c0b-48f0-8117-28bd34164e53","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e8ffcde8-9c0b-48f0-8117-28bd34164e53","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"LayerId":{"name":"12c56a55-cf7c-452d-ab4e-26b48370f88a","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bleu_battle","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","name":"e8ffcde8-9c0b-48f0-8117-28bd34164e53","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"00c074c0-dbd6-4dd3-a1c0-e36fd0bc3d1f","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"00c074c0-dbd6-4dd3-a1c0-e36fd0bc3d1f","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"LayerId":{"name":"12c56a55-cf7c-452d-ab4e-26b48370f88a","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bleu_battle","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","name":"00c074c0-dbd6-4dd3-a1c0-e36fd0bc3d1f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2989291c-e91a-4cc6-acf8-7391443c2efc","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2989291c-e91a-4cc6-acf8-7391443c2efc","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"LayerId":{"name":"12c56a55-cf7c-452d-ab4e-26b48370f88a","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bleu_battle","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","name":"2989291c-e91a-4cc6-acf8-7391443c2efc","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_bleu_battle","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9d2a9693-0d8e-4032-a654-e319a8062f01","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ab58ec0c-eb97-467a-993f-31797cbd7eb6","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2db43900-1a01-41e4-a2e6-08dda428d041","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6d26b76d-8311-4677-9de5-c4ec3b2f2a58","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"66d99a33-187e-4f87-994b-356290e943dc","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0db28638-67c1-485d-b71a-7281fe73e780","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f7a4e3d0-7bb9-4939-bef9-96f7583f7db2","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e8ffcde8-9c0b-48f0-8117-28bd34164e53","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"630e2856-c385-4d43-b3a8-547afbfc7414","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"00c074c0-dbd6-4dd3-a1c0-e36fd0bc3d1f","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5a7773c1-3ef9-4945-9dec-811f247da696","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2989291c-e91a-4cc6-acf8-7391443c2efc","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_bleu_battle","path":"sprites/spr_bleu_battle/spr_bleu_battle.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"12c56a55-cf7c-452d-ab4e-26b48370f88a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Battle",
    "path": "folders/Sprites/Battle.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_bleu_battle",
  "tags": [],
  "resourceType": "GMSprite",
}