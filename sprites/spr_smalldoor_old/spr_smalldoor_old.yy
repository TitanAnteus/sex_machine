{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_smalldoor_old",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 35,
  "bbox_top": 0,
  "bbox_bottom": 53,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "DynamicTexturePage": false,
  "width": 36,
  "height": 54,
  "textureGroupId": {
    "name": "Map",
    "path": "texturegroups/Map",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"428f3a21-c4c2-4e3f-94af-1f935a732da3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a142ca89-25d6-4a87-9813-f92b76372ba4",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9e4687fd-95ea-4260-861e-602aea737643",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"217c79eb-6272-4bed-ab51-507a0d1cd830",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "spr_smalldoor_old",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"3299cbf9-15f9-4405-bda2-73f69c7f7ada","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"428f3a21-c4c2-4e3f-94af-1f935a732da3","path":"sprites/spr_smalldoor_old/spr_smalldoor_old.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c49a71f0-2777-4393-9ac1-d5992cd345a5","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a142ca89-25d6-4a87-9813-f92b76372ba4","path":"sprites/spr_smalldoor_old/spr_smalldoor_old.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2a70570a-8327-4286-aa6b-0e5459169d57","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9e4687fd-95ea-4260-861e-602aea737643","path":"sprites/spr_smalldoor_old/spr_smalldoor_old.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"427b09d0-184a-4119-8504-fc1b96592b25","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"217c79eb-6272-4bed-ab51-507a0d1cd830","path":"sprites/spr_smalldoor_old/spr_smalldoor_old.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 18,
    "yorigin": 54,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"afe58cbb-6d90-4037-bd75-ba4314240753","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Environment",
    "path": "folders/Sprites/Overworld/Environment.yy",
  },
}