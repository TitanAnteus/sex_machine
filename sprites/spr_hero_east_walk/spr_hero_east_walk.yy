{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_hero_east_walk",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 8,
  "bbox_right": 24,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "DynamicTexturePage": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Overworld",
    "path": "texturegroups/Overworld",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"852a843c-4e7d-46d7-83ea-94f89e2c48d6",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"03a71f4f-7860-4048-b92f-4966be6a2bac",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"0a20fc4d-91fa-46a9-86e7-4240de050387",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e25a359a-c229-427e-9b66-635e484871dd",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"193286b1-46eb-46c5-984c-c3ed100fb114","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"852a843c-4e7d-46d7-83ea-94f89e2c48d6","path":"sprites/spr_hero_east_walk/spr_hero_east_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"67f7db28-ff88-4c92-a657-c00d20cfc471","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"03a71f4f-7860-4048-b92f-4966be6a2bac","path":"sprites/spr_hero_east_walk/spr_hero_east_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2bd14ff3-0354-4ef2-93fa-02d630878a77","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0a20fc4d-91fa-46a9-86e7-4240de050387","path":"sprites/spr_hero_east_walk/spr_hero_east_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3c786127-66cb-4385-80fb-591156db6202","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e25a359a-c229-427e-9b66-635e484871dd","path":"sprites/spr_hero_east_walk/spr_hero_east_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"01f0efc1-6008-4078-abcb-4f72ce5e60a4","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Hero",
    "path": "folders/Sprites/Overworld/Hero.yy",
  },
}