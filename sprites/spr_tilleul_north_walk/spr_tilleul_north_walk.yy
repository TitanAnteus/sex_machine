{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_tilleul_north_walk",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 26,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "DynamicTexturePage": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Overworld",
    "path": "texturegroups/Overworld",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"0ba324fa-d087-42dc-9230-97ab0dfc8213",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d8c0611b-e2af-494d-8978-b87be107542d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"bf92d6e1-ca4b-4eb8-b471-cfec9e55ac7b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a9fe41c5-c49a-4d13-8219-39e91df9c508",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "spr_tilleul_north_walk",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"cc990d7c-504c-493b-98ca-3a29d95d28c6","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0ba324fa-d087-42dc-9230-97ab0dfc8213","path":"sprites/spr_tilleul_north_walk/spr_tilleul_north_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dbaef9af-2090-4c56-a0de-1597c0e4c20e","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d8c0611b-e2af-494d-8978-b87be107542d","path":"sprites/spr_tilleul_north_walk/spr_tilleul_north_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d2e5d995-8b8d-4d9d-b921-d3c259277260","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bf92d6e1-ca4b-4eb8-b471-cfec9e55ac7b","path":"sprites/spr_tilleul_north_walk/spr_tilleul_north_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4d949b66-ba23-4b91-a8ff-5272c9d9a0c2","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a9fe41c5-c49a-4d13-8219-39e91df9c508","path":"sprites/spr_tilleul_north_walk/spr_tilleul_north_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"7b7476ff-c5ad-4d50-95bb-f0ec6ee7b11d","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Tilleul",
    "path": "folders/Sprites/Overworld/Enemy/Tilleul.yy",
  },
}