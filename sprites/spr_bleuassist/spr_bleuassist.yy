{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_bleuassist",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "DynamicTexturePage": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Assist",
    "path": "texturegroups/Assist",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f72c5179-4383-449b-b635-66a845d46573",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f163e6a1-f7af-4cde-b5ab-71f6a77518d0",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"df50c51b-a7b1-42ab-819d-a15177e096de",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e9fed402-317d-4317-9376-ef5c73ecc301",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"91114d2f-9e1f-4931-8aeb-32052044e326",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ab145e01-cc48-48df-b699-6dd21d5e61d2",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"94055f95-dd36-457b-b64f-1728530021c1",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"fd065dcc-aad7-4c47-9c5b-7b8ac633a784",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"863d0cc7-2deb-448f-8223-00693ab020cf",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"5402eebf-c198-4869-9796-c887dbb0e2a4",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"19c599a1-eb04-41a7-b140-0c44a444c8c6",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"6e99d67d-ad91-467c-80c9-890467866a75",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c6f0b4ec-a337-4978-b2f4-b3e7a4ee6d59",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ff19aecc-e08a-4c28-93a2-31ef4e1601da",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"437a88c9-803e-43ac-a245-0691f0ecc141",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"86fb8607-31c2-488f-ab6b-3b1ea60d3c1e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"874a1963-e73f-4964-bdc6-dc1a13a4e800",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"553535df-333d-4a69-9453-c58b281031f4",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"b39127f9-6c9a-46cf-9bbe-cea15e3944e3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e94917e5-fbfd-46d0-8138-efe2efb5c4ca",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c78329b3-7be2-4939-bb00-45aea01d1048",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"63475bfe-f4bc-44be-bb2f-8ab8af08b342",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"27d0cfdf-13ff-4967-b36f-4720287e8841",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"eea3b14e-c93e-4256-97e3-a0e5457aa7ad",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"bfc362f9-61ee-4f13-930c-142b69a9c464",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7225d8e2-ca42-4ffc-ba38-9c417b5f531a",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7c9b6d20-bb94-47b9-8f61-37629ff5da56",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"195d7770-4fa0-4123-9e4e-e36172203367",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"cb39115d-a594-4994-9fbc-0202a09f5a7a",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"6db9ac08-c20e-4336-8772-3d455af3aebd",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 30.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"051cfa66-249c-41f3-8c51-03555829f97e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f72c5179-4383-449b-b635-66a845d46573","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1ed169a8-ce89-496b-920d-a40cd015b4d9","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f163e6a1-f7af-4cde-b5ab-71f6a77518d0","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0d440ef5-add9-44aa-aca8-8e923485b37c","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"df50c51b-a7b1-42ab-819d-a15177e096de","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9635a6ba-ec4c-4001-a781-6744606e3dcc","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e9fed402-317d-4317-9376-ef5c73ecc301","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"77eaf518-d033-461e-b873-02829552b91a","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"91114d2f-9e1f-4931-8aeb-32052044e326","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"419408cb-4fea-433d-9856-ee9d664ab981","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ab145e01-cc48-48df-b699-6dd21d5e61d2","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1a5cdb31-c4db-4935-942d-e93bf7eded4e","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"94055f95-dd36-457b-b64f-1728530021c1","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3f898d06-5a87-4dbb-8283-5a5d05bf99c6","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fd065dcc-aad7-4c47-9c5b-7b8ac633a784","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3da0785d-7588-43fc-ab91-b675a79c33ea","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"863d0cc7-2deb-448f-8223-00693ab020cf","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d47fa57e-0534-4b6d-a9c1-47392e9da902","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5402eebf-c198-4869-9796-c887dbb0e2a4","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"55b435bf-40be-4899-8763-fd8d85ca7daa","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"19c599a1-eb04-41a7-b140-0c44a444c8c6","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4d83a077-a05c-47f6-8f03-67843ec45ebe","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6e99d67d-ad91-467c-80c9-890467866a75","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b77e1d86-5224-4a07-99c3-64b0aaf58251","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c6f0b4ec-a337-4978-b2f4-b3e7a4ee6d59","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a941320e-2d18-4d39-bfac-c61800abd107","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ff19aecc-e08a-4c28-93a2-31ef4e1601da","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1274985b-d447-45f0-9da6-2c23f765e8dd","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"437a88c9-803e-43ac-a245-0691f0ecc141","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7c173c9c-b4ee-4ada-a791-4d86038c2dcd","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"86fb8607-31c2-488f-ab6b-3b1ea60d3c1e","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1312e373-d217-4ade-a162-8c8fc80ebbc3","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"874a1963-e73f-4964-bdc6-dc1a13a4e800","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b0f4bab8-1d14-4da6-9a2c-051752879652","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"553535df-333d-4a69-9453-c58b281031f4","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5d9f96ec-21be-41ad-a456-5a9d76497112","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b39127f9-6c9a-46cf-9bbe-cea15e3944e3","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a0ca33ca-2a18-42b3-9b8a-630479b2cb10","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e94917e5-fbfd-46d0-8138-efe2efb5c4ca","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"611ff998-fe29-47e7-851f-05e19928e807","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c78329b3-7be2-4939-bb00-45aea01d1048","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bbe3234f-4470-4acb-bb80-5a449b4d8e45","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"63475bfe-f4bc-44be-bb2f-8ab8af08b342","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"07f10ab1-7960-4304-8262-57b314cca30d","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"27d0cfdf-13ff-4967-b36f-4720287e8841","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f5d29954-ba8a-4e93-bd22-037dd3408b31","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eea3b14e-c93e-4256-97e3-a0e5457aa7ad","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"faddf7de-20bf-4a4d-aede-c315b7726bb6","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bfc362f9-61ee-4f13-930c-142b69a9c464","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4a56de60-ebfa-445b-a6d9-c8da841c493b","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7225d8e2-ca42-4ffc-ba38-9c417b5f531a","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eb58b19c-63b9-42e4-a74b-f7ee10a9ef77","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7c9b6d20-bb94-47b9-8f61-37629ff5da56","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"24cb80bf-2269-451d-98ce-54408b5e1f14","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"195d7770-4fa0-4123-9e4e-e36172203367","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"73264626-cc19-4b30-a76c-5c9902814316","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cb39115d-a594-4994-9fbc-0202a09f5a7a","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e4c503cf-3f7b-4d2e-a988-8849f3c0ca49","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6db9ac08-c20e-4336-8772-3d455af3aebd","path":"sprites/spr_bleuassist/spr_bleuassist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"5c1ed922-1c26-4122-bbb6-9a87b2c65378","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Assist",
    "path": "folders/Sprites/Assist.yy",
  },
}