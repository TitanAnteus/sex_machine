{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 319,
  "bbox_top": 0,
  "bbox_bottom": 319,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 320,
  "height": 320,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"49f02ec1-51c7-4361-ba5f-2afcb137cf83","path":"sprites/spr_jaunehead/spr_jaunehead.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"49f02ec1-51c7-4361-ba5f-2afcb137cf83","path":"sprites/spr_jaunehead/spr_jaunehead.yy",},"LayerId":{"name":"db6ae524-df36-4ad8-af21-2f64c2818c18","path":"sprites/spr_jaunehead/spr_jaunehead.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_jaunehead","path":"sprites/spr_jaunehead/spr_jaunehead.yy",},"resourceVersion":"1.0","name":"49f02ec1-51c7-4361-ba5f-2afcb137cf83","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_jaunehead","path":"sprites/spr_jaunehead/spr_jaunehead.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"88b6184f-0da1-4bc1-94c3-e1c01a1ec495","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"49f02ec1-51c7-4361-ba5f-2afcb137cf83","path":"sprites/spr_jaunehead/spr_jaunehead.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_jaunehead","path":"sprites/spr_jaunehead/spr_jaunehead.yy",},
    "resourceVersion": "1.3",
    "name": "spr_jaunehead",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"db6ae524-df36-4ad8-af21-2f64c2818c18","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Portraits",
    "path": "folders/Sprites/Portraits.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_jaunehead",
  "tags": [],
  "resourceType": "GMSprite",
}