{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_rougeassistcum",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "DynamicTexturePage": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Assist",
    "path": "texturegroups/Assist",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"192874a4-d438-4d58-a4dc-a668f445a649",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"12329f21-c28d-4421-aa5c-2662324ea4ed",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"cfc0b8df-34fd-48f4-ab6d-3e846d7c583d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"4d80b65b-8dc0-42f9-918e-1145ec617f36",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"0bbc3605-c781-4fd5-981e-41bdb92d1ec4",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"fbda7303-6197-4046-ad37-2a46a8d415f4",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2049780d-2f3c-4a07-aa79-f55b7cc443d3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"930b6a23-0a7e-44ae-ae28-42c2d9f1fb96",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"49437deb-bc72-40cb-90c2-81e1fdaf8c9e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"178a298f-85ea-4766-b984-64e97616e9ab",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"439e142e-549e-477d-9374-1cd17bfa0028",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7017e606-12c1-4946-8a16-e6f49d9ebca0",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"4f4da032-9ea4-42b6-ab29-140df83ac79b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ac2ce725-0250-460d-8f41-49e99c6dd7bb",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"139ae98b-e11b-42fa-8137-ff94a5eb5c19",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c165f7c2-94cd-46a2-9e72-cd9858c6578a",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2ce00185-ca34-49ff-aefc-797d12178e63",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"151749bd-a607-4973-84e6-0ae32ecff859",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2dd5921e-e93f-486d-ac0d-53c1ea4bb882",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"8ea183a7-66f2-409a-836d-b94fd3e41706",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f6df213a-dc1a-4762-a6e1-4fd24825e08b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9aa2780c-e272-4f3f-8e15-e4d54becb845",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"8bdf5721-11fa-4710-b525-79d30fae47d2",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"13ab9528-4c30-4521-8a09-3177a2a69ed0",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9e5dcee6-32e7-4d7b-85f1-a534b4a18e7e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"46ac7361-24b1-4b9c-ae7e-104776f451c4",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"b93704bf-0181-412d-a410-e9ac054daa98",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"75f68c1b-2403-4a98-9bd2-23a7a0152893",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"b0fcbf34-f530-404b-83b7-d8c24771bbce",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 29.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6d77d777-6b60-49f8-8043-665e0a9834bd","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"192874a4-d438-4d58-a4dc-a668f445a649","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8f223122-3c24-4c5c-856a-658c2779def6","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12329f21-c28d-4421-aa5c-2662324ea4ed","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d8451a99-f6ce-432e-93b9-cef13d88871c","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cfc0b8df-34fd-48f4-ab6d-3e846d7c583d","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6d3e566f-2e06-4168-86ee-a36d5df3e518","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4d80b65b-8dc0-42f9-918e-1145ec617f36","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d346798e-d2d3-4505-bfe9-e5e9f56c1e1c","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0bbc3605-c781-4fd5-981e-41bdb92d1ec4","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"53829cc7-6a60-4411-94ad-15bb0609a800","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fbda7303-6197-4046-ad37-2a46a8d415f4","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"edf4c449-de29-43ca-88cc-a7054a80ffee","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2049780d-2f3c-4a07-aa79-f55b7cc443d3","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3a456623-b7c1-4e33-a43b-060cf287d217","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"930b6a23-0a7e-44ae-ae28-42c2d9f1fb96","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a02d4423-b3a9-41cf-a16b-0144cd534aad","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"49437deb-bc72-40cb-90c2-81e1fdaf8c9e","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"43954a5e-9c99-4d6c-b6bf-e275a30b82ea","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"178a298f-85ea-4766-b984-64e97616e9ab","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b6f21a72-32f7-4678-ada3-3c3bece3da47","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"439e142e-549e-477d-9374-1cd17bfa0028","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4be66dec-3652-414e-b6f8-176fb0e75fef","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7017e606-12c1-4946-8a16-e6f49d9ebca0","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8ee1d693-239c-4142-9cfc-95f08c0c6e7f","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4f4da032-9ea4-42b6-ab29-140df83ac79b","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"81184d1b-0763-4267-bfb3-7288bea3b2e2","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ac2ce725-0250-460d-8f41-49e99c6dd7bb","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"808d9126-1429-4eaa-913e-01ed6b5d167a","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"139ae98b-e11b-42fa-8137-ff94a5eb5c19","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5aa8a5c9-49b2-4cbe-8668-c25300435433","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c165f7c2-94cd-46a2-9e72-cd9858c6578a","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"11ef688d-a962-405f-b7c2-9d40b06f8d7f","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2ce00185-ca34-49ff-aefc-797d12178e63","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"af0d736c-4ea8-4555-bcf1-073cdbdfde0d","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"151749bd-a607-4973-84e6-0ae32ecff859","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bda6bda1-4949-4477-865d-064857a2b592","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2dd5921e-e93f-486d-ac0d-53c1ea4bb882","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"552955d3-28da-432e-ba4d-7c7db7ce243f","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8ea183a7-66f2-409a-836d-b94fd3e41706","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"11ecbb6f-fb75-4ac0-841b-11c2cec35e8f","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f6df213a-dc1a-4762-a6e1-4fd24825e08b","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dac984eb-6aba-454b-94a5-f184d883417b","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9aa2780c-e272-4f3f-8e15-e4d54becb845","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f1a9e073-485d-406d-951b-ca0ee6322d73","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8bdf5721-11fa-4710-b525-79d30fae47d2","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7f5f8aa3-dce4-4035-a811-a09f9791a455","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"13ab9528-4c30-4521-8a09-3177a2a69ed0","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c34f218e-4ecf-44f0-84dd-918ce55ec0ed","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9e5dcee6-32e7-4d7b-85f1-a534b4a18e7e","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e5bc0b49-dfac-4e7b-9ca8-4ab4be86735f","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"46ac7361-24b1-4b9c-ae7e-104776f451c4","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bcbdcf76-1a9b-4ac4-8519-a4e1006d3012","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b93704bf-0181-412d-a410-e9ac054daa98","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e80a40f6-da8e-46dd-8705-3c209148f945","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"75f68c1b-2403-4a98-9bd2-23a7a0152893","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0513ace3-9c53-415d-b294-30c894ffc644","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b0fcbf34-f530-404b-83b7-d8c24771bbce","path":"sprites/spr_rougeassistcum/spr_rougeassistcum.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"877fbe32-e677-44e5-bcdd-f13b9fb55128","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Assist",
    "path": "folders/Sprites/Assist.yy",
  },
}