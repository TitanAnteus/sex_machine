{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 30,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Overworld",
    "path": "texturegroups/Overworld",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"7449c7d3-d6d3-41d5-ac23-b30f12deca50","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7449c7d3-d6d3-41d5-ac23-b30f12deca50","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"LayerId":{"name":"2ed7c00a-2edd-409c-a174-eb378ddbd224","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_south_walk","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"resourceVersion":"1.0","name":"7449c7d3-d6d3-41d5-ac23-b30f12deca50","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e2e702d3-8160-4424-b0d1-8d1df747b05b","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e2e702d3-8160-4424-b0d1-8d1df747b05b","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"LayerId":{"name":"2ed7c00a-2edd-409c-a174-eb378ddbd224","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_south_walk","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"resourceVersion":"1.0","name":"e2e702d3-8160-4424-b0d1-8d1df747b05b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8aa04b48-e936-4cec-ae29-dc7e5f83c9c2","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8aa04b48-e936-4cec-ae29-dc7e5f83c9c2","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"LayerId":{"name":"2ed7c00a-2edd-409c-a174-eb378ddbd224","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_south_walk","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"resourceVersion":"1.0","name":"8aa04b48-e936-4cec-ae29-dc7e5f83c9c2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2104f3a3-af34-4979-a8cc-4990b6491773","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2104f3a3-af34-4979-a8cc-4990b6491773","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"LayerId":{"name":"2ed7c00a-2edd-409c-a174-eb378ddbd224","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_south_walk","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"resourceVersion":"1.0","name":"2104f3a3-af34-4979-a8cc-4990b6491773","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_green_south_walk","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"bdb857b2-2c60-4b87-ba6c-7bc6abc3d3d5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7449c7d3-d6d3-41d5-ac23-b30f12deca50","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2109fcdd-cce6-484e-975d-7cee81cf2aa0","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e2e702d3-8160-4424-b0d1-8d1df747b05b","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e046d8b9-6e22-4454-af34-00bb98169751","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8aa04b48-e936-4cec-ae29-dc7e5f83c9c2","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"53233267-e096-4704-9932-22d5720fca49","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2104f3a3-af34-4979-a8cc-4990b6491773","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_green_south_walk","path":"sprites/spr_green_south_walk/spr_green_south_walk.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"2ed7c00a-2edd-409c-a174-eb378ddbd224","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Vert",
    "path": "folders/Sprites/Overworld/Enemy/Vert.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_green_south_walk",
  "tags": [],
  "resourceType": "GMSprite",
}