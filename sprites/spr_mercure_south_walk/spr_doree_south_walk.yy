{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 8,
  "bbox_right": 24,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Overworld",
    "path": "texturegroups/Overworld",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"78bf8f6a-091a-4fdf-b4e6-f05c4ffa3fe5","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"78bf8f6a-091a-4fdf-b4e6-f05c4ffa3fe5","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"LayerId":{"name":"4fae08b5-b5c7-459a-be81-a7b375d2c308","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_doree_south_walk","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"resourceVersion":"1.0","name":"78bf8f6a-091a-4fdf-b4e6-f05c4ffa3fe5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c0b78914-dfee-41b8-9b49-eee35ed9f84d","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c0b78914-dfee-41b8-9b49-eee35ed9f84d","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"LayerId":{"name":"4fae08b5-b5c7-459a-be81-a7b375d2c308","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_doree_south_walk","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"resourceVersion":"1.0","name":"c0b78914-dfee-41b8-9b49-eee35ed9f84d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c1154449-3e7d-4f43-9ab2-f976fc62920e","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c1154449-3e7d-4f43-9ab2-f976fc62920e","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"LayerId":{"name":"4fae08b5-b5c7-459a-be81-a7b375d2c308","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_doree_south_walk","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"resourceVersion":"1.0","name":"c1154449-3e7d-4f43-9ab2-f976fc62920e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c70549d4-25ab-4005-8f2f-223a3fc51816","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c70549d4-25ab-4005-8f2f-223a3fc51816","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"LayerId":{"name":"4fae08b5-b5c7-459a-be81-a7b375d2c308","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_doree_south_walk","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"resourceVersion":"1.0","name":"c70549d4-25ab-4005-8f2f-223a3fc51816","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_doree_south_walk","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a7c9b758-e6d7-4cb1-920e-b764f3354ddc","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"78bf8f6a-091a-4fdf-b4e6-f05c4ffa3fe5","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8c471cf1-a666-42a9-bd7a-1d3435099740","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c0b78914-dfee-41b8-9b49-eee35ed9f84d","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bda9d2da-3824-487e-8256-bda478094780","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c1154449-3e7d-4f43-9ab2-f976fc62920e","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0d71e61c-2f2b-45e0-9c45-f9b3fa05b864","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c70549d4-25ab-4005-8f2f-223a3fc51816","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_doree_south_walk","path":"sprites/spr_doree_south_walk/spr_doree_south_walk.yy",},
    "resourceVersion": "1.3",
    "name": "spr_doree_south_walk",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4fae08b5-b5c7-459a-be81-a7b375d2c308","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Doree",
    "path": "folders/Sprites/Overworld/Enemy/Doree.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_doree_south_walk",
  "tags": [],
  "resourceType": "GMSprite",
}