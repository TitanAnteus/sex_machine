{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"6fd7c45e-5251-43a7-9a81-bc8d95cef8b3","path":"sprites/spr_acierhead2/spr_acierhead2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6fd7c45e-5251-43a7-9a81-bc8d95cef8b3","path":"sprites/spr_acierhead2/spr_acierhead2.yy",},"LayerId":{"name":"f1800b97-98f5-4b53-863f-5a5c8e7fcdf6","path":"sprites/spr_acierhead2/spr_acierhead2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"6fd7c45e-5251-43a7-9a81-bc8d95cef8b3","path":"sprites/spr_acierhead2/spr_acierhead2.yy",},"LayerId":{"name":"7c2097c1-bab6-43d0-b282-44a97291e459","path":"sprites/spr_acierhead2/spr_acierhead2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_acierhead2","path":"sprites/spr_acierhead2/spr_acierhead2.yy",},"resourceVersion":"1.0","name":"6fd7c45e-5251-43a7-9a81-bc8d95cef8b3","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_acierhead2","path":"sprites/spr_acierhead2/spr_acierhead2.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"54609434-364d-4921-93d6-8e3f1169a48a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6fd7c45e-5251-43a7-9a81-bc8d95cef8b3","path":"sprites/spr_acierhead2/spr_acierhead2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_acierhead2","path":"sprites/spr_acierhead2/spr_acierhead2.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"7c2097c1-bab6-43d0-b282-44a97291e459","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f1800b97-98f5-4b53-863f-5a5c8e7fcdf6","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Portraits",
    "path": "folders/Sprites/Portraits.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_acierhead2",
  "tags": [],
  "resourceType": "GMSprite",
}