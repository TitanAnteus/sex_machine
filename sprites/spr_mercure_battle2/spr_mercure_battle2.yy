{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_mercure_battle2",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "DynamicTexturePage": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Battle",
    "path": "texturegroups/Battle",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"4dd3344b-646a-416c-9d69-f492ef77b18b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"b4db4ed9-b5b7-4cf5-9103-488326441f4f",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"8a87f151-14da-46ef-bdef-e45754548722",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f132fa7c-16c3-4f41-aa48-eb5ddd60c342",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ddc7a711-55bf-4ead-8dfd-d0df027a6ccb",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"15aced06-28e9-4465-a27c-769ea7c1cdf9",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "spr_mercure_battle2",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ba294c4a-f930-4ee8-a2d3-4d3e260cfdc3","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4dd3344b-646a-416c-9d69-f492ef77b18b","path":"sprites/spr_mercure_battle2/spr_mercure_battle2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"58179c05-db29-47c4-8be2-22d40a9b47fa","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b4db4ed9-b5b7-4cf5-9103-488326441f4f","path":"sprites/spr_mercure_battle2/spr_mercure_battle2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b598f3ab-ba9d-4f8e-868a-ef0ebfc36bb5","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8a87f151-14da-46ef-bdef-e45754548722","path":"sprites/spr_mercure_battle2/spr_mercure_battle2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e248939d-a247-46b9-a8ac-152e66d88d96","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f132fa7c-16c3-4f41-aa48-eb5ddd60c342","path":"sprites/spr_mercure_battle2/spr_mercure_battle2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9e5cdf33-554b-4bf7-af23-0a647b6fb01c","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ddc7a711-55bf-4ead-8dfd-d0df027a6ccb","path":"sprites/spr_mercure_battle2/spr_mercure_battle2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"032e634a-7763-462e-b204-8cf7f6ef0803","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"15aced06-28e9-4465-a27c-769ea7c1cdf9","path":"sprites/spr_mercure_battle2/spr_mercure_battle2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"c7f0f6e9-037e-47b0-a393-a4e5c6bd8dae","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Battle",
    "path": "folders/Sprites/Battle.yy",
  },
}